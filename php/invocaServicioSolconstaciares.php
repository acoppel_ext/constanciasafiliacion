<?php
include_once ('clases/global.php');
include_once ('clases/CMetodoGeneral.php');
include_once ('clases/CServicioBusTramites.php');
include_once("JSON.php");
$json = new Services_JSON();
$arrResp = array();

//Se declaran variables donde se Optiene el valor que les fue mandado del js atraves de ajax
//Primera seccion donde se obtienen los datos del formulario
$iEmpleado =  isset($_POST['empleado']) ? $_POST['empleado']: '0';
$opcion = isset($_POST['opcion']) ? $_POST['opcion']: '';
$iTipoConstancia = isset($_POST['tipoconstancia']) ? $_POST['tipoconstancia']: '0'; 
$sCurp = isset($_POST['curp']) ? $_POST['curp']: ''; 
$sAppaterno = isset($_POST['appaterno']) ? $_POST['appaterno']: '';  
$sApmaterno = isset($_POST['apmaterno']) ? $_POST['apmaterno']: ''; 
$sNombre = isset($_POST['nombre']) ? $_POST['nombre']: '';  
$sNss = isset($_POST['nss']) ? $_POST['nss']: '';
$sNumeroCell = isset($_POST['numerocel']) ? $_POST['numerocel']: ''; 
$iCompaniaCel = isset($_POST['companiacel']) ? $_POST['companiacel']: '0'; 
$sTipoComprobante = isset($_POST['tipocomprobante']) ? $_POST['tipocomprobante']: '';
$sFechaComprobante = isset($_POST['fechacomprobante']) ? $_POST['fechacomprobante']: ''; 
$sNumeroCliente = isset($_POST['numclientecoppel']) ? $_POST['numclientecoppel']: '';

//Segunda seccion donde se obtienen los datos del formulario
$iTipoTelefono = isset($_POST['tipotelefono']) ? $_POST['tipotelefono']: '0'; 
$sTelefonoContacto = isset($_POST['telcontacto']) ? $_POST['telcontacto']: '';
$iCompTel = isset($_POST['comptelefonica']) ? $_POST['comptelefonica']: '0';

//Tercera seccion donde se obtienen los datos del formulario
$sCalle = isset($_POST['calle']) ? $_POST['calle']: '';   
$sNumExt = isset($_POST['numeroexterior']) ? $_POST['numeroexterior']: ''; 
$sNumInt = isset($_POST['numerointerior']) ? $_POST['numerointerior']: ''; 
$sCodPost = isset($_POST['codigopostal']) ? $_POST['codigopostal']: '';
$iColonia = isset($_POST['colonia']) ? $_POST['colonia']: '0';
$iDelegacion = isset($_POST['delegacion']) ? $_POST['delegacion']: '0'; 
$iEstado = isset($_POST['estado']) ? $_POST['estado']: '0';   
$iPais = 'MEX';//isset($_POST['pais']) ? $_POST['pais']: '0'; //342 indica que es mexico
$sLugarSol = isset($_POST['lugarsol']) ? $_POST['lugarsol']: ''; 
$claveconsar =  isset($_POST['claveconsar']) ? $_POST['claveconsar']: ''; 

 //Se obtiene el sello
$sverificacion = isset($_POST['selloverificacion']) ? $_POST['selloverificacion']: '0';

$iFolioSerAfore =  isset($_POST['folioservicioafore']) ? $_POST['folioservicioafore']: '0'; 
$iFolio =  isset($_POST['folio']) ? $_POST['folio']: '0'; 
//para el consulmo del servicio
$idServicio = 9901;
//$idServicio = 9904;
$idServidor = 5;


switch($opcion) 
{
	case 1:
		$arrResp = ejecutarAplicacionsolconstancia($idServicio,$idServidor, $iEmpleado,$iTipoConstancia,$sNss,$sCurp,$sAppaterno,$sApmaterno,$sNombre,$iCompaniaCel,$sNumeroCell,$sCalle,$sNumExt,$sNumInt,$iColonia,$iDelegacion,$sCodPost,$iEstado,$iPais,$sLugarSol,$claveconsar,$iFolio,$sverificacion);
		break;
	case 2:
		$arrResp =  obtenerRespuestasolconstancia($idServidor,$iFolioSerAfore,$iFolio);
		break;
	case 3:
		$arrResp = obtenerRespuestaRechazo($iFolioSerAfore);
		break;
}
echo $json->encode($arrResp);


 	function ejecutarAplicacionsolconstancia($idServicio,$idServidor, $iEmpleado,$iTipoConstancia,$sNss,$sCurp,$sAppaterno,$sApmaterno,$sNombre,$iCompaniaCel,$sNumeroCell,$sCalle,$sNumExt,$sNumInt,$iColonia,$iDelegacion,$sCodPost,$iEstado,$iPais,$sLugarSol,$claveconsar,$iFolio,$sverificacion)
	{
		//$objGn->grabarLogx('[' . __FILE__ . ']'.'Entro a metodo del servicio ejecutarAplicacionsolconstancia');
		//se crea arreglo a retornar
		$arrResp = array();

		$arrCons = array();

		//se crea objeto para invocar los metodos de la clase
		$objGn = new CMetodoGeneral();

		//se declaran varibles en vacio y 0 por si falla la incacion tenga algo por default
		$arrResp->descripcionRespuesta= '';
		$arrResp->folioServicioAfore = 0;
		$arrResp->respondioServicio = 0;

		//se obtine la fecha del servidor
		date_default_timezone_set('America/Mazatlan');
		setlocale(LC_TIME, 'spanish');
		$fechaSolicitud =  strftime("%Y/%m/%d");

		if ($iTipoConstancia == 26 || $iTipoConstancia == 33)
		{
			$iTipoConstancia = 1;
		}
		else
		{
			$iTipoConstancia = 2;
		}

		$iEstado = str_pad($iEstado, 2, "0", STR_PAD_LEFT);

		$sLugarSol = str_pad($sLugarSol, 2, "0", STR_PAD_LEFT);
		
		$claveconsar = str_pad($claveconsar, 10, "0", STR_PAD_LEFT);

		//Se crea array a mandar 
		$array = array(
			    'tipoConstancia' => $iTipoConstancia,
			    'identificadorEnvioContrasena' => 1, //SMS
			    'fechaSolicitud' => $fechaSolicitud,
			    'nss' => $sNss,
			    'curp' => $sCurp,
			    'apellidoPaterno' => $sAppaterno,
			    'apellidoMaterno' => $sApmaterno,
			    'nombreTrabajador' => $sNombre,
			    'companiaTelefonica' => $iCompaniaCel,
			    'telContactoCelular' => $sNumeroCell,
			    'telContactoFijo' => 0,
			    'extension' => '0', //no se ocupa
			    'calle' => $sCalle,
			    'numeroExterior' => $sNumExt,
			    'numeroInterior' => $sNumInt,
			    'colonia' => $iColonia,
			    'delegacion' => $iDelegacion,
			    'codigoPostal' => $sCodPost,
			    'entidadFederativa' => $iEstado,
			    'pais' => $iPais,
			    'numeroEmpleado' => $iEmpleado,
			    'lugarFirmaSolicitud' => $sLugarSol,
			    'claveAgentePromotor' => $claveconsar,
				'selloVerificacionBiometrica' => $sverificacion
			    );
		
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $array);
		
		//Crear XML a enviar de parametros	
		$xml = $objGn->obtenerXML($array);

		$mensaje = "XML:".$xml;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//actualizara el estatus del folio en la tabla solconstancia
		ActualizaestatusSolconstancia($iFolio);

		//Ejecutar el Servicio 
		$arrResp = $objGn->consumirServicioEjecutarAplicacion($idServicio,$idServidor,$xml);

		//pasa la respuesta obtenida del metodo anterior
		$mensaje = $arrResp->descripcionRespuesta;
		$folioServicioAfore = $arrResp->folioServicioAfore;
		$respuestaservicio = $arrResp->respondioServicio;

		$mensajelog = "Mensaje:".$mensaje.'FolioAfore:'.$folioServicioAfore."RespServicioInv:".$respuestaservicio;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensajelog);
	
		return $arrResp;
	}

	function obtenerRespuestasolconstancia($idServidor,$iFolioSerAfore,$iFolio)
	{
		
		//se crea objeto para invocar los metodos de la clase
		$objGn = new CMetodoGeneral();

		//se crea arreglo a retornar
		$arrResp = array();
		$mensaje = '';
		$arrResp['descripcion'] = '';
		$arrDatos['estatus'] = '0';

		$mensaje = "Parametro a enviarle a consultar: idservidor:".$idServidor. ' Folio Afore:'.$iFolioSerAfore;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//Ejecutar el Servicio 
		$arrResp = $objGn->consumirServicioObtenerRespuesta($idServidor,$iFolioSerAfore);
		//$mensaje = $arrResp;

		$mensaje = $arrResp->descripcionRespuesta;
		$iRespuesta = $arrResp->respondioServicio;

		$mensaje = "Mensaje: ".$mensaje." RespondioSErvicio:".$iRespuesta;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//solamente si el metodo consumir servicio responde correctamente, este consultara el estatus del folio
		if ($iRespuesta == 1)
		{
			$arrResp = ConsultaSolconstancia($iFolio);
		}

		return $arrResp;

	}

	//funcion que permite consultar la solicitud de la solconstancia
	function ConsultaSolconstancia ($iFolio)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();

		//declara un arreglo
		$arrDatos = array();
		$cSql = '';
		$arrDatos['descripcion'] = '';
		$arrDatos['estatus'] = '';

		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);

			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql = "select estatus,diagnostico,curp from fnconsultasolconstancia($iFolio);";
				
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);

				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estado'] = OK___;

					foreach($resulSet as $reg)
					{			
						$arrDatos['registros'][]=array_map('trim', $reg);
						$arrDatos['estatus'] = $reg['estatus'];
						$arrDatos['diagnostico'] = trim($reg['diagnostico']);
						$arrDatos['curp'] = trim($reg['curp']);

						$mensaje ='estatus:'.$arrDatos['estatus'].' Diag:'.$arrDatos['diagnostico'].' Curp:'.$arrDatos['curp'];
						//$objGn->grabarLogx( '[' . __FILE__ . ']' .$mensaje);
					}

					//si el estatus proceso es igual a 4 significa que fue contestada por procesar
					//si el codigo diagnostico es diferente de vacio, significa que hay un rechazo
					if ($arrDatos['estatus'] == 4 && $arrDatos['diagnostico'] !='')
					{
						//atraves de la curp se obtiene la descripcion del rechazo
						$arrDatos['descripcion'] = ConsultaRechazo($arrDatos['curp']);
						$mensaje = 'DES:'.$arrDatos['descripcion'];
						//$objGn->grabarLogx( '[' . __FILE__ . ']' .$mensaje);
					}
					else
					{
						$arrDatos['descripcion'] =trim ($arrDatos['descripcion']);
					}

				}
				else
				{
					// Si existe un error en la consulta mostrará el siguiente mensaje 
					$arrDatos['estado'] = ERR__;
					$arrDatos['estatus'] = '';
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar ĺa consulta";

					throw new Exception("constanciaafiliacion.php\tConsultaSolconstancia"."\tError al ejecutar la consulta \t"." | " . pg_errormessage().'Consulta:'.$cSql);
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
				$mensaje = "\tconstanciaafiliacion.php\tConsultaSolconstancia"."\tNo pudo abrir la conexion a PostGreSQL (aforeglobal)";
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
				$arrDatos['estado'] = ERR__;
				$arrDatos['estatus'] = '';
				$arrDatos['descripcion'] = "No abrio Conexion a PostGreSQL";
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;	

	}

	//funcion que permite consultar el rechazo atraves de la curp
	function ConsultaRechazo($sCurp)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();

		//declara un arreglo
		$arrDatos = array();
		$cSql = '';
		$sDescripcion = '';

		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);

			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql = "select descripcion from fn_consulta_rechazo('$sCurp');";
				
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);

				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{

					foreach($resulSet as $reg)
					{			
						$arrDatos['registros'][]=array_map('trim', $reg);
						$sDescripcion = utf8_encode($reg['descripcion']);
					}
				}
				else
				{

					throw new Exception("constanciaafiliacion.php\tConsultaRechazo"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
				$mensaje = "\tconstanciaafiliacion.php\tConsultaRechazo"."\tNo pudo abrir la conexion a PostGreSQL (aforeglobal)";
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode(). 'Consulta:'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $sDescripcion;	
	}

	//funcion que permite actualizar el campo estatusproceso a 3, que es enviado a procesar
	function ActualizaestatusSolconstancia($iFolio)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();

		//declara un arreglo
		$arrDatos = array();
		$cSql = '';
		$sRespuesta = '';

		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);

			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql = "select fnactualizaestatussolconstancia as respuesta from fnactualizaestatussolconstancia($iFolio,'');";
				
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);

				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{

					foreach($resulSet as $reg)
					{			
						$arrDatos['registros'][]=array_map('trim', $reg);
						$sRespuesta = $reg['respuesta'];

						if ($sRespuesta == 1)
						{
							$mensaje = "Se Actualizo el estatuscontingente a 1 para el folio: ".$iFolio;
							$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
						}
					}
				}
				else
				{

					throw new Exception("constanciaafiliacion.php\tConsultaRechazo"."\tError al ejecutar la consulta \t"." | " . pg_errormessage()." consulta:".$cSql );
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
				$mensaje = "\tconstanciaafiliacion.php\tConsultaRechazo"."\tNo pudo abrir la conexion a PostGreSQL (aforeglobal)";
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode(). 'Consulta:'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}	
	}

	function obtenerRespuestaRechazo($iFolioSerAfore)
	{
		
		$datos = array();
		$objGn = new CMetodoGeneral();

		$cnxPgBusTramites =  new PDO("pgsql:host=".IPSERVIDORBUSTRAMITES.";port=5432;dbname=".BASEDEDATOSBUSTRAMITES, USUARIOBUSTRAMITES, PASSWORDBUSTRAMITES);
		
		if($cnxPgBusTramites)
		{
			$cSql="select fnobtenervalorxml::character(3) as rechazo from fnobtenervalorxml('diagnosticoProcesar', 'respuestaxml', 'bitacoracliente', 'folioservicioafore=$iFolioSerAfore')";
			$objGn->grabarLogx($cSql);
			//Ejecuta la consulta
			$resulSet = $cnxPgBusTramites->query($cSql);
			
			//Verifica que se haya ejecutado correctamente
			if($resulSet)
			{	
				foreach($resulSet as $reg)
				{			
					$datos['rechazado']  = trim($reg['rechazo']);
				}
				
			}
			else
			{
				$objGn->grabarLogx( '[' . __FILE__ . '] [ obtenerRespuestaRechazo ] Error  al ejecutar el metodo consulta obtenerRespuestaRechazo ' );
				//throw new Exception("CServiciosBusTramite.php\"."\tError al ejecutar la consulta \t"."  . pg_errormessage() );
			}
		}
		else
		{
			$arrErr = $cnxPgBusTramites->errorInfo();
			$objGn->grabarLogx( '[' . __FILE__ . '] [ obtenerRespuestaRechazo ] Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			$arrResp['descripcion'] = "No abrio Conexion";
		}
		
		$cnxPgBusTramites = null;

		return $datos;
	}
	
?>
