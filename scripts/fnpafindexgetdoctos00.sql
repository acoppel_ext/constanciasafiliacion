-- Function: fnpafindexgetdoctos00(integer, integer, character)

-- DROP FUNCTION fnpafindexgetdoctos00(integer, integer, character);

CREATE OR REPLACE FUNCTION fnpafindexgetdoctos00(
    integer,
    integer,
    character)
  RETURNS SETOF tpopafindexdoctos AS
$BODY$

DECLARE

	iIdPadre		ALIAS FOR $1;
	idOperacionX		ALIAS FOR $2;
	cFolio			ALIAS FOR $3;
	reg			tpoPafIndexDoctos;
	cIdHijos		CHARACTER(150);
	cFolioImplicacion	CHARACTER(20);
	iEsDoctoPublicado	INTEGER;
	iFolioEnrolamiento	INTEGER;
	iFolioAux		INTEGER;
	iLongitud		INTEGER;
	iTipoSolicitante	SMALLINT;
	bAgregar		BOOLEAN;
	cSql			TEXT;
	-------------------------
	iTipoServicio		INTEGER;
	cCurp			CHARACTER(18);
	cCurpAux		CHARACTER(18);
	cCurpOriginal		CHARACTER(18);
	chNss			CHARACTER(11);
	iTipoSolicitanteCons	INTEGER;
	iTipoComprobante	INTEGER;
	iModificoCurp		INTEGER;
	cPrimerHijo		char(2);
	
	-----Folio: 893.1-----
	iTipoSolicitanteT	SMALLINT = 0;
	iTipoConstancia		INTEGER = 0;
	----------------------

	cNombre 		CHARACTER(40);
	cPaterno 		CHARACTER(40);
	cMaterno 		CHARACTER(40);
	iSexo			INTEGER;
	cFena			CHARACTER(12);
	iEntidadNacimiento	INTEGER;
	chNombre 		CHARACTER(40);
	chPaterno 		CHARACTER(40);
	chMaterno 		CHARACTER(40);
	iAfSexo			INTEGER;
	chFena			CHARACTER(12);
	iAfEntidadNacimiento	INTEGER;

	--Folio 1034
	iEnrolpermanente 	INTEGER;

	iCodigoPostal 			INTEGER;
	iColonia 				INTEGER;
	cCalle 					CHARACTER(65);
	cNumExterior 			CHARACTER(15);
	cNumInterior 			CHARACTER(15);
	iDelegMunicipio 		INTEGER;
	iCiudad 				INTEGER;
	iEntidadFederativa 		INTEGER;
	cRfcMenor 				CHARACTER(13);

	iCodigoPostalAux 		INTEGER;
	iColoniaAux 			INTEGER;
	cCalleAux 				CHARACTER(65);
	cNumExteriorAux 		CHARACTER(15);
	cNumInteriorAux 		CHARACTER(15);
	iDelegMunicipioAux 		INTEGER;
	iCiudadAux 				INTEGER;
	iEntidadFederativaAux 	INTEGER;
	cRfcMenorAux 			CHARACTER(13);

	cNssMenor 				CHARACTER(11);
	iFolioServOriginal 		INTEGER;

	iModificoRfcMenor		INTEGER;
	iModificoDomicilioMenor		INTEGER;


BEGIN
	iEnrolpermanente 	= 0;
	cFolioImplicacion 	= '';
	iFolioEnrolamiento 	= 0;
	iFolioAux 		= 0;
	iLongitud 		= 0;
	iModificoCurp 		= 0;
	---------------------------------------------- Folio: 316.1 ----------------------------------------------
	iTipoServicio	 	= 0;
	cCurp 			= '';
	-----------------------------------------------------------------------------------------------------------
	iTipoSolicitanteCons 	= 0;
	iTipoComprobante 	= 0;
	cPrimerHijo 		= '';

	cNombre 		= '';			
	cPaterno 		= ''; 			
	cMaterno 		= '';			
	iSexo 			= 0;				
	cFena 			= '';				
	iEntidadNacimiento 	= 0;	
	chNombre 		= '';			
	chPaterno 		= '';			
	chMaterno 		= '';			
	iAfSexo			= 0;			
	chFena 			= '';				
	iAfEntidadNacimiento 	= 0;

	iCodigoPostal 			= 0;
	iColonia 				= 0;
	cCalle 					= '';
	cNumExterior 			= '';
	cNumInterior 			= '';
	iDelegMunicipio 		= 0;
	iCiudad 				= 0;
	iEntidadFederativa 		= 0;
	cRfcMenor 				= '';

	iCodigoPostalAux 		= 0;
	iColoniaAux 			= 0;
	cCalleAux 				= '';
	cNumExteriorAux 		= '';
	cNumInteriorAux 		= '';
	iDelegMunicipioAux 		= 0;
	iCiudadAux 				= 0;
	iEntidadFederativaAux 	= 0;
	cRfcMenorAux 			= '';

	cNssMenor 				= '';
	iFolioServOriginal 		= 0;
	iModificoRfcMenor 		= 0;
	iModificoDomicilioMenor 		= 0;
	
	--FOLIO 786
	IF RIGHT(cFolio, 2) = '-S' THEN
		SELECT  tiposolicitante
			INTO iTipoSolicitanteT
			FROM recserviciosafore WHERE folioservicio = REPLACE(cFolio,'-S','')::INTEGER;
	ELSE 
		SELECT tiposolicitante, tipoconstancia,enrolpermanente
			INTO iTipoSolicitanteT, iTipoConstancia,iEnrolpermanente
			FROM solconstancia where folio = cFolio::INTEGER;
	END IF;
	
	--FOLIO 786
	SELECT REPLACE( identificadorHijos, '|', ',' ) INTO cIdHijos FROM pafIndexImg WHERE identificadorPadre = iIdPadre AND identificadorHijos != '-1' AND idOperacion = idOperacionX;
		
	IF FOUND THEN	
		IF RIGHT(cFolio, 2) != '-S' THEN		
			IF iIdPadre = 275 OR iIdPadre = 276 OR iIdPadre = 277 OR iIdPadre = 278 THEN
				SELECT TRIM(folioimplicaciontraspaso), curp INTO cFolioImplicacion, cCurp FROM solconstancia WHERE folio = cFolio::integer AND tipoconstancia = 27;
			
				IF FOUND THEN
					IF cFolioImplicacion = '' THEN						
						SELECT REPLACE(LEFT(identificadorHijos, LENGTH(identificadorHijos) - 4), '|', ',') INTO cIdHijos
						FROM pafIndexImg
						WHERE identificadorPadre = iIdPadre  AND identificadorHijos != '-1' AND idOperacion = idOperacionX;
						
					END IF;
				END IF;
			END IF;

			IF iIdPadre = 302 OR iIdPadre = 303 THEN				
				SELECT tiposolicitante, tipocomprobante INTO iTipoSolicitanteCons, iTipoComprobante FROM solconstancia WHERE folio = cFolio::integer;
				
				IF FOUND THEN
					IF iTipoSolicitanteCons = 1 THEN
						cIdHijos = REPLACE (cIdHijos, ',462','');
					ELSE
						cIdHijos = REPLACE (cIdHijos, ',373', ''); --Eliminar el documento del formato de enrolamiento
					END IF;

					IF iTipoComprobante != 1 THEN
						cIdHijos = REPLACE (cIdHijos, '6,','');
					END IF;
				END IF;
				
				SELECT folioenrolamiento, curp INTO iFolioEnrolamiento, cCurp FROM solconstancia WHERE folio = cFolio::integer;
				
				IF FOUND THEN
					IF iFolioEnrolamiento = 0 THEN
					
						--Valida que tenga un enrolamiento conformado ante procesar
						IF (SELECT enrolpermanente FROM solconstancia WHERE folio = cFolio::integer) = 0 THEN
							SELECT REPLACE(LEFT(identificadorHijos, LENGTH(identificadorHijos) - 4), '|', ',') INTO cIdHijos
							FROM pafIndexImg
							WHERE identificadorPadre = iIdPadre  AND identificadorHijos != '-1' AND idOperacion = idOperacionX;
							
							IF iTipoSolicitanteCons = 1 THEN
								cIdHijos = REPLACE (cIdHijos, ',462','');							
							END IF;
							
						ELSE
							cIdHijos = REPLACE (cIdHijos, ',373', ''); --Eliminar el documento del formato de enrolamiento
						END IF;

					END IF;
				END IF;
			END IF;			
			
			---------------------------------------------- Folio: 654.2 / 991.1 ----------------------------------------------
			IF iIdPadre IN (500,509,524) THEN -- Validar si es un Traspaso con Autenticacion INE/BCPL 1498: se agrega nuevo tipo de autenticaacion 'Folio 48-Dactilar'(524)
				--Validar si la solicitud tiene Constancia para Implicaciones de Traspaso
				IF NOT EXISTS( SELECT folioimplicaciontraspaso FROM solconstancia WHERE folio = cFolio::integer AND length(trim(folioimplicaciontraspaso)) > 0 ) THEN
					--Si no tiene folio de la constancia de implicaciones
					cIdHijos = REPLACE (cIdHijos, ',377', ''); --Eliminar el documento de la Constancia para Implicaciones de Traspaso (CI)
				END IF;				
			END IF;
			
			---------------------------------------------- Folio: 1425 ----------------------------------------------
		    IF iIdPadre IN (275,276,277,278,500,509,524) THEN --Traspaso / 1498: se agrega nuevo tipo de autenticaacion 'Folio 48-Dactilar' (524)
					
				cIdHijos = REPLACE (cIdHijos, ',7', ''); --Eliminar Contrato  de Administracion de Fondos
			END IF;				
		
			 
			IF iIdPadre IN (1) THEN --Registro
					
				cIdHijos = REPLACE (cIdHijos, ',007', ''); --Eliminar  documento Contrato de Administracion de Fondos
			END IF;				
				
			----------------------------------------------------------------------------------------------------------	

			SELECT curp INTO cCurp FROM solconstancia WHERE folio = cFolio::INTEGER;
			IF NOT EXISTS( SELECT 'OK' FROM bitacoraautorizacionenrolamiento WHERE foliosolicitud = cFolio::INTEGER AND curptrabajador = cCurp AND tipoenrolamiento IN ( 1, 2, 3, 4 ) ) THEN
				cIdHijos = REPLACE(cIdHijos, ',435', '');
			END IF;
			
			-----------------------------------------------folio 1034--------------------------------------------------------------------------------------

			/*
			--1, Registro/Registro independiente
			--278 tiene edocta = 1  itipotraspaso IN (0,1,327,427,527)
			--276 itipotraspaso = 827,itieneedocta = 4
			--500 itipotraspaso IN (828, 829)  traspasos IMSS e ISSSTE con AutenticaciÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â³n INE
			--509 tipotraspaso IN (830, 831 )Traspasos IMSS e ISSSTE con AutenticaciÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â³n BCPL
			--524 tipotraspaso in (832,833) Traspaso IMSS e ISSSTE con Autenticacion Folio 48 Dactilar
			*/
			IF iIdPadre IN(1,278,276,500,509,524) THEN
				IF iEnrolpermanente = 0 AND iTipoSolicitanteT = 1 THEN
					cIdHijos = cIdHijos ||',373';
					raise notice 'entro';
				END IF;
	
			END IF;
			-----------------------------------------------folio 1034---------------------------------------------------------------------------------------
			
		ELSE

			IF iIdPadre = 315 OR iIdPadre = 316 OR iIdPadre = 317 OR iIdPadre = 318 OR iIdPadre = 319 OR iIdPadre = 320 OR iIdPadre = 321 OR iIdPadre = 327 OR iIdPadre = 328 OR iIdPadre = 505 OR iIdPadre = 494 THEN
				iLongitud = LENGTH(cFolio);
				iFolioAux = SUBSTRING(cFolio, 1, iLongitud-2);

				SELECT folioenrolamiento, tiposolicitante, codigomotivo, curp
				INTO iFolioEnrolamiento, iTipoSolicitante, iTipoServicio, cCurp
				FROM recserviciosafore WHERE folioservicio = iFolioAux;
				
				IF FOUND THEN		
					
				
					IF iTipoServicio = 2040 OR iTipoServicio = 2022 OR iTipoServicio = 2044 THEN
					
						-- 810.1
						IF iTipoServicio = 2022 THEN
							SELECT curp, nss, nombre, apellidopaterno, apellidomaterno, genero, fechanacimiento, entidadnacimiento 
							INTO cCurpAux, chNss, cNombre, cPaterno, cMaterno, iSexo, cFena, iEntidadNacimiento
							FROM eiserviciosdatospersonales WHERE folio = cFolio;
							
							SELECT n_unico, nombres,paterno,materno,sexo,fena, entidad_nacimiento 
							INTO cCurpOriginal,chNombre,chPaterno,chMaterno,iAfSexo,chFena,iAfEntidadNacimiento
							FROM afop_cuota_diaria WHERE n_seguro = chNss;

							IF(cCurpAux = cCurpOriginal AND cNombre = chNombre  AND cPaterno = chPaterno AND cMaterno = chMaterno AND iSexo = iAfSexo  AND cFena = chFena AND iEntidadNacimiento = iAfEntidadNacimiento) THEN
								iModificoCurp = 1;	
							END IF;
						END IF;	
						
						IF iTipoServicio = 2044 THEN

							SELECT nss 
							INTO cNssMenor 
							FROM recserviciosafore 
							WHERE folioservicio = REPLACE(cFolio,'-S', '')::INTEGER;

							SELECT c.folio
							INTO iFolioServOriginal
							FROM recserviciosafore a
							INNER JOIN afop_cuota_diaria b ON (b.n_folio = a.foliosolicitud)
							INNER JOIN eimaestro c ON (c.curp = b.n_unico)
							WHERE a.folioServicio = REPLACE(cFolio, '-S', '')::INT
							AND a.tiposervicio = 'S';

							-- Domicilio certificado
							SELECT a.codigopostal,a.colonia,a.calle,a.numexterior,a.numinterior,a.delegmunicipio,a.ciudad,a.entidadfederativa
							INTO iCodigoPostal,iColonia,cCalle,cNumExterior,cNumInterior,iDelegMunicipio,iCiudad,iEntidadFederativa
							FROM eiserviciosdatosdomicilio AS a
							LEFT JOIN cattipodomicilioei AS b ON (a.tipodomicilio::SMALLINT = b.idtipodomicilio::SMALLINT)
							LEFT JOIN ColInfxColonias AS cx ON (a.colonia = cx.idniv4asentamiento)
							LEFT JOIN ColInfxniv1regionedo AS dx ON (a.entidadfederativa = dx.idniv1regionedo AND cx.idniv1regionedo = dx.idniv1regionedo)
							LEFT JOIN ColInfxniv3municipio AS ex ON (a.delegmunicipio = ex.idniv3municipio AND cx.idniv1regionedo = ex.idniv1regionedo AND cx.idniv3municipio = ex.idniv3municipio)
							LEFT JOIN ColInfxniv2ciudad AS fx ON (a.ciudad = fx.idniv2ciudad AND cx.idniv1regionedo = fx.idniv1regionedo AND fx.idniv2ciudad = fx.idniv2ciudad)
							LEFT JOIN colinfxcatalogosistema AS g ON (a.pais::INTEGER = g.idcatalogo)
							WHERE a.folio = iFolioServOriginal||'-S'
							order by tipodomicilio;

							-- Domicilio actual
							SELECT a.codigopostal,a.colonia,a.calle,a.numexterior,a.numinterior,a.delegmunicipio,a.ciudad,a.entidadfederativa
							INTO iCodigoPostalAux,iColoniaAux,cCalleAux,cNumExteriorAux,cNumInteriorAux,iDelegMunicipioAux,iCiudadAux,iEntidadFederativaAux
							FROM eiserviciosdatosdomicilio AS a
							LEFT JOIN cattipodomicilioei AS b ON (a.tipodomicilio::SMALLINT = b.idtipodomicilio::SMALLINT)
							LEFT JOIN ColInfxColonias AS cx ON (a.colonia = cx.idniv4asentamiento)
							LEFT JOIN ColInfxniv1regionedo AS dx ON (a.entidadfederativa = dx.idniv1regionedo AND cx.idniv1regionedo = dx.idniv1regionedo)
							LEFT JOIN ColInfxniv3municipio AS ex ON (a.delegmunicipio = ex.idniv3municipio AND cx.idniv1regionedo = ex.idniv1regionedo AND cx.idniv3municipio = ex.idniv3municipio)
							LEFT JOIN ColInfxniv2ciudad AS fx ON (a.ciudad = fx.idniv2ciudad AND cx.idniv1regionedo = fx.idniv1regionedo AND fx.idniv2ciudad = fx.idniv2ciudad)
							LEFT JOIN colinfxcatalogosistema AS g ON (a.pais::INTEGER = g.idcatalogo)
							WHERE a.folio = cFolio
							order by tipodomicilio;

							-- Rfc certificado
							SELECT rfc 
							INTO cRfcMenor
							FROM eiServiciosDatosPersonales WHERE folio = (iFolioServOriginal||'-S') AND nss = cNssMenor;

							-- Rfc actual
							SELECT rfc 
							INTO cRfcMenorAux
							FROM eiServiciosDatosPersonales WHERE folio = cFolio AND nss = cNssMenor;

							
							IF (cRfcMenor = cRfcMenorAux AND LENGTH(cRfcMenorAux) = 10) THEN
								iModificoRfcMenor = 1;
							END IF;

							IF (iCodigoPostal = iCodigoPostalAux AND iColonia = iColoniaAux AND cCalle = cCalleAux AND cNumExterior = cNumExteriorAux AND cNumInterior = cNumInteriorAux AND iDelegMunicipio = iDelegMunicipioAux AND iCiudad = iCiudadAux AND iEntidadFederativa = iEntidadFederativaAux) THEN
								iModificoDomicilioMenor = 1;
							END IF;
							
						END IF;	
						
						IF NOT EXISTS( SELECT 'OK' FROM bitacoraautorizacionenrolamiento WHERE foliosolicitud = iFolioAux::INTEGER AND curptrabajador = cCurp AND tipoenrolamiento IN (1, 2, 3, 4) ) THEN
							cIdHijos = REPLACE(cIdHijos, ',435', '');
						END IF;
						
					ELSE
					
						cIdHijos = REPLACE(cIdHijos, ',435', '');
						
					END IF;
					
					IF iTipoServicio IN(2040,2022) THEN --> 810.1	*quitar documentos para terceras personas

						IF iTipoSolicitante = 1 THEN 				
							cIdHijos = REPLACE(cIdHijos, ',219,468,475,462', '');
						ELSE
							cIdHijos = REPLACE(cIdHijos, ',373', '');
						END IF;
						
						/*IF iTipoSolicitante IN(2) THEN --quitar identificacion de terceros
							cIdHijos = REPLACE(cIdHijos, ',475', ''); 
						END IF;*/
						
						IF iTipoSolicitante IN(3,4) THEN --quitar acta de defuncion
							cIdHijos = REPLACE(cIdHijos, ',219', ''); 
						END IF;
						
						IF iModificoCurp = 1 THEN
							cPrimerHijo = substring(cIdHijos, 1, 2);
							if(cPrimerHijo = '8,') then
								cIdHijos = substring(cIdHijos, 3, length(cIdHijos)-2);	
							end if;
						END IF;
					ELSIF iTipoServicio = 2044 THEN
						
						cIdHijos = REPLACE(cIdHijos, ',494', '');
						
						IF iModificoRfcMenor = 1 THEN
							cIdHijos = REPLACE(cIdHijos, ',496', '');
						END IF;
						
						IF iModificoDomicilioMenor = 1 THEN
							cIdHijos = REPLACE(cIdHijos, ',6', '');
						END IF;
					ELSE
						IF iTipoSolicitante = 1 THEN						
							cIdHijos = REPLACE(cIdHijos, ',219,468,475', '');
						END IF;	
					END IF;--< 810.1
					
					IF iFolioEnrolamiento = 0 THEN						
						cIdHijos = REPLACE(cIdHijos, ',373', '');						
					END IF;

				END IF;				
			END IF;			
		END IF;
		
		cSql = 'SELECT Identificador, DEscripcion, CveOperacion, idDocumento, DoctosRequeridos FROM catPafIndexImg WHERE identificador IN( '|| cIdHijos ||') ORDER BY Descripcion';
		
		FOR reg IN EXECUTE cSql
		
		LOOP

			IF iIdPadre IN(1,275,276,277,278,500,509,524) THEN --> 1425.1 * Mandar al final el identificador OTRO  / 1498: se agrega nuevo tipo de autenticaacion 'Folio 48-Dactilar' (524)
				IF reg.tpoidentificador=116 OR reg.tpoidentificador=112 THEN
						reg.tpoidentificador=7;
				END IF;
			END IF;
			IF iTipoServicio IN(2040,2022) THEN --> 810.1 *Cambiar la descripcion del documento ID00 dependiendo del tipo solicitante
				IF iTipoSolicitanteT = 2 THEN
					IF reg.tpoidentificador=475 THEN	reg.tpodescripcion='Identificacion Oficial del Beneficiario';	
					END IF;
					/*IF reg.tpoidentificador=475 THEN	reg.tpodescripcion='Identificacion Oficial reverso del Beneficiario';	
					END IF;*/
				END IF;
			ELSE
				--------------------- 893.1 Cambiar descripciones de documentos para terceras figuras registro---------------------
				IF(iTipoConstancia IN (26,33)) THEN 
					IF(iTipoSolicitanteT::INTEGER = 2) THEN --Beneficiario
						IF reg.tpoidentificador= 204 THEN
							reg.tpodescripcion='FotografÃƒÆ’Ã‚Â­a del Beneficiario';
						END IF;
						IF reg.tpoidentificador= 348 THEN
							reg.tpodescripcion='Documento Probatorio';
						END IF;
					ELSIF (iTipoSolicitanteT::INTEGER = 3) THEN --Apoderado Legal
						IF reg.tpoidentificador= 204 THEN
							reg.tpodescripcion='FotografÃƒÆ’Ã‚Â­a del Apoderado Legal';
						ELSIF reg.tpoidentificador = 221 THEN
							reg.tpodescripcion='IdentificaciÃƒÆ’Ã‚Â³n Oficial del Apoderado Legal';
						END IF;
					ELSIF (iTipoSolicitanteT::INTEGER = 4) THEN --Curador
						IF reg.tpoidentificador= 204 THEN
							reg.tpodescripcion='FotografÃƒÆ’Ã‚Â­a del Curador';
						ELSIF reg.tpoidentificador = 221 THEN
							reg.tpodescripcion='IdentificaciÃƒÆ’Ã‚Â³n Oficial del Curador';
						END IF;
					END IF;
				------------------------------------------------------- 893.1 -------------------------------------------------------
				ELSE
					IF iTipoSolicitanteT::INTEGER = 2 THEN
						IF reg.tpoidentificador=85 THEN	reg.tpodescripcion='Identificacion Oficial del Beneficiario';
						END IF;
						IF reg.tpoidentificador=86 THEN	reg.tpodescripcion='IdentificaciÃƒÆ’Ã‚Â³n Oficial reverso del Beneficiario';
						END IF;
					END IF;
				END IF;
				
			END IF;--< 810.1
				
			IF reg.tpoidentificador = 14 THEN
				IF EXISTS (SELECT folio FROM colsolicitudes WHERE folio=cFolio::INT AND tiposolicitud != 27 AND finado = '1') THEN
					bAgregar = TRUE;
				ELSE
					bAgregar = FALSE;
				END IF;				
			ELSE
				bAgregar = TRUE;
			END IF;
			
			IF bAgregar THEN		
				SELECT fnExisteDocumentoPublicado INTO iEsDoctoPublicado FROM fnExisteDocumentoPublicado(reg.tpoiddocumento, cFolio);
				
				IF iEsDoctoPublicado = 0 THEN
					RETURN NEXT reg;
				END IF;
				
			END IF;
			
		END LOOP;
		
	END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION fnpafindexgetdoctos00(integer, integer, character)
  OWNER TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fnpafindexgetdoctos00(integer, integer, character) TO public;
GRANT EXECUTE ON FUNCTION fnpafindexgetdoctos00(integer, integer, character) TO sysaforeglobal;
