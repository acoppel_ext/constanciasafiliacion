﻿//variables para enviar el valor por formulario para levantar la pagina del video
var urlGrabadoVideo = "";
var urlVideo = "";
var opcion = 0;
var nombrevideo = "";
var flag = 0;
var numeroempleado ="";// parametro recibido por url y pasar a la pagina del video
var cFormato = "";
var iCalidad = 0;
var folio = "";// parametro recibido por url y pasar a la pagina del video
var folioRegTrasp = "";
var cdialogoVideo = "";
var iTiempoMinimo = 0;
var iTiempoMaximo = 0;
var cIpModulo = "";

var valordialogo = "";// parametro recibido por url y pasar a la pagina del video

//variables para los datos del trabajador y promotor para mostrar en el dialogo
var curpsolicitante = "";
var cNombrePromotor = "";
var cApellidoPatPromotor = "";
var cApellidoMatPromotor = "";
var iClaveConsar = 0;
var cNombreTrabajador = "";	
var cApellidoPatTrabajador = "";
var cApellidoMatTrabajador = "";
var cCurpTrabajador = "";
var cNombreSolicitante = "";
var cApellidoPatSolicitante = "";
var cApellidoMatSolicitante = "";
var curpSolicitante = "";
var iCelular = "";
var dia = '';
var mes = '';
var anio = '';
var aforeActual = '';
var tipoConstancia = '';
var ciudadConstancia = '';
var claveOPeracion = '';
var ligaCase = 'php/constanciaafiliacion.php';
var iTipodegrabado;
var BanderaGuardado = 0;

var iTipoSolicitante = 0;
$(document).ready(function(){
	
	numeroempleado = getQueryVariable("numeroempleado");
	folio = getQueryVariable("folio");
	valordialogo = getQueryVariable("valordialogo");
	nombrevideo = getQueryVariable("nombrevideo");
	iTipoSolicitante = getQueryVariable("tiposolicitante");

	iTipodegrabado = 2;
	//funcion para obtener los datos del titular, beneficiario y promotor
	obtenerDatosCapturaVideo(folio, numeroempleado);
	//funcion para obtener las url, la de guardado del video y la url de la pagina del grabado del video.
	obtenerUrlsVideo();
	//funcion para obtener el valor de los parametros del dialogo, los tiempos minimo y maximo y la ip de la pc.
	obtenerDialogos(valordialogo);
	//funcion que crea el formulario para el envio de datos para la grabacion del video
	formularioVideo(urlGrabadoVideo);
	//funcion que nos trae la clave de operacion de la afiliacion para agregarlo al nombre
	//obtenerClaveOperacion();
	
	opcion = 1;
	//nombrevideo = cCurpTrabajador+folio+claveOPeracion;
	flag = 1;
	
	registrarVideoAfiliacion();
	
	
	if (BanderaGuardado ==1){
		$('#idopcion').val(opcion);
		$('#nombrevideo').val(nombrevideo);
		$('#flag').val(flag);
		$('#numeroempleado').val(numeroempleado);
		$('#ipmodulo').val(cIpModulo);
	
		cdialogoVideo = cdialogoVideo.replace('&aacute;','á');
		cdialogoVideo = cdialogoVideo.replace('&eacute;','é');
		cdialogoVideo = cdialogoVideo.replace('&iacute;','í');
		cdialogoVideo = cdialogoVideo.replace('&oacute;','ó');
		cdialogoVideo = cdialogoVideo.replace('&uacute;','ú');

		$('#dialogo').val(cdialogoVideo);
		$('#formato').val(cFormato);
		$('#calidad').val(iCalidad);
		$('#folio').val(folio);
		$('#folioRegTrasp').val(folioRegTrasp);
		$('#tiempominimo').val(iTiempoMinimo);
		$('#tiempomaximo').val(iTiempoMaximo);
		$('#urlsubirvideo').val(urlVideo);
		$('#tipodegrabado').val(iTipodegrabado);
		
		console.log(document.getElementById('formularioVideo'));
		
		document.formularioVideo.submit();
	}
	else
	{
		alert('Problema al generar el grabado del video');
		$('#capturaVideo').dialog( "close" );
	}
});

//Obtiene l valor da la variable pasada por url
function getQueryVariable(varGet)
{
	//Obtener la url compleata del navegador
	var sUrl = window.location.search.substring(1);
	//Separar los parametros junto con su valor
	var vars = sUrl.split("&");
	for (var i=0;i<vars.length;i++) {
		//Obtener el parametro y su valor en arreglo de 2
		var par = vars[i].split("=");
		if(par[0] == varGet) // [0] nombre de variable, [1] valor
		{
			return par[1];
		}
	}
	return(false);
}

function obtenerUrlsVideo()
{
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:48},
		success: function(data)
		{
			datos = data;
			if(datos.respuesta == 1)
			{
				urlGrabadoVideo = datos.urlgrabado.trim();
				urlVideo = datos.urlguardado.trim();
			}
			else
			{
				alert('NO SE EJECUTO CORRECTAMENTE LA APLICACIÓN, INTENTE DE NUEVO');
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
}

function formularioVideo(urlGrabadoVideo)
{
	var newHtml = "<div style ='display:none'>";
	newHtml += "<form  method='POST' action= ' "+ urlGrabadoVideo + "' id='formularioVideo' name='formularioVideo'>";
	newHtml += "<input name='idopcion' id='idopcion'>";
	newHtml += "<input name='nombrevideo' id='nombrevideo'>";
	newHtml += "<input name='flag' id='flag'>";
	newHtml += "<input name='numeroempleado' id='numeroempleado'>";
	newHtml += "<input name='ipmodulo' id='ipmodulo'>";
	newHtml += "<input name='dialogo' id='dialogo'>";
	newHtml += "<input name='formato' id='formato'>";
	newHtml += "<input name='calidad' id='calidad'>";
	newHtml += "<input name='folio' id='folio'>";
	newHtml += "<input name='folioRegTrasp' id='folioRegTrasp'>";
	newHtml += "<input name='tiempominimo' id='tiempominimo'>";
	newHtml += "<input name='tiempomaximo' id='tiempomaximo'>";
	newHtml += "<input name='urlsubirvideo' id='urlsubirvideo'>";
	newHtml += "<input name='tipodegrabado' id='tipodegrabado'>";
	newHtml += "</form>";
	newHtml += "</div>";
	$('#divFormularioVideo').html(newHtml);
}


function obtenerDialogos(valordialogo)
{
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:47,tipodialogo:valordialogo},
		success: function(data)
		{
			datos = data;
			if(datos.respuesta == 1)
			{
				cdialogoVideo = datos.dialogo.trim();
				iTiempoMinimo = datos.tiempominimo;
				iTiempoMaximo = datos.tiempomaximo;
				cFormato 	  = datos.formato;
				iCalidad = datos.calidad;
				cIpModulo = datos.ipmodulo;
				
				switch(valordialogo)
				{
					case '1':
					
					
					cdialogoVideo = cdialogoVideo.replace("[Nombre]",cNombreTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[ApellidoPat]",cApellidoPatTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[ApellidoMat]",cApellidoMatTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[celulartrab]",iCelular);
					cdialogoVideo = cdialogoVideo.replace("[AforeCedente]",aforeActual);
					break;
					case '2':
					cdialogoVideo = cdialogoVideo.replace("[Nombre]",cNombreTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[ApellidoPat]",cApellidoPatTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[ApellidoMat]",cApellidoMatTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[celulartrab]",iCelular);
					break;
					case '3':
					case '5':
					case '6':
					cdialogoVideo = cdialogoVideo.replace("[nombrebeneficiario]",cNombreSolicitante);
					cdialogoVideo = cdialogoVideo.replace("[apellidoparbenef]",cApellidoPatSolicitante);
					cdialogoVideo = cdialogoVideo.replace("[apellidomatbenef]",cApellidoMatSolicitante);
					cdialogoVideo = cdialogoVideo.replace("[nombretrab]",cNombreTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[apellidopattrab]",cApellidoPatTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[apellidomattrab]",cApellidoMatTrabajador);
					break;
					case '4':
					cdialogoVideo = cdialogoVideo.replace("[NombreProm]",cNombrePromotor);
					cdialogoVideo = cdialogoVideo.replace("[ApellidoPatProm]",cApellidoPatPromotor);
					cdialogoVideo = cdialogoVideo.replace("[ApellidoMatProm]",cApellidoMatPromotor);
					cdialogoVideo = cdialogoVideo.replace("[NombreDeTrab]",cNombreTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[ApellidoPaternoTrab]",cApellidoPatTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[ApellidoMaternoTRab]",cApellidoMatTrabajador);
					cdialogoVideo = cdialogoVideo.replace("[celulartrab]",iCelular);
					break;
					case '7':
						var sSolicitante = ""

						if(iTipoSolicitante == 2)
							sSolicitante ="Beneficiario";
						else if (iTipoSolicitante == 3) 
							sSolicitante = "Apoderado Legal";
						else if (iTipoSolicitante == 4)
							sSolicitante = "Curador";

						cdialogoVideo = cdialogoVideo.replace("[NombreProm]",cNombrePromotor);
						cdialogoVideo = cdialogoVideo.replace("[ApellidoPatProm]",cApellidoPatPromotor);
						cdialogoVideo = cdialogoVideo.replace("[ApellidoMatProm]",cApellidoMatPromotor);
						cdialogoVideo = cdialogoVideo.replace("[TipoSolicitante]",sSolicitante);
						cdialogoVideo = cdialogoVideo.replace("[NombreDeTrab]",cNombreSolicitante);
						cdialogoVideo = cdialogoVideo.replace("[ApellidoPaternoTrab]",cApellidoPatSolicitante);
						cdialogoVideo = cdialogoVideo.replace("[ApellidoMaternoTRab]",cApellidoMatSolicitante);
						cdialogoVideo = cdialogoVideo.replace("[celulartrab]",iCelular);
					break;
				}
			}
			else
			{
				alert('NO SE EJECUTO CORRECTAMENTE LA APLICACIÓN, INTENTE DE NUEVO');
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
	
}

function obtenerDatosCapturaVideo(folio, numeroempleado)
{
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:49,folioConst:folio,empleado:numeroempleado},
		success: function(data)
		{
			datos = data;
			if(datos.respuesta == 1)
			{
				//ciudadConstancia = datos.ciudad.trim();
				cNombrePromotor = datos.nombrepromotor.trim();
				cApellidoPatPromotor = datos.apellidopaternoprom.trim();
				cApellidoMatPromotor = datos.apellidomaternoprom.trim();
				iClaveConsar = datos.claveconsar;
				cNombreTrabajador = datos.nombretrabajador.trim();	
				cApellidoPatTrabajador = datos.apellidopaternotrab.trim();
				cApellidoMatTrabajador = datos.apellidomaternotrab.trim();
				cCurpTrabajador = datos.curptrabajador.trim();
				iCelular = datos.telefonocelular.trim();
				aforeActual = datos.aforeactual.trim();
				cNombreSolicitante = datos.nombresolicitante.trim();
				cApellidoPatSolicitante = datos.apellidopaternosol.trim();
				cApellidoMatSolicitante = datos.apellidomaternosol.trim();
				curpSolicitante = datos.curpsolicitante.trim();
				dia = datos.dia;
				mes = datos.mes.trim();
				anio = datos.anio;
				
				if(datos.tipoconstancia == 26 || datos.tipoconstancia == 33)
				{
					tipoConstancia = 'REGISTRO';
				}
				else if(datos.tipoconstancia == 27 || datos.tipoconstancia == 1 || datos.tipoconstancia == 527)
				{
					tipoConstancia = 'TRASPASO';
				}
			}
			else
			{
				alert('NO SE EJECUTO CORRECTAMENTE LA APLICACIÓN, INTENTE DE NUEVO');
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
}



function obtenerClaveOperacion()
{
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:53},
		success: function(data)
		{
			datos = data;
			claveOPeracion = datos.ClaveOP.trim();
			
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
}

function registrarVideoAfiliacion()
{
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:52,nombrevideo:nombrevideo,folioConst:folio},
		success: function(data)
		{
			datos = data;
			if(datos.respuesta == 1)
			{
				BanderaGuardado = 1;
			}
			else
			{
				BanderaGuardado = 0;
			}
			
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
}