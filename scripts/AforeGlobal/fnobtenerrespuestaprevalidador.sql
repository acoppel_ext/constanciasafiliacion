-- Function: fnobtenerrespuestaprevalidador(bigint)

-- DROP FUNCTION fnobtenerrespuestaprevalidador(bigint);

CREATE OR REPLACE FUNCTION fnobtenerrespuestaprevalidador(ifoliosolicitud bigint)
  RETURNS SETOF tpdatosrespuestaprevalidador AS
$BODY$

DECLARE
    datosrespuesta tpdatosrespuestaprevalidador;
    datavacia integer;

BEGIN
    SELECT mrp.valMarcas, mrp.valFoliosConstanciasImplicaciones, mrp.valRegimenPensionario, 
           mrp.valNumTraspTreintaSeisMeses, mrp.valCitActiva, mrp.valRendimMenor,
           mrp.traspasoPrevioRendimientos, mrp.valCurpFuncionarioAdministradora, mrp.foliosolicitud, 
           mrp.valCurpTrabajador, mrp.valNssTrabajador, mrp.valApellidoPaterno, mrp.valApellidoMaterno, 
           mrp.valNombreTrab, mrp.cveEntTransBD, mrp.motRec, mrp.folioservicioafore, 
           mrp.clavepeticionservicio, mrp.codigo_error, mep.curp_promotor,
           mrp.fecha1traspasoprevio, mrp.fecha2traspasoprevio,  mrp.fecha3traspasoprevio, mrp.fecha4traspasoprevio,
           mrp.fecha5traspasoprevio
       INTO datosrespuesta.valMarcas, datosrespuesta.valFoliosConstanciasImplicaciones, 
         datosrespuesta.valRegimenPensionario, datosrespuesta.valNumTraspTreintaSeisMeses, 
         datosrespuesta.valCitActiva, datosrespuesta.valRendimMenor, datosrespuesta.traspasoPrevioRendimientos, 
         datosrespuesta.valCurpFuncionarioAdministradora, datosrespuesta.foliosolicitud, 
         datosrespuesta.valCurpTrabajador, datosrespuesta.valNssTrabajador, datosrespuesta.valApellidoPaterno, 
         datosrespuesta.valApellidoMaterno, datosrespuesta.valNombreTrab, datosrespuesta.cveEntTransBD, 
         datosrespuesta.motRec, datosrespuesta.folioservicioafore, datosrespuesta.clavepeticionservicio, 
         datosrespuesta.codigo_error, datosrespuesta.curp_promotor, datosrespuesta.fecha1traspasoprevio,
         datosrespuesta.fecha2traspasoprevio, datosrespuesta.fecha3traspasoprevio, datosrespuesta.fecha4traspasoprevio,
         datosrespuesta.fecha5traspasoprevio
    FROM maedatosrespuestaprevalidador AS mrp
    INNER JOIN maedatosenviarprevalidador AS mep ON mep.foliosolicitud = mrp.foliosolicitud
    WHERE mrp.foliosolicitud = ifoliosolicitud;

    GET DIAGNOSTICS datavacia = ROW_COUNT;

    IF datavacia > 0 THEN
        RETURN NEXT datosrespuesta;
    END IF;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION fnobtenerrespuestaprevalidador(bigint)
  OWNER TO sysaforeglobal;
