var sParametrosDigi = ""; 
//arreglo con el codigo ascii de las teclas espacio, backspace, tab, etc.
var arrCaracteresPermitidosCons = [0, 46, 127, 8, 9, 37, 38, 32, 39, 40, 180, 190, 209, 241];
var arrCaracteresCurp = [0, 8, 9, 241];
var arrCaracteresNombre = [0, 46, 8, 32, 241, 209];
var arrCaracteresApellidos = [0, 8, 32, 241];
var arrCaracteresNumCasa = [0, 8, 32, 45];
var folio = 0;
var iFoliobancoppel = 0;
var iTieneautBancoppel = 0;
var sClaveConsar = '';
var sNombrePromotor = '';
var iEmpleado = 0;
var sDescripcionLugar = '';
var sFechaAct = '';
var lFolioServicioAfore = 0;
var solProceso = 0;
var banderabancoppel = 0;
var entroine = 0;
var printButton = null;
var printFrame = null;
var iRenapo = 0;
var codError = "";
var bError = false;
var bPrioridad = false;
var bP16 = false;
var bP18 = true;
var BanderaAntiguedadTraspaso = false;
//
var respmodal = false;
var sParametros = '';
var sParametrosElimina = '';
var iSignpadConectado = 0;
var banderapreguntarine = 0;
var iCont = 0;
var EXITO = 1;
var digitaliza = 1;
var iOpcion = 1;
var ifirma = 0;
var Signpadcontado = 0;
var ESTA_CONECTADO_SIGNPAD = 1;
var CONSULTA_BIOMETRICO = 12;
var VERIFICACION_EMPLEADO = 13;
var VERIFICACION_TRABAJADOR = 14;
var ENROLAR_TRABAJADOR = 15;
var ELIMINAR_PROCESO = 16;
var LECTURA_TARGETA = 17;
var COMPARACION_BIOMETRICA_BCPL = 18;
var SUBIR_VOLUMEN = 99;
var iFirmaSolicitada = 0;
var iRevision = 0;
var DIGITALIZAR = 0;
var TOMAR_FIRMA = 2;
var NOMBRE_TRAB = 0;
var FIRMA_TRAB = 1;
var FIRMA_PROM = 2;
var iTipoSolCol = 0;
var sRutaPdfTemp = '';
var sTitle = "";
var sMensaje = "";
//var GRABACION = 20;
//Variables
var sTipoSolicitud = '';
var sCurpEnrolado = '';
var mensaje = '';
var mensajenuevo = '';
var iTiempoEspera = 0;
var iBanderaFlujo = 0;
var bBanderaRenapo = false;
var esRegistro = 0;
var banderamodal = 0;
var iFlagVideo = false;				//790
var iFlagNoEcontro = false;			//790
var bResubirVideo = false;

//variables del consumo del servicio de verificacion
var iFolioVerificacion = 0;
var iRespVerificacion = 0;
var sMensajeVerificacion = '';

//variables del consumo del servicio de consulta
var iFolioConsulta = 0;
var iRespConsulta = 0;
var sMensajeConsulta = '';

//variable de retoro
var iRetorno = 0;
var iTrabajadorFinado = 0;
var sRuta = "C:\\SYS\\PROGS\\HETRABAJADOR.EXE";
var iEstatusEnrol = 0;
var sCurpTrabajador = '';

var iContador = 0;
var contador = 0;
var iTipoOperacion = 0;
var iTipoServicio = 0;
var iTipoPersona = 0;

//VARIABLES PARA RECIBIR LOS VALORES DE LA CONSULTA A LA BASE DE DATOS
var sConsultaEstatus = '';
var sConsultaDiagnostico = '';
var sVerificacionEstatus = '';
var sVerificacionDiganostico = '';
var sConsultaExpediente = '';
var sConsultaEnrolamiento = '';
var iSelloVerificacion = 0;
var iRespuestaConsutla = 0;

//VARIABLES DE CODIGOS PARA EJECUTAR UNA ACCION A SEGUIR (SERVICIOS)
var iCodEjecutarContra = '';
var iCodEjecutarEnrol = '';
var iCodCerrarNavegador = '';
var iCodEjecutarVideo = '';

//VARIABLES PARA LOS Checks
var checkTrabSinManos = 0;
var checkTrabManosLesionadas = 0;
var sMotivoSinHuella = '';
var sCodigoHuella = '';
var checkSeleccionado = 0;
var iBanderaEntrada = 0;

var iRetornoActualizacion = 0;
var iEstatusContingente = 0;
var iContadorAlterno = 0;
var iCaminoAlterno = 0;

//AU --> ALtaUnica, bloque de ariables para guardar las direcciones
var sDireccionElegida = '';
var sPrimeraDireccion = '';
var sSegundaDireccion = '';
var iDireccionMostrar = 0;
var iOpcionDireccion = 0;

//AU --> AltaUnica, Variable para guardar toda la informacion del cliente Bancoppel
var aInformacionBancoppel = new Array();
var sFlujoExtraccion = 0;

//AU --> varaible para guardar el Keyx donde se guardo el numero de targeta
var iKeyxtargeta = 0;
var iKeyxinfBancoppel = 0;
var iFlagIdentificacion = 0;
var imagenR = '';
var imagenA = '';
var sEstatusImagen = 0;

//AU --> Bloque de variables para la respuesta de la toma de la huella
var iOperacionBancoppel = 0;
var sCodigoRetorno = '';

//variable para funciones de renapo
var iFolAfore = 0;
var iFolAforeSol = 0;
var bEsRenapo = false;
var bEsRenapoSol = false;
var sNombreRenapo = '';
var sApePatRenapo = '';
var sApeMatRenapo = '';
var sCurpRenapo = '';
var sCurpRenapoSol = '';
var iErrorRenapo = -1;//variable que trae el codigoError de renapo
var iErrorRenapoSol = -1;//variable que trae el codigoError de renapo

var Soltraspaso = 'F';
var sHtml2 = '';
var sHtml = '';

var sCurpSolicitante = "";
var sTipoSolicitante = "";
var sExcepcion = "";

var iTipoAfiliacion = 0;
var iTipoSolicitante = 0;
var iExcepcion = 0;

var iFolioSol = 0;//folio que se obtiene de procesar
var iSolReg = 0;//variable para el tipo de afiliacion

var respExcepcion = 0;

//var nombreClaveProm = "";
var nombrePromotor = "";//nombre del promotor
//datos del trabajador titular
var sNombreTrabajador = "";
var sApellidoPaternoTrab = "";
var sApellidoMaternoTrab = "";
var sCelularTrabajador = "";
//datos del beneficiario
var sNombreBeneficiario = "";
var sApellidoPaternoBenef = "";
var sApellidoMaternoBenef = "";

var valordialogo = 0;//tomo el valor del tipo de dialogo segun el tipo de solicitud
var estatusVideo = "";//estatus del video si se grabo y guardo correctamente

var ligaCase = 'php/constanciaafiliacion.php';
var apellidoPatBenef = '';
var apellidoMatBenef = '';
var nombreBeneficiario = '';

var nombrevideo = '';
var nombreVideoPublica = '';
var estatusFinado = 0;
var claveOPeracion = '';

var intentosGrabado = 0;
var opcionVideoP = 0;
var cFormato = '';
var ipCluster = '';
var intentoPublicaVideo = 0;
var iFolioEnrolamiento = '';
var iEstatusEnrolVideo = -1;
var iRecepcion = 0;

var curpValidaH = '';
var nssValidaH = '';
var respuestaProcesar = 0;
var reenviofolio = false;
var BanderaEstatus = 0;
var banderaContinuarV = 0;
var OSName = "Unknown";

var field = [];
var errorFields = {};
var iRespuestaConsulta = 0;
var numeroEmpleado = 0;
var contadorRespuesta = 0;
var iFolioConstanciaImplicaciones = "";
var aforeCedente = "";
var tipoTraspaso = 0;
var guardarSolConstancia = false;

//Prevalidador
var preval = false;
var folioPrevalidador = 0;
var respuestaPreval = false;
var bPrevalidacionEnCurso = false;
var habilitarCampos = false;
var dataRegistrarPrevalidador = 0;
var folioRespuestaPrevalidador = 0;
var messages = false;
var errorMessages = [];
var afterPrevalidacion = false;
var codigoErrorPrevalidador = "";
var prevalExitosa = false;
var tipoAfiliacion = "";
var valProcesarNssTrabajador = "";
var limpiarVariables = false;
var MensajePromotor = 0;
var sNumTarj = 0;
var sNumTarjC = 0;
var sNumCvc = 0;
var sTipoSol;
var iContadorVideo = 0;
var banderaIne = 0;
var banderaautbancoppel = 0;
var banderaTraspaso = 0;
var numeroConsulta = 1;
var VALIDACION_INE = 100;
var VALIDACION_BANCOPPEL = 101;
var ligaIne = '';
var estatusValIne = 0;
var estatusValBancoppel = 0;
var traspasoIssste = false;
var traspasoNoAfiliado = false;
var datosteline = false;
var datostelbancoppel = false;
var iTiempoMinimo = 0;
var iTiempoMaximo = 0;
var sFecha1TrasPrevio = '';
var sFecha2TrasPrevio = '';
var sFecha3TrasPrevio = '';
var sFecha4TrasPrevio = '';
var sFecha5TrasPrevio = '';

var noExcepcion = true;
var cuentaconAutIne = false;
var cuentaconAutBancoppel = false;
var folioIneVal = 0;
var folioInePrevio = 0;
var iRespuestaRenapoTitular = 0;
var iTelefonoautbancoppel = 0;
var iCompaniabancoppel = 0;

//variables 605
var sMensajeVerExpIden = '';
var bRealizar0407 = false;
var intentosServicio = 0;
var SUBIR_ARCHIVO = 88;
var PUBLICA_HUELLAS_IZQ = 1;
var PUBLICA_HUELLAS_DER = 2;
var RUTA_LOCAL_HUELLAS = 'C:\\TEMP\\huellas';
var RUTA_HUELLAS_ENROLAMIENTO = '/sysx/progs/web/entrada/huellas/enrolamiento/';
var nombreArchivoIzq = "";
var nombreArchivoDer = "";
var iContHuellas = 0;
var enrolamientopermanente = false;
var banderaVideo = 0;

////////////////////////////////////////////
var iFolioConocimientoTraspaso = "";
var diagnosticoprocesarFct = '';
var traeErrorFtc = 0;
var folioSolisitud = 0;
var ContadorIntermitencia = 0;
var contadorMensajeErrorFtc = 0;
var optionButtons = {};
var fechaInicio = new Date(2020, 0, 22);
var fechaActual = new Date();
var diferencia = (fechaActual - fechaInicio);
diferencia = (diferencia / 86400000);

/////////////////  1028.1  /////////////////
var TemplateDerTF = "";
var TemplateIzqTF = "";
var intentosGenerarArchivo = 0;
var bBanderaHuellaMovil = false;
////////////////////////////////////////////

//Variables 539
var bandCapturaDatos = 0;
var curpCapturaDatos ="";
var cuentaHabienteBancoppel = [];
var banderabancoppel539 = false;
var Bine = false;
var Gcuentahabiente =[];
var cActualiza = 0;				   

// Autenticacion dactilar
var bCservicio0101 = false;	// ejecutaWebService instante
var bCservicio0407 = false;	// ejecutaWebService instante
//var iRespuesta0101 = 0; //Para saber si esta disponible el Servicio 0101
var bFnormal = false; //Habilitar solamente cuando entre por validacion por INE/BANCOPPEL
var bSinExpe = false; //Habilitar solamente cuando entre por validacion por dactilar E.I y E.E
var bConExpe = false; //Habilitar solamente cuando entre por validacion por E.E y no por E.E
var GESTOR_DE_SELLOS = 22; // para entrar al switch del servio y obtener la resp
var iflag = 0; // variable para mandarlo en la funcion
var iFlagGestor = 0; // Flag para encender/apagar el Servicio del gestor
var bGestorexito = false; // Se valida que se halla autenticado 48 dactilar
var iFolioGestor = 0; // variable solo para transpaso, el cual consiste en obtener un folio de solicitud y mandarle el parametro al gestor de sellos
var iAutdactilar = 1; // Bandera para detonar un update a la tabla solconstancia al campo validacion INE a 3, por el tipo de transpaso 832,833
//variables para la funcion Autenticadactilar
var iSelloBiometrico = 0;
var sRespBiometrico = '';
var iBanderaBiometrico = 0;
var iResSellos = false; // Variable retorno para ver si es con exito la respuesta Autenticadactilar
var bDetenerProceso = true; // variable para detener el proceso
var bContinuarProceso = true; // bandera para no entrar en la consulta para obtener E.I y E.E
var bPrevalNoExitoso = false; // para en casi si el prevalidador no es con exito
var iOpcionGestor = 0; //
var iOpcionSinExp = 1;
var ArrCodigoErrorPrevalidador = ['188','E70','E03','E04','494','C46','P19','P18','P17','P14','P13','P01','P02','P03','P04','P05','P12','P07','P08','P10','P11','P06','P09'];
var icontador0101 = 0;
var arrConsumo = [];

$(document).ready(function () {

	sistemaOperativo();
	$('.content-loader').hide();

	$.ajaxSetup({
		async: false,
		cache: false,
		url: ligaCase,
		type: "POST",
		dataType: "json",
		error: function (a, b, c) {
			switch (a.status) {
				case 404:
					marcarLog("[404]: Pagina no Encontrada");
					break;
				case 401:
					mostrarMensajeCierreSesion();
					break;
				case 409:
					marcarLog("[409]:" + a.responseText);
					break;
				default:
					marcarLog("[default]:" + a + b + c);
					break;
			}
		}
	});


	if (OSName == "Android") {
		if (getQueryVariable("tokenAfore")) {
			sesion(getQueryVariable("tokenAfore"));
		}
		iEmpleado = getQueryVariable('empleado');
	} else {
		iEmpleado = getQueryVariable("empleado");
	}

	function sesion(token) {
		$.ajax({
			data: { opcion: 613, token: token },
			success: function () {
				return true;
			}
		});

		return false;
	}

	//Funcion para saber si se encuentra conectada una signpad
	iOpcion = ESTA_CONECTADO_SIGNPAD;
	opcionejecuta("");

	document.onmousedown = anularBotonDerecho;

	document.querySelector('html').onmouseover = function () {
		if (iCodEjecutarVideo == 'VID') {
			$('.content-loader').show();
			iCodEjecutarVideo = '';
			consultaEstatusenrol();
		}
	};

	//Instruccion que permite el no copiar y pegar con el mause
	document.oncontextmenu = function () { return false }

	//FUNCION PARA IMPEDIR QUE VEAN LA URL
	$(document).keydown
		(
			function (e) {
				var shift, ctrl, alt;
				// Mozilla(Firefox, NN) and Opera
				if (e != null) {
					keycode = e.which;
					ctrl = typeof e.modifiers == 'undefined' ? e.ctrlKey : e.modifiers & Event.CONTROL_MASK;
					shift = typeof e.modifiers == 'undefined' ? e.shiftKey : e.modifiers & Event.SHIFT_MASK;
					alt = typeof e.modifiers == 'undefined' ? e.altKey : e.modifiers & Event.ALT_MASK;
					// Internet Explorer
				} else {
					keycode = e.keyCode;
					ctrl = e.ctrlKey;
					shift = e.shiftKey;
					alt = e.altKey;
				}
				if (ctrl || shift || alt) {
					if ((((keycode >= 65 && keycode <= 90) && !shift) || ((keycode >= 97 && keycode <= 122) && shift)) && (!ctrl && !alt))
						return true;
					else {// Mozilla(Firefox, NN) and Opera
						if (e != null) {
							e.which = 0;
							e.preventDefault();
							e.stopPropagation();
							e.keyCode = 0;
							e.returnValue = false;
							e.cancelBubble = true;
							// Internet Explorer
						} else {
							event.keyCode = 0;
							event.returnValue = false;
							event.cancelBubble = true;
						}
						return false;
					}
				}
				return true;
			}
		);
	//obtiene la fecha del servidor
	sFechaAct = obtenerfechaservidor();

	//Crea el objeto de tipo calendario
	$("#dtpFechaComprobante").datepicker({
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		//Formato de la fecha.
		dateFormat: 'yy-mm-dd',
		//Nombre corto de los dias.
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		//Se establece la fecha maxima que se podra seleccionar.
		maxDate: sFechaAct, //$("#dtpFechaComprobante").val(),
		//evento que se desencadena cuando se cierra el control datetime
		onClose: function (sFechaAct) {

		}
	});


	//$('#btnConsultaCurp,#AltaUnica').button();
	$("#txtCurp,#txtCurp2").keypress(function (event) { return validarKeypressCurp(event); });
	$("#dtpFechaComprobante").keypress(function (event) { event.preventDefault(); });
	//Desabilita botones que no le permitiran hacer la captura ni la ejecucion de la digitalizacion
	$('#bAceptar').attr('disabled', true);
	document.getElementById("bAceptar").classList.add("deshabilitarImg");
	$('#btnConsultaCurp').attr('disabled', false);
	document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");
	/* Folio 539 $('#AltaUnica').attr('disabled', true);
	document.getElementById("AltaUnica").classList.add("deshabilitarBtn");*/
	$('#btnConsultaCurp').attr('disabled', true);
	document.getElementById("btnConsultaCurp").classList.add("deshabilitarBtn");
	$('#bGrabacion').attr('disabled', true);
	document.getElementById("bGrabacion").classList.add("deshabilitarImg");
	$('#bContinuar').attr('disabled', true);
	document.getElementById("bContinuar").classList.add("deshabilitarImg");
	$('#bDigitalizar').attr('disabled', true);
	document.getElementById("bDigitalizar").classList.add("deshabilitarImg");
	$('#bRenapoTitular').attr('disabled', true);
	document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
	$('#bRenapoSol').attr('disabled', true);
	document.getElementById("bRenapoSol").classList.add("deshabilitarImg");
	$('#bCancelarModulo').attr('disabled', false);
	document.getElementById("bCancelarModulo").classList.add("habilitarImg");
	$('#txtClienteCoppel').attr('disabled', true);
	$('#dtpFechaComprobante').attr('disabled', true);
	$('#txtCurp2').attr('disabled', true);
	$('#txtCurp').attr('disabled', true);
	$('#cboTipoSolicitante').attr('disabled', true);
	$('#cboExcepciones').attr('disabled', true);
	$('#cboTipoRecepcion').attr('disabled', true);
	//Guarda el valor que retorna el metodo validarenrolamiento
	iEstatusEnrol = validarenrolamiento(iEmpleado);
	//iEstatusEnrol = 1;
	//Guarda la curp del empleado que inicio sesion
	sCurpEnrolado = obtenerCurp(iEmpleado);
	//Ocultar parametros de URL
	var stateObject = {};
	var title = " "; //Solicitud de Constancias Registro / Traspaso
	var newUrl = "indexConstanciasafiliacion.html";
	//history.replaceState(stateObject,title,newUrl);
	//carga los datos del promotor
	obtenerdatospromotor(iEmpleado);
	//funcion que permite cargar el combo con los diferentes comprobantes de domicilio
	cargacatalogocomp();
	//funcion que permite cargar el combo con los diferentes tipo de solicitante
	cargacatalogoSol();
	//Funcion que permite cargar en la pagina en el combo de compañias, las diferentes compañias de telefonos celulares
	cargacatalogocompcelulares();
	//Funcion que permite cargar en la pagina en el conbo lugar de solicitud, los diferentes lugares donde se puede realizar esta.
	cargacatalogoLugarSolicitud();
	//Bloqueo de los campos que no se ocuparan antes de la vericacion
	boqueoCajasdeTexto();
	//Funcion que permite cargar en la pagina en el combo de estadocivil,
	cargacatalogoestadocivil();
	//Funcion que permite cargar en la pagina en el combo de excepciones,
	cargaExcepciones();
	//al dar clic sobre el boton aceptar invocara un metodo
	$('#btnConsultaCurp').click(function () {
		noExcepcion = $("#cboExcepciones").val() == 26 ? true : false;
		banderaIne = 0;
		banderaautbancoppel = 0;

		if(JSON.stringify(Gcuentahabiente) == '[]')
		{
			
		}
		else
		{
			$("#txTelCelular").attr("disabled", false);
			$("#cboCompTelef").attr("disabled", false);
			$("#cboTipoComprobante").attr("disabled", false);
			$("#cboEstadoCivil").attr("disabled", false);
			$("#cboTipoTelefono").attr("disabled", false);
			$("#txtTelContacto").attr("disabled", false);
			$("#cboCompTelefCont").attr("disabled", false);
			$("#txtClienteCoppel").attr("disabled", false);
		}
		if (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) {
			banderaTraspaso = 0;
			//Se valida que el tipo solicitante sea diferente a Titular
			if ($('#cboTipoSolicitante').val() != '1') {
				tieneautenticacionbancoppel();
				fntieneautenticacionine();
				if (noExcepcion) {
					if (!datosteline || !datostelbancoppel) {
						banderaIne = 1;
						banderabancoppel = 1;
						if ($('#txtCurp2').val() != $('#txtCurp').val()) {
							preguntaconsulta();
						}
					}
				}
				else {
					cancelacionRegistroTerceros("Promotor, para poder realizar la afiliación se requiere que el solicitante no cuente con excepciones en sus manos.");
					return;
				}
			}
		}
		else {
			if (bContinuarProceso){ //Proceso para dener el gestor de sellos o continuar
			
				fnConsultaServicio0101(); // Preguntar si tiene E.I y E.E
			}
		}

		setTimeout(() => {
			validarPrevio();
		}, 100);

		function validarPrevio() {
			//Se obtiene la curp de la caja de texto y el tipo de la solicitud
			sCurpTrabajador = $('#txtCurp').val();
			sCurpSolicitante = $('#txtCurp2').val();
			sTipoSolicitud = $('#cboTipoConstancia').val();
			sTipoSolicitante = $('#cboTipoSolicitante').val();
			var sTipoConstancia = $('#cboTipoConstancia').val();
			tipoAfiliacion = parseInt(sTipoConstancia);

			if (sTipoConstancia != -1) {
				validacurp(iEmpleado);
			}
			else {
				respuestaPreval = true;
				habilitarCampos = true;
				var title = " ";
				var mensaje = "Promotor debe seleccionar el tipo de afiliación";
				mostrarMensaje(mensaje, title);
			}
		}
	});

	$('#cboTipoConstancia').change(function () {
		boqueoCajasdeTexto();

		if ($('#cboTipoConstancia').val() != -1) {
			//$('#txtCurp').attr("disabled", false);
		}
		else {
			$('#txtCurp').attr("disabled", true);
		}


		document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
		document.getElementById("bRenapoSol").classList.add("deshabilitarImg");

		if ($('#cboTipoConstancia').val() == 1 || $('#cboTipoConstancia').val() == 27 || $('#cboTipoConstancia').val() == 527) {
			banderaRenapoTitular = true;
			$('#bRenapoTitular').attr('disabled', false);
			$("#cboTipoSolicitante").val(1).trigger("change");
			$("#cboTipoSolicitante").attr("disabled", true);

			if ($('#cboTipoConstancia').val() == 1 || $('#cboTipoConstancia').val() == 527) {
				$("#txtNss").val("");
				$("#txtNss").attr("disabled", true);
				$("#txtNombre").attr("disabled", false);
				$("#txtApellidoMatero").attr("disabled", false);
				$("#txtApellidoPaterno").attr("disabled", false);
			}
			else {
				$("#txtNss").attr("disabled", false);
				$("#txtNombre").attr("disabled", false);
				$("#txtApellidoMatero").attr("disabled", false);
				$("#txtApellidoPaterno").attr("disabled", false);
			}

			if (OSName == 'Android') {
				$('#bRenapoTitular').attr('disabled', false);
				$('#txtCurp').attr('disabled', true);
			}
		}
		else if ($('#cboTipoConstancia').val() == 26 || $('#cboTipoConstancia').val() == 33) {
			banderaRenapoTitular = true;
			$('#bRenapoTitular').attr('disabled', true);
			if ($('#cboTipoConstancia').val() == 26) {
				if (esRegistro == 1) {
					if (OSName != "Android") {
						$("#cboExcepciones").val(-1);
					}
					$("#txtNss").attr("disabled", true);
					$("#txtNombre").attr("disabled", true);
					$("#cboExcepciones").attr("disabled", true);
					$("#txtApellidoMatero").attr("disabled", true);
					$("#txtApellidoPaterno").attr("disabled", true);
					esRegistro = 0;
				}
				else {
					$("#txtNombre").val("");
					if (OSName != "Android") {
						$("#cboExcepciones").val(-1);
					}
					$("#txtApellidoMatero").val("");
					$("#txtApellidoPaterno").val("");
					$("#txtNss").attr("disabled", true);
					$("#txtNombre").attr("disabled", true);
					$("#cboExcepciones").attr("disabled", true);
					$("#txtApellidoMatero").attr("disabled", true);
					$("#txtApellidoPaterno").attr("disabled", true);
				}
			}
			else if ($('#cboTipoConstancia').val() == 33) {
				/*$("#cboTipoSolicitante").val(1).trigger("change");
				$("#cboTipoSolicitante").attr("disabled", true);*/
				$("#txtNss").val("");
				if (OSName != "Android") {
					$("#cboExcepciones").val(-1);
					$("#cboExcepciones").attr("disabled", false);
				}
				$("#txtNss").attr("disabled", true);
				$("#txtNombre").attr("disabled", true);
				$("#cboExcepciones").attr("disabled", true);
				$("#txtApellidoMatero").attr("disabled", true);
				$("#txtApellidoPaterno").attr("disabled", true);
			}
		}
		else {
			$('#bRenapoTitular').attr('disabled', false);
		}
	});

	$("#cboTipoComprobante").change(function () {
		//funcion que valida los campos correspondientes segun el tipo de comprobante elejido
		validatipocomprobante();
	});

	//Nuevo boton que realizara la Alta Unica
	/* Folio 539
	$('#AltaUnica').click(function () {
		sFlujoExtraccion = 0;

		sTipoSolicitud = $('#cboTipoConstancia').val();

		//Se inicializa variable para cuando no sea la targeta correcta esta variable permite que vuelva apedir la targeta
		iRetorno = 0;
		$('.content-loader').hide();
		sMensaje = "Promotor: Favor de verificar que el Trabajador sea cliente BanCoppel y que cuente con su tarjeta activa a la mano";
		mensajesnuevoproceso(sMensaje);
	});*/


	$('#bDigitalizar').click(function () {
		$('#bDigitalizar').attr("disabled", true);
		document.getElementById("bDigitalizar").classList.add("deshabilitarImg");
	});

	//Convierte el texto que este en la caja del control de minuscula a mayuscula
	$('#txtApellidoPaterno,#txtApellidoMatero,#txtNombre').bind('change', aMayusculas);

	$('#txtCurp,#txtCurp2,#txtCalle,#txtNumExt,#txtNumInt').bind('change', aMayusculas);

	$("#sTipoComprobante").change(function () {
		$('#dtpFechaComprobante').val('');
		$('#txtClienteCoppel').val('');
	});

	if (iEstatusEnrol == 2 || iEstatusEnrol == -1 || iEstatusEnrol == 0) {
		sMensaje = "Promotor: No tienes un enrolamiento aceptado y no debes realizar solicitudes.\nConsulta el Estatus de tu Enrolamiento";
		//Ejecutar Metodo Cerrar Navegador
		iCodCerrarNavegador = 'EMCN';
		mostrarMensaje(sMensaje, '');

	}

	$("#capturaVideo").dialog
		({
			//title: 'Captura de Video',
			autoOpen: false,
			resizable: false,
			closeText: "",
			width: 1200,
			height: 780,
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			open: function (event, ui) { $(".ui-dialog-titlebar-close").show(); },			//790		
			beforeClose: function () {														//790
				$('.content-loader').show();
				opcionVideoP = 5;
				opcionejecutaPublicaVideo();

				if (iFlagVideo) {
					//$(this).dialog("close");
					//$('.content-loader').show();
					var sHtml = '<iframe src=\" "></iframe>';
					$("#capturaVideo").empty();
					$("#capturaVideo").html(sHtml);
					$("#capturaVideo").hide();
					$(".ui-dialog-titlebar").hide();
					//$(this).dialog("close");

					//obtenerestatusVideo();
					intentosGrabado++;
					opcionVideoP = 2;
					opcionejecutaPublicaVideo();
					desbloquearModal();

				} else if (iFlagNoEcontro) {
					desbloquearModal();
					var mensajeCancelar = 'Promotor, ¿Desea cancelar la grabación?';
					MuestraDialogoGrabacion(mensajeCancelar);
				}


			},
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro

		});

	obtenerIpCluster();
	obtenerClaveOperacion();

});

function detenerFlujo() {
	sTitle = "";
	sMensaje = "Promotor, para poder realizar la afiliación se requiere que el solicitante no cuente con excepciones en sus manos.";
	mostrarMensaje(sMensaje, sTitle);


	$('#bRenapoTitular').attr('disabled', true);
	document.getElementById("bRenapoTitular").classList.remove("deshabilitarImg");
	$('#txtCurp').attr('disabled', true);

	$('#bRenapoSol').attr('disabled', true);
	document.getElementById("bRenapoSol").classList.remove("deshabilitarImg");
	$('#txtCurp2').attr('disabled', true);


	$('#btnConsultaCurp').attr('disabled', true);
	$('#cboTipoRecepcion').attr('disabled', true);
	$("#txtCurp").val('');
	$("#txtCurp2").val('');
}

function validaExcepciones(value) {
	deshabilitaCamposTercerasFiguras(3);
	if (value != "-1") {
		iTipoExcepcion = $("#cboExcepciones").val();
		if (
			(iTipoSolicitante == 2 ||
				iTipoSolicitante == 3 ||
				iTipoSolicitante == 4) &&
			(iTipoExcepcion == 27 || iTipoExcepcion == 33)
		) {
			detenerFlujo();
		} else {
			banderaRenapoTitular = true;
			banderaRenapoSolicitante = true;

			$("#bRenapoTitular").attr("disabled", false);
			document.getElementById("bRenapoTitular").classList.add("habilitarImg");
			if (bTerceraFigura) {
				$("#txtCurp2").attr("disabled", true);
			} else {
				$("#txtCurp2").attr("disabled", false);
			}

			$("#cboTipoRecepcion").attr("disabled", false);

			if (value == 26) {
				iTrabajadorFinado = 0;
				checkTrabSinManos = 0;
				checkTrabManosLesionadas = 0;

				if (Soltraspaso == "T") {
					noExcepcion = true;
				}
			} else if (value == 27) {
				checkTrabSinManos = 2;
				noExcepcion = false;
			} else if (value == 33) {
				checkTrabManosLesionadas = 2;
				noExcepcion = false;
			}
		}
	} else {
		$("#txtCurp2").attr("disabled", true);

		banderaRenapoTitular = false;
		banderaRenapoSolicitante = false;

		$("#bAceptar").attr("disabled", true);
		document.getElementById("bAceptar").classList.add("deshabilitarImg");
		$("#bRenapoSol").attr("disabled", true);
		document.getElementById("bRenapoSol").classList.add("deshabilitarImg");
		$("#cboTipoRecepcion").val(-1);
		$("#cboTipoRecepcion").attr("disabled", true);

		if (bTerceraFigura) {
			deshabilitaCamposTercerasFiguras(3);
		}
	}
}


function validaTipoSol(value) {
	deshabilitaCamposTercerasFiguras(1);
	if (value != "-1") {
		iSolReg = value;
		if (value === "27" || value === "1" || value === "527") {
			Soltraspaso = 'T';
			cargacatalogoSol();
			iSolReg = '27';
			if (value === "27") {
				iTipoAfiliacion = 27;
			}
			else if (value === "1") {
				iTipoAfiliacion = 1;
				traspasoIssste = true;
				traspasoNoAfiliado = false;
			}
			else if (value === "527") {
				iTipoAfiliacion = 527;
				traspasoNoAfiliado = true;
				traspasoIssste = false;
			}
		}
		else {
			Soltraspaso = 'F';
			cargacatalogoSol();
			if (value === "26") {
				iTipoAfiliacion = 26;
			}
			else if (value === "33") {
				iTipoAfiliacion = 33;
			}
		}
		$('#cboTipoSolicitante').attr('disabled', false);

		if (OSName == "Android") {
			$('#cboTipoRecepcion').attr('disabled', false);
		}
	}
	else {
		iTipoAfiliacion = -1;
		$('#cboTipoSolicitante').attr('disabled', true);
		$('#cboTipoSolicitante').val(-1);
		$('#cboTipoRecepcion').val(-1);
		$('#cboExcepciones').attr('disabled', true);
		$('#cboExcepciones').val(-1);
		$('#txtCurp').attr('disabled', true);
		$('#txtCurp2').attr('disabled', true);
		/*539
		$('#AltaUnica').attr('disabled', true);
		$('#AltaUnica').attr('disabled', true);
		document.getElementById("AltaUnica").classList.add("deshabilitarBtn");*/
		document.getElementById("btnConsultaCurp").classList.add("deshabilitarBtn");
		$('#bAceptar').attr('disabled', true);
		document.getElementById("bAceptar").classList.add("deshabilitarImg");
		$('#bRenapoTitular').attr('disabled', true);
		document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
		$('#bRenapoSol').attr('disabled', true);
		document.getElementById("bRenapoSol").classList.add("deshabilitarImg");
		$('#cboTipoRecepcion').attr('disabled', true);
	}
}

function activaCampoExcep(value) {
	deshabilitaCamposTercerasFiguras(2);
	if (value != "-1") {
		if (OSName != "Android") {
			$('#cboExcepciones').attr('disabled', false);
		}
		else {
			$('#bRenapoTitular').attr('disabled', false);
		}

		$('#cboTipoRecepcion').attr('disabled', false);
		if (value == "2") {
			iTipoSolicitante = value;
			//validaSolictante();
		}

		/* Folio 539
		$('#AltaUnica').attr('disabled', false);
		// document.getElementById("AltaUnica").classList.add("habilitarBtn");
		// document.getElementById("AltaUnica").classlist.remove("deshabilitarBtn");

		$('#AltaUnica').addClass("habilitarBtn");
		$('#AltaUnica').removeClass("deshabilitarBtn");
		$('#AltaUnica').removeClass("deshabilitarImg"); */

		//INICIO 893.1
		if (value == "2" || value == "3" || value == "4") {
			bTerceraFigura = true;
		}
		else {
			bTerceraFigura = false;
		}
		//FIN

		if (value == "1") {
			$("#bRenapoSol").css('visibility', 'hidden');
			$("#txtCurp2").css('visibility', 'hidden');
			iTipoSolicitante = value;
			//document.getElementById("tdCampoCurpSol").style.visibility="hidden";
		}
		else {
			$("#bRenapoSol").css('visibility', 'visible');
			$("#txtCurp2").css('visibility', 'visible');
			iTipoSolicitante = value;
		}

		if (OSName == "Android") {
			validaExcepciones("26");
		}

	}
	else {
		if (OSName != "Android") {
			$('#cboExcepciones').attr('disabled', true);
			$('#cboExcepciones').val(-1);
		}

		/* Folio 539
		$('#AltaUnica').attr('disabled', true);


		$('#AltaUnica').addClass("deshabilitarbtn");
		$('#AltaUnica').addClass("deshabilitarImg");
		$('#AltaUnica').removeClass("habilitarBtn");*/

		$('#cboTipoRecepcion').val(-1);
		$("#bRenapoSol").css('visibility', 'visible');
		$("#txtCurp2").css('visibility', 'visible');
		$('#cboExcepciones').val(-1);
		$('#cboTipoRecepcion').val(-1);
		$('#txtCurp').attr('disabled', true);
		$('#txtCurp2').attr('disabled', true);
		/*Folio 539
		$('#AltaUnica').attr('disabled', true);
		document.getElementById("AltaUnica").classList.add("deshabilitarBtn");*/
		$('#btnConsultaCurp').attr('disabled', true);
		document.getElementById("btnConsultaCurp").classList.add("deshabilitarBtn");
		$('#bAceptar').attr('disabled', true);
		document.getElementById("bAceptar").classList.add("deshabilitarImg");
		$('#bRenapoTitular').attr('disabled', true);
		document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
		$('#bRenapoSol').attr('disabled', true);
		document.getElementById("bRenapoSol").classList.add("deshabilitarImg");
		$('#cboTipoRecepcion').attr('disabled', true);
	}
}

function valorRecepcion(value) {
	iRecepcion = value;
}

function renapo() {
	if (banderaRenapoTitular) {
		//activarCampos();
		deshabilitaCampos();
		consultarRenapo(iEmpleado);
	}
}

function renapoSol() {
	if (banderaRenapoSolicitante) {
		//activarCampos();
		deshabilitaCampos();
		consultarRenapoSol(iEmpleado);
	}
}


//Al dar clic sobre el boton aceptar invocara un metodo
function aceptar() {
	//funcion que manda llamar una secuencia de validaciones previos ala insercion del registro
	preval = false;
	if (iTipoSolicitante != '1') {
		if (sNombreBeneficiario != '' & sApellidoPaternoBenef != '') {

			//Folio 891 Llamada a guardar datos para el acuse--listo
			//guardarDatosCartaDerechos();

			ObtenerDatos();
		}
		else {
			mensaje = 'Promotor, los datos del Solicitante no están capturados correctamente';
			MuestraDialogoSolicitante(mensaje);
		}
	}
	else {
		//Folio 891 Llamada a guardar datos para el acuse--listo
		//guardarDatosCartaDerechos();

		ObtenerDatos();
	}
};

function continuar() {
	/*mensaje='&nbsp &nbsp &nbsp Continua con la Captura de Afiliación';
	opcionContinuar(mensaje);*/

	var curp = $('#txtCurp').val();
	var nss = $('#txtNss').val();

	iFolioSol = obtenerRecuperacionFolioConstancia(curp);

	//alert(iFolioSol);//Descomentar PruebaStatusVideo
	if(!validaEstatusVideo(iFolioSol)){
		bResubirVideo = true;
		intentoPublicaVideo = 0;
		intentosGrabado = 0;
		opcionVideoP = 2;
		ipCluster = obtenerIpCluster();
		folio = iFolioSol;
		opcionejecutaPublicaVideo();
	}

	//Folio 891 Llamada a guardar datos para el acuse--listo
	guardarDatosCartaDerechos();

	actualizarSolEstatusExpiden(iFolioSol, curp);

	if (estatusValIne == 0) {
		validarExpediente(curp, nss);
	}
	else {
		if (bTerceraFigura) {
			capturarCodigoAutenticacion();
		}
		else
			validarExpediente(curp, nss);
	}
};

function validaEstatusVideo(iFolioSol){
	datos ={opcion: 95, folioafiliacion: iFolioSol};
	var bEstatusV = false;
	$.ajax({
		async: false,
		cache: false,
		data: datos,
		url: 'php/constanciaafiliacion.php',
		type: 'post',
		dataType: 'json',
		success: function (data) {
			if (data.resultado == '1')
				bEstatusV = true;
			else
				nombreVideoPublica = data.resultado;
		},
		error: function (a, b, c) {
		}
	});
	return bEstatusV;
}

function digitalizar() {

	$('#bAceptar').attr('disabled', true);
	document.getElementById("bAceptar").classList.add("deshabilitarImg");

	$('#bDigitalizar').attr('disabled', true);
	document.getElementById("bDigitalizar").classList.add("deshabilitarImg");

	//Se agrega el llamado al comando system para eliminar las instancias de la aplicacion DIGIDOCTOSCLIENTE.EXE
	iOpcion = ELIMINAR_PROCESO;
	sParametrosElimina = "";
	sParametrosElimina = "/F /IM DIGIDOCTOSCLIENTE.EXE";
	opcionejecuta(sParametrosElimina);

};

function deshabilitaCamposTercerasFiguras(opcion) {
	switch (opcion) {
		case 1: //Cambia Tipo Afiliacion
			$('#cboTipoSolicitante').attr('disabled', true);
			$('#cboTipoSolicitante').val(-1);
		//break;
		case 2: //Cambia Tipo Solicitante
			$('#cboExcepciones').attr('disabled', true);
			$('#cboExcepciones').val(-1);
		//break;
		case 3: //Cambia Excepciones
			$('#bRenapoTitular').attr('disabled', true);
			document.getElementById("bRenapoTitular").classList.remove("deshabilitarImg");
			$('#txtCurp').attr('disabled', true);
			$('#txtCurp').val('');

			$('#bRenapoSol').attr('disabled', true);
			document.getElementById("bRenapoSol").classList.remove("deshabilitarImg");
			$('#txtCurp2').attr('disabled', true);
			$('#txtCurp2').val('');

			$('#btnConsultaCurp').attr('disabled', true);
			break;
	}
}

function boqueoCajasdeTexto() {
	$('#txtCurp').attr('disabled', true);
	$('#txtApellidoPaterno').attr('disabled', true);
	$('#txtApellidoMatero').attr('disabled', true);
	$('#txtNombre').attr('disabled', true);
	$('#txtNss').attr('disabled', true);
	$('#txTelCelular').attr('disabled', true);
	$('#cboCompTelef').attr('disabled', true);
	$('#cboTipoComprobante').attr('disabled', true);
	$('#dtpFechaComprobante').attr('disabled', true);
	$('#txtClienteCoppel').attr('disabled', true);
	$('#cboTipoTelefono').attr('disabled', true);
	$('#txtTelContacto').attr('disabled', true);
	$('#cboCompTelefCont').attr('disabled', true);
	$('#cboEstadoCivil').attr('disabled', true);
	/*Folio 539
	$('#AltaUnica').attr('disabled', true);
	document.getElementById("AltaUnica").classList.add("deshabilitarImg");*/
}

function desboqueoCajasdeTexto() {
	//si la respuesta de renapo fue 0 (implica que los datos vienen llenos)
	if (iErrorRenapo == 0) {
		//$('#txtCurp').attr('disabled', true);
		$("#txtApellidoPaterno").attr('disabled', true);
		$("#txtApellidoMatero").attr('disabled', true);
		$("#txtNombre").attr('disabled', true);
	}
	else {
		$('#txtApellidoPaterno').attr('disabled', false);
		$('#txtApellidoMatero').attr('disabled', false);
		$('#txtNombre').attr('disabled', false);
		//$('#txtCurp').attr('disabled', true);
	}
	$('#dtpFechaComprobante').attr('disabled', true);

	if (bRealizar0407 == false && BanderaEstatus != 7 && BanderaEstatus != 8) {
		$('#bAceptar').attr('disabled', false);
		document.getElementById("bAceptar").classList.add("habilitarImg");
	} else if (BanderaEstatus == 7 || BanderaEstatus == 8) {
		$('#bGrabacion').attr('disabled', false);
		document.getElementById("bGrabacion").classList.add("habilitarImg");
	}

	if (BanderaEstatus != 7 && BanderaEstatus != 8) {
		$('#txtNss').attr('disabled', false);
		$('#txTelCelular').attr('disabled', false);
		$('#cboCompTelef').attr('disabled', false);
		$('#cboTipoComprobante').attr('disabled', false);
		$('#txtClienteCoppel').attr('disabled', false);
		$('#cboTipoTelefono').attr('disabled', false);
		$('#txtTelContacto').attr('disabled', false);
		$('#cboCompTelefCont').attr('disabled', false);
		$('#txtCalle').attr('disabled', false);
		$('#txtNumExt').attr('disabled', false);
		$('#txtNumInt').attr('disabled', false);
		$('#txtCodPostal').attr('disabled', false);
		$('#txtdesColonia').attr('disabled', false);
		$('#txtdesMunicipio').attr('disabled', false);
		$('#txtdesEstado').attr('disabled', false);
		$('#btnBuscarCP').attr('disabled', false);
		$('#txtApellidoPaterno').focus();
		//Se agrego para el capturaafiliacion
		$('#cboEstadoCivil').attr('disabled', false);
	}
	if (datosteline) {
		$('#txTelCelular').attr('disabled', true);
		$('#cboCompTelef').attr('disabled', true);
		$('#cboTipoComprobante').attr('disabled', false);
		$('#cboEstadoCivil').attr('disabled', false);
	}
	else {
		if (BanderaEstatus != 7 && BanderaEstatus != 8) {
			$('#txTelCelular').attr('disabled', false);
			$('#cboCompTelef').attr('disabled', false);
		}
	}

	if (banderabancoppel == 1) {
		$('#cboTipoComprobante').attr('disabled', false);
		$('#cboEstadoCivil').attr('disabled', false);
	}

	if ($('#cboTipoConstancia').val() == 33)
		$('#txtNss').attr('disabled', true);
}

//Funcion que se ejecuta cuando se da clic sobre el boton aceptar
//Invoca una seria de eventos.
function ObtenerDatos() {
	if (ValidaCamposObligatorios()) {
		if (validalogitudtelefonos()) {
			if (validarListaNegraTelefonoCel()) {
				if (validaTelefonoCelular())//te regresa true o 1
				{
					AlmacenarDatosFormulario();
				}
			}
		}
	}
}

//funcion que permite validar el telefono del trabajador y contacto
function validalogitudtelefonos() {
	var iContacto = 0;
	var iTrabajador = 0;
	var band = false;

	var sNumeroCel = $('#txTelCelular').val();
	var sCompaniaCel = $('#cboCompTelef').val();

	var sTipoTelefono = $('#cboCompTelef').val();
	var sTelContacto = $('#txtTelContacto').val();
	var sCompTelCon = $('#cboCompTelefCont').val();

	if (sNumeroCel != '') {
		if (sNumeroCel.length == 10) {
			if (sCompaniaCel != -1) {
				iTrabajador = 1;
			}
			else {
				sTitle = "";
				sMensaje = "Promotor: Por favor seleccione compañia del telefono del trabajador";
				mostrarMensaje(sMensaje, sTitle);
			}
		}
		else {
			sTitle = "";
			sMensaje = "Promotor: La longitud del número de teléfono del trabajador no es valida";
			mostrarMensaje(sMensaje, sTitle);
			$('#txTelCelular').focus();
		}
	}
	else {
		sTitle = "";
		sMensaje = "El campo Número de celular es obligatorio, Favor de proporcionar uno.";
		mostrarMensaje(sMensaje, sTitle);
		$('#txTelCelular').focus();
	}


	//si se valido bien el numero del trabajador, se valida el del contacto, si no. no tiene caso validarlo
	if (iTrabajador == 1) {
		if (sTelContacto != '') //valida que el campo telefono de contacto venga diferente de vacio
		{
			if (sTelContacto.length == 10) {
				if (sCompTelCon != -1) {
					iContacto = 1;
				}
				else {
					sTitle = "";
					sMensaje = "Promotor: Por favor seleccione compañia del telefono de contacto.";
					mostrarMensaje(sMensaje, sTitle);
				}
			}
			else {
				sTitle = "";
				sMensaje = "Promotor: La longitud del número de teléfono de contacto no es valida";
				mostrarMensaje(sMensaje, sTitle);
				$('#txtTelContacto').focus();
			}
		}
		else {
			iContacto = 1;
			$('#cboCompTelefCont').val('-1');
		}
	}

	//si el telefono del trabajador y contacto es valido regresa true, caso contrario false
	if (iTrabajador == 1 && iContacto == 1) {
		band = true;
	}


	return band;

}

//Funcion que permite validar que se cumplan los campos obligatorios,
//En  caso de que esten todos los campos obligatorios retornara true
//Caso de que falte alguno retornara false
function ValidaCamposObligatorios() {
	var band = false;
	//se valida el primer bloque de los datos del Trabajador
	//obtiene los valores de las cajas de texto, para verificar que tenga datos
	//Bloque de codigo de los Datos del Trabajador
	var sNSS = $('#txtNss').val();
	var sCurp = $('#txtCurp').val();
	var sAppaterno = $('#txtApellidoPaterno').val();
	var sNombre = $('#txtNombre').val();
	var sNumeroCel = $('#txTelCelular').val();
	var sCompaniaCel = $('#cboCompTelef').val();
	var sTipoComprobante = $('#cboTipoComprobante').val();
	var sFechaComp = $('#dtpFechaComprobante').val();
	var sClieCoppel = $('#txtClienteCoppel').val();
	//Boque de Datos del Domicilio
	var sCalle = $('#txtCalle').val();
	var sNumExt = $('#txtNumExt').val();
	var sCP = $('#txtCodPostal').val();
	var sColia = $('#txtColonia').val();
	var sDelMun = $('#txtDelMun').val();
	var sEstado = $('#txtEstado').val();
	var sPais = $('#txtPais').val();
	var iestadoCivil = $('#cboEstadoCivil').val();
	var sTipoConstancia = $('#cboTipoConstancia').val();
	var iRecepcion = $("#cboTipoRecepcion").val();
	var excepciones = $("#cboExcepciones").val();

	if (sCurp.length > 0) {
		if (sAppaterno.length > 0) {
			if (sNombre.length > 0) {
				if (sNumeroCel.length > 0) {
					if (sCompaniaCel != "-1") {
						if (sNSS.length > 0) {
							if (sTipoConstancia != "1") {
								if (sTipoConstancia != "527") {
									if (validarNSS()) {
										if (sTipoComprobante != "-1") {
											if (excepciones != "-1") {
												if (iRecepcion != "-1") {
													if (iestadoCivil != "-1") {
														if (sFechaComp == '' && sTipoComprobante != "1") {
															sTitle = "";
															sMensaje = "Favor de proporcionar una fecha, ya que selecciono un tipo comprobante.";
															mostrarMensaje(sMensaje, sTitle);
														}
														else {
															if (sTipoComprobante == "6" && sClieCoppel == '') {
																sTitle = "";
																sMensaje = "Favor de proporcionar un número de cliente, ya que selecciono estado de cuenta departamental.";
																mostrarMensaje(sMensaje, sTitle);
															}
															else {
																band = true;
															}
														}
													}
													else {
														sTitle = "";
														sMensaje = "Promotor: Favor de seleccionar un estado civil para continuar.";
														mostrarMensaje(sMensaje, sTitle);
													}
												}
												else {
													$("#cboTipoRecepcion").attr("disabled", false);
													sTitle = "";
													mensaje = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Promotor, favor de seleccionar la &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recepción del Folio.';
													mostrarMensaje(mensaje, sTitle);
												}
											}
											else {
												sTitle = "";
												sMensaje = "Promotor: Favor de seleccionar un tipo de excepción para continuar.";
												mostrarMensaje(sMensaje, sTitle);
											}
										}
										else {
											sTitle = "";
											sMensaje = "Favor de seleccionar un tipo comprobante.";
											mostrarMensaje(sMensaje, sTitle);
										}
									}

								}
								else {
									sTitle = "";
									sMensaje = "El tipo de afiliación seleccionado no debe tener NSS capturado, favor de verificar si se seleccionó el tipo de afiliación correcta o borrar el dato de NSS";
									mostrarMensaje(sMensaje, sTitle);
								}
							}
							else {
								sTitle = "";
								sMensaje = "El tipo de afiliación seleccionado no debe tener NSS capturado, favor de verificar si se seleccionó el tipo de afiliación correcta o borrar el dato de NSS";
								mostrarMensaje(sMensaje, sTitle);
							}
						}
						else {
							// si la longitud del nss no es mayor a 0, se deja pasar ya que no es un campo obligatorio
							//se valida el tipo de solicitud, si ya tiene uno en bd independiente se le obliga a capturar
							//un NSS
							if (iTipoSolCol != 8 && sTipoConstancia == "33")//si es independiente deja pasar sin nss
							{
								// si la longitud del nss no es mayor a 0, se deja pasar ya que no es un campo obligatorio
								if (sTipoComprobante != -1) {
									if (iRecepcion != "-1") {
										if (iestadoCivil != -1) {
											if (sFechaComp == '' && sTipoComprobante != 1) {
												sTitle = "";
												sMensaje = "Favor de proporcionar una fecha, ya que selecciono un tipo comprobante.";
												mostrarMensaje(sMensaje, sTitle);
											}
											else {
												if (sTipoComprobante == 6 && sClieCoppel == '') {
													sTitle = "";
													sMensaje = "Favor de proporcionar un número de cliente, ya que selecciono estado de cuenta departamental.";
													mostrarMensaje(sMensaje, sTitle);
												}
												else {
													band = true;
												}
											}
										}
										else {
											sTitle = "";
											sMensaje = "Promotor: Favor de seleccionar un estado civil para continuar.";
											mostrarMensaje(sMensaje, sTitle);
										}
									}
									else {
										$("#cboTipoRecepcion").attr("disabled", false);
										sTitle = "";
										mensaje = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Promotor, favor de seleccionar la &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recepción del Folio.';
										mostrarMensaje(mensaje, sTitle);
									}
								}
								else {
									$('#dtpFechaComprobante').val('');
									$('#txtClienteCoppel').val('');
									band = true;
								}
							}

							else if (sTipoConstancia == "1" || sTipoConstancia == "527")//traspaso issste ó traspaso no afiliado
							{
								// si la longitud del nss no es mayor a 0, se deja pasar ya que no es un campo obligatorio
								if (sTipoComprobante != -1) {
									if (iRecepcion != "-1") {
										if (iestadoCivil != -1) {
											if (sFechaComp == '' && sTipoComprobante != 1) {
												sTitle = "";
												sMensaje = "Favor de proporcionar una fecha, ya que selecciono un tipo comprobante.";
												mostrarMensaje(sMensaje, sTitle);
											}
											else {
												if (sTipoComprobante == 6 && sClieCoppel == '') {
													sTitle = "";
													sMensaje = "Favor de proporcionar un número de cliente, ya que selecciono estado de cuenta departamental.";
													mostrarMensaje(sMensaje, sTitle);
												}
												else {
													band = true;
												}
											}
										}
										else {
											sTitle = "";
											sMensaje = "Promotor: Favor de seleccionar un estado civil para continuar.";
											mostrarMensaje(sMensaje, sTitle);
										}
									}
									else {
										$("#cboTipoRecepcion").attr("disabled", false);
										sTitle = "";
										mensaje = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Promotor, favor de seleccionar la &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recepción del Folio.';
										//MuestraDialogoMensajeRechazo(mensaje);
										mostrarMensaje(mensaje, sTitle);
									}
								}
								else {
									$('#dtpFechaComprobante').val('');
									$('#txtClienteCoppel').val('');
									band = true;
								}
							}
							else {
								sTitle = "";
								sMensaje = "Promotor, es necesario capturar el NSS del Trabajador para continuar con el proceso.";
								mostrarMensaje(sMensaje, sTitle);
								$('#txtNss').focus();
								$('#txtNss').focus();
							}
						}
					}
					else {
						sTitle = "";
						sMensaje = "El campo Compañía de Celular es obligatorio, Favor de seleccionar uno.";
						mostrarMensaje(sMensaje, sTitle);
					}
				}
				else {
					sTitle = "";
					sMensaje = "El campo Número de celular es obligatorio, Favor de proporcionar uno.";
					mostrarMensaje(sMensaje, sTitle);
					$('#txTelCelular').focus();
				}
			}
			else {
				sTitle = "";
				sMensaje = "El campo Nombre es obligatorio, Favor de proporcionar uno.";
				mostrarMensaje(sMensaje, sTitle);
				$('#txtNombre').focus();
			}
		}
		else {
			sTitle = "";
			sMensaje = "El campo Apellido es obligatorio, Favor de proporcionar uno.";

			mostrarMensaje(sMensaje, sTitle);
			$('#txtApellidoPaterno').focus();
		}
	}
	else {
		sTitle = "";
		sMensaje = "El campo CURP es obligatorio, Favor de proporcionar uno.";
		mostrarMensaje(sMensaje, sTitle);
		$('#txtCurp').focus();
	}

	return band;
}

//Funcion que permite mandar al php la insercion de la informacion obtenida del modulo
function AlmacenarDatosFormulario() {
	var tipoExcepcion = 0;
	$("#lentitud").hide();
	tipoExcepcion = $('#cboExcepciones').val();
	//Se bloque la caja de texto para que el promotor no pueda cambiar el valor de la caja de texto, antes de enviar al enrolamiento
	$('#txtCurp,#txtCurp2').attr('disabled', true);
	//Se inicializa la variable para usarla posteriormente
	// iCodEjecutarContra = '';
	iCodEjecutarVideo = '';
	//se mandan los dos folios que se obtuvo de los servicios
	//respuestaServicios(iFolioConsulta, iFolioVerificacion);
	iRespuestaConsutla = 1;
	sConsultaEstatus = 'OK';
	sConsultaDiagnostico = '0';
	sVerificacionEstatus = 'OK';
	sVerificacionDiganostico = '0';
	sConsultaExpediente = '0';
	sConsultaEnrolamiento = '0';
	iSelloVerificacion = '1';
	
	iRetornoActualizacion = actualizaEnrolHuellaServicio(iFolioConsulta, iFolioVerificacion, sCurpTrabajador);

	var cadena = '';
	//se valida el primer bloque de los datos del Trabajador
	//obtiene los valores de las cajas de texto, para verificar que tenga datos
	//Bloque de codigo de los Datos del Trabajador
	var sTipoConstancia = $('#cboTipoConstancia').val();

	var sCurp = '';
	var sAppaterno = '';
	var sApmaterno = '';
	var sNombre = '';

	//04/05/2017
	if (bEsRenapo) {//Si se utiliza renapo
		//valida que la curp que se tiene capturada sea la misma que se tiene al consultar renapo
		var sCurpCapturado = '';//variable auxiliar que se llena con la curp caturada en pantalla
		sCurpCapturado = $('#txtCurp').val(); //se pasa el valor de pantalla a variable
		var sCurpCapturadoSol = '';//variable auxiliar que se llena con la curp caturada en pantalla
		sCurpCapturadoSol = $('#txtCurp2').val(); //se pasa el valor de pantalla a variable
		if (sCurpCapturado == sCurpRenapo) {//si la curpo en pantalla es la curp que se obtuvo al ejecutar renapo se mostrara en impresion los datos que se obtuvieron de renapo
			sCurp = sCurpRenapo;
			sNombre = sNombreRenapo;
			sAppaterno = sApePatRenapo
			sApmaterno = sApeMatRenapo;
		}
		else {//si no se muestran los datos que estan en pantalla
			sCurp = $('#txtCurp').val();
			sNombre = $('#txtNombre').val();
			sAppaterno = $('#txtApellidoPaterno').val();
			sApmaterno = $('#txtApellidoMatero').val();
		}
	}
	else {//si no se muestran los datos que estan en pantalla
		sCurp = $('#txtCurp').val();
		sNombre = $('#txtNombre').val();
		sAppaterno = $('#txtApellidoPaterno').val();
		sApmaterno = $('#txtApellidoMatero').val();
	}
	if (iTipoSolicitante == '1') {
		sCurpSolicitante = $('#txtCurp').val();
	}
	else {
		sCurpSolicitante = $('#txtCurp2').val();
	}
	var sNSS = $('#txtNss').val();
	var sNumeroCel = $('#txTelCelular').val();
	var sCompaniaCel = $('#cboCompTelef').val();
	var sTipoComprobante = $('#cboTipoComprobante').val();
	var sFechaComp = $('#dtpFechaComprobante').val();
	var sClieCoppel = $('#txtClienteCoppel').val();
	curpValidaH = sCurp;
	nssValidaH = $('#txtNss').val();
	//Bloque de datos del de los Datos del Contacto
	var sTipoTelefono = $('#cboCompTelef').val();
	var sTelContacto = $('#txtTelContacto').val();
	var sCompTelCon = $('#cboCompTelefCont').val();

	//Bloque de Datos del Domicilio
	var sCalle = "";
	var sNumExt = "";
	var sNumInt = "";
	var sCP = "";
	var sColia = 0;
	var sDelMun = 0;
	var sEstado = 0;
	var sPais = 342;
	var sLugSol = 0;

	var desColonia = "";
	var desMunicipio = "";
	var desEstado = "";
	var desPais = "";

	var iestadoCivil = $('#cboEstadoCivil').val();

	if (folio == 0) {
		if (iRespuestaConsutla == 1) {
			grabarLogJs(sConsultaEstatus, sConsultaDiagnostico, sVerificacionEstatus, sVerificacionDiganostico, sConsultaExpediente, sConsultaEnrolamiento, iSelloVerificacion, sTipoConstancia);

			if (((sConsultaEstatus == 'OK') || (sConsultaEstatus == '01')) || ((sConsultaEstatus == 'RE' && sConsultaDiagnostico == '049') && (sTipoConstancia == 26 || sTipoConstancia == 33))) {
				if ((sVerificacionEstatus == 'OK') || (sVerificacionEstatus == '01')) {
					if (sConsultaExpediente != '02') {
						if (iSelloVerificacion > 0) {
							if (sTelContacto == sNumeroCel) {
								sTitle = "";
								sMensaje = "Promotor: El numero de celular no puede ser igual al numero para el contacto. En caso de no contar con otro numero dejar vacio el telefono para el contacto.";
								mostrarMensaje(sMensaje, sTitle);
								$('#txtTelContacto').val('');
								$('#txtTelContacto').focus();
							}
							else {
								if (iSignpadConectado == 1) {
									if ($("#chkfirma").is(":checked")) {
										ifirma = 2;
									}
									else {
										ifirma = 1;
									}
								}
								else {
									ifirma = 0;
								}

								if (sTipoConstancia == 1 || sTipoConstancia == 527) {
									if (sTipoConstancia == 1) {
										tipoTraspaso = 1;
									}
									else {
										tipoTraspaso = 2;
									}

									sTipoConstancia = 27;
								}
								
								
								//faltan las variables de los 3 DIAGNOSTICOS, del SELLO DE VERICACION y VIGENCIA DE SELLO DE VERIFICACION.
								cadena += "opcion=1&tipoconstancia=" + sTipoConstancia + "&curp=" + sCurp + "&appaterno=" + sAppaterno + "&apmaterno=" + sApmaterno + "&nombre=" + sNombre + "&nss=" + sNSS;
								cadena += "&numerocel=" + sNumeroCel + "&companiacel=" + sCompaniaCel + "&tipocomprobante=" + sTipoComprobante + "&fechacomprobante=" + sFechaComp + "&numclientecoppel=" + sClieCoppel;
								cadena += "&tipotelefono=" + sTipoTelefono + "&telcontacto=" + sTelContacto + "&comptelefonica=" + sCompTelCon;
								cadena += "&calle=" + sCalle + "&numeroexterior=" + sNumExt + "&numerointerior=" + sNumInt + "&codigopostal=" + sCP + "&colonia=" + sColia + "&delegacion=" + sDelMun + "&estado=" + sEstado + "&pais=" + sPais + "&lugarsol=" + sLugSol;
								cadena += "&desCol=" + desColonia + "&desMun=" + desMunicipio + "&desEdo=" + desEstado + "&desPais=" + desPais + "&empleado=" + iEmpleado + '&firma=' + ifirma + '&foliobcpl=' + iKeyxinfBancoppel + "&curpsolicitante=" + sCurpSolicitante + "&tiposolicitante=" + iTipoSolicitante;
								cadena += "&iFolioConstanciaImplicaciones=" + iFolioConstanciaImplicaciones;
								cadena += "&estadocivil=" + iestadoCivil + "&aforeCedente=" + aforeCedente + "&tipoTraspaso=" + tipoTraspaso;
								cadena += "&tipoexcepcion=" + tipoExcepcion;
								cadena += "&folioPrevalidador=" + folioPrevalidador + "&iFolioConocimientoTraspaso=" + iFolioConocimientoTraspaso + "&iFolioSolicitud=" + folioSolisitud;

								$('#bAceptar').attr('disabled', true);
								document.getElementById("bAceptar").classList.add("deshabilitarImg");
								marcarLog("CURP: " + sCurpTrabajador + " - ALMACENARA DATOS DEL FORMULARIO");//logs

								$.ajax(
									{
										data: cadena,
										success: function (data) {
											datos = data;
											if (datos.inserto == 0) {
												marcarLog("CURP: " + sCurpTrabajador + " - NO PUDO ALMACENAR DATOS DEL FORMULARIO");//logs
												guardarSolConstancia = false;

												if (datos.respTipoComprobante == -1) {
													sTitle = "";
													sMensaje = "Promotor: No se pudo obtener el tipo de comprobante. Favor de capturarlo de nuevo.";
													mostrarMensaje(sMensaje, sTitle);
													$('#cboTipoComprobante').val(-1);
													$("#cboTipoComprobante").prop("disabled", false);
													$('#cboTipoComprobante').focus();
												}
												if (datos.respTel == 0 || datos.respTel == 1) {
													sTitle = "";
													sMensaje = "Promotor: El teléfono celular ya ha sido registrado con otra curp....Favor de solicitar el teléfono celular.";
													mostrarMensaje(sMensaje, sTitle);
													$('#txTelCelular').focus();
												}
												if (datos.respTelCon == 0 || datos.respTelCon == 1) {
													sTitle = "";
													sMensaje = "Promotor: El teléfono contacto ya ha sido registrado con otra curp....Favor de solicitar el teléfono del contacto.";
													mostrarMensaje(sMensaje, sTitle);
													$('#txtTelContacto').focus();
												}
												if (datos.respacomp == 2) {
													sTitle = "";
													sMensaje = "Promotor: El comprobante de domicilio No está vigente....Favor de solicitar un comprobante reciente.";
													mostrarMensaje(sMensaje, sTitle);
												}

												if (datos.respValTel == 0) {
													sTitle = "";
													sMensaje = "Promotor: El número telefónico proporcionado no es valido ....Favor de proporcionar un nuevo número.";
													mostrarMensaje(sMensaje, sTitle);
													$('#txTelCelular').focus();
												}

												if (datos.respValTelCont == 0) {
													sTitle = "";
													sMensaje = "Promotor: El número telefónico del contacto no es valido ....Favor de proporcionar un nuevo número.";
													mostrarMensaje(sMensaje, sTitle);
													$('#txtTelContacto').focus();
												}

												if (datos.respValTelTienda == 1) {
													sTitle = "";
													sMensaje = "Promotor: El número telefónico proporcionado no es valido, Le pertenece a una tienda .... \n Favor de proporcionar un nuevo número.";
													mostrarMensaje(sMensaje, sTitle);
													$('#txTelCelular').focus();
												}

												if (datos.respValTelContTienda == 1) {
													sTitle = "";
													sMensaje = "Promotor: El número telefónico del contacto no es valido, Le pertenece a una tienda ....Favor de proporcionar un nuevo número.";
													mostrarMensaje(sMensaje, sTitle);
													$('#txtTelContacto').focus();
												}
												if (datos.respValDomTien == 1) {
													sTitle = "";
													sMensaje = "Promotor: El número de domicilio proporcionado, Le pertenece a una tienda ....Favor de proporcionar un nuevo domicilio.";
													mostrarMensaje(sMensaje, sTitle);
													$('#txtCalle').focus();
												}
												if (datos.respComtelpromae == 1) {
													sTitle = "";
													sMensaje = "El número de celular no puede ser utilizado para el trámite debido a que corresponde a un Funcionario Coppel.";
													mostrarMensaje(sMensaje, sTitle);
													$('#txTelCelular').focus();
												}
												if (datos.respComContProm == 1) {
													sTitle = "";
													sMensaje = "El número de celular no puede ser utilizado para el trámite debido a que corresponde a un Funcionario Coppel.";
													mostrarMensaje(sMensaje, sTitle);
													$('#txtTelContacto').focus();
												}

												if (datos.respcalidaclientecartera == -1) {
													sTitle = "";
													sMensaje = "Promotor: El número de cliente que proporciono no es valido, favor de proporcionar otro.";
													mostrarMensaje(sMensaje, sTitle);
													$('#txtClienteCoppel').focus();
												}
												if (datos.curpPromotor == '') {
													sTitle = "";
													sMensaje = "Número de empleado no valido, favor de proporcionar otro.";
													mostrarMensaje(sMensaje, sTitle);

												}

												$('#bAceptar').attr('disabled', false);
												document.getElementById("bAceptar").classList.add("habilitarImg");
											}
											else if (datos.inserto == 1) {
												marcarLog("CURP: " + sCurpTrabajador + " - ALMACENO DATOS DEL FORMULARIO");//logs
												guardarSolConstancia = true;
												
												if (guardarSolConstancia) { // modificacion que valida si inserto en la solconstancia
													if (bGestorexito) { 	// Se valida que se halla autenticado 48 dactilar
														setTimeout(function () {
															iAutdactilar = 2;
															Autenticadactilar(); // se inserta tipo autenticacion = 3 48 dactilar
														}, 5000);
													}
												}
												
												folio = datos.folio;
												iFolioSol = datos.folio;
												guardarControlEnvio();
												almacenaDispositivoAfiliacion();
												if(JSON.stringify(Gcuentahabiente) == '[]')
												{
													
												}
												else
												{
													guardaBitacoraRespuestaBancoppel(1,Gcuentahabiente);
												}
												
												bloquearModal();
												if (sTipoConstancia == 27){
													var iResp = 0;
													var i = 0;
													iResp = ActualizarExpedientePermante(iFolioSol, sCurp);
													marcarLog('respuesta de expediente '+iResp);
													while (iResp == 2 && i < 5){
														iResp = ActualizarExpedientePermante(iFolioSol, sCurp);
														i = i +1;
													}
												}
												desbloquearModal();

												if (estatusValIne == 1) {
													actualizarFolioIne();
												}
												else {
													marcarLog('NO SE NECESITA ACTUALIZAR EL FOLIO INE YA QUE NO SE REALIZÓ LA VALIDACIÓN INE');
												}

												if (estatusValBancoppel == 1) {
													actualizarFolioBancoppel();
												}
												else {
													marcarLog('NO SE NECESITA ACTUALIZAR EL FOLIO BANCOPPEL YA QUE NO SE REALIZÓ LA VALIDACIÓN INE');
												}

												$('#bAceptar').attr('disabled', true);
												document.getElementById("bAceptar").classList.add("deshabilitarImg");
												$('#bGrabacion').attr('disabled', true);
												document.getElementById("bGrabacion").classList.add("deshabilitarImg");
												$('#bRenapoTitular').attr('disabled', true);
												document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
												$('#bRenapoSol').attr('disabled', true);
												document.getElementById("bRenapoSol").classList.add("deshabilitarImg");
												$('#cboTipoSolicitante').attr('disabled', true);
												$('#cboExcepciones').attr('disabled', true);
												$('#cboTipoRecepcion').attr('disabled', true);
												/*Folio 539
												$('#AltaUnica').attr('disabled', true);
												document.getElementById("AltaUnica").classList.add("deshabilitarBtn");*/
												if (iRevision != 1) {
													//se manda llamar la funcion que actualiza el folio en la tabla de renapo
													if (iFolAfore != "") {
														actualizaFolioRenapo(folio, iFolAfore);
													}
													//se manda llamar la funcion que actualiza el folio en la tabla de renapo del solicitante
													if (iFolAforeSol != "") {
														actualizaFolioRenapoSol(folio, iFolAforeSol);
													}

													//se manda guardar los datos del beneficiario si el solicitante no es el titular
													if (iTipoSolicitante != 1) {
														actualizadatossolicitanterenapo();
													}
													else {
														marcarLog('NO SE NECESITA GUARDAR EL REGISTRO DEL SOLICITANTE YA QUE ES EL TITULAR');
													}
													//se ejecuta el servicio para obtener el folio de procesar para el registro y traspaso
													ejecutaServicioFolio();
													//SE MANDA LLAMAR A LA FUNCION QUE ACTUALIZARA LA TABLA SolConstancia
													//sleep(5000);
													actualizaSolConstancia();

													actualizarestadocivil(iestadoCivil);

													if (bEsRenapo)//si es renapo esta activo
													{
														if (iFolAfore > 0) {//si el folio afore es mayor a cero
															if (sCurpCapturado == sCurpRenapo) {//si la curp en pantalla es la misma que respondio renapo
																//genera el formato de la curp por RENAPO
																generarFormatoRenapo(iFolAfore);
															}
															else {
																marcarLog('NO SE CREA FORMATO DE RENAPO PARA FOLIORENAPO[' + iFolAfore + '] --> LA CURP EN PANTALLA NO COINCIDE CON LA QUE RENAPO ARROJO');
															}
														}
														else {
															marcarLog('NO SE CREA FORMATO DE RENAPO [EL FOLIO ES CERO]');
														}
													}
													else {
														marcarLog('NO SE CREA FORMATO DE RENAPO PARA FOLIORENAPO[' + iFolAfore + '] -->LA CONSULTA ES NO EXITOSA');
													}

													if (bEsRenapoSol)//si es renapo esta activo para solicitante
													{
														if (iFolAforeSol > 0) {//si el folio afore es mayor a cero
															if (sCurpCapturadoSol == sCurpRenapoSol) {//si la curp en pantalla es la misma que respondio renapo
																//genera el formato de la curp por RENAPO
																generarFormatoRenapoSol(iFolAforeSol);
															}
															else {
																marcarLog('NO SE CREA FORMATO DE RENAPO EN SOLICTANTE PARA FOLIORENAPO[' + iFolAforeSol + '] --> LA CURP DEL SOLICITANTE EN PANTALLA NO COINCIDE CON LA QUE RENAPO ARROJO');
															}
														}
														else {
															marcarLog('NO SE CREA FORMATO DE RENAPO [EL FOLIO ES CERO] PARA EL SOLICITANTE');
														}
													}
													else {
														marcarLog('NO SE CREA FORMATO DE RENAPO EN SOLICTANTE PARA FOLIORENAPO [' + iFolAforeSol + '] -->LA CONSULTA ES NO EXITOSA');
													}
												}
												$('#btnConsultaCurp').attr('disabled', true);
												document.getElementById("btnConsultaCurp").classList.add("deshabilitarImg");
												$('#bRenapoTitular').attr('disabled', true);
												document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
												$('#bRenapoSol').attr('disabled', true);
												document.getElementById("bRenapoSol").classList.add("deshabilitarImg");
												$('#bAceptar').attr('disabled', true);
												document.getElementById("bAceptar").classList.add("deshabilitarImg");
												$('#bDigitalizar').attr('disabled', true);
												document.getElementById("bDigitalizar").classList.add("deshabilitarImg");
												
												
												bFuse = verificajfuse();
												if(cActualiza == 3 || bFuse < 2)
												{
													$("#Daceptap").hide();
													$("#Daceptas").show();
													$("#lentitud").show();
												}
												

												//$('#btnBuscarCP').attr('disabled', true);
												$("#cboTipoConstancia").attr('disabled', true);
												$("#txtCurp").attr('disabled', true);
												$("#txtApellidoPaterno").attr('disabled', true);
												$("#txtApellidoMatero").attr('disabled', true);
												$("#txtNombre").attr('disabled', true);
												$("#txtNss").attr('disabled', true);
												$("#txTelCelular").attr('disabled', true);
												$("#cboCompTelef").attr('disabled', true);
												$('#dtpFechaComprobante').attr('disabled', true);
												$("#cboTipoComprobante").attr('disabled', true);
												$("#cboTipoTelefono").attr('disabled', true);
												$("#txtTelContacto").attr('disabled', true);
												$("#cboCompTelefCont").attr('disabled', true);
												$("#chkfirma").attr('disabled', true);
											}

										}
									}
								);
							}

						}
					}
					else {
						sTitle = "";
						sMensaje = "Promotor: El trabajador cuenta con un proceso de Enrolamiento que no ha concluido. \n Favor de esperar 4 días hábiles para volver a realizar la consulta";

						iCodCerrarNavegador = 'EMCN';
						mostrarMensaje(sMensaje, sTitle);
						$('#btnConsultaCurp').attr('disabled', false);
						document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");


						$('#bRenapoTitular').attr('disabled', false);
						$('#bRenapoSol').attr('disabled', false);
						document.getElementById("bRenapoTitular").classList.add("habilitarImg");
						document.getElementById("bRenapoSol").classList.add("habilitarImg");

					}
				}
				else {
					if (sVerificacionDiganostico != '') {
						llamadoErrores(sVerificacionDiganostico);
						//Se desbloquea la caja de texto para que se corrija en caso de algun error en la toma de la curp
						$('#txtCurp,#txtCurp2').attr('disabled', false);
					}
					else {
						sTitle = "";
						mensaje = "Promotor: No se ha obtenido respuesta del servicio de verificacion favor de esperar.";
						mostrarMensaje(mensaje, sTitle);
					}

					$('#btnConsultaCurp').attr('disabled', false);
					document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

					$('#bRenapoSol').attr('disabled', false);
					document.getElementById("bRenapoSol").classList.add("habilitarImg");

					$('#bRenapoTitular').attr('disabled', false);
					document.getElementById("bRenapoTitular").classList.add("habilitarImg");

					$('#bAceptar').attr('disabled', false);
					document.getElementById("bAceptar").classList.add("habilitarImg");
				}
			}
			else {
				if (sConsultaDiagnostico != '') {
					llamadoErrores(sConsultaDiagnostico);
				}
				else {
					sTitle = "";
					mensaje = "Promotor: No se ha obtenido respuesta del servicio de consulta favor de esperar.";
					mostrarMensaje(mensaje, sTitle);
				}
				$('#btnConsultaCurp').attr('disabled', false);
				document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");
				$('#bRenapoSol').attr('disabled', false);
				document.getElementById("bRenapoSol").classList.add("habilitarImg");
				$('#bRenapoTitular').attr('disabled', false);
				document.getElementById("bRenapoTitular").classList.add("habilitarImg");
				$('#bAceptar').attr('disabled', false);
				document.getElementById("bAceptar").classList.add("habilitarImg");
			}

		}
		else {
			sTitle = "";
			mensaje = "Promotor: No se ha obtenido respuesta de los servicios de consulta y verificacion favor de esperar.";
			mostrarMensaje(mensaje, sTitle);
			$('#btnConsultaCurp').attr('disabled', false);
			document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

			$('#bRenapoSol').attr('disabled', false);
			document.getElementById("bRenapoSol").classList.add("habilitarImg");
			$('#bRenapoTitular').attr('disabled', false);
			document.getElementById("bRenapoTitular").classList.add("habilitarImg");
			$('#bAceptar').attr('disabled', false);
			document.getElementById("bAceptar").classList.add("habilitarImg");
		}

	}
	else {

	}
}

//Funcion para obtener el catalogo de comprobantes de domicilio y llenarlo en la pagina como opciones
function cargacatalogocomp() {

	$.ajax(
		{
			data: { opcion: 2 },
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;

				//si es caso 1, crea la opcion, caso diferente, es un error y se debe de mostrar el mensaje
				switch (datos.estatus) {
					case 1:
						sHtml = "<option value = -1>Seleccione una Opción</option>";
						for (var i = 0; i < datos.registros.length; i++) {
							sHtml += ("<option id='tipocom_" + datos.registros[i].numcomp + "' value =" + datos.registros[i].numcomp + ">" + datos.registros[i].desccomp + "</option>");

						}
						$('#cboTipoComprobante').empty();
						$('#cboTipoComprobante').html(sHtml);
						break;
					default:
						sTitle = "";
						sMensaje = datos.descripcion;
						mostrarMensaje(sMensaje, sTitle);
						break;
				}

			}
		}
	);
}

function cargaExcepciones() {

	if (OSName == 'Android') {
		sHtml = ("<option value ='26' selected>NINGUNO</option>");
	} else {
		sHtml = "<option value = -1 selected>Seleccione una Opción</option>";
		sHtml += ("<option value ='26'>NINGUNO</option>");
		sHtml += ("<option value ='27'>TRABAJADOR SIN MANOS</option>");
		sHtml += ("<option value ='33'>MANOS LESIONADAS</option>");
		sHtml += ("<option value ='2'>TRABAJADOR SOLO MANO DERECHA</option>");
		sHtml += ("<option value ='3'>TRABAJADOR SOLO MANO IZQUIERDA</option>");
	}

	$('#cboExcepciones').empty();
	$('#cboExcepciones').html(sHtml);
}

function cargacatalogoSol() {
	var TipoAfiliacionCat = 0;
	TipoAfiliacionCat = parseInt($("#cboTipoConstancia").val());

	$.ajax(
		{
			data: { opcion: 46 },
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;

				//si es caso 1, crea la opcion, caso diferente, es un error y se debe de mostrar el mensaje
				switch (datos.estatus) {
					case 1:
						sHtml = "<option value = -1>Seleccione una Opción</option>";
						for (var i = 0; i < datos.registros.length; i++) {
							if (!(TipoAfiliacionCat == 33 && datos.registros[i].iidtipo == 2)) {
								if (parseInt(datos.registros[i].iidtipo) != 5)
									sHtml += ("<option value =" + datos.registros[i].iidtipo + ">" + datos.registros[i].cdescripcion.toUpperCase() + "</option>");
							}

						}
						$('#cboTipoSolicitante').empty();
						$('#cboTipoSolicitante').html(sHtml);

						break;
					default:
						sMensaje = mensaje;
						mostrarMensaje(sMensaje, '');
						break;
				}

			}
		}
	);
}
//Funcion para obtener el catalogo de compañias de celulares y llenarlo en la pagina como opciones
function cargacatalogocompcelulares() {

	$.ajax(
		{
			data: { opcion: 3 },
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;

				//si es caso 1, crea la opcion, caso diferente, es un error y se debe de mostrar el mensaje
				switch (datos.estatus) {
					case 1:
						sHtml = "<option value = -1>Seleccione una Opción</option>";
						sHtml2 = "<option value = -1>Seleccione una Opción</option>";
						for (var i = 0; i < datos.registros.length; i++) {
							sHtml += ("<option id='companiacel_" + datos.registros[i].clavec + "' value =" + datos.registros[i].clavec + ">" + datos.registros[i].desccomp.toUpperCase() + "</option>");
							sHtml2 += ("<option id='companiatel_" + datos.registros[i].clavec + "' value =" + datos.registros[i].clavec + ">" + datos.registros[i].desccomp.toUpperCase() + "</option>");
						}
						$('#cboCompTelef').empty();
						$('#cboCompTelef').html(sHtml);

						$('#cboCompTelefCont').empty();
						$('#cboCompTelefCont').html(sHtml2);
						break;
					default:
						sTitle = "";
						sMensaje = datos.descripcion;
						mostrarMensaje(sMensaje, sTitle);
						break;
				}

			}
		}
	);
}

//Valida la estructura de la curp
function validarEstructuraCurp(curp) {
	var esValida = 0;
	var sCurp = $('#txtCurp').val();
	//AAAA######AAAAAA[#,A]#
	if (sCurp.length == 0) {
		sTitle = "";
		boqueoCajasdeTexto();
		$('#txtCurp').attr("disabled", false);

		if ($("#cboTipoConstancia").val() == 26) {
			$('#txtNss').attr("disabled", false);
		}
		else if ($("#cboTipoConstancia").val() == 27) {
			$('#txtNss').attr("disabled", false);
			$('#txtNombre').attr("disabled", false);
			$('#txtApellidoMatero').attr("disabled", false);
			$('#txtApellidoPaterno').attr("disabled", false);
		}
		else {
			$('#txtNombre').attr("disabled", false);
			$('#txtApellidoMatero').attr("disabled", false);
			$('#txtApellidoPaterno').attr("disabled", false);
		}

		sMensaje = "El campo CURP es obligatorio, Favor de proporcionar uno";
		mostrarMensaje(sMensaje, sTitle);
		$('#txtCurp').focus();
		esValida = 0;
	}
	else if (sCurp.length == 18) {
		if (curp.match(/^([A-Z])([A,E,I,O,U,X])([A-Z]{2})([0-9]{2})([0-1])([0-9])([0-3])([0-9])([M,H])([A-Z]{2})([B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3})([0-9,A-Z])([0-9])$/i)) {
			esValida = 1;
		}
		else {
			sTitle = "";
			sMensaje = "La curp proporcionada no cumple con la estructura correcta";
			mostrarMensaje(sMensaje, sTitle);
			$('#txtCurp').val('');
			$('#txtCurp').focus();
			esValida = 0;
		}
	}
	else {
		sTitle = "";
		sMensaje = "La curp capturada es incorrecta";
		mostrarMensaje(sMensaje, sTitle);
		$('#txtCurp').focus();
		$('#txtCurp').val('');
		esValida = 0;
	}

	return esValida;
}

function consultarValidaciones() {
	var sTipoConstancia = $("#cboTipoConstancia").val();
	var validaciones = false;

	if (sTipoConstancia == 1 || sTipoConstancia == 27 || sTipoConstancia == 527) {
		if (sTipoConstancia == 27) {
			var result = validarNSS();

			if (result) {
				var camposValidados = validarCamposPrevalidacion();
				if (camposValidados) {
					validaciones = true;
				}
				else {
					validaciones = false;
				}
			}
			else {
				validaciones = false;
			}
		}
		else {
			var camposValidados = validarCamposPrevalidacion();

			if (camposValidados) {
				validaciones = true;
			}
			else {
				validaciones = false;
			}
		}
	}
	else {
		var tipoSolicitante = $("#cboTipoSolicitante").val();

		if (tipoSolicitante != -1) {
			if (sTipoConstancia == 26) {
				var result = validarNSS();

				if (result == true) {
					validaciones = true;
				}
			}
			else {
				validaciones = true;
			}
		}
		else {
			var title = "";
			var mensaje = "Promotor debe seleccionar el tipo de solicitante";
			mostrarMensaje(mensaje, title);
		}
	}

	if (validaciones) {
		if ($("#cboExcepciones").val() != -1) {
			validaciones = true;
		}
		else {
			var title = "";
			validaciones = false;
			var mensaje = "Promotor debe seleccionar una excepción";
			mostrarMensaje(mensaje, title);
		}
	}

	return validaciones;
}

//funcion que permite validar la curp
function validacurp(iEmpleado) {
	var texto = '';
	var sCurp = $('#txtCurp').val();
	var sCurpSol = $('#txtCurp2').val();
	var sTipoConstancia = $('#cboTipoConstancia').val();
	var cadena = '';
	var mensaje = '';
	var opcion = 0;
	var iEstructuraCurp = 0;
	var sCURPCol = '';
	var iTipoSolColAux = 0;
	cadena += "opcion=4&curp=" + sCurp + "&tipoconstancia=" + sTipoConstancia;
	iEstructuraCurp = validarEstructuraCurp(sCurp);

	if (iEstructuraCurp == 1) {
		var validaCurpS = 0;
		validaCurpS = validacionCurpSol(sCurpSol, sCurp);
		if (validaCurpS == 1) {
			$.ajax({
				data: cadena,
				success: function (data) {
					//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
					datos = data;
					opcion = parseInt(datos.estatusvig.trim());

					switch (opcion) {
						case 0: //No entro por ningun camino en la funcion fn_valida_curp y se debe de dar como registro.
						case 1: //Indica que no existe en bd y se debe registrar.
						case 3: //Indica que es un registro existente en bd, pero este ya expiro sus 5 dias, por lo cual se deben de hacer otro registro.
						case 4:
						case 6: //Indica que es una solicitud la cual fue rechazada por no darle seguimiento y se debe capturar como registro nuevo.
						case 10://Indica que es una solicitud la cual fue rechazada por no darle seguimiento y se debe capturar como registro nuevo.
							//valida la curp constra la colsolicitudes
							sCURPCol = consultacolsolicitudes();
							iTipoSolCol = consultatipoconstancia();
							if (iTipoSolCol == 8) {
								iTipoSolColAux = 33;
							}
							if (sTipoConstancia == iTipoSolColAux) {
								sTitle = "";
								sMensaje = "La CURP proporcionada ya esta afiliada.";
								modalAutenticacion();
								mostrarMensaje(sMensaje, sTitle);
								$('#txtCurp').focus();
							}
							else {
								if (sCURPCol != '' && iTipoSolCol != 8) {
									sTitle = "";
									sMensaje = "La CURP proporcionada ya esta afiliada.";
									modalAutenticacion();
									mostrarMensaje(sMensaje, sTitle);
									$('#txtCurp').focus();
								}
								else {
									validarregistroprevio(sCurp);

									//Desabilita botones que no le permitiran hacer la captura ni la ejecucion de la digitalizacion
									$('#bDigitalizar').attr('disabled', true);
									document.getElementById("bDigitalizar").classList.add("deshabilitarImg");

									$('#bAceptar').attr('disabled', false);
									document.getElementById("bAceptar").classList.add("habilitarImg");

									if (OSName == "Android") {
										$('#bCancelarModulo').attr('disabled', false);
										document.getElementById("bCancelarModulo").classList.add("habilitarImg");
									} else {
										$('#bCancelarModulo').attr('disabled', false);
										document.getElementById("bCancelarModulo").classList.add("habilitarImg");
									}

									$('#btnConsultaCurp').attr('disabled', false);
									document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

									$('#bRenapoTitular').attr('disabled', false);
									document.getElementById("bRenapoTitular").classList.add("habilitarImg");
									$('#bRenapoSol').attr('disabled', false);
									document.getElementById("bRenapoSol").classList.add("habilitarImg");

									//Comparacion para que no ejecute los .EXE y los servicios
									if (iTrabajadorFinado == 1) {
										desboqueoCajasdeTexto();
										//Pone el foco del control en la caja de texto indicando que es un cliente que se puede afiliar
										$('#txtApellidoPaterno').focus();
									}
									else { // Se valida el proceso dependiendo la respuesta del servicio 0101 E.E y E.I
										
										if(bConExpe){// si tiene E.I y E.E	-- 0101 Gestor,0407,0501 
											if (iOpcionGestor == 1){
												bDetenerProceso = false;
												fnDetonarGestor4Huellas();
											}
											else if(iOpcionGestor == 2){
												bRealizar0407=false;
												bDetenerProceso = true;
											}
										}
										if(bSinExpe){ //si tiene E.I y no tiene E.E	-- 0101 0407 validacion ine/bancoppel 0501	
											if(iOpcionSinExp == 1){
												//iRetorno = 1;
												bCservicio0407 = true;
												bRealizar0407 = verificaEjecucionServicio();
												if (bRealizar0407) {
													elminarinformacionServicio();
													obtenermensajeVerificacion(0);
													
													bCservicio0407 = false;
													bDetenerProceso = false;
													bRealizar0407=false;
													iOpcionSinExp = 2;
													fnpreguntaconsulta();
												}
												setTimeout(() => {
													bloquearCamposDatosTrabajador();
												}, 4000);
											}
											else if (iOpcionSinExp == 2){
												bDetenerProceso = true;
											}
										}
										if(bDetenerProceso){ //bandera para detener el proceso siempre y cuando el gestor falle
															 //si no tiene E.I y E.E entonces fluhjo normal
											var validaciones = consultarValidaciones();
											if (validaciones) {
												if (sTipoConstancia == 1 || sTipoConstancia == 27 || sTipoConstancia == 527) {
													ejecutaPrevalidacion();
												}

												//La variable bPrevalidacionEnCurso se inicializa con el valor false
												if ((banderapreguntarine == 0 && bPrevalidacionEnCurso) || banderapreguntarine == 2) {
													iRetorno = 0;
													contador = 0;
													iContador = 0;
													$('.content-loader').hide();
													sTitle = " ";
													//sMensaje = "Promotor: Favor de colocar su dedo indice sobre el sensor";
													//mostrarMensaje(sMensaje, sTitle);
													mostrarmensaje2();
													$('#txtCurp').attr('disabled', true);
													//$('#btnConsultaCurp').attr('disabled', true);
													$('#bRenapoTitular').attr('disabled', true);
													document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
													$('#bRenapoSol').attr('disabled', true);
													document.getElementById("bRenapoSol").classList.add("deshabilitarImg");
													$('#bAceptar').attr('disabled', true);
													document.getElementById("bAceptar").classList.remove("deshabilitarImg");
													$('#bAceptar').attr('disabled', false);
													
													if (iOpcionSinExp == 2){ // Habilitar el btn aceptar 
														$('#bAceptar').attr('disabled', false);
														document.getElementById("bAceptar").classList.add("habilitarImg");
													}

												}
												else if ((sConsultaEstatus == "") && (sVerificacionEstatus == "") && (iSelloVerificacion == 0)) {
													iRetorno = 0;
													contador = 0;
													iContador = 0;

													if (banderapreguntarine == 0 && bPrevalidacionEnCurso == false || contadorRespuesta > 3) {
														contadorRespuesta = 0;
														respuestaPreval = false;
														$('.content-loader').hide();
														sTitle = " ";
														//sMensaje = "Promotor: Favor de colocar su dedo indice sobre el sensor";
														//mostrarMensaje(sMensaje, sTitle);
														mostrarmensaje2();
													}
													$('#txtCurp').attr('disabled', true);
													$('#bRenapoTitular').attr('disabled', true);
													document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
													$('#bRenapoSol').attr('disabled', true);
													document.getElementById("bRenapoSol").classList.add("deshabilitarImg");
													banderabancoppel = 1;

												}
												else if (sConsultaEstatus == "RE") {
													iContador = 0;
													contador = 0;
													iRetorno = 0;
													$('.content-loader').hide();
													sTitle = " ";
													//sMensaje = "Promotor: Favor de colocar su dedo indice sobre el sensor";
													//mostrarMensaje(sMensaje, sTitle);
													mostrarmensaje2();
													$('#txtCurp').attr('disabled', true);
													$('#btnConsultaCurp').attr('disabled', true);
													$('#bRenapoTitular').attr('disabled', true);
													document.getElementById("bRenapoSol").classList.add("deshabilitarImg");
													$('#bRenapoSol').attr('disabled', true);
													document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
												}
												else if (sVerificacionEstatus == "02") {
													iContador = 0;

													iRetorno = 1;
													if (contadorRespuesta > 3) {
														contadorRespuesta = 0;
														respuestaPreval = false;
														obtenermensajeVerificacion(0);
													}
													$('#txtCurp').attr('disabled', true);

													$('#btnConsultaCurp').attr('disabled', true);

													document.getElementById("btnConsultaCurp").classList.add("deshabilitarBtn");
													$('#bRenapoTitular').attr('disabled', true);
													$('#bRenapoSol').attr('disabled', true);
													document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
													document.getElementById("bRenapoSol").classList.add("deshabilitarImg");

												}
												else {
													iRetorno = 0;
													contador = 0;
													iContador = 0;

													if (contadorRespuesta > 3) {
														$('.content-loader').hide();
														sTitle = " ";
														contadorRespuesta = 0;
														respuestaPreval = false;
														//sMensaje = "Promotor: Favor de colocar su dedo indice sobre el sensor";
														//mostrarMensaje(sMensaje, sTitle);
														mostrarmensaje2();
													}
													$('#txtCurp').attr('disabled', true);
													//$('#btnConsultaCurp').attr('disabled', true);
													$('#bRenapoTitular').attr('disabled', true);
													$('#bRenapoSol').attr('disabled', true);
													document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
													document.getElementById("bRenapoSol").classList.add("deshabilitarImg");

												}
											}
										}
									}
								}
							}
							break;


						case 2:
							/*if (datos.diagnostico != '')
							{
								//indidca que tiene unsa solicitud vigente rechazada por procesar
								//manda llamar al metodo para obtener la descripcion del rechazo
								consultaRechazoProcesar();
							}
							else*/
							if (datos.diagnostico == '') {
								//indica que el trabajdor tiene una solicitud vigente
								modalAutenticacion();
								texto = formatearFecha(3, datos.fecha);
								mensaje = "El trabajador tiene una captura de datos vigente con la fecha: " + texto + ".";
								mensaje += "\n Favor de continuar con la Solicitud de Afiliación";
								//manda llamar la ventana con la cual se abrira el dialogo
								MuestraDialogoMensaje(mensaje);
							}

							//Desabilita botones que no le permitiran hacer la captura ni la ejecucion de la digitalizacion
							$('#bDigitalizar').attr('disabled', true);
							document.getElementById("bDigitalizar").classList.add("deshabilitarImg");

							$('#bAceptar').attr('disabled', true);
							document.getElementById("bAceptar").classList.add("deshabilitarImg");

							if (OSName == "Android") {
								$('#bCancelarModulo').attr('disabled', false);
								document.getElementById("bCancelarModulo").classList.add("habilitarImg");
							} else {
								$('#bCancelarModulo').attr('disabled', false);
								document.getElementById("bCancelarModulo").classList.add("habilitarImg");
							}

							$('#btnConsultaCurp').attr('disabled', false);
							document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

							$('#bRenapoTitular').attr('disabled', false);
							$('#bRenapoSol').attr('disabled', false);
							document.getElementById("bRenapoTitular").classList.add("habilitarImg");
							document.getElementById("bRenapoSol").classList.add("habilitarImg");



						case 5: // es una solicitud que se guardo, digitalizo y publico, se requiere invocar el servicio.
							/*Este metodo aplica cuando una solicitud tiene estatusproceso 2,
							donde se manda llenar el formulario y se muestra una ventana de dialogo,
							donde solicita la constraseña al trabajador.*/
							iRetorno = 0;
							contador = 0;
							iContador = 0;
							iCaminoAlterno = 1;
							iContadorAlterno = 0;
							//consultasolconstanciaestatus2();

							$('#bAceptar').attr('disabled', true);
							document.getElementById("bAceptar").classList.add("deshabilitarImg");

							$('#btnConsultaCurp').attr('disabled', false);
							document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

							$('#bRenapoSol').attr('disabled', false);
							$('#bRenapoTitular').attr('disabled', false);
							document.getElementById("bRenapoTitular").classList.add("habilitarImg");
							document.getElementById("bRenapoSol").classList.add("habilitarImg");

							if (OSName == "Android") {
								$('#bCancelarModulo').attr('disabled', false);
								document.getElementById("bCancelarModulo").classList.add("habilitarImg");
							} else {
								$('#bCancelarModulo').attr('disabled', false);
								document.getElementById("bCancelarModulo").classList.add("habilitarImg");
							}


							$('#bDigitalizar').attr('disabled', true);
							document.getElementById("bDigitalizar").classList.add("deshabilitarImg");
							break;
						case 7: //es una solicitu que tienes estatusproceso = 3
							modalAutenticacion();
							BanderaEstatus = 7;
							banderaContinuarV = 1;
							mensaje = 'Promotor: El Trabajador tiene una captura de datos en proceso.';
							//manda llamar un dialogo para mostrar el mensaje, no significa que este rechazado aún, es solo el nombre del metodo
							MuestraDialogoMensajeRechazo(mensaje);
							$('#bAceptar').attr('disabled', true);
							document.getElementById("bAceptar").classList.add("deshabilitarImg");

							$('#btnConsultaCurp').attr('disabled', false);
							document.getElementById("btnConsultaCurp").classList.add("deshabilitarImg");

							solProceso = 1;

							$('#bRenapoSol').attr('disabled', true);
							$('#bRenapoTitular').attr('disabled', true);
							document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
							document.getElementById("bRenapoSol").classList.add("deshabilitarImg");

							if (OSName == "Android") {
								$('#bCancelarModulo').attr('disabled', false);
								document.getElementById("bCancelarModulo").classList.add("habilitarImg");
							} else {
								$('#bCancelarModulo').attr('disabled', false);
								document.getElementById("bCancelarModulo").classList.add("habilitarImg");
							}

							$('#bDigitalizar').attr('disabled', true);
							document.getElementById("bDigitalizar").classList.add("deshabilitarImg");

							break;

						case 8:
							modalAutenticacion();
							BanderaEstatus = 7;
							banderaContinuarV = 1;
							mensaje = 'Promotor: El Trabajador tiene una captura de datos en proceso.';
							//manda llamar un dialogo para mostrar el mensaje, no significa que este rechazado aún, es solo el nombre del metodo
							MuestraDialogoMensajeRechazo(mensaje);
							$('#bAceptar').attr('disabled', true);
							document.getElementById("bAceptar").classList.add("habilitarImg");

							$('#btnConsultaCurp').attr('disabled', false);
							document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

							$('#bRenapoSol').attr('disabled', false);
							$('#bRenapoTitular').attr('disabled', false);
							document.getElementById("bRenapoTitular").classList.add("habilitarImg");
							document.getElementById("bRenapoSol").classList.add("habilitarImg");

							if (OSName == "Android") {
								$('#bCancelarModulo').attr('disabled', false);
								document.getElementById("bCancelarModulo").classList.add("habilitarImg");
							} else {
								$('#bCancelarModulo').attr('disabled', false);
								document.getElementById("bCancelarModulo").classList.add("habilitarImg");
							}

							$('#bDigitalizar').attr('disabled', false);
							document.getElementById("bDigitalizar").classList.add("habilitarImg");

							$('#bGrabacion').attr('disabled', true);
							document.getElementById("bGrabacion").classList.add("deshabilitarImg");



							break;
						case 9:
							modalAutenticacion();
							texto = formatearFecha(3, datos.fecha);
							mensaje = "El trabajador tiene una captura de datos vigente con la fecha: " + texto + ".";
							mensaje += "\n Favor de continuar con la Solicitud de Afiliación";
							//manda llamar la ventana con la cual se abrira el dialogo
							MuestraDialogoMensaje(mensaje);
							break;
						default:
							sTitle = "";
							sMensaje = datos.descripcion;
							mostrarMensaje(sMensaje, sTitle);
							break;
					}
				}
			});
		}
	}
}

function validacionCurpSol(sCurpSol, sCurp) {
	var bresp = 0;
	if (iTipoSolicitante != 1) {
		if (sCurpSol != sCurp) {
			if (sCurpSol != sCurpEnrolado) {
				if (sCurpSol.length == 0) {
					sMensaje = "El campo CURP del solicitante es obligatorio, Favor de proporcionar uno";
					mostrarMensaje(sMensaje, '');
					$('#txtCurp2').focus();
				}
				else if (sCurpSol.length == 18) {
					var iEstructuraCurpSol;
					iEstructuraCurpSol = validarEstructuraCurp(sCurpSol);
					if (iEstructuraCurpSol != 1) {
						sMensaje = "La curp proporcionada del solicitante no cumple con la estructura correcta";
						mostrarMensaje(sMensaje, '');
						$('#txtCurp2').val('');
						$('#txtCurp2').focus();
					}
					else {
						bresp = 1;
					}
				}
				else {
					sMensaje = "La curp del solicitante capturada es incorrecta";
					mostrarMensaje(sMensaje, '');
					$('#txtCurp2').focus();
					$('#txtCurp2').val('');
				}
			}
			else {
				sMensaje = "La curp del promotor y del solicitante no deben ser iguales";
				mostrarMensaje(sMensaje, '');
				$('#txtCurp2').val('');
				$('#txtCurp2').focus();
			}
		}
		else {
			sMensaje = "La curp del trabajador y del solicitante no deben ser iguales";
			mostrarMensaje(sMensaje, '');
			$('#txtCurp2').val('');
			$('#txtCurp2').focus();
			$('#txtCurp').val('');
			$('#txtCurp').focus();
		}
	}
	else {
		bresp = 1;
	}
	return bresp;
}
//funcion que permite validar la fecha del comprobante contra la fecha del servidor
function consultaRechazoProcesar() {
	var sCurp = $('#txtCurp').val();
	var cadena = '';
	var opcion = 0;

	//Desabilita botones que no le permitiran hacer la captura ni la ejecucion de la digitalizacion
	$('#bDigitalizar').attr('disabled', true);
	document.getElementById("bDigitalizar").classList.add("deshabilitarImg");

	$('#bAceptar').attr('disabled', true);
	document.getElementById("bAceptar").classList.add("deshabilitarImg");

	if (OSName == "Android") {
		$('#bCancelarModulo').attr('disabled', false);
		document.getElementById("bCancelarModulo").classList.add("habilitarImg");
	} else {
		$('#bCancelarModulo').attr('disabled', false);
		document.getElementById("bCancelarModulo").classList.add("habilitarImg");
	}

	$('#btnConsultaCurp').attr('disabled', false);
	document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

	$('#bRenapoSol').attr('disabled', false);
	$('#bRenapoTitular').attr('disabled', false);
	document.getElementById("bRenapoSol").classList.add("habilitarImg");
	document.getElementById("bRenapoTitular").classList.add("habilitarImg");

	cadena += "opcion=5&curp=" + sCurp;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;

				if (datos.estado == 1) //indica que se ejecuto correctamente la consulta para obtener los datos
				{
					//indica que se ejecuto, donde el diagnostico procesar= 4 y el codigo esta en el catalogo
					if (datos.estatus == 1) {
						mensaje = "El trabajador tiene una solicitud previa vigente,la cual fue rechazada por: " + datos.descripcion;
						mensaje += "\n ,Todavía no es posible solicitar de nuevo la constancia";

						//manda llamar la ventana con la cual se abrira el dialogo
						MuestraDialogoMensajeRechazo(mensaje);

						$('#txtCurp').focus();
					}
					if (datos.estatus == 3) {
						//se habilita el boton para capturar un registro
						//Desabilita botones que no le permitiran hacer la captura ni la ejecucion de la digitalizacion
						$('#bDigitalizar').attr('disabled', true);
						document.getElementById("bDigitalizar").classList.add("deshabilitarImg");

						$('#bAceptar').attr('disabled', false);
						document.getElementById("bAceptar").classList.add("habilitarImg");

						if (OSName == "Android") {
							$('#bCancelarModulo').attr('disabled', false);
							document.getElementById("bCancelarModulo").classList.add("habilitarImg");
						} else {
							$('#bCancelarModulo').attr('disabled', false);
							document.getElementById("bCancelarModulo").classList.add("habilitarImg");
						}

						$('#btnConsultaCurp').attr('disabled', false);
						document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

						/*Movil-Sergio*/
						$('#bRenapoSol').attr('disabled', false);
						$('#bRenapoTitular').attr('disabled', false);
						document.getElementById("bRenapoTitular").classList.add("habilitarImg");
						document.getElementById("bRenapoSol").classList.add("habilitarImg");
						//Pone el foco del control en la caja de texto indicando que es un cliente que se puede afiliar
						$('#txtApellidoPaterno').focus();

					}
				}
				else {
					sTitle = "";
					sMensaje = datos.descripcion;
					mostrarMensaje(sMensaje, sTitle);
				}

			}
		}
	);

}

//Funcion para obtener el catalogo de lugares de solicitud
function cargacatalogoLugarSolicitud() {

	$.ajax(
		{
			data: { opcion: 6 },
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;

				//si es caso 1, crea la opcion, caso diferente, es un error y se debe de mostrar el mensaje
				switch (datos.estatus) {
					case 1:
						sHtml = "";
						for (var i = 0; i < datos.registros.length; i++) {
							if (datos.registros[i].clave == 7) {
								sHtml += ("<option value =" + datos.registros[i].clave + ">" + datos.registros[i].descripcioncat.toUpperCase() + "</option>");
								sDescripcionLugar = datos.registros[i].descripcioncat.toUpperCase();
							}
						}

						$('#cboCompLugSol').empty();
						$('#cboCompLugSol').html(sHtml);
						$("#cboCompLugSol").attr('disabled', true);
						break;
					default:
						sTitle = "";
						sMensaje = datos.descripcion;
						mostrarMensaje(sMensaje, sTitle);
						break;
				}
			}
		}
	);
}

function obtenerdatospromotor(iEmpleado) {
	var cadena = '';
	cadena += "opcion=7&empleado=" + iEmpleado;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;

				sClaveConsar = datos.claveconsar;
				sNombrePromotor = datos.nombre.trim();

				$('#txtnumEmp').val(iEmpleado);
				$('#txtClaveConsar').val(datos.claveconsar);
				$('#txtNomPro').val(datos.nombre);

			}
		}
	);
}

function obtenerfechaservidor() {
	sFecha = '1900-01-01';

	$.ajax(
		{
			data: { opcion: 8 },
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				sFecha = datos.resp;
			}
		}
	);
	return sFecha;
}

function consultacolsolicitudes() {
	var sCurp = $('#txtCurp').val();
	var cadena = '';
	cadena += "opcion=9&curp=" + sCurp;
	var sRegresaCurp = '';
	$.ajax(
		{
			data: cadena,
			url: 'php/constanciaafiliacion.php',
			type: 'POST',
			dataType: 'json',
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				sRegresaCurp = datos.curp;

			}
		}
	);
	return sRegresaCurp;
}

function consultatipoconstancia() {
	var sCurp = $('#txtCurp').val();
	var cadena = '';
	cadena += "opcion=11&curp=" + sCurp;
	var iRegresaTipoSol = 0;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				iRegresaTipoSol = datos.tiposolicitud;
			}
		}
	);
	return iRegresaTipoSol;
}

//funcion que habilitara controles segun el tipo de comprobante que se elija
function validatipocomprobante() {
	var sTipoComprobante = $('#cboTipoComprobante').val();
	var sFechaComp = $('#dtpFechaComprobante').val();
	var sClieCoppel = $('#txtClienteCoppel').val();

	//Desabilita el cuadro de texto para evitar que capturen un dato
	$('#txtClienteCoppel').attr('disabled', true);

	//limpia la caja de texto, para en caso de que quedara con algo, este se limpiara
	$('#txtClienteCoppel').val('');

	if (sTipoComprobante == -1) {
		$('#dtpFechaComprobante').val('');
		$('#txtClienteCoppel').val('');
	}
	else if (sTipoComprobante == 1) //Es la CREDENCIAL INE
	{
		$('#dtpFechaComprobante').val('');
		$('#txtClienteCoppel').val('');
		$('#dtpFechaComprobante').attr('disabled', true);

	}
	else if (sTipoComprobante == 6) //EStado de cuenta departamental
	{
		$('#txtClienteCoppel').val('');
		$('#dtpFechaComprobante').val('');
		$('#txtClienteCoppel').attr('disabled', false);
		//$("#dtpFechaComprobante").datepicker("option", "maxDate", sFechaAct);
	}
	else {
		$('#dtpFechaComprobante').val('');
		$('#txtClienteCoppel').val('');
		$('#dtpFechaComprobante').attr('disabled', false);
		$("#dtpFechaComprobante").datepicker("option", "maxDate", sFechaAct);

	}
}


//METODO PARA CONVERTIR EL PDF A IMAGEN Y PUBLICARLO EN EL SERVIDOR INTERMEDIO
function publicarImagenSolicitudConstancia(sRutaPdfTemp) {
	$.ajax({
		data: {
			folio: folio,
			curp: $('#txtCurp').val(),
			ifirma: ifirma,
			rutaPdfTemp: sRutaPdfTemp
		},
		success: function (data) {
			switch (data.estado) {
				case 1:
					break;
				case -1:
				case -2:
					MuestraDialogoMensajeSignpad("PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.", function () { });
					break;
				default:
					MuestraDialogoMensajeSignpad("PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente. ", function () { });
					break;
			}
		}
	});
}

function LlamarEjecutaApplet(iOpcion) {
	var sMensaje = "";
	iOpcion = TOMAR_FIRMA;
	var sParametros = '';
	if (OSName == "Android") {
		if (!($('#chkfirma').is(':checked'))) {
			iFirmaSolicitada = NOMBRE_TRAB;
			if (($('#cboTipoConstancia').val() === "26") || ($('#cboTipoConstancia').val() === "33")) {
				sMensaje = " PROMOTOR, Favor de comunicar lo siguiente al trabajador: Se le va capturar su nombre completo de manera digital que se plasmará en la solicitud de constancia para Registro";
			}
			else {
				sMensaje = " PROMOTOR, Favor de comunicar lo siguiente al trabajador: Se le va capturar su nombre completo de manera digital que se plasmará en la solicitud de constancia para Traspaso";
			}
			MuestraDialogoMensajeSignpad(sMensaje, function () {
				sParametros = "1 1 " + "SCTR_" + folio + "_NTRAB";
				opcionejecutaFirma(sParametros);
			});
		}
	} else {
		if (!($('#chkfirma').is(':checked'))) {
			iFirmaSolicitada = NOMBRE_TRAB;
			if (($('#cboTipoConstancia').val() === "26") || ($('#cboTipoConstancia').val() === "33")) {
				sMensaje = " PROMOTOR, Favor de comunicar lo siguiente al trabajador: Se le va capturar su nombre completo de manera digital que se plasmará en la solicitud de constancia para Registro";
			}
			else {
				sMensaje = " PROMOTOR, Favor de comunicar lo siguiente al trabajador: Se le va capturar su nombre completo de manera digital que se plasmará en la solicitud de constancia para Traspaso";
			}
			MuestraDialogoMensajeSignpad(sMensaje, function () {
				sParametros = "1 1 " + "SCTR_" + folio + "_NTRAB";
				opcionejecuta(sParametros);
			});
		}
	}
}

/*---------------------------------------WEBSERVICE EJECUTAR EXE---------------------------------------*/
function opcionejecuta(sParametros) {

	if(iOpcion == VERIFICACION_EMPLEADO)
	{
		marcarLog("Entro caso excepcion");							
		var consulta = sParametros.indexOf("enrolamiento");
		if(consulta != -1)
		{
			iOpcion = ENROLAR_TRABAJADOR;
		}
		{
			marcarLog("No se asigno enrolamiento");
		}
		
	}
	
	


	// MOVIL
	if (OSName == "Android") {
		switch (iOpcion) {
			case ENROLAR_TRABAJADOR:
				$('.content-loader').show();
				sTipoSol = $('#cboTipoConstancia').val();

				if (sTipoSol == 1 || sTipoSol == 527) {
					sTipoSol = 27;
				}

				Android.llamaEnrolamiento(iEmpleado, sCurpTrabajador, sTipoSol, folio);
				break;
			case ELIMINAR_PROCESO:
				respuestaWebService(1, iOpcion);
				break;
			case DIGITALIZAR:
				sTipoSol = $('#cboTipoConstancia').val();
				$('.content-loader').show();
				if (sTipoSol == 1 || sTipoSol == 527) {
					sTipoSol = 27;
				}
				Android.llamaComponenteDigitalizador(folio, iEmpleado, folio, 3, sTipoSol); /* sTipoConstancia */


				$('#bDigitalizar').attr('disabled', true);
				document.getElementById("bDigitalizar").classList.add("deshabilitarImg");

				break;
			case ESTA_CONECTADO_SIGNPAD:
				break;
			case TOMAR_FIRMA:
				$('.content-loader').show();
				Android.llamaComponenteFirmaD(folio, folio, iFirmaSolicitada);
				break;
			case CONSULTA_BIOMETRICO:
				$('.content-loader').show();
				if (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) {
					if (bPrevalidacionEnCurso || iRetorno == 0) {
						if (bPrevalidacionEnCurso) {
							iOpcion = VERIFICACION_EMPLEADO;
							Android.llamaComponenteHuellas(4, iEmpleado, 1, 2, iEmpleado, sTipoSolicitud, sCurpTrabajador);
						} else {
							Android.llamaComponenteHuellas(4, iEmpleado, 1, 0, iEmpleado, sTipoSolicitud, sCurpTrabajador);
						}
					}

				}
				else {
					if (bPrevalidacionEnCurso || iRetorno == 0) {
						if (bPrevalidacionEnCurso) {
							iOpcion = VERIFICACION_EMPLEADO;
							Android.llamaComponenteHuellas(4, iEmpleado, 1, 2, iEmpleado, sTipoSolicitud, sCurpTrabajador);
						} else {
							Android.llamaComponenteHuellas(4, iEmpleado, 1, 0, iEmpleado, sTipoSolicitud, sCurpTrabajador);
						}
					}
					else if (iRetorno == 1) {
						//CAPTURAR HUELLA TRABAJADOR
						iOpcion = VERIFICACION_TRABAJADOR;
						Android.llamaComponenteHuellas(7, 0, 1, 1, iEmpleado, sTipoSolicitud, sCurpTrabajador);

					}
					else if (iRetorno == 2) {
						//CAPTURAR HUELLA PROMOTOR
						iOpcion = VERIFICACION_EMPLEADO;
						Android.llamaComponenteHuellas(4, iEmpleado, 1, 2, iEmpleado, sTipoSolicitud, sCurpTrabajador);
					}
				}

				break;
			case LECTURA_TARGETA:
				capturarTarjeta();
				break;
			case COMPARACION_BIOMETRICA_BCPL:
				Android.llamaComponenteHuellas(4, iEmpleado, 1, 3, iEmpleado, sTipoSolicitud, sCurpTrabajador);
				break;
			case VALIDACION_INE:
				Android.llamaComponenteEnlaceIne(sParametros);
				break;
			default:
				break;
		}

		if (iOpcion == CONSULTA_BIOMETRICO || iOpcion == VERIFICACION_TRABAJADOR || iOpcion == VERIFICACION_EMPLEADO) {
			$('.content-loader').show();
		}

	} else {
		switch (iOpcion) {
			case ENROLAR_TRABAJADOR:
				var sRuta = 'C:\\sys\\firefoxportable\\navegadorAfore.exe';
				break;
			case VALIDACION_INE:
				var sRuta = 'C:\\sys\\firefoxportable\\navegadorAfore.exe';
				break;
			case VALIDACION_BANCOPPEL:
				var Aut = 12;
				ligaIne = obtenerLigaIne01(Aut);
				$(this).dialog("close");
				//console.log("entra btnFrame");
				var myDlg = new dialog_frame(document.getElementById('divDlgFrame'));
				myDlg.FramePagina(80, 80, ligaIne);
				//539
				bloquearcamposine();
				break;
			case ELIMINAR_PROCESO:
				var sRuta = 'C:\\Windows\\System32\\taskkill.exe';
				break;
			case DIGITALIZAR:
				var sRuta = 'C:\\sys\\pafscanimgnet\\DIGIDOCTOSCLIENTE.EXE';
				$('#bDigitalizar').attr('disabled', true);
				document.getElementById("bDigitalizar").classList.add("deshabilitarImg");
				break;
			case ESTA_CONECTADO_SIGNPAD:
			case TOMAR_FIRMA:
				var sRuta = 'C:\\sys\\signpad\\CAPTURAFIRMAAFORE.EXE';
				break;
			case LECTURA_TARGETA:
				var sRuta = 'C:\\sys\\modulo\\LEERTARJETABANCOPPEL.EXE';
				break;
			case SUBIR_VOLUMEN:
				var sRuta = 'C:\\SYS\\PROGS\\VerificaMicrofono.EXE';
				break;
			case GESTOR_DE_SELLOS:
				 sRuta = "C:\\SYS\\PROGS\\GESTORDESELLOS.EXE";
			break;
			case CONSULTA_BIOMETRICO:
				sRuta = "C:\\SYS\\PROGS\\HETRABAJADOR.EXE";

				if (bPrevalidacionEnCurso) {
					iOpcion = VERIFICACION_EMPLEADO;
				}
				else if (iRetorno == 1) {
					iOpcion = VERIFICACION_TRABAJADOR;
				}
				else if (iRetorno == 2) {
					iOpcion = VERIFICACION_EMPLEADO;
				}
				break;
			case COMPARACION_BIOMETRICA_BCPL:
				sRuta = "C:\\SYS\\PROGS\\HTCONSULTABCPL.EXE";
				//Se bloquea la pagina, Hasta obtener una respuesta seguira bloqueada
				bloquearUI();
				break;
			default:
				break;
		}

		if (iOpcion == CONSULTA_BIOMETRICO || iOpcion == VERIFICACION_TRABAJADOR || iOpcion == VERIFICACION_EMPLEADO) {
			$('.content-loader').show();
		}

		ejecutaWebService(sRuta, sParametros, iOpcion);
	}
}

// MOVIL
function opcionejecutaFirma(sParametros) {
	//console.log("Los parametros son --> " + sParametros + ' La Opcion es --> ' + iOpcion);
	iOpcion = TOMAR_FIRMA;

	switch (iOpcion) {
		case TOMAR_FIRMA:
			Android.llamaComponenteFirmaD(folio, 0, iFirmaSolicitada);
			break;

		default:
			break;
	}
	$('.content-loader').show();
}

//METODO PARA MANDAR LLAMAR AL DIGITALIZADOR: YA QUE SI LO DEJAMOS COMO ESTA EN EL METODO OPCIONEJECUTA, TOMA LA IOPCION = 16 Y SE QUEDA CICLADO Y NUNCA MANDA LLAMAR AL DIGITALIZADOR
function opcionejecutaDigi(sParametrosDigi, iopc) {
	var sRuta = 'C:\\sys\\pafscanimgnet\\DIGIDOCTOSCLIENTE.EXE';

	$('#bDigitalizar').attr('disabled', true);
	document.getElementById("bDigitalizar").classList.add("deshabilitarImg");

	iOpcion = 0;
	iOpcion = DIGITALIZAR;

	// MOVIL
	if (OSName == "Android") {
		opcionejecuta(sParametrosDigi);
	} else {
		ejecutaWebService(sRuta, sParametrosDigi, iOpcion);
	}
}

//EJECUTA WEBSERVICE
function ejecutaWebService(sRuta, sParametros, iOpcion) {
	if (OSName != "Android") {
		console.log("Parametros EjecutaWebService son --> " + sParametros + ' La Opcion es --> ' + iOpcion);
		soapData = "",
			httpObject = null,
			docXml = null,
			iestado = 0,
			sMensaje = "";
		sUrlSoap = "http://127.0.0.1:20044/";

		soapData =
			'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
			'<SOAP-ENV:Envelope' +
			' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"' +
			' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"' +
			' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"' +
			' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"' +
			' xmlns:ns2=\"urn:ServiciosWebx\">' +
			'<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">' +
			'<ns2:ejecutarAplicacion>' +
			'<inParam>' +
			'<Esperar>1</Esperar>' +
			'<RutaAplicacion>' + sRuta + '</RutaAplicacion>' +
			'<parametros><![CDATA[' + sParametros + ']]></parametros>' +
			'</inParam>' +
			'</ns2:ejecutarAplicacion>' +
			'</SOAP-ENV:Body>' +
			'</SOAP-ENV:Envelope>';

		httpObject = getHTTPObject();

		if (httpObject) {
			if (httpObject.overrideMimeType) {
				httpObject.overrideMimeType("false");
			}

			httpObject.open('POST', sUrlSoap, false); //-- NO ASINCRONO
			httpObject.setRequestHeader("Accept-Language", null);
			httpObject.onreadystatechange = function () {
				if (httpObject.readyState == 4 && httpObject.status == 200) {
					parser = new DOMParser();
					docXml = parser.parseFromString(httpObject.responseText, "text/xml");
					iestado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;

					//LLAMADA A LA FUNCIÓN QUE RECIBE LA RESPUESTA DEL WEBSERVICE
					if (iOpcion != SUBIR_VOLUMEN) {
						
						if(bCservicio0101 || bCservicio0407){
							respuestaWebService(iestado, iOpcion);
						}
						else{
							setTimeout(function () {
								respuestaWebService(iestado, iOpcion);
							}, 4000);
						}
					}
				}
				else {
					console.log(httpObject.status);
				}
			};
			httpObject.send(soapData);
		}
	}
}

function getHTTPObject() {
	var xhr = false;
	if (window.ActiveXObject) {
		try {
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				xhr = false;
			}
		}
	} else if (window.XMLHttpRequest) {
		try {
			xhr = new XMLHttpRequest();
		} catch (e) {
			xhr = false;
		}
	}
	return xhr;
}

//funcion que permite obtener la respuesta que arrojo el applet
//function respuestaApplet(iRespuesta)
function respuestaWebService(iRespuesta, iOpcion) {
	console.log('EjecutaRespuestaWebService es: ' + iOpcion + '    La Respuesta es: ' + iRespuesta);

	switch (iOpcion) {
		case COMPARACION_BIOMETRICA_BCPL:
			if (iRespuesta > 0) {
				iRetorno = 0;
				//Se guarda el valor de la respueta dos veces, por el hecho que se puede o no guaradar el folio de bancoppel en la solconstancia
				iKeyxinfBancoppel = iRespuesta;
				//En cuanto me haya regresado una respuesta se manad llamar al metodo siguiente
				desbloquearUI();
				//Una vez obtenida la respuesta, se va y verifica a la base de datos que significa esa respuesta
				if (sFlujoExtraccion == 0) {
					obtenervalorfolioretorno(iRespuesta);
				}
			}
			break;

		case LECTURA_TARGETA:
			if (iRespuesta == -3) {
				sMensaje = "Promotor: Ocurrio un problema al leer la tarjeta";
				mostrarMensaje(sMensaje, '');
			}
			else if (iRespuesta == -10) {
				sMensaje = "Promotor: La tarjeta no es BanCoppel. Intenta con una tarjeta BanCoppel o realizar la captura manualmente";
				mostrarMensaje(sMensaje, '');
			}
			else if (iRespuesta == -2 || iRespuesta == -1 || iRespuesta == 0) {

			}
			else if (iRespuesta > 0) {
				//Se guarda el keyx
				iKeyxtargeta = iRespuesta;
				iRetorno = 5;
				sTitle = " ";
				sMensaje = "Promotor: Solicita al trabajador que coloque el dedo con el que se autentifica en Bancoppel";
				mostrarMensaje(sMensaje, sTitle);
			}
			break;
		case ENROLAR_TRABAJADOR:
			if (iRespuesta == 0) {
				setTimeout(function () {
					// MOVIL
					if (OSName == "Android") {
						sTitle = "Android";
					} else {
						sTitle = "";
					}
					//sMensaje = "Promotor: culminó el proceso de enrolamiento.";
					marcarLog("CURP: " + sCurpTrabajador + " - CULMINÓ EL PROCESO DE ENROLAMIENTO --> " + ' Respuesta = ' + iRespuesta + ' FOLIO = ' + iFolioSol + ' CodigoEjecuta = ' + iCodEjecutarVideo);//logs
					//Ejecutar Servicio Contraseña Afore(ESCA)
					//iCodEjecutarContra = 'ESCA';
					iCodEjecutarVideo = 'VID';
					//mostrarMensaje(sMensaje, sTitle);
					mostrarmensaje2();
				}, 6000);
			}
			if (iRespuesta == 1) {
				$('#bAceptar').attr('disabled', true);
				document.getElementById("bAceptar").classList.add("deshabilitarImg");

				//indexarFormatoEnrolamiento();
				setTimeout(function () {
					// MOVIL
					$('.content-loader').hide();
					$('#bGrabacion').attr('disabled', false);
					document.getElementById("bGrabacion").classList.add("habilitarImg");
					banderaContinuarV = 1;
					sTitle = "";
					iOpcion = "";
					//sMensaje = "Promotor: finalizó el proceso de enrolamiento. Por favor continuar con la grabación del video.";
					marcarLog("CURP: " + sCurpTrabajador + " - FINALIZÓ EL PROCESO DE ENROLAMIENTO. POR FAVOR CONTINUAR CON LA GRABACIÓN DEL VIDEO -" + ' Respuesta = ' + iRespuesta + ' FOLIO = ' + iFolioSol + ' CodigoEjecuta = ' + iCodEjecutarVideo);//logs
					//mostrarMensaje(sMensaje, sTitle);
					mostrarmensaje2();
				}, 6000);
			}
			break;
		case CONSULTA_BIOMETRICO:
			if (iRespuesta == 1) {
				iBanderaEntrada = 1;
				//llamado de servicio
				iTipoPersona = 2;
				iTipoServicio = 9909;
				iTipoOperacion = "0101";
				ObtenerRespuestaXML(sCurpTrabajador, sCurpEnrolado, iTipoPersona, iTipoServicio, iTipoOperacion);
				
				if(bCservicio0101){ 
					bCservicio0101 = false;
					validarCServicio0101();
				}
				else{
					setTimeout(function () {
						validarConsumoServicio0101();
					}, 5000);
				}
			}
			else {
				obtenerMensajeConsulta(iRespuesta);
				contador++;
			}
			break;
			
		case GESTOR_DE_SELLOS:
			setTimeout(function () {
				if (iRespuesta == '1')  
				{			
					var itotalintentosdac = 5;
					for(var i=0;i < 5; i++){
						Autenticadactilar();
						if(iResSellos == false){
							
							sleep(10000);
						}
						else{
							i = itotalintentosdac;
						}
					}
					if(iResSellos){ // si en los intentos se obtuvo una resp entrara por autenticacion dactilar.
					iOpcion = CONSULTA_BIOMETRICO;
					bGestorexito = true;
					iOpcionGestor = 2;
					
						bRealizar0407 = verificaEjecucionServicio();   
						if (bRealizar0407) {
							iRetorno = 1;
							elminarinformacionServicio();
							obtenermensajeVerificacion(0);
						}
					}
					else{ // si en los intentos no se obtuvo una resp entrara por INE/BANCOPPEL.
						bFnormal = true;
						bConExpe = false;
						$('.content-loader').hide();
						bloquearCamposServicio(); // desahbilitar campos, algunos servicios los habilita
						fnpreguntaconsulta(); // INE/BANCOPPEL
						bDetenerProceso = true;
					}
				}
				else{
					if(iRespuesta == '107'){
						fnDetonarGestor4Huellas();
					}
					else{ // si en los intentos no se obtuvo una resp entrara por INE/BANCOPPEL.
						bFnormal = true;
						bConExpe = false;
						$('.content-loader').hide();
						bloquearCamposServicio(); // desahbilitar campos, algunos servicios los habilita
						fnpreguntaconsulta(); // INE/BANCOPPEL
						bDetenerProceso = true;	
					}
				}
			}, 20000);
        break;
		
		case VALIDACION_INE:
			if (iRespuesta == 0) {
				setTimeout(function () {
					sMensaje = "Promotor: finalizó la Validación INE";
					banderapreguntarine = 2;
					mostrarMensajeIne(sMensaje);
					bloquearcamposine();		 
				}, 6000);
			}
			if (iRespuesta == 1) {
				setTimeout(function () {
					iOpcion = "";
					sMensaje = "Promotor: finalizó la Validación INE";
					banderapreguntarine = 2;
					mostrarMensajeIne(sMensaje);
					bloquearcamposine();		 
				}, 6000);
			}
			break;
		case VERIFICACION_TRABAJADOR:
			if (iRespuesta == 1) {
				iRetorno = 2;
				iContador = 0;
				obtenermensajeVerificacion(0);
				banderabancoppel = 1;
				desboqueoCajasdeTexto(banderabancoppel);
				$('#cboTipoComprobante').attr('disabled', false);
				$('#cboEstadoCivil').attr('disabled', false);
			}
			else {
				iContador++;
				obtenermensajeVerificacion(iRespuesta);
			}
			break;
		case VERIFICACION_EMPLEADO:
			if (bPrevalidacionEnCurso) {
				if (iRespuesta == 1) {
					iBanderaEntrada = 1;
					//llamado de servicio
					iTipoPersona = 2;
					iTipoServicio = 9911;
					iTipoOperacion = "0501";
					ObtenerRespuestaXML(sCurpTrabajador, sCurpEnrolado, iTipoPersona, iTipoServicio, iTipoOperacion);
				}
				else {

					if (iContador > 2) {
						iOpcion = 1;
						$('#txtCurp').val('');
						$('#txtCurp').focus();
						iContador = 0;
						$('#btnConsultaCurp').attr('disabled', false);
						document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

						$('#bRenapoSol').attr('disabled', false);
						$('#bRenapoTitular').attr('disabled', false);
						document.getElementById("bRenapoSol").classList.add("habilitarImg");
						document.getElementById("bRenapoTitular").classList.add("habilitarImg");

						$('.content-loader').hide();
						sTitle = "";
						bPrevalidacionEnCurso = false;
						mensaje = "Promotor: Favor de ingresar nuevamente la curp del trabajador";
						$('#txtCurp').attr('disabled', false);
						mostrarMensaje(mensaje, sTitle);
						inicializarvariablesServicio();
						limpiarcampos();
					}
					else {
						iContador++;
						mensajesErrorConsultaVerificacion(iRespuesta);
					}
				}
			}
			else if (iRespuesta == 1) {
				//SERVICIO QUE SE EJECUTARA
				iContador = 0;
				iTipoPersona = 3;
				iTipoServicio = 9910;
				if(bSinExpe){
					iRetorno=0;
				}
				//iTipoOperacion = "0401";
				iTipoOperacion = "0407";
				ObtenerRespuestaXML(sCurpTrabajador, sCurpEnrolado, iTipoPersona, iTipoServicio, iTipoOperacion);
			}
			else {
				iContador++;
				obtenermensajeVerificacion(iRespuesta);
			}
			break;
		case ELIMINAR_PROCESO:
			if (iRespuesta == 1) {
				setTimeout(function () {
					//Se ejecuta la aplicacion DIGIDOCTOSCLIENTE.EXE
					iOpcion = 0;
					iOpcion = DIGITALIZAR;
					sParametrosDigi = "";
					sParametrosDigi = "1 3 " + folio + " " + iEmpleado;
					opcionejecutaDigi(sParametrosDigi, DIGITALIZAR);
				}, 4000);
			}
			else {
				setTimeout(function () {
					//Se ejecuta la aplicacion DIGIDOCTOSCLIENTE.EXE
					iOpcion = 0;
					iOpcion = DIGITALIZAR;
					sParametrosDigi = "";
					sParametrosDigi = "1 3 " + folio + " " + iEmpleado;
					opcionejecutaDigi(sParametrosDigi, DIGITALIZAR);
				}, 4000);
			}

			break;
		case DIGITALIZAR:
			$('.content-loader').hide();

			if (iRespuesta == 1) {
				actualizarestatusprocesosolconstancia();
				$('#bAceptar').attr('disabled', true);
				document.getElementById("bAceptar").classList.add("deshabilitarImg");

				$('#bDigitalizar').attr('disabled', true);
				document.getElementById("bDigitalizar").classList.add("deshabilitarImg");

				$('#bContinuar').attr('disabled', false);
				document.getElementById("bContinuar").classList.add("habilitarImg");

				//891 ---------------------
				//envioctdimg();

				// ejecutarObtenerRespuesta();
			}
			else if (iRespuesta == 2) {
				$('#bDigitalizar').attr('disabled', false);
				document.getElementById("bDigitalizar").classList.add("habilitarImg");
			}
			else {
				sTitle = "";
				sMensaje = "No se finalizó la digitalizacion con exito, favor de comunicarse con Mesa de Ayuda";
				mostrarMensaje(sMensaje, sTitle);
				$('#bDigitalizar').attr('disabled', false);
				document.getElementById("bDigitalizar").classList.add("habilitarImg");
			}
			break;
		case ESTA_CONECTADO_SIGNPAD:
			$('.content-loader').hide();
			if (iRespuesta == -6) {
				document.getElementById("chkfirma").style.visibility = "hidden";//++
				document.getElementById("labelfirma").style.visibility = "hidden";//++
				ESTA_CONECTADO_SIGNPAD = 0;
			}
			if (iRespuesta == 1) {
				iSignpadConectado = 1;
			}
			break;
		case TOMAR_FIRMA:
			$('.content-loader').hide();
			switch (iRespuesta) {
				case "1":
					switch (iFirmaSolicitada) {
						case NOMBRE_TRAB:
							$('.content-loader').hide();

							if (($('#cboTipoConstancia').val() === "26") || ($('#cboTipoConstancia').val() === "33")) {
								sMensaje = "PROMOTOR, comunícale lo siguiente al Trabajador: Se le va capturar su firma digital que se plasmará en la solicitud de constancia para Registro";
							}
							else {
								sMensaje = "PROMOTOR, comunícale lo siguiente al Trabajador: Se le va capturar su firma digital que se plasmará en la solicitud de constancia para Traspaso";
							}
							MuestraDialogoMensajeSignpad(sMensaje, function () {
								sParametros = "1 1 " + "SCTR_" + folio + "_FTRAB";
								if (OSName == "Android") {
									opcionejecutaFirma(sParametros);
								} else {
									iFirmaSolicitada = FIRMA_TRAB;
									opcionejecuta(sParametros);
								}

							});
							break;
						case FIRMA_TRAB:
							$('.content-loader').hide();
							iFirmaSolicitada = FIRMA_PROM;
							if (($('#cboTipoConstancia').val() === "26") || ($('#cboTipoConstancia').val() === "33")) {
								sMensaje = "PROMOTOR, Favor de capturar tu firma digital que se plasmará en la solicitud de constancia para Registro";
							}
							else {
								sMensaje = "PROMOTOR, Favor de capturar tu firma digital que se plasmará en la solicitud de constancia para Traspaso";
							}
							MuestraDialogoMensajeSignpad(sMensaje, function () {
								sParametros = "1 1 " + "SCTR_" + folio + "_FPROM";
								if (OSName == "Android") {
									opcionejecutaFirma(sParametros);
								} else {
									opcionejecuta(sParametros);
								}
							});
							break;
						case FIRMA_PROM:
							$('.content-loader').hide();
							MuestraDialogoMensajeSignpad("Las firmas se capturaron de manera exitosa", function () {
								imprimirSolContanciaReistroTraspaso();
							});
							$('#bDigitalizar').attr('disabled', false);
							document.getElementById("bDigitalizar").classList.add("habilitarImg");
							break;
						default:
							break;
					}
					break;
				case "-1":
					MuestraDialogoMensajeSignpad("Se cancelo la captura de firmas, Favor de tomarlas nuevamente");
					break;
				case 1:
					switch (iFirmaSolicitada) {
						case NOMBRE_TRAB:
							if (($('#cboTipoConstancia').val() === "26") || ($('#cboTipoConstancia').val() === "33")) {
								sMensaje = "PROMOTOR, comunícale lo siguiente al Trabajador: Se le va capturar su firma digital que se plasmará en la solicitud de constancia para Registro";
							}
							else {
								sMensaje = "PROMOTOR, comunícale lo siguiente al Trabajador: Se le va capturar su firma digital que se plasmará en la solicitud de constancia para Traspaso";
							}
							MuestraDialogoMensajeSignpad(sMensaje, function () {
								sParametros = "1 1 " + "SCTR_" + folio + "_FTRAB";
								if (OSName == "Android") {
									iFirmaSolicitada = FIRMA_TRAB;
									opcionejecutaFirma(sParametros);
								} else {
									opcionejecuta(sParametros);
								}

							});
							break;
						case FIRMA_TRAB:
							iFirmaSolicitada = FIRMA_PROM;
							if (($('#cboTipoConstancia').val() === "26") || ($('#cboTipoConstancia').val() === "33")) {
								sMensaje = "PROMOTOR, Favor de capturar tu firma digital que se plasmará en la solicitud de constancia para Registro";
							}
							else {
								sMensaje = "PROMOTOR, Favor de capturar tu firma digital que se plasmará en la solicitud de constancia para Traspaso";
							}
							MuestraDialogoMensajeSignpad(sMensaje, function () {
								sParametros = "1 1 " + "SCTR_" + folio + "_FPROM";
								if (OSName == "Android") {
									iFirmaSolicitada = FIRMA_PROM;
									opcionejecutaFirma(sParametros);
								} else {
									opcionejecuta(sParametros);
								}
							});
							break;
						case FIRMA_PROM:
							MuestraDialogoMensajeSignpad("Las firmas se capturaron de manera exitosa", function () {
								imprimirSolContanciaReistroTraspaso();
							});
							$('#bDigitalizar').attr('disabled', false);
							document.getElementById("bDigitalizar").classList.add("habilitarImg");
							break;
						default:
							break;
					}
					break;
				case 0:
					MuestraDialogoMensajeSignpad("Se cancelo la captura de firmas, Favor de tomarlas nuevamente");
					break;
				default:
					MuestraDialogoMensajeSignpad("Ocurrio un error al capturar la Firma. error: ".iRespuesta);
					break;
			}
			break;
		case SUBIR_ARCHIVO:
			switch (iOpcionHuella) {
				case PUBLICA_HUELLAS_IZQ:
					if (iRespuesta == 1) {
						iContHuellas = 0;
						parametrosHuellas = nombreArchivoDer + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_ENROLAMIENTO + " " + ipCluster + " 0";
						publicahuellasder(parametrosHuellas);
					}
					else {
						if (iContHuellas < 3) {
							parametrosHuellas = nombreArchivoIzq + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_ENROLAMIENTO + " " + ipCluster + " 0";
							publicahuellasizq(parametrosHuellas);
						}
					}
					break;
				case PUBLICA_HUELLAS_DER:
					if (iRespuesta == 1) {
						guardarHuellasIndices(nombreArchivoIzq, nombreArchivoDer, sCurpTrabajador, iEmpleado);
						iContHuellas = 0;
					}
					else {
						if (iContHuellas < 3) {
							parametrosHuellas = nombreArchivoDer + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_ENROLAMIENTO + " " + ipCluster + " 0";
							publicahuellasder(parametrosHuellas);
						}
					}
					break;

				default:
					break;
			}
			break;
		default:
			break;
	}
}


function validarConsumoServicio0101() {
	var arresp = verificaExpedientePermanente();
	bRealizar0407 = verificaEjecucionServicio();
	if (arresp['indicadordecimo'] == '1') { // termino proceso

		sMensajeVerExpIden = "De la revisión efectuada a su solicitud de registro, detectamos "
			+ "que la CURP que desea registrar corresponde a aportaciones del ISSSTE con "
			+ "Régimen Pensionario del Décimo Transitorio, motivo por el cual y con "
			+ "fundamento en el Artículo Décimo Primero de los Transitorios de la Ley del "
			+ "ISSSTE los recursos de las cuentas bajo este Régimen deben ser "
			+ "administrados exclusivamente por el PENSIONISSSTE, por lo anterior deberá "
			+ "acudir a realizar el registro de su cuenta en las oficinas de atención del Fondo "
			+ "Nacional de Pensiones, cuya localización podrá consultarla en "
			+ "https://www.gob.mx/pensionissste o llamar al teléfono 50620555 en la Ciudad "
			+ "de México, Lada sin costo 01800 400 1000 o 01800 400 2000";
		mensajeVerExpIden(sMensajeVerExpIden);
		bRealizar0407 = false;
		return;

	}
	else if (arresp['estatusexpiden'] == '05' && (arresp['indicadordecimo'] == '' || arresp['indicadordecimo'] == '0') && (arresp['curpduplicado'] == '' || arresp['curpduplicado'] == '0')) {
		if (bRealizar0407) {
			elminarinformacionServicio();
			obtenermensajeVerificacion(0);
		}
	}
	else {
		bRealizar0407 = false;
		$('.content-loader').hide();
	}
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e8; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
}

function validarCServicio0101() { 

	$('.content-loader').show();
	var itotalintentos = 5;
		for(var i=0;i < 5; i++){
			arrConsumo  = verificaExpedientePermanente();
			if(arrConsumo['estatusexpiden'] == null && arrConsumo['estatusenrol'] == null )		   
			{				   
				bDetenerProceso = false;		   
				sleep(10000);
			}
			else{
				$('.content-loader').hide();
				i = itotalintentos;
				bloquearCamposServicio(); // desahbilitar campos, algunos servicios los habilita  
			}
	}
	if (arrConsumo['estatusexpiden'] == '05' && arrConsumo['estatusenrol'] == '05') {
		//Autenticacion dactilar -- Servicio 0101, Gestor dactilar, Servicio 0407, Servicio 0501 (Prevalidador).
		obtenernumerofolio(); // Obtener el folio de la solicitud
		fnGestorBandera();    // Preguntar si el Flag esta encendido o apagado
		bConExpe = true;
		bContinuarProceso=false;
		iOpcionGestor = 1;
		$('.content-loader').hide();
	}
	else if (arrConsumo['estatusexpiden'] == '05' && arrConsumo['estatusenrol'] != '05' ) {
		//Servicio 0101, Servicio 0407, INE/BANCOPPEL, Servicio 0501 (Prevalidador).
		bSinExpe = true;
		bContinuarProceso=false;
		$('.content-loader').hide();			
	} 
	else if(arrConsumo['estatusexpiden'] != null && arrConsumo['estatusenrol'] != null )  {
		//Servicio 0101, INE/BANCOPPEL, Servicio 0501 (Prevalidador).
		bFnormal = true;
		bContinuarProceso=false;
		$('.content-loader').hide();
		bloquearCamposServicio(); // desahbilitar campos, algunos servicios los habilita
		fnpreguntaconsulta(); // INE/BANCOPPEL
		bDetenerProceso = true;
		
	}
	else{
		// para los casos de la respuesta null del resultado (estatusexpiden y estatusenrol), el flujo continuara por dactilar.
		//Autenticacion dactilar -- Servicio 0101, Gestor dactilar, Servicio 0407, Servicio 0501 (Prevalidador).
		obtenernumerofolio(); // Obtener el folio de la solicitud
		fnGestorBandera();    // Preguntar si el Flag esta encendido o apagado
		bConExpe = true;
		bContinuarProceso=false;
		iOpcionGestor = 1;
		$('.content-loader').hide();
	}	
}

//Metodo para refrescar la pagina actual
function refrescar() {
   location.reload(true)
}

function obtenernumerofolio() {
	
	var arrdatos = Array();
	$.ajax(
		{
		data: { opcion: 16 },
		success: function (data) {
			
			arrdatos = eval(data);
			iFolioGestor  = arrdatos;
		}
	});
	return arrdatos;
}

function fnGestorBandera() {
	
    datos = {
        opcion: 98,
        iflag: iflag,
    };
    $.ajax({
        async: false,
        cache: false,
        data: datos,
        url: 'php/constanciaafiliacion.php',
        type: 'post',
        dataType: 'json',
        success: function (data) {
            if (data.respuesta == '1'){
                iFlagGestor = 1;
            }
        },
        error: function (a, b, c) {}
    });
}

function  Autenticadactilar(){
	
	var cadenaF = '';
	
	if (iAutdactilar == 1){
		cadenaF += "opcion=97&iFolioGestor=" + iFolioGestor + "&curp=" + sCurpTrabajador + "&iopc=" + 1;
	}
	else if (iAutdactilar == 2){
		cadenaF += "opcion=97&iFolioGestor=" + folioSolisitud + "&curp=" + sCurpTrabajador + "&iopc=" + 2;
	}
	else if (iAutdactilar == 3){
		cadenaF += "opcion=97&iFolioGestor=" + folioSolisitud + "&curp=" + sCurpTrabajador + "&iopc=" + 3;
	}
	$.ajax({
		async: false,
		cache: false,
		data: cadenaF,
		url: 'php/constanciaafiliacion.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
			respuesta=data;
			if(iAutdactilar == 1){
				iSelloBiometrico = respuesta.selloverificacionbiometricavoluntram; 
				sRespBiometrico = respuesta.resultadoverificacionvoluntram;
				iBanderaBiometrico = respuesta.banderadiag;
				console.log('iSelloVoluntad : '+iSelloBiometrico);
				console.log('sRespBiometrico : '+sRespBiometrico);
			}
			if(iAutdactilar == 3){
				console.log('Se realizo el update del folio implicacion : '+iBanderaBiometrico);
			}
			
			if(iSelloBiometrico > 0 && sRespBiometrico == '01'){
				iResSellos = true;
			}		
		},
		error: function(a, b, c){
			console.log("Error" + a + b + c);
		}
	});
}

function fnpreguntaconsulta(){
	banderaTraspaso = 1;
	tieneautenticacionbancoppel();
	fntieneautenticacionine();
	console.log(cuentaconAutIne);
	console.log(cuentaconAutBancoppel);
	if (cuentaconAutIne == false || cuentaconAutBancoppel == false) {
		if (noExcepcion) {
			if (numeroConsulta == 1) {
				banderaIne = 1;
				banderaautbancoppel = 1;
				preguntaconsulta();	
			}
		}
	}
}

function fnConsultaServicio0101(){
	
	sCurpTrabajador = $('#txtCurp').val();
	bCservicio0101 = true;
	sTipoSolicitud = 27;
	llamadoApplet();

}

function fnDetonarGestor4Huellas(){
	
	$('.content-loader').show();
	var forzargestor = true;
	iOpcion = GESTOR_DE_SELLOS;    
	iOpcionGestor = 2;
	var sParametrosGest = " 3 2 " + sCurpTrabajador + " " + iEmpleado + " "+ iFolioGestor + " 3 102 1";
	opcionejecuta(sParametrosGest);
	
	if(iFolioGestor > 0){
		folioSolisitud = iFolioGestor;
	} 
}

function validarCNNS() {

	var cboTipoAfil = document.getElementById('cboTipoConstancia').value;
	var txtCurp = document.getElementById('txtCurp').value;
	var txtApellidoPaterno = document.getElementById('txtApellidoPaterno').value;
	var txtNombre = document.getElementById('txtNombre').value;

	var txtcnss = document.getElementById('txtNss');
	txtcnss.value = txtcnss.value.toUpperCase();	
	var txtNSS = $('#txtNss').val();

	if (cboTipoAfil == 27)
	{
		if(txtNSS.length == 11){
			
			if(txtCurp != '' && txtApellidoPaterno != '' && txtNombre != ''){
				
				$('#btnConsultaCurp').attr("disabled",false);
			}
		}
		else{
			$('#btnConsultaCurp').attr("disabled",true);
		}
	}
}

function bloquearCamposDatosTrabajador(){
	$("#cboTipoConstancia").attr("disabled", true);
	$("#cboTipoSolicitante").attr("disabled", true);
	$("#cboExcepciones").attr("disabled", true);
	$("#cboTipoRecepcion").attr("disabled", true);
	$("#txtCurp").attr("disabled", true);
	$("#txtNss").attr("disabled", true);
	$('#txTelCelular').attr("disabled", true);
	$('#cboCompTelef').attr("disabled", true);
	$('#cboTipoComprobante').attr("disabled", true);
	$('#dtpFechaComprobante').attr("disabled", true);
	$('#txtClienteCoppel').attr("disabled", true);
	$("#cboEstadoCivil").attr("disabled", true);
	$("#cboTipoTelefono").attr("disabled", true);
	$("#txtTelContacto").attr("disabled", true);
	$("#cboCompTelefCont").attr("disabled", true);
}

function bloquearCamposServicio(){
	
	$("#cboTipoConstancia").attr("disabled", false);
	$("#cboTipoSolicitante").attr("disabled", true);
	$("#cboExcepciones").attr("disabled", true);
	$("#cboTipoRecepcion").attr("disabled", true);
	$("#txtCurp").attr("disabled", true);
	$("#txtCurp2").attr("disabled", true);
	//datos trabajador
	$("#txtApellidoPaterno").attr("disabled", true);
	$('#txtApellidoMatero').attr("disabled", true);
	$('#txtNombre').attr("disabled", true);
	$("#txtNss").attr("disabled", true);
	$('#txTelCelular').attr("disabled", true);
	$('#cboCompTelef').attr("disabled", true);
	$('#cboTipoComprobante').attr("disabled", true);
	$('#dtpFechaComprobante').attr("disabled", true);
	$('#txtClienteCoppel').attr("disabled", true);
	$("#cboEstadoCivil").attr("disabled", true);
	//Datos contacto
	$("#cboTipoTelefono").attr("disabled", true);
	$("#txtTelContacto").attr("disabled", true);
	$("#cboCompTelefCont").attr("disabled", true);
	
	$("#chkfirma").attr("disabled", true);
	//botones
	$('#bAceptar').attr('disabled', true);
	$('#bAceptar').removeClass("habilitarImg").addClass("deshabilitarImg");
}

function bloquearCamposProceso(){	

	setTimeout(() => { 
		$("#cboTipoConstancia").attr("disabled", true);
		$("#cboTipoSolicitante").attr("disabled", true);
		$("#cboExcepciones").attr("disabled", true);
		$("#cboTipoRecepcion").attr("disabled", true);
		$("#txtCurp").attr("disabled", true);
		$("#txtCurp2").attr("disabled", true);
			
		//datos trabajador
		$("#txtApellidoPaterno").attr("disabled", true);
		$('#txtApellidoMatero').attr("disabled", true);
		$('#txtNombre').attr("disabled", true);
		$("#txtNss").attr("disabled", true);
		$('#txTelCelular').attr("disabled", true);
		$('#cboCompTelef').attr("disabled", true);
		$('#cboTipoComprobante').attr("disabled", true);
		$('#dtpFechaComprobante').attr("disabled", true);
		$('#txtClienteCoppel').attr("disabled", true);
		$("#cboEstadoCivil").attr("disabled", true);
			
		//Datos contacto
		$("#cboTipoTelefono").attr("disabled", true);
		$("#txtTelContacto").attr("disabled", true);
		$("#cboCompTelefCont").attr("disabled", true);
			
		$("#chkfirma").attr("disabled", true);
		//botones
			
		$('#bRenapoTitular').attr('disabled', true);
		$('#bRenapoTitular').removeClass("habilitarImg").addClass("deshabilitarImg");
			
		$('#btnConsultaCurp').attr('disabled', true);
		$('#btnConsultaCurp').removeClass("habilitarImg").addClass("deshabilitarImg");
		
		$('#bAceptar').attr('disabled', true);
		$('#bAceptar').removeClass("habilitarImg").addClass("deshabilitarImg");
		
		$('#bGrabacion').attr('disabled', true);
		$('#bGrabacion').removeClass("habilitarImg").addClass("deshabilitarImg");
		
		$('#bDigitalizar').attr('disabled', true);
		$('#bDigitalizar').removeClass("habilitarImg").addClass("deshabilitarImg");
			
		$('#bContinuar').attr('disabled', true);
		$('#bContinuar').removeClass("habilitarImg").addClass("deshabilitarImg");
			
	}, 5000);	
}
function HabilitarCamposServicioPrev(){

	$('#txTelCelular').attr("disabled", false);
	$('#cboCompTelef').attr("disabled", false);
	$('#cboTipoComprobante').attr("disabled", false);
	$('#dtpFechaComprobante').attr("disabled", false);
	$('#txtClienteCoppel').attr("disabled", false);
	$("#cboEstadoCivil").attr("disabled", false);
	$("#cboTipoTelefono").attr("disabled", false);
	$("#txtTelContacto").attr("disabled", false);
	$("#cboCompTelefCont").attr("disabled", false);
	$("#bAceptar").attr("disabled", false);
	
	$('#bRenapoTitular').attr('disabled', true);
	$('#bRenapoTitular').removeClass("habilitarImg").addClass("deshabilitarImg");
	
	$('#btnConsultaCurp').attr('disabled', true);
	$('#btnConsultaCurp').removeClass("habilitarImg").addClass("deshabilitarImg");
			
}

/*Funcion que permite consultar en la tabla solconstancia, los datos apartir de la curp
cuando el estatusproceso es igual a 0, obtiene la informacion de la bd para llenar el formulario, habilita los
botones de imprimir, cancelar y digitaliza. inavilita el boton guaradar para evitar guaradar un registro existente.
*/
function consultasolconstancia() {
	var sCurp = $('#txtCurp').val();
	var cadena = '';
	cadena += "opcion=10&curp=" + sCurp + "&estatusproceso=3";
	if (BanderaEstatus != 7) {
		sMensaje = "Promotor: El trabajador ya tiene un registro de captura de datos para afiliación, favor de continuar con el enrolamiento.";
		mostrarMensaje(sMensaje, '');
	}

	$.ajax(
		{
			data: cadena,
			success: function (data) {
				var datos = eval(data);
				folio = datos.folio;
				iFolioSol = datos.folio;
				iSelloVerificacion = '1';
				LlenaFormulario(datos);

				if (iSignpadConectado == 1) {
					if ($("#chkfirma").is(":checked")) {
						ifirma = 2;
					}
					else {
						ifirma = 1;
					}
				}
				else {
					ifirma = 0;
				}
			}
		}
	);
}

//funcion que permite llenar los datos del formulario
function LlenaFormulario(datos) {
	iBanderaFlujo = 0;

	if (datos.nss.length == 0 || datos.nss.trim() == '') {
		if (datos.tipotraspaso == 1) {
			$('#cboTipoConstancia').val(1);
		}
		else {
			$('#cboTipoConstancia').val(527);
		}
	} else {
		$('#cboTipoConstancia').val(datos.tipoconstancia);
	}

	$('#txtCurp').val(datos.curp);
	$('#txtApellidoPaterno').val(datos.appaterno);
	$('#txtApellidoMatero').val(datos.apmaterno);
	$('#txtNombre').val(datos.nombres);
	$('#txtNss').val(datos.nss);
	$('#txTelCelular').val(datos.telefonocelular);
	$('#dtpFechaComprobante').val(datos.fechacomprobante);
	$('#txtClienteCoppel').val(datos.noclientecoppel);
	$('#txtTelContacto').val(datos.telefonocontacto);
	$("#cboTipoTelefono").val(datos.tipotelefono);
	$("#cboCompTelef").val(datos.compatelefonica);
	$("#cboTipoComprobante").val(datos.tipocomprobante);
	$("#cboEstadoCivil").val(datos.estadocivil);
	$("#cboCompTelefCont").val(datos.comptelefonicacontacto);

	if (datos.telefonocelular.length > 0 || datos.telefonocelular != "") {
		$('#txtNombre').attr("disabled", true);
		$('#txtApellidoMatero').attr("disabled", true);
		$('#txtApellidoPaterno').attr("disabled", true);

		$('#cboTipoRecepcion').attr("disabled", true);
		$('#txTelCelular').attr("disabled", true);
		$('#cboCompTelef').attr("disabled", true);
		$('#cboTipoComprobante').attr("disabled", true);
		$('#txtNss').attr("disabled", true);
		$('#txtClienteCoppel').attr("disabled", true);
		$('#cboEstadoCivil').attr("disabled", true);
		$('#cboTipoTelefono').attr("disabled", true);
		$('#txtTelContacto').attr("disabled", true);
		$('#cboCompTelefCont').attr("disabled", true);
		$("#btnConsultaCurp").removeClass("habilitarBtn");
		$("#btnConsultaCurp").addClass("deshabilitarBtn");
		$("#btnConsultaCurp").attr("disabled", true);
	}

	if (BanderaEstatus == 7 && (iTipoAfiliacion == 26 || iTipoAfiliacion == 33)) {
		$('#bGrabacion').attr('disabled', false);
		document.getElementById("bGrabacion").classList.add("habilitarImg");
	}

	validarEjecucionServicio(datos.curp);
}

//function muestra un dialogo emergente
function MuestraDialogoMensaje(mensaje) {

	//mostrarCombos(false);
	$("#divMensaje").dialog
		({
			title: 'Captura de Datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			//open: function()
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{

				"Aceptar": function () {
					//mostrarCombos(true);
					$('#txtCurp').focus();
					$(this).dialog("close");
				}/*,

				"Reenvío contraseña" : function()
				{
					//mostrarCombos(true);
					$(this).dialog( "close" );
					//var win = window.open('http://www.consar.gob.mx', '_blank');
					EjecutaESarSolconstancia();
 	 				win.focus();
				}*/
			}

		});
}

function EjecutaESarSolconstancia() {
	ejecutarPaginaEsarSolconstancia('C:\\sys\\firefoxportable\\navegadorAfore.exe https://www.e-sar.com.mx/PortalEsar/public/iniciarReenvioContrasenia.do');
}

function ejecutarPaginaEsarSolconstancia(strPrograma) {
	var theShell = new ActiveXObject("WScript.Shell");
	var x = theShell.run(strPrograma, 4, true);
}

//function muestra un dialogo emergente
function MuestraDialogoMensajeSignpad(mensaje, fucntionAceptar) {

	//mostrarCombos(false);
	$("#divMensaje").dialog
		({
			title: 'Captura de datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			//open: function()
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{

				"Continuar": function () {
					$(this).dialog("close");
					if (typeof fucntionAceptar == 'function') {
						fucntionAceptar();
					}
				}
			}

		});
}

//function que muestra un dialogo con la descripcion del rechazo()
function MuestraDialogoMensajeRechazo(mensaje) {
	//mostrarCombos(false);
	$("#divMensaje").dialog
		({
			title: 'Captura de datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"Aceptar": function () {
					//mostrarCombos(true);
					$(this).dialog("close");
					if (BanderaEstatus == 7) {
						consultasolconstancia();
						BanderaEstatus = 8;
					}

				}
			}
		});
}

//Convierte a mayusculas el valor de la caja de texto llama a al metodo
function aMayusculas(e) {
	var sCadena = $(e.currentTarget).val();

	sCadena = sCadena.toUpperCase();

	$(e.currentTarget).val(sCadena);
}

//limpia todos los campos del formulario
function limpiarcampos() {
	$('#txtCurp').val('');
	$('#txtApellidoPaterno').val('');
	$('#txtApellidoMatero').val('');
	$('#txtNombre').val('');
	$('#txtNss').val('');
	$('#txTelCelular').val('');
	$('#dtpFechaComprobante').val('');
	$('#txtClienteCoppel').val('');
	$('#txtTelContacto').val('');
	$('#txtCodPostal').val('');
	$("#cboTipoTelefono").val(-1);
	$("#cboCompTelef").val(-1);
	$("#cboTipoComprobante").val(-1);
	$("#cboCompTelefCont").val(-1);
	$("#cboTipoConstancia").val(-1);
	$("#cboTipoSolicitante").val(-1);
	$("#cboExcepciones").val(-1);
	$("#cboTipoRecepcion").val(-1);
	$("#cboEstadoCivil").val(-1);
	$("#cboTipoConstancia").attr("disabled", false);
}

function formatearFecha(piTipoFormato, pcFechaEntrada) {
	var cFechaSalida = null;
	if (pcFechaEntrada.length > 0) {
		if (piTipoFormato == 1) //LA DEVUELVE EN: YYYYMMDD
		{
			cFechaSalida = pcFechaEntrada.substr(6, 4) + pcFechaEntrada.substr(3, 2) + pcFechaEntrada.substr(0, 2);
		}
		else if (piTipoFormato == 2) //LA DEVUELVE EN: YYYY-MM-DD
		{
			cFechaSalida = pcFechaEntrada.substr(6, 4) + '-' + pcFechaEntrada.substr(3, 2) + '-' + pcFechaEntrada.substr(0, 2);
		}
		else if (piTipoFormato == 3) //LA DEVUELVE EN: DD/MM/YYYY
		{
			//cFechaSalida = pcFechaEntrada.substr(0, 2) + pcFechaEntrada.substr(3, 2) + pcFechaEntrada.substr(6, 4);
			cFechaSalida = pcFechaEntrada.substr(8, 2) + '/' + pcFechaEntrada.substr(5, 2) + '/' + pcFechaEntrada.substr(0, 4);
		}
		else if (piTipoFormato == 4) //LA DEVUELVE EN: MDY(MM,DD,YYYY)
		{
			cFechaSalida = pcFechaEntrada.substr(8, 2) + '-' + pcFechaEntrada.substr(5, 2) + '-' + pcFechaEntrada.substr(0, 4);

		}

	}
	return cFechaSalida;
}

function validarEjecucionServicio(pCurp) {
	var cadena = '';
	var bRetorna = false;
	cadena += "opcion=12&curp=" + pCurp;

	$.ajax(
		{
			data: cadena,
			success: function (data) {
				iEstatusContingente = data.estatuscontingente;

				if (data.estatuscontingente != '2' || data.estatuscontingente != 2) {
					//se mandan ejecutar los servicios
					sTitle = " ";
					bPrevalidacionEnCurso = false;
					//sMensaje = "Promotor: Favor de colocar tu huella sobre el sensor.";
					//mostrarMensaje(sMensaje, sTitle);
					//se manda llamar el metodo para saber cuando se obtuvo respuesta de los servicios
					mostrarmensaje2();
				}
			}
		}
	);
}

//METODOS IMPLEMENTADOS POR ANDRES

function obenerResupuestaServicios(iFolioConsulta, iFolioVerificacion) {
	var tiempo = 40000;
	iTiempoEspera += tiempo;
	var respuesta = respuestaServicios(iFolioConsulta, iFolioVerificacion);

	if (respuesta == 0)//-->cuando haya respuesta de solo un servicio debe de seguir dando vueltas asta que se llegue al tm
	{
		setTimeout(function () {
			SeguirCaminoAlterno();
		}, tiempo);
	}
	else if ((iTiempoEspera < 200000 && respuesta > 0)) {
		setTimeout(function () {
			SeguirCaminoAlterno();
		}, tiempo);
	}
	else {

		grabarLogJs(sConsultaEstatus, sConsultaDiagnostico, sVerificacionEstatus, sVerificacionDiganostico, sConsultaExpediente, sConsultaEnrolamiento, iSelloVerificacion, sTipoSolicitud);
		if (sConsultaEstatus == '') {
			SeguirCaminoAlterno();
		}
		if (((sConsultaEstatus == 'OK') || (sConsultaEstatus == '01')) || ((sConsultaEstatus == 'RE' && sConsultaDiagnostico == '049') && (sTipoSolicitud == 26 || sTipoSolicitud == 33))) {
			if (sVerificacionEstatus == '') {
				SeguirCaminoAlterno();
			}
			if ((sVerificacionEstatus == 'OK') || (sVerificacionEstatus == '01')) {
				if (sConsultaExpediente != '02') {
					if (iSelloVerificacion > 0) {
						if (iEstatusContingente == 2) {
							//ejecutarinvocacion(1);
						}
						else {
							//ejecutarinvocacion(0);
						}
					}
				}
				else {
					sTitle = "";
					sMensaje = "Promotor: El trabajador cuenta con un proceso de Enrolamiento que no ha concluido. \n Favor de esperar 4 días hábiles para volver a realizar la consulta";
					//Ejecutar Metodo Cerrar Navegador
					iCodCerrarNavegador = 'EMCN';
					mostrarMensaje(sMensaje, sTitle);
					$('#btnConsultaCurp').attr('disabled', false);
					document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

					$('#bRenapoTitular').attr('disabled', false);
					$('#bRenapoSol').attr('disabled', false);
					document.getElementById("bRenapoTitular").classList.add("habilitarImg");
					document.getElementById("bRenapoSol").classList.add("habilitarImg");
				}
			}
			else {
				llamadoErrores(sVerificacionDiganostico);
				$('#btnConsultaCurp').attr('disabled', false);
				document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

				$('#bRenapoTitular').attr('disabled', false);
				$('#bRenapoSol').attr('disabled', false);
				document.getElementById("bRenapoTitular").classList.add("habilitarImg");
				document.getElementById("bRenapoSol").classList.add("habilitarImg");
			}
		}
		else {
			llamadoErrores(sConsultaDiagnostico);
			$('#btnConsultaCurp').attr('disabled', false);
			document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

			$('#bRenapoTitular').attr('disabled', false);
			$('#bRenapoSol').attr('disabled', false);
			document.getElementById("bRenapoTitular").classList.add("habilitarImg");
			document.getElementById("bRenapoSol").classList.add("habilitarImg");
		}
	}

}

function SeguirCaminoAlterno() {
	iContadorAlterno++;

	if (iContadorAlterno == 6) {
		$('.content-loader').hide();
		sTitle = "";
		sMensaje = "Promotor: Se presento un error al ejecutar los servicios biometricos, favor de reportarlo a Mesa de Ayuda";
		mostrarMensaje(sMensaje, sTitle);
		inicializarvariablesServicio();
	}
	else {
		obenerResupuestaServicios(iFolioConsulta, iFolioVerificacion);
	}
}

//funcion para desplegar un mensaje
function mostrarMensaje(mensaje, sTitle) {
	$('.content-loader').hide();
	$("#divMensaje").dialog
		({
			title: sTitle,
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();

				if (Array.isArray(mensaje)) {
					var message = ""

					for (i = 0; i <= mensaje.length; i++) {
						if (mensaje[i] == undefined) {
							mensaje.splice(i, 1);
							continue;
						}

						message += mensaje[i] + "<br/> <br/>";
					}

					$(this).html("<p>" + message + "</p>");
				}
				else {
					$(this).html("<p>" + mensaje + "</p>");
				}
			},
			buttons:
			{
				"Aceptar": function () {
					if (bPrevalidacionEnCurso && sTitle != "Android") {
						$(this).dialog("close");
						iOpcion = "";
						sParametrosDigi = "";
						llamadoApplet();
					}
					else if (sTitle == "") {
						$(this).dialog("close");

						
						
					if(bError){
						$(this).dialog("close");
					}else if( codError == 'P14' ||  codError == 'P13' ||  codError == 'P01' ||  codError == 'P02' ||  codError == 'P03' ||  codError == 'P04' ||   codError == 'P05' || 
					     	codError == 'P12' || codError == 'P07'|| codError == 'P08'|| codError == 'P10'|| codError == 'P11'|| codError == 'P06' || codError == 'P09' ){
							
							//limpiarCamposPrevalidacion();
							$(this).dialog("close");
					}
					
					
						/*if (iCodEjecutarVideo == 'VID')
						{
							consultaEstatusenrol();
						}*/
						//saber cuando la pagina de enrolamiento trabajador fue cerrarda o el enrolamiento se cancelo y mandarse llamar la pagina
						if (iCodEjecutarEnrol == 'EET') {
							marcarLog("CURP: " + sCurpTrabajador + " - VA A OBTENER EL ENLACE PARA EL ENROLAMIENTO");//logs
							
							obtenerenlace();
						}
						else if (iCodEjecutarVideo == 'VID') {
							consultaEstatusenrol();
						}
						/*else if(mensaje == "Promotor: Se presento un error al solicitar el servicio de la contraseña, favor de reportarlo a Mesa de Ayuda.")
						{
							ejecutarinvocacion(0);
						}*/
						else if (iCodCerrarNavegador == 'EMCN') {
							cerrarNavegador();
						}
						else {
							if (Array.isArray(field)) {
								$.each(field, function (index, value) {
									value.attr('disabled', false);
									field = [];
								});

								errorMessages = [];

								if (guardarSolConstancia || limpiarVariables) {
									limpiarVariables = false;
									inicializarVariables();
								}
								// Refrescar cuando marque algunos de los 24 tipo de error del prevalidador
								const ErrorFiltered = ArrCodigoErrorPrevalidador.filter((name) => name === codigoErrorPrevalidador);
								if(ErrorFiltered.length > 0){
									refrescar();
								}
							}
							else {
								$(field).attr('disabled', false);
								$(field).focus();
								field = [];
							}
						}
					}
					else if (sTitle == " ") {
						// Variables bPrevalidacionEnCurso y respuestaPreval utilizada en el proceso de prevalidación
						if (respuestaPreval == true && preval == true) {
							respuestaPreval = false;
							$(this).dialog("close");
							iRetorno = 1;
							llamadoApplet();
						}
						else if (respuestaPreval == true && habilitarCampos == false) {
							respuestaPreval = false;
							bPrevalidacionEnCurso = false;
							if ($("#cboTipoConstancia").val() == 1 || $("#cboTipoConstancia").val() == 527) {
								sTipoSolicitud = 27;
							}
							$(this).dialog("close");
							llamadoApplet();
						}
						else if (bPrevalidacionEnCurso == false && respuestaPreval == false) {
							$(this).dialog("close");
							iOpcion = "";
							sParametrosDigi = "";
							llamadoApplet();
						}
						else if ((respuestaPreval == true && bPrevalidacionEnCurso == false) || preval == false) {
							respuestaPreval = false;
							habilitarCampos = false;
							$(this).dialog("close");
							$('.ui-button-text-only').click(function (event) {
								inicializarVariables();
							});
						}
					}
					else if (sTitle == "Android" && iOpcion == ENROLAR_TRABAJADOR) // MOVIL
					{
						$(this).dialog("close");
						iOpcion = ENROLAR_TRABAJADOR;
						sTitle = "";
						sParametrosDigi = "";
						llamadoApplet();
					}
					else if (sTitle == "Android" && (iOpcion == FIRMA_PROM || iOpcion == FIRMA_TRAB || iOpcion == NOMBRE_TRAB)) // MOVIL
					{
						$(this).dialog("close");
						sTitle = "";
						sParametrosDigi = "";
						llamadoApplet();
					}
				}
			}
		});
}

function mostrarmensaje2() {

	if (bPrevalidacionEnCurso && sTitle != "Android") {
		//$(this).dialog( "close" );
		iOpcion = "";
		sParametrosDigi = "";
		llamadoApplet();
	}
	else if (sTitle == "") {

		//saber cuando la pagina de enrolamiento trabajador fue cerrarda o el enrolamiento se cancelo y mandarse llamar la pagina
		if (iCodEjecutarEnrol == 'EET') {
			marcarLog("CURP: " + sCurpTrabajador + " - VA A OBTENER EL ENLACE PARA EL ENROLAMIENTO");//logs
			
			obtenerenlace();
		}
		else if (iCodCerrarNavegador == 'EMCN') {
			cerrarNavegador();
		}
		else {
			if (Array.isArray(field)) {
				$.each(field, function (index, value) {
					value.attr('disabled', false);
					field = [];
				});

				errorMessages = [];

				if (guardarSolConstancia || limpiarVariables) {
					limpiarVariables = false;
					inicializarVariables();
				}
			}
			else {
				$(field).attr('disabled', false);
				$(field).focus();
				field = [];
			}
		}
	}
	else if (sTitle == " ") {
		// Variables bPrevalidacionEnCurso y respuestaPreval utilizada en el proceso de prevalidación
		if (respuestaPreval == true && preval == true) {
			respuestaPreval = false;
			//$(this).dialog( "close" );
			iRetorno = 1;
			llamadoApplet();
		}
		else if (respuestaPreval == true && habilitarCampos == false) {

			if(iTipoAfiliacion == 1 ||iTipoAfiliacion == 27 ||iTipoAfiliacion == 527) {
				if (bFnormal || bSinExpe || bConExpe){ //Se detiene el proceso del servicio 0101 
					setTimeout(function () {
						$('#bAceptar').attr('disabled', false);
						document.getElementById("bAceptar").classList.add("habilitarImg");
					}, 2000);

				}
			}
			else {
				respuestaPreval = false;
				bPrevalidacionEnCurso = false;
				if ($("#cboTipoConstancia").val() == 1 || $("#cboTipoConstancia").val() == 527) {
					sTipoSolicitud = 27;
				}
				//$(this).dialog( "close" );
				llamadoApplet();
			}
		}
		else if (bPrevalidacionEnCurso == false && respuestaPreval == false) {
			//$(this).dialog( "close" );
			iOpcion = "";
			sParametrosDigi = "";
			llamadoApplet();
		}
		else if ((respuestaPreval == true && bPrevalidacionEnCurso == false) || preval == false) {
			respuestaPreval = false;
			habilitarCampos = false;
			//$(this).dialog("close");
			$('.ui-button-text-only').click(function (event) {
				inicializarVariables();
			});
		}
	}
	else if (sTitle == "Android" && iOpcion == ENROLAR_TRABAJADOR) // MOVIL
	{
		iOpcion = ENROLAR_TRABAJADOR;
		sTitle = "";
		sParametrosDigi = "";
		llamadoApplet();
	}
	else if (sTitle == "Android" && (iOpcion == FIRMA_PROM || iOpcion == FIRMA_TRAB || iOpcion == NOMBRE_TRAB)) // MOVIL
	{
		sTitle = "";
		sParametrosDigi = "";
		llamadoApplet();
	}
}

//funcion para desplegar un mensaje
function mostrarMensajeProm(mensaje, sTitle) {
	$('.content-loader').hide();
	$("#divMensaje").dialog
		({
			title: sTitle,
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();

				if (Array.isArray(mensaje)) {
					var message = ""

					for (i = 0; i <= mensaje.length; i++) {
						if (mensaje[i] == undefined) {
							mensaje.splice(i, 1);
							continue;
						}

						message += mensaje[i] + "<br/> <br/>";
					}

					$(this).html("<p>" + message + "</p>");
				}
				else {
					$(this).html("<p>" + mensaje + "</p>");
				}
			},
			buttons:
			{
				"Aceptar": function () {
					$(this).dialog("close");
					actualizarBandMensajePromotor(folioPrevalidador, 1);
					sTitle = " ";
					iRetorno = 0;
					habilitarCampos = false;
					afterPrevalidacion = true;
										
					if(BanderaAntiguedadTraspaso)
					{
						mostrarMensaje(errorMessages, title);
					}
					else{
						mostrarmensaje2();//790
					}
				}
			}
		});
}

//funcion para desplegar un mensaje
function mostrarMensajeFolioImpl(mensaje, sTitle) {
	$('.content-loader').hide();
	$("#divMensaje").dialog
		({
			title: sTitle,
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();

				if (Array.isArray(mensaje)) {
					var message = ""

					for (i = 0; i <= mensaje.length; i++) {
						if (mensaje[i] == undefined) {
							mensaje.splice(i, 1);
							continue;
						}

						message += mensaje[i] + "<br/> <br/>";
					}

					$(this).html("<p>" + message + "</p>");
				}
				else {
					$(this).html("<p>" + mensaje + "</p>");
				}
			},
			buttons:
			{
				"Aceptar": function () {
					$(this).dialog("close");
					sTitle = " ";
					iRetorno = 0;
					habilitarCampos = false;
					afterPrevalidacion = true;
					mostrarmensaje2();
				}
			}
		});
}

function mostrarMensajeCancelEnrol(mensaje, sTitle) {
	$('.content-loader').hide();
	$("#divMensaje").dialog
		({
			title: sTitle,
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();

				if (Array.isArray(mensaje)) {
					var message = ""

					for (i = 0; i <= mensaje.length; i++) {
						if (mensaje[i] == undefined) {
							mensaje.splice(i, 1);
							continue;
						}

						message += mensaje[i] + "<br/> <br/>";
					}

					$(this).html("<p>" + message + "</p>");
				}
				else {
					$(this).html("<p>" + mensaje + "</p>");
				}
			},
			buttons:
			{
				"Cancelar": function () {
					$(this).dialog("close");
					location.reload();

				},
				"Aceptar": function () {
					if (sTitle == "") {
						$(this).dialog("close");

						if (iCodEjecutarEnrol == 'EET') {
							
							obtenerenlace();
						}
					}
				}

			}
		});
}

//funcion para desplegar un mensaje
function mostrarMensajeFinal(mensaje, sTitle) {
	$('.content-loader').hide();
	$("#divMensaje").dialog
		({
			title: sTitle,
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"Aceptar": function () {

					$(this).dialog("close");
					if (OSName == "Android") {
						Android.terminarSolConst("1");
					}

				}
			}
		});
}

//Metodo para mostrar en pantalla los mensajes del nuevo flujo
function mensajesnuevoproceso(mensaje) {
	$('.content-loader').hide();
	$("#divMensaje").dialog
		({
			title: '',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"Aceptar": function () {
					$(this).dialog("close");
					//La siguiente comparacion es para saber si se llamara de nuevo la toma de la huella o la lectura de la targeta
					if (iRetorno == 3) {
						iOpcion = "";
						sParametrosDigi = "";
						llamadoApplet();
					}
					else {
						iOpcion = LECTURA_TARGETA;
						opcionejecuta(0);
					}
				},

				"Cancelar": function () {
					$(this).dialog("close");
				}
			}

		});
}

function obtenervalorfolioretorno(folioretorno) {
	var cadena = "";
	cadena += "opcion=27&folioretorno=" + folioretorno;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				sMensaje = data.sMensajeBancoppel;
				iOperacionBancoppel = data.iOperacionBancoppel;
				sCodigoRetorno = data.sCodigoBancoppel;
				if (data.iOperacionBancoppel == 4 && data.sCodigoBancoppel == '0000') {
					informacionclienteBanCoppel(iKeyxinfBancoppel);
				}
				else {
					if (data.sCodigoBancoppel == '9975' || data.sCodigoBancoppel == '9977' || data.sCodigoBancoppel == '0007' ||
						data.sCodigoBancoppel == '9976' || data.sCodigoBancoppel == '9979' || data.sCodigoBancoppel == '9980' ||
						data.sCodigoBancoppel == '9996' || data.sCodigoBancoppel == '9998' || data.sCodigoBancoppel == '9999') {
						//$('#AltaUnica').attr('disabled', true);
					}
					//Si se obtiene un mensaje en donde diga que las huella no coinciden se mandara el mensaje y se debera ejecutar la toma de la huella
					//nuevamente, para ello se mandara llamar al metodo mensajesnuevoproceso de la siguinte manera
					//Se manda llamar al metodo para mostrar un mensaje
					mostrarMensaje(sMensaje, '')
				}
			}
		}
	);
}

function llenaformulariodatosBancoppel(datos) {
	if (iDireccionMostrar == 0) {
		validarcamposbancoppel(datos);
	}
	else {
		if (iDireccionMostrar > 0 && (sDireccionElegida == '' || sDireccionElegida != '')) {

			/***********************************************************************************
			  se validan los datos para saber si ya se hizo una validacion por RENAPO
			  si se encuentra registro en la tabla de RENAPO no mostrara los datos de bancoppel
			  ya se le da prioridad a esos datos que ya estan planchados en la constancia
			  pero si no encuentra el registro si los mostrara
			************************************************************************************/
			if (!validarRegistroRenapo()) {
				$('#txtCurp').val(datos.curpinfbancoppel);
				$('#txtApellidoPaterno').val(datos.appinfbancoppel);
				$('#txtApellidoMatero').val(datos.apminfbancoppel);
				$('#txtNombre').val(datos.nombreinfbancoppel);
			}

			$('#txtNombre').val(datos.nombreinfbancoppel);
			$('#txTelCelular').val(datos.telefonoinfbancoppel);
			$('#txtClienteCoppel').val(datos.numclienteinfbancoppel);

			$('#txtTelContacto').val(datos.telcontactoinfbancoppel);

			desboqueoCajasdeTexto();
			sTitle = "";
			iDireccionmostrar = 0;
			sMensaje = "La extracci\u00F3n de datos desde Bancoppel ha sido exitosa, por favor contin\u00FAa con el proceso de Afiliaci\u00F3n";
			mostrarMensaje(sMensaje, sTitle);
		}
	}
}

function informacionclienteBanCoppel(iKeyxinfBancoppel) {
	var cadena = "";
	cadena += "opcion=28&keyxbancoppel=" + iKeyxinfBancoppel;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				imagenR = data.anverso;
				imagenA = data.reverso;
				if (imagenR != '' && imagenA != '') {
					base64toimage(iKeyxinfBancoppel);
				}
				aInformacionBancoppel = eval(data);
				llenaformulariodatosBancoppel(aInformacionBancoppel);
			}
		}
	);
}

//Metodo para converitir una imagen Base64 a Imagen
function base64toimage(iKeyxinfBancoppel) {

	var sDescripcion = '';
	var cadena = "";
	cadena = "opcion=31&keyxbancoppel=" + iKeyxinfBancoppel;
	$.ajax({
		data: cadena,
		success: function (data) {
			sEstatusImagen = data.estatus;
			sDescripcion = data.descripcion;
		}
	});
}

//Metodo para verificar que datos de la direccion vienen vacios
function validarcamposbancoppel(datos) {
	//Primera direccion
	sPrimeraDireccion = datos.estadoinfbancoppel + ", " + datos.delmuninfbancoppel;
	//Segunda direccion
	sSegundaDireccion = datos.estado1infbancoppel + ", " + datos.delmun1infbancoppel;
	if ((sPrimeraDireccion != ", " && sPrimeraDireccion != "null, null") && (sDireccionElegida == '')) {
		//Si el numero exterior viene vacio se muestra un 0 para que el campo no se muestre vacio
		if (datos.ninteriorinfbancoppel == "null" || datos.ninteriorinfbancoppel == '') {
			datos.ninteriorinfbancoppel = 0;
		}
		sPrimeraDireccion += "<br/>" + datos.ciudadinfbancoppel + ", " + datos.coloniainfbancoppel + ", " + datos.calleinfbancoppel;
		sPrimeraDireccion += "<br/>EXT:" + datos.nexteriorinfbancoppel + ", INT:" + datos.ninteriorinfbancoppel + "<br>C.P:" + datos.codpostalinfbancoppel;
		//Se cambia el valor de la variable para continuar, en caso
		//que no se entre al segundo if, solamente se ingrese los valores de la primera direccion
		iDireccionMostrar = 1;
	}
	if ((sSegundaDireccion != ", " && sSegundaDireccion != "null, null") && (sDireccionElegida == '')) {
		//Si el numero exterior viene vacio se muestra un 0 para que el campo no se muestre vacio
		if (datos.ninterior1infbancoppel == "null" || datos.ninterior1infbancoppel == '') {
			datos.ninterior1infbancoppel = 0;
		}
		sSegundaDireccion += "<br/>" + datos.ciudad1infbancoppel + ", " + datos.colonia1infbancoppel + ", " + datos.calle1infbancoppel;
		sSegundaDireccion += "<br/>EXT:" + datos.nexterior1infbancoppel + ", INT:" + datos.ninterior1infbancoppel + "<br/>C.P: " + datos.codpostal1infbancoppel;
		if (iDireccionMostrar == 1) {
			//Se cambia el valor de la variable para que continue con el flujo correcto y llene los campos de la constancia
			iDireccionMostrar = 3;
			//En caso que iDireccionMostrar = 1 se toma la primera y segunda direccion y se muestra en un mensaje para seleccionar una direccion
			mostrarIfeDireccion();
		}
		else {
			iDireccionMostrar = 2;
		}
		if (iDireccionMostrar != 1 && iDireccionMostrar != 3) {
			//Se cambia el valor de la variable para que solo ingrese los datos de la segunda direccion
			iDireccionMostrar = 2;
			if (sEstatusImagen == 1) {
				mostrarIfeDireccion();
			}
			else {
				llenaformulariodatosBancoppel(aInformacionBancoppel);
			}
		}
	}
	if (iDireccionMostrar == 0) {
		iDireccionMostrar = 4;
		actualizarflagclientebcpl(iKeyxinfBancoppel);
		if (sEstatusImagen == 1) {
			mostrarIfeDireccion();
		}
		else {
			llenaformulariodatosBancoppel(aInformacionBancoppel);
		}
	}
	if (iDireccionMostrar == 1 || iDireccionMostrar == 2) {
		actualizarflagclientebcpl(iKeyxinfBancoppel);
		if (sEstatusImagen == 1) {
			mostrarIfeDireccion();
		}
		else {
			llenaformulariodatosBancoppel(aInformacionBancoppel);
		}
	}
}

function cerrarNavegador() {
	// MOVIL
	if (OSName == "Android") {
		Android.terminarSolConst("1");
	} else {
		//firefox
		if (navigator.appName.indexOf('Netscape') >= 0) {
			//NAVEGADOR FIREFOX
			javascript: window.close();
		}
		else {
			if (navigator.appName.indexOf('Microsoft') >= 0) {//internet explorer
				var ventana = window.self;
				ventana.opener = window.self;
				ventana.close();
			}
		}
	}
}

function obtenerMensajeConsulta(iRespuesta) {
	if (contador < 2) {
		mensajesErrorConsultaVerificacion(iRespuesta);
	}
	else {
		$('#txtCurp').val('');
		$('#txtCurp').focus();
		$('.content-loader').hide();
		contador = 0;
		sTitle = "";
		mensaje = "Promotor: Favor de ingresar nuevamente la curp del trabajador";
		mostrarMensaje(mensaje, sTitle);
		$('#txtCurp').attr('disabled', false);
		$('#btnConsultaCurp').attr('disabled', false);
		document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");
		$('#bRenapoTitular').attr('disabled', false);
		$('#bRenapoSol').attr('disabled', false)
		document.getElementById("bRenapoTitular").classList.add("habilitarImg");
		document.getElementById("bRenapoSol").classList.add("habilitarBtn");
		inicializarvariablesServicio();
		limpiarcampos();
	}
}

function mensajesErrorConsultaVerificacion(iRespuesta) {
	mensaje = "";
	sTitle = " ";
	$('.content-loader').hide();
	if (iRespuesta == 100) {
		mensaje = "Promotor: La huella no es válida o no es el Promotor activo en la sesión.";
		mostrarMensaje(mensaje, sTitle);
	}
	else if (iRespuesta == 101) {
		mensaje = "Promotor: Ocurrio un error al grabar la huella del promotor.";
		mostrarMensaje(mensaje, sTitle);
	}
	else if (iRespuesta == 102) {
		mensaje = "Promotor: Favor de indicarle al Trabajador que coloque correctamente su dedo indice sobre el sensor.";
		mostrarMensaje(mensaje, sTitle);
	}
	else if (iRespuesta == 103) {
		mensaje = "Promotor: Ocurrio un error al capturar la huella, intente de nuevo.";
		mostrarMensaje(mensaje, sTitle);
	}
	else if (iRespuesta == 104) {
		mensaje = "Promotor: Ocurrio un error al mandar llamar la aplicacion.";
		mostrarMensaje(mensaje, sTitle);
	}
	else if (iRespuesta == 106) {
		mensaje = "Promotor: La huella del trabajador no coincide con la de su enrolamiento.";
		mostrarMensaje(mensaje, sTitle);
	}
	else if (iRespuesta == 107) {
		mensaje = "Promotor: Favor de colocar tu huella sobre el sensor, ya que el Promotor cancelo previamente la captura de la huella.";
		mostrarMensaje(mensaje, sTitle);
	}
	else if (iRespuesta == -12) {
		mensaje = "Promotor: La huella no coincide con la de su enrolamiento, colocar su dedo indice nuevamente.";
		mostrarMensaje(mensaje, sTitle);
	}
	else {
		mensaje = "Promotor: ocurrio un error en la captura de huella, por favor colocar su dedo indice nuevamente.";
		mostrarMensaje(mensaje, sTitle);
	}
}

function obtenermensajeVerificacion(iRespuesta) {
	if (iTipoAfiliacion == 27 || iTipoAfiliacion == 1 || iTipoAfiliacion == 527) //Si es un traspaso
	{
		if (iContador <= 2) {
			sTitle = " ";
			if (iRetorno == 1) {
				if (iContador == 0) {
					llamadoApplet();
				}
				else {
					mensajesErrorConsultaVerificacion(102);
				}
			}

			if (iRetorno == 2) {
				if (iContador == 0) {
					llamadoApplet();
				}
				else {
					mensajesErrorConsultaVerificacion(iRespuesta);
				}
			}
		}
		else {
			iOpcion = 1;
			$('#txtCurp').val('');
			$('#txtCurp').focus();
			iContador = 0;
			$('#btnConsultaCurp').attr('disabled', false);
			document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

			$('#bRenapoSol').attr('disabled', false);
			$('#bRenapoTitular').attr('disabled', false);
			document.getElementById("bRenapoSol").classList.add("habilitarImg");
			document.getElementById("bRenapoTitular").classList.add("habilitarImg");

			$('.content-loader').hide();
			sTitle = "";
			mensaje = "Promotor: Favor de ingresar nuevamente la curp del trabajador";
			$('#txtCurp').attr('disabled', false);
			mostrarMensaje(mensaje, sTitle);
			inicializarvariablesServicio();
			limpiarcampos();
		}
	}
	else //Registro IMSS e Independientes
	{
		if (iContador <= 2) {
			$('.content-loader').hide();
			$('#cboTipoComprobante').attr('disabled', false);
			$('#cboEstadoCivil').attr('disabled', false);
			$('#cboTipoTelefono').attr('disabled', false);
			$('#txtTelContacto').attr('disabled', false);

			if (iTipoSolicitante == 1) {
				$('#txTelCelular').attr('disabled', false);
				$('#cboCompTelef').attr('disabled', false);
			}
			else {
				$('#txTelCelular').attr('disabled', true);
				$('#cboCompTelef').attr('disabled', true);
			}

			$('#cboCompTelefCont').attr('disabled', false);

			if (!bEsRenapo) {
				$('#txtApellidoPaterno').attr("disabled", false);
				$('#txtApellidoMatero').attr("disabled", false);
				$('#txtNombre').attr("disabled", false);
				if (iTipoAfiliacion == 26 || iTipoAfiliacion == 27 || iTipoAfiliacion == 527)
					$('#txtNss').attr("disabled", false);
				$('#txtCurp').attr("disabled", true);
			}
		}
		else {
			iOpcion = 1;
			$('#txtCurp').val('');
			$('#txtCurp').focus();
			iContador = 0;
			$('#btnConsultaCurp').attr('disabled', false);
			document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");

			$('#bRenapoSol').attr('disabled', false);
			$('#bRenapoTitular').attr('disabled', false);
			document.getElementById("bRenapoSol").classList.add("habilitarImg");
			document.getElementById("bRenapoTitular").classList.add("habilitarImg");

			$('.content-loader').hide();
			sTitle = "";
			mensaje = "Promotor: Favor de ingresar nuevamente la curp del trabajador";
			$('#txtCurp').attr('disabled', false);
			mostrarMensaje(mensaje, sTitle);
			inicializarvariablesServicio();
			limpiarcampos();
		}
	}
}

function inicializarvariablesServicio() {
	sConsultaEstatus = '';
	sConsultaDiagnostico = '';
	sVerificacionEstatus = '';
	sVerificacionDiganostico = '';
	sConsultaExpediente = '';
	sConsultaEnrolamiento = '';
	iSelloVerificacion = 0;
	iCaminoAlterno = 0;
}

function llamadoApplet() {
	if (iOpcion == ENROLAR_TRABAJADOR) {
		// MOVIL
		if ($("#cboTipoConstancia").val() == 1 || $("#cboTipoConstancia").val() == 527) {
			//var sTipoSolConstancia = $('#cboTipoConstancia').val();
			sTipoSolicitud = 27;
		}
		//sParametrosDigi = iTipoPersona+" 2 "+sCurpTrabajador+" "+iEmpleado+" 0 1"+" "+sTipoSolConstancia;
		sParametrosDigi = iTipoPersona + " 2 " + sCurpTrabajador + " " + iEmpleado + " 0 1" + " " + sTipoSolicitud;
		opcionejecuta(sParametrosDigi);
	} else {
	// Detener el proceso, para el caso del prevalidador/servicio 0501 y no denotar el servicio el 0101 a como actualmente lo hacia
		if (bPrevalNoExitoso & (iTipoAfiliacion == 1 || iTipoAfiliacion == 27 || iTipoAfiliacion == 527)){
			setTimeout(() => { 
				HabilitarCamposServicioPrev();
			}, 3000);	
		}
		else{
			iOpcion = CONSULTA_BIOMETRICO;

			if (bPrevalidacionEnCurso) {
				sTipoSolicitud = 101;
			}
			else if (sTipoSolicitud == 1 || sTipoSolicitud == 527 || sTipoSolicitud == 101) {
				sTipoSolicitud = 27;
			}
			if (bRealizar0407) // Se modifica el tiposolicitud a 102 y se valida la bandera para que se levante el aplicativo  siempre y cuando sea true.
			{
				sTipoSolicitud = 102;
				//sTipoSolicitud =27;
			}
			if (iRetorno == 1) {
				iTipoPersona = 3;
				sParametrosDigi = iTipoPersona + " 2 " + sCurpTrabajador + " " + iEmpleado + " 0 1" + " " + sTipoSolicitud;
				opcionejecuta(sParametrosDigi);
			}
			else if (iRetorno == 2) {
				iTipoPersona = 2;
				sParametrosDigi = iTipoPersona + " 2 " + sCurpTrabajador + " " + iEmpleado + " 0 1" + " " + sTipoSolicitud;
				opcionejecuta(sParametrosDigi);
			}
			else {
				//HETRABAJADOR.exe 2 1 OUPE800805HSLSRD01 91607833 0 1 27
				if (iRetorno == 5) {
					iRetorno = 0;
					iOpcion = COMPARACION_BIOMETRICA_BCPL;
					//Parametro para el HTCONSULTABCPL
					sParametrosDigi = iKeyxtargeta;
				}
				else {
					sParametrosDigi = "2 1 " + sCurpTrabajador + " " + iEmpleado + " 0 1" + " " + sTipoSolicitud;
				}
				opcionejecuta(sParametrosDigi);
			}
		}
	}
}

//Funcion para el XML consulta biometrico
function ObtenerRespuestaXML(sCurpTrabajador, sCurpEnrolado, iTipoPersona, iTipoServicio, iTipoOperacion) {
	var cadena = '';

	if (bPrevalidacionEnCurso) {
		cadena += "opcion=67&curpTrabajador=" + sCurpTrabajador + "&curpEmpleado=" + sCurpEnrolado + "&tipoPersona=" + iTipoPersona + "&tipoServicio=" + iTipoServicio + "&tipoOperacion=" + iTipoOperacion;
	}
	else if (iBanderaEntrada == 1) {
		cadena += "opcion=17&curpTrabajador=" + sCurpTrabajador + "&curpEmpleado=" + sCurpEnrolado + "&tipoPersona=" + iTipoPersona + "&tipoServicio=" + iTipoServicio + "&tipoOperacion=" + iTipoOperacion;
	}
	else {
		cadena += "opcion=22&curpTrabajador=" + sCurpTrabajador + "&curpEmpleado=" + sCurpEnrolado + "&tipoPersona=" + iTipoPersona + "&tipoServicio=" + iTipoServicio + "&tipoOperacion=" + iTipoOperacion + "&checkSeleccionado=" + checkSeleccionado + "&motivoSinHuella=" + sMotivoSinHuella + "&codigoHuella=" + sCodigoHuella;
	}

	$.ajax(
		{
			async: false,
			cache: false,
			data: cadena,
			url: 'php/constanciaafiliacion.php',
			type: 'post',
			dataType: 'json',
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				if (bPrevalidacionEnCurso && iTipoServicio == 9911) {
					iFolioVerificacion = datos.folioServicioAfore;
					respuestaServicios(0, iFolioVerificacion, getDataRespuesta);

				}
				else if (iTipoServicio == 9909) {
					iFolioConsulta = datos.folioServicioAfore;
					iRespConsulta = datos.respondioServicio;
					sMensajeConsulta = datos.descripcionRespuesta;

					if (checkTrabSinManos == 2 || iTipoSolicitante != 1) {
						if (iTipoSolicitante == 2 || iTipoSolicitante == 4 || iTipoSolicitante == 5) {
							if (checkTrabSinManos == 2) {
								iRetorno = 2;
							}
							else if (checkTrabManosLesionadas == 2) {
								iRetorno = 2;
							} else {
								iRetorno = 1;
							}
						}
						else {
							iRetorno = 2;
						}
						iBanderaEntrada = 0;

						checkSeleccionado = 0;
						if (iTipoSolicitante != 1) {
							sMotivoSinHuella = ' ';
							sCodigoHuella = '';
						}
						else {
							sMotivoSinHuella = '001';
							sCodigoHuella = 'XX';//si no tiene huella se guarda asi
						}
						desboqueoCajasdeTexto();
					}
					else if (checkTrabManosLesionadas == 2) {
						iBanderaEntrada = 0;
						iRetorno = 2;
						checkSeleccionado = 0;
						sMotivoSinHuella = '004';
						sCodigoHuella = 'UP';
						desboqueoCajasdeTexto();
					}
					else {
						iBanderaEntrada = 0;
						checkSeleccionado = 1;
						sMotivoSinHuella = ' ';
						sCodigoHuella = '';
						if (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) {
							iRetorno = 3;
						}
						else {
							iRetorno = 1;
						}
						$('.content-loader').hide();

						desboqueoCajasdeTexto();
						bloquearCamposPrevalidacion();

					}
					if (BanderaEstatus == 7 || BanderaEstatus == 8) {
						obtenerEstatusAfiliacion();
					}
				}
				else if (iTipoServicio == 9910) {
					iFolioVerificacion = datos.folioServicioAfore;
					iRespVerificacion = datos.respondioServicio;
					sMensajeVerificacion = datos.descripcionRespuesta;
					$('.content-loader').hide();
					desboqueoCajasdeTexto();
					bloquearCamposPrevalidacion();
					
					if(bGestorexito){
						setTimeout(function () {
							bloquearCamposServicio();
						 }, 3000);
					}

					if (BanderaEstatus == 7 || solProceso == 1) {
						$('#bGrabacion').attr('disabled', false);
						document.getElementById("bGrabacion").classList.add("habilitarImg");
					}
					else {
						$('#bAceptar').attr('disabled', false);
						document.getElementById("bAceptar").classList.add("habilitarImg");
					}
					if (iCaminoAlterno == 1) {
						//obenerResupuestaServicios(iFolioConsulta,iFolioVerificacion);
					}
					if (iTipoAfiliacion != 26 && iTipoAfiliacion != 33)//diferente a registro/independiente
					{
						if (iRespVerificacion == 1) {
							//console.log("me ejecute correctamenete");
						}
						else {
							if (intentosServicio < 6) {
								intentosServicio++;
								if (iRespVerificacion == 0) {
									setTimeout(function () { ObtenerRespuestaXML(sCurpTrabajador, sCurpEnrolado, iTipoPersona, iTipoServicio, iTipoOperacion); }, 5000);
									return;
								}
								else {
									return;
								}
							}
							else {
								if (iRespVerificacion == 0) {
									//console.log('fallo modal y desbloqueo campos')
								}
								else {
									return;
								}
							}
						}
					}
				}
			},
			error: function (xhr, status, error) {
				sMensaje = status + ' - ' + error;
				mostrarMensaje(sMensaje, "");
			}
		}
	);
}

function MuestramensajeCancelacion(mensaje) {
	$('.content-loader').hide();
	$("#divMensaje").dialog
		({
			title: '',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"Si": function () {
					$(this).dialog("close");
					sDireccionElegida = '004';

					if (iFlagIdentificacion == 1) {
						actualizarflagclientebcpl(iKeyxinfBancoppel);
					}

					llenaformulariodatosBancoppel(aInformacionBancoppel);
				},

				"No": function () {
					$(this).dialog("close");
					mostrarIfeDireccion();
				}
			}

		});
}

function Muestramensajenoseleccionado(mensaje) {
	$('.content-loader').hide();
	$("#divMensaje").dialog
		({
			title: '',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"Aceptar": function () {
					$(this).dialog("close");
					mostrarIfeDireccion();
				}

			}

		});
}
//Fin del bloque de codigo para mostrar las direcciones en pantalla

//funcion para optener la curp del empleado que esta enrolando
function obtenerCurp(iEmpleado) {
	var cadena = '';
	cadena += "opcion=14&empleado=" + iEmpleado;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				sCurpEnrolado = datos.curpE;
			}
		}
	);
	return sCurpEnrolado;
}

function respuestaServicios(iFolioConsulta, iFolioVerificacion, getDataRespuesta = null) {
	var cadena = "";
	cadena += "opcion=18&cfolio=" + iFolioConsulta + "&vfolio=" + iFolioVerificacion;

	$.ajax(
		{
			data: cadena,
			success: function (data) {
				obtenerRespuestaServicios(data);
			}
		}
	);

	return iRespuestaConsulta;
}

function obtenerRespuestaServicios(data) {
	sVerificacionEstatus = data.sVerifiEstatus;

	if (bPrevalidacionEnCurso) {
		if (data.sVerifiEstatus == "01") {
			getDataRespuesta(data);
			actualizaEnrolHuellaServicio(0, iFolioVerificacion, sCurpTrabajador);
			bPrevalidacionEnCurso = false;

		}
		else {
			if (contadorRespuesta <= 3) {
				var tiempoEspera = 10000;
				contadorRespuesta++;
				setTimeout(function () {
					respuestaServicios(iFolioConsulta, iFolioVerificacion);
				}, tiempoEspera);
			}
			else {
				sTitle = " ";
				contadorRespuesta = 0;
				preval = false;
				bPrevalNoExitoso = true;
				respuestaPreval = true;
				bPrevalidacionEnCurso = false;
				$('.content-loader').hide();
				sMensaje = "Promotor: Ocurrio un problema obteniendo el sello de verificación. ";
				sMensaje += "Por favor continuar con el proceso manualmente.";
				mostrarMensaje(sMensaje, sTitle);
			}
		}
	} else {
		datos = data;

		if (datos.sConsulEstatus == "") {
			sConsultaEstatus = datos.sVerifiEstatus
		}
		else {
			sConsultaEstatus = datos.sConsulEstatus;
		}

		sConsultaDiagnostico = datos.sConsulDiagnostico;
		sVerificacionEstatus = datos.sVerifiEstatus;
		sVerificacionDiganostico = datos.sVerifiDiagnostico;
		sConsultaExpediente = datos.sEstatusVerifica;
		sConsultaEnrolamiento = datos.sEnrolamiento;
		iSelloVerificacion = datos.iSelloVerificacion;

		if ((datos.sConsulEstatus == '') && (datos.sVerifiEstatus == '') && (datos.iSelloVerificacion == 0)) {
			iRespuestaConsutla = 0;
		}
		else {
			iRespuestaConsutla = 1;
		}
	}
}

function validarNSS() {
	var preval = true;
	var valido = true;
	var sNSS = $('#txtNss').val();
	//sNSS = sNSS.trim();

	var sTipoConstancia = $('#cboTipoConstancia').val();
	var sApellidoPaterno = $('#txtApellidoPaterno').val();
	var curp = $('#txtCurp').val();

	if ((sTipoConstancia == 26 || sTipoConstancia == 27) && curp != "") {
		if (sNSS == "") {
			var title = "";
			$('#txtNss').attr("disabled", false);
			var mensaje = "Promotor, favor de capturar el NSS del Trabajador y dar clic nuevamente en el botón Consultar";
			mostrarMensaje(mensaje, title);
			field = sNSS;
			valido = false;
		}
		else if (sNSS.length > 0 && sNSS.length < 11) {
			var title = " ";
			preval = false;
			respuestaPreval = true;
			habilitarCampos = true;
			var mensaje = "El nss proporcionado no es válido, favor de proporcionar otro.";
			mostrarMensaje(mensaje, title);
			valido = false;
		}
		else {
			if (sNSS.length == 11) {
				if (!sNSS.match(/^\d+$/)) {
					var title = " ";
					preval = false;
					respuestaPreval = true;
					habilitarCampos = true;
					var mensaje = "El nss proporcionada no cumple con la estructura correcta";
					mostrarMensaje(mensaje, title);
					valido = false;
				}
			}
			else {
				var title = " ";
				preval = false;
				respuestaPreval = true;
				habilitarCampos = true;
				var mensaje = "El nss proporcionado no es válido, favor de proporcionar otro.";
				mostrarMensaje(mensaje, title);
				valido = false;
			}
		}
	}

	return valido;
}

function validarCamposPrevalidacion() {
	var sApellidoPaterno = $('#txtApellidoPaterno');
	var nombres = $('#txtNombre');
	var camposCompletos = true;
	var sTitle = "";

	if (sApellidoPaterno.val() == "") {
		sMensaje = "Debe especificar el apellido paterno.";
		errorMessages.push(sMensaje);
		errorFields.campo = sApellidoPaterno;
		errorFields.mensaje = sMensaje;
		field.push(sApellidoPaterno);
		camposCompletos = false;
		$('#txtApellidoPaterno').attr("disabled", false);
	}

	if (nombres.val() == "") {
		sMensaje = "Debe especificar el nombre o los nombres.";
		errorMessages.push(sMensaje);
		errorFields.campo = nombres;
		errorFields.mensaje = sMensaje;
		field.push(nombres);
		camposCompletos = false;
		$('#txtNombre').attr("disabled", false);
	}

	if (errorMessages.length > 0) {
		mostrarMensaje(errorMessages, sTitle);
	}

	errorMessages = [];

	return camposCompletos;
}

function getDataRespuesta(data) {
	var sConsultaEstatus = data.sConsulEstatus;
	var sConsultaDiagnostico = data.sConsulDiagnostico;
	var sVerificacionEstatus = data.sVerifiEstatus;
	var sVerificacionDiganostico = data.sVerifiDiagnostico;
	var sConsultaExpediente = data.sEstatusVerifica;
	var sConsultaEnrolamiento = data.sEnrolamiento;
	iSelloVerificacion = data.iSelloVerificacion;

	if (sConsultaEstatus == '' && sVerificacionEstatus == '' && iSelloVerificacion == 0) {
		iRespuestaConsulta = 0;
	}
	else {
		iRespuestaConsulta = 1;
	}

	var sCurp = $('#txtCurp').val();
	var sApPaterno = $('#txtApellidoPaterno').val();
	var sApMaterno = $('#txtApellidoMatero').val();
	var sNombres = $('#txtNombre').val();
	var sNSS = $('#txtNss').val();

	var datos =
	{
		curpTrabajador: sCurp,
		apellidoPaterno: sApPaterno,
		apellidoMaterno: sApMaterno,
		nombres: sNombres,
		nss: sNSS,
		curpPromotor: sCurpEnrolado,
		numeroEmpleado: iEmpleado,
		selloVerificacion: iSelloVerificacion
	};

	prevalidador(datos);
}

function prevalidador(data) {
	dataRegistrarPrevalidador = data;
	var numeroEmpleado = data['numeroEmpleado'];
	var datos =
	{
		opcion: 65,
		data
	};

	$.ajax({
		data: datos,
		success: function (data) {
			if (data.estatus == 1) {
				var datos = {
					folioSolicitud: data['folioSolicitud'],
					numeroEmpleado: numeroEmpleado
				};
				respuestaPrevalidador(datos);
			}
			else {
				if (contadorRespuesta <= 3) {
					var tiempoEspera = 10000;
					contadorRespuesta++;
					setTimeout(function () {
						prevalidador(dataRegistrarPrevalidador);
					}, tiempoEspera);
				}
				else {
					$('.content-loader').hide();
					bPrevalNoExitoso = true; 
					bPrevalidacionEnCurso = false;
					respuestaPreval = true;
					contadorRespuesta = 0;
					mensaje = "Promotor: Ocurrió un problema registrando los datos a enviar al prevalidador. ";
					mensaje += "Por favor continue el proceso manualmente.";
					mostrarMensaje(mensaje, sTitle);
				}
			}
		}
	});
}

//Limpiar CURP y NSS si el Trabajador cuenta con una marca operativa en proceso
function validaMarca(iValMarcas) {
	var valMarcas = iValMarcas;

	if (valMarcas == 1) {
		limpiarVariables = true;
		var codigoError = "P13";
		codError = codigoError;
		limpiarCamposPrevalidacion();
		obtenerMensajeErrorPrevalidador(codigoError);
	}
}

// Respuesta de Constancia de Implicaciones
function validaFolioConstanciaImplicaciones(data) {
	var sTipoConstancia = $('#cboTipoConstancia');

	if (sTipoConstancia.val() == 27 || sTipoConstancia.val() == 1 || sTipoConstancia.val() == 527 || tipoAfiliacion == 27 || tipoAfiliacion == 1 || tipoAfiliacion == 527) {
		if (data.iValFolioConstanciaImplicaciones == 1 || data.valNumTrasp36meses == 1 || data.valCitActiva == 1) {
			if ((MensajePromotor == 1 && errorMessages.length == 1) || errorMessages.length == 0) {
				if (data.valCitActiva == 1) {
					var title = "";
					messages = true;
					var codigoError = "P16";
					bPrioridad = true;
					obtenerMensajeErrorPrevalidador(codigoError);
					var sMensaje = errorMessages;
					mostrarMensajeCapturaFolio(mensaje, sTitle);
				}
				else {
					var title = "";
					messages = true;
					bPrioridad = true;
					var codigoError = "P15";
					obtenerMensajeErrorPrevalidador(codigoError);
					var sMensaje = errorMessages;
					mostrarMensajeCapturaFolio(mensaje, sTitle);
				}
			}
			else {
				if (data.valCitActiva == 1) {
					var codigoError = "P16";
					bPrioridad = true;
					obtenerMensajeErrorPrevalidador(codigoError);
				}
				else {
					var codigoError = "P15";
					bPrioridad = true;
					obtenerMensajeErrorPrevalidador(codigoError);
				}
			}
		}
		else if (data.iValFolioConstanciaImplicaciones == 0 || data.valNumTrasp36meses == 0 || data.valCitActiva == 1) {
			if (errorMessages.length == 0) {
				habilitarCampos = true;
			}
		}
	}
}
//funcion para desplegar mensaje con captura de folio
function mostrarMensajeCapturaFolio(mensaje, sTitle) {
	mensaje = '';
	var optionButtons2 = "Cancelar";
	var botonAceptar = false;
	var optionButtons =
	{
		"Cancelar": function () {
			limpiarVariables = true;
			//limpiarCamposPrevalidacion();
			errorMessages = [];
			$(this).dialog("close");
			envíoCartaObtenciónFCT();
		}
	};

	$("#divMensaje").dialog
		({
			title: sTitle,
			autoOpen: true,
			resizable: false,
			width: '500',
			height: 'auto',
			modal: true,
			buttons: '',
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();

				if (Array.isArray(mensaje)) {
					var message = ""

					for (i = 0; i <= mensaje.length; i++) {
						if (mensaje[i] == undefined) {
							mensaje.splice(i, 1);
							continue;
						}

						message += mensaje[i] + "<br/> <br/>";
					} if (diferencia <= 45) {
						var mensaje = '';
						mensaje += '<div><p>Promotor, trabajador requiere el Folio de Constancia de Implicaciones de Traspaso o Folio de Conocimiento de Traspaso.</p></div>';
						sHtml = '';
						sHtml += "<p>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;Folio.<input id='txtFolio' onkeyup='bloquearBotonCapturaFolio()' name='txtFolio' type='text' minlength='6' maxlength='9' tabindex = 2>";
						sHtml += "<button id='btnCapturaFolio' disabled class='ui-state-default ui-widget-content' onclick='clickContinuarCapturaFolio()'>Continuar</button></p>";
						sHtml += "<label id='labelFolioImplicacion' style='font-size:9; color:#FF0000;'></label>";
						mensaje += sHtml;
					} else {
						var mensaje = '';
						mensaje += '<div><p>Promotor, trabajador requiere el Folio de Conocimiento de Traspaso.</p></div>';
						sHtml = '';
						sHtml += "<p>Folio de Conocimiento de trasp.<input id='txtFolio' onkeyup='bloquearBotonCapturaFolio45dias()' name='txtFolio' type='text' minlength='9' maxlength='9' tabindex = 2>";
						sHtml += "<button id='btnCapturaFolio' disabled class='ui-state-default ui-widget-content' onclick='clickContinuarCapturaFolio()'>Continuar</button></p>";
						sHtml += "<label id='labelFolioImplicacion' style='font-size:9; color:#FF0000;'></label>";
						mensaje += sHtml;
					}
					$(this).html("<p>" + mensaje + "</p>");
				}
				else {
					if (diferencia <= 45) {
						var mensaje = '';
						mensaje += '<div><p>Promotor, trabajador requiere el Folio de Constancia de Implicaciones de Traspaso o Folio de Conocimiento de Traspaso.</p></div>';
						sHtml = '';
						sHtml += "<p>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;Folio.<input id='txtFolio' onkeyup='bloquearBotonCapturaFolio()' name='txtFolio' type='text' minlength='6' maxlength='9' tabindex = 2>";
						sHtml += "<button id='btnCapturaFolio' disabled class='ui-state-default ui-widget-content' onclick='clickContinuarCapturaFolio()'>Continuar</button></p>";
						sHtml += "<label id='labelFolioImplicacion' style='font-size:9; color:#FF0000;'></label>";
						mensaje += sHtml;
					} else {
						var mensaje = '';
						mensaje += '<div><p>Promotor, trabajador requiere el Folio de Conocimiento de Traspaso.</p></div>';
						sHtml = '';
						sHtml += "<p>Folio de Conocimiento de trasp.<input id='txtFolio' onkeyup='bloquearBotonCapturaFolio45dias()' name='txtFolio' type='text' minlength='9' maxlength='9' tabindex = 2>";
						sHtml += "<button id='btnCapturaFolio' disabled class='ui-state-default ui-widget-content' onclick='clickContinuarCapturaFolio()'>Continuar</button></p>";
						sHtml += "<label id='labelFolioImplicacion' style='font-size:9; color:#FF0000;'></label>";
						mensaje += sHtml;
					}
					$(this).html("<p>" + mensaje + "</p>");
				}

				$(this).html(buttons);
			},
			buttons: optionButtons
		});
}

// Validar régimen pensionario
function validaRegimenPensionario(iValRegimenPensionario) {
	var valRegimenPensionario = iValRegimenPensionario;

	if (valRegimenPensionario == 1) {
		limpiarVariables = true;
		var codigoError = "P14";
		codError = codigoError;
		limpiarCamposPrevalidacion();
		obtenerMensajeErrorPrevalidador(codigoError);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
$("#txtFolio").keypress((event) => {
	return validarKeypressCurp(event);
});
//validacion para abilitar boton captura folio implicacion o captura de traspaso
function bloquearBotonCapturaFolio() {
	if ($('#txtFolio').val().length == 6 || $('#txtFolio').val().length == 9) {
		$('#btnCapturaFolio').prop("disabled", false);
	} else {
		$('#btnCapturaFolio').prop("disabled", true);
	}
}
//validacion para abilitar boton folio captura traspaso
function bloquearBotonCapturaFolio45dias() {
	if ($('#txtFolio').val().length === 9) {
		$('#btnCapturaFolio').prop("disabled", false);
	} else {
		$('#btnCapturaFolio').prop("disabled", true);
	}
}
// funcion que detona al click del boton captura de folio
function clickContinuarCapturaFolio() {
	//console.log('entre');
	var iFolio = $('#txtFolio');
	iFolio.bind('change', aMayusculas);
	var longitudFolio = $("#txtFolio").val().length;
	console.log(longitudFolio);
	if (OSName != "Android") {
		$("#txtFolio").keydown(function (event) {
			return validarKeypressCurp(event);
		});
	}
	if (longitudFolio === "") {
		$('#labelFolioImplicacion').html("No se ha capturado el Folio de Constancia de Implicaciones");
		$('#txtFolio').focus();
	}
	else if (longitudFolio === 6) {
		var validaNumerosFolio = iFolio.val().replace(/\D/g, '');

		if ($("#txtFolio").val()[0] == "ñ" || $("#txtFolio").val()[0] == "Ñ" || $("#txtFolio").val().length < 6 || ($("#txtFolio").val().length == 6 && validaNumerosFolio.length < 2 || validaNumerosFolio.length > 3)) {
			$('#labelFolioImplicacion').html("El Folio de Constancia de Implicaciones no es válido");
			$('#txtFolio').focus();
		}
		else {
			$('#divMensaje').dialog("close");
			iFolioConstanciaImplicaciones = iFolio.val();
			iFolioConstanciaImplicaciones = iFolioConstanciaImplicaciones.toUpperCase()
			if (traeErrorFtc === 0) {

				$('#divMensaje').dialog("close");
				levantamostarmensajeFolioImpli();
			}
		}
	} else if (longitudFolio === 9) {
		if ($("#txtFolio").val()[0] == "ñ" || $("#txtFolio").val()[0] == "Ñ" || $("#txtFolio").val().length < 9 || ($("#txtFolio").val().length != 9)) {
			$('#labelFolioImplicacion').html("El Folio de Constancia de Implicaciones no es válido");
			$('#txtFolio').focus();
		}
		else {
			$('#divMensaje').dialog("close");
			iFolioConocimientoTraspaso = iFolio.val();
			iFolioConocimientoTraspaso = iFolioConocimientoTraspaso.toUpperCase()
			obtencionFolioSulicitud();
			guardarDatosConsultaFolio(iFolioConocimientoTraspaso);
			validarFolioConocimientoTraspaso(iFolioConocimientoTraspaso);
			if (traeErrorFtc === 0) {

				$('#divMensaje').dialog("close");
				levantamostarmensajeFolioImpli();
			}
		}
	}
}

function obtencionFolioSulicitud() {
	var resul = '';
	$.ajax(
		{
			data: { opcion: 510, curp: $('#txtCurp').val() },
			success: function (data) {
				console.log(data);
				if (data.irespuesta) {
					folioSolisitud = data.irespuesta;
					
					if(bGestorexito)
					{
						if(folioSolisitud == 0){
						  folioSolisitud = iFolioGestor;
						}
						else{
							iAutdactilar = 3;
							Autenticadactilar();
						}
					}
				}
			}
		});
	return resul;
}

function guardarDatosConsultaFolio(folioObtener) {
	var resul = '';
	var parametro = window.location.href;
	var directorio = new URL(parametro);
	var iEmpleado = directorio.searchParams.get('empleado');
	$.ajax(
		{
			data: { opcion: 509, empleado: iEmpleado, curp: $('#txtCurp').val(), iFolioConocimientoTraspaso: folioObtener, iFolioSolicitud: folioSolisitud },
			success: function (data) {
				datos = (data);
				console.log(datos);
				if (datos.estatus == 1) {
					resul = true;
				}
				else {
					console.log('Promotor: Ocurrió un error al ejecutar el WS, favor de comunicarse con mesa de ayuda');
				}
			}
		});
	return resul;
}

function guardarFolioColsolicitudes(folioGuardar) {
	var folioTrasCono = folioGuardar;
	var resul = '';
	$.ajax(
		{
			data: { opcion: 508, cfolioTrasCono: folioTrasCono, cCurpBucar: $('#txtCurp').val() },
			success: function (data) {
				console.log(data);
				if (data.irespuesta = !'') {
					resul = data.irespuesta;
				} else {
					resul = '0';
				}
				console.log(resul);
			}
		});
}

function levantamostarmensajeFolioImpli() {
	sHtml = "<p><center>Folio de Conocimiento  de Traspaso capturado</center></p>";
	$("#divMensaje").html(sHtml);
	var title = " ";
	var mensaje = "Folio de Constancia de Implicaciones capturado";
	iRetorno = 0;
	preval = false;
	bPrevalidacionEnCurso = false;
	habilitarCampos = true;
	mostrarMensajeFolioImpl(mensaje, title);
	afterPrevalidacion = true;
	habilitaCamposPrevalidador("OK");
	$('#txtCurp').attr('disabled', true);
	$('#txtNss').attr('disabled', true);
}

function validarFolioConocimientoTraspaso(folioVerificar) {

	var cRespuesta = verificarMensajeErrorFct(folioVerificar.toUpperCase());
	if (contadorMensajeErrorFtc === 3 && cRespuesta.uno === null) {
		return;
	}
	if (contadorMensajeErrorFtc < 3) {
		if (cRespuesta.uno === null || cRespuesta.dos === '046') {
			diagnosticoprocesarFct = cRespuesta.dos;
			return;
		} else if (cRespuesta.dos === '000') {
			if (ContadorIntermitencia === 2) {
				diagnosticoprocesarFct = cRespuesta.dos;
				return;
			} else {
				mostrarMensajeObtebcionCodigoErrorIntermitencia(cRespuesta, folioVerificar);
				diagnosticoprocesarFct = cRespuesta.dos;
			}

		} else {
			mostrarMensajeObtencionCodigoError(cRespuesta, folioVerificar);
		}
	} else {
		mostrarMensajeObtebcionCodigoError3Intentos(cRespuesta);
	}

}

function mostrarMensajeObtebcionCodigoError3Intentos(cRespuesta) {
	traeErrorFtc = 1;
	var cMensaje = '';
	var cCurptrabajador = $('#txtCurp').val();
	cMensaje += '<div id="divCont" class="modal-content">';

	cMensaje += '<div class="modal-header">';
	cMensaje += '</div>';

	cMensaje += '<div id="divBody" class="modal-body" style="background-color:rgb(241,245,248);">';
	cMensaje += '<p>PROMOTOR: Favor de comunicarse a Mesa de Ayuda por el diagnostico ' + cRespuesta.dos + ' en la curp ' + cCurptrabajador + '.</p>';
	cMensaje += '<div class="modal-footer"> ';
	cMensaje += '<button type="button" style="background-color: rgb(78,147,206);" class="btn btn-primary"';
	cMensaje += "onclick='cambiarURLDivMensaje()'>Cerrar</button>";
	cMensaje += '</div>';
	cMensaje += '</div>';
	cMensaje += '</div>';
	ModalErrorFct(cMensaje);
}

function cambiarURL() {
	$('#myModalPDF').dialog("close");
	window.close();
}

function cambiarURLMC() {
	$('#myModal').dialog("close");
	window.close();
}

function cambiarURLDivMensaje() {
	$('#divMensaje').dialog("close");
	window.close();
}

window.addEventListener('afterprint', (event) => {
	//console.log('a ver que jeje');
	// window.location.replace('http://10.44.172.234/menuafore/indexMenu.html');
});

window.onafterprint = (event) => {
	//console.log('After print huehuehuehuehuehue');
	window.location.replace('http://10.44.172.234/menuafore/indexMenu.html');
};

function mostrarMensajeObtebcionCodigoErrorIntermitencia(cRespuesta, folioVerificar) {
	traeErrorFtc = 1;
	var arr = cRespuesta.uno.split('folio');
	var mensajeError = arr[0] + ' folio ' + folioVerificar + arr[1];
	var cMensaje = '';

	cMensaje += '<div id="divCont" class="modal-content">';

	cMensaje += '<div class="modal-header">';
	cMensaje += '</div>';

	cMensaje += '<div id="divBody" class="modal-body" style="background-color:rgb(241,245,248);">';
	cMensaje += '<p>' + mensajeError + '</p>';
	cMensaje += '</div>';

	cMensaje += '<div class="modal-footer"> ';
	cMensaje += '<button type="button" style="background-color: rgb(78,147,206);" class="btn btn-primary"';
	cMensaje += 'onclick="borrarCapoAgregarOtroFct()">Modificar</button>';
	cMensaje += '<button type="button" style="background-color: rgb(78,147,206);" class="btn btn-primary"';
	cMensaje += 'onclick="levantamostarmensajeFolioImpli()">Continuar</button>';
	cMensaje += '</div>';

	cMensaje += '</div>';
	cMensaje += '</div>';
	ModalErrorFct(cMensaje);

}

function mostrarMensajeObtencionCodioErrorDiferente(codiError) {
	var mensajeError = 'PROMOTOR: Favor de comunicarse a Mesa de Ayuda ya que el diagnostico ' + codiError + ' no se encuentra registrado.';
	var cMensaje = '';
	cMensaje += '<div id="divCont" class="modal-content">';

	cMensaje += '<div class="modal-header">';
	cMensaje += '</div>';

	cMensaje += '<div id="divBody" class="modal-body" style="background-color:rgb(241,245,248);">';
	cMensaje += '<p>' + mensajeError + '</p>';
	cMensaje += '</div>';

	cMensaje += '<div class="modal-footer"> ';
	cMensaje += '<button type="button" style="background-color: rgb(78,147,206);" class="btn btn-primary"';
	cMensaje += 'onclick="cambiarURLDivMensaje()">Aceptar</button>';
	cMensaje += '</div>';

	cMensaje += '</div>';
	cMensaje += '</div>';
	ModalErrorFct(cMensaje);
}

function mostrarMensajeObtencionCodigoError(cRespuesta, folioVerificar) {
	traeErrorFtc = 1;
	var codiError = cRespuesta.dos;
	if (codiError === '025' || codiError === '493' || codiError === 'B67') {
		var arr = cRespuesta.uno.split('traspaso');
		var mensajeError = arr[0] + ' traspaso ' + folioVerificar + arr[1];
	}
	else if (codiError === '045') {
		var mensajeError = cRespuesta.uno;
	} else if (codiError === 'N/A') {
		var mensajeError = cRespuesta.uno;
	}
	else if (codiError === '044') {
		var arr = cRespuesta.uno.split('folio');
		var mensajeError = arr[0] + ' folio ' + folioVerificar + arr[1];
	} else {
		mostrarMensajeObtencionCodioErrorDiferente(codiError, folioVerificar);
		return;
	}
	var cMensaje = '';
	cMensaje += '<div id="divCont" class="modal-content">';

	cMensaje += '<div class="modal-header">';
	cMensaje += '</div>';

	cMensaje += '<div id="divBody" class="modal-body" style="background-color:rgb(241,245,248);">';
	cMensaje += '<p>' + mensajeError + '</p>';
	cMensaje += '</div>';

	cMensaje += '<div class="modal-footer"> ';
	cMensaje += '<button type="button" style="background-color: rgb(78,147,206);" class="btn btn-primary"';
	cMensaje += 'onclick="borrarCapoAgregarOtroFct()">Aceptar</button>';
	cMensaje += '</div>';

	cMensaje += '</div>';
	cMensaje += '</div>';
	ModalErrorFct(cMensaje);
}

function borrarCapoAgregarOtroFct() {
	$('#txtFolio').html("");
	$('#divMensaje').dialog("close");
	mostrarMensajeCapturaFolio();
}

function verificarMensajeErrorFct(folioVerificar) {

	traeErrorFtc = 0;
	var foliotrasp = folioVerificar;
	var resul = {};
	$.ajax(
		{
			data: { opcion: 506, cFolioConoTrasp: foliotrasp },
			success: function (data) {
				console.log(data);
				if (data.irespuesta === '') {
					resul = '';
				} else {
					resul['uno'] = data.irespuesta;
					resul['dos'] = data.icodigo;
				}
				if (data.icodigo === '000') {
					ContadorIntermitencia = ContadorIntermitencia + 1;
				}
			}
		});
	contadorMensajeErrorFtc += 1;

	return resul;
}

//valida CURP y NSS en BDNSAR
function validaCurpyNssBDNSAR(iValNssTrabajador, iValCurpTrabajador) {
	var valNssTrabajador = iValNssTrabajador;
	valProcesarNssTrabajador = valNssTrabajador;
	var valCurpTrabajador = iValCurpTrabajador;
	var sTipoSolicitud = $('#cboTipoConstancia');

	if (sTipoSolicitud.val() == 27 || tipoAfiliacion == 27) {
		if (valNssTrabajador == 1 && valCurpTrabajador == 0) {
			$('#txtCurp').val('');
			var codigoError = "P05";//prioridad 20	
			codError = codigoError;
			limpiarCamposPrevalidacion();
			obtenerMensajeErrorPrevalidador(codigoError);
		}
		else if (valNssTrabajador == 0 && valCurpTrabajador == 0) {
			$('#txtNss').val('');
			$('#txtCurp').val('');
			var codigoError = "P04";//prioridad 19
			codError = codigoError;
			limpiarCamposPrevalidacion();
			obtenerMensajeErrorPrevalidador(codigoError);
		}
		else if (valNssTrabajador == 0) {
			$('#txtNss').val('');
			var codigoError = "P01";//prioridad 16
			codError = codigoError;
			bP16 = true;
			limpiarCamposPrevalidacion();
			obtenerMensajeErrorPrevalidador(codigoError);
		}
		else if (valNssTrabajador == 2) {
			sTipoSolicitud.val(26);
			iSelloVerificacion = 0;
			var codigoError = "P02";//prioridad 17
			codError = codigoError;
			esRegistro = 1;
			afterPrevalidacion = true;
			sVerificacionEstatus = "";
			sTipoSolicitud.trigger("change");
			$('#txtCurp').attr("disabled", false);
			limpiarCamposPrevalidacion();
			obtenerMensajeErrorPrevalidador(codigoError);
		}
		else if( (isNaN(valNssTrabajador) && isNaN(valCurpTrabajador)) || (valNssTrabajador == 0 && valCurpTrabajador == 0) || (valNssTrabajador === null && valCurpTrabajador === null) ||  (valNssTrabajador == "" && valCurpTrabajador == "")){//prioridad 18
			$('#txtNss').val('');
			$('#txtCurp').val('');
			var codigoError = "P03";//prioridad 18
			codError = codigoError;
			bP18 = false;
			limpiarCamposPrevalidacion();
			obtenerMensajeErrorPrevalidador(codigoError);
		}
	}
	else {
		if (valCurpTrabajador == 0 || isNaN(valCurpTrabajador)) {
			$('#txtCurp').val('');
			var codigoError = "P03";
			codError = codigoError;
			bP18 = false;
			limpiarCamposPrevalidacion();
			obtenerMensajeErrorPrevalidador(codigoError);
		}
	}
}

//valida Apellidos y Nombre del trabajador
function validaApellidosyNombre(iValApellidoPaterno, iValApellidoMaterno, iValNombreTrab) {
	var nombres = $('#txtNombre');
	var valNombreTrab = iValNombreTrab;
	var curpTrabajador = $("#txtCurp");
	var valApellidoPaterno = iValApellidoPaterno;
	var valApellidoMaterno = iValApellidoMaterno;
	var apellidoMaterno = $('#txtApellidoMatero');
	var apellidoPaterno = $('#txtApellidoPaterno');

	//Validar Apellidos y Nombres
	if (valApellidoPaterno == 0 && valApellidoMaterno == 0 && valNombreTrab == 0) {//prioridad 21
		var codigoError = "P12";//prioridad 21
		codError = codigoError;
		limpiarCamposPrevalidacion();
		obtenerMensajeErrorPrevalidador(codigoError);
	}
	else {
		if (valApellidoPaterno == 0) {//prioridad 22
			if (valApellidoMaterno == 0) {
				var codigoError = "P07";//prioridad 22
				codError = codigoError;
				obtenerMensajeErrorPrevalidador(codigoError);
			}
			else if (valNombreTrab == 0) {//prioridad 23
				var codigoError = "P08";//prioridad 23
				codError = codigoError;
				obtenerMensajeErrorPrevalidador(codigoError);
			}
			else {
				var codigoError = "P06";//prioridad 26
				codError = codigoError;
				obtenerMensajeErrorPrevalidador(codigoError);
			}

			$('#txtNss').val('');
			$('#txtCurp').val('');
			limpiarCamposPrevalidacion();
		}
		else if (valApellidoMaterno == 0) {//prioridad 24
			if (valNombreTrab == 0) {
				var codigoError = "P10";//prioridad 24
				codError = codigoError;
				obtenerMensajeErrorPrevalidador(codigoError);
			}
			else {
				var codigoError = "P09";//prioridad 27
				codError = codigoError;
				obtenerMensajeErrorPrevalidador(codigoError);
			}

			$('#txtNss').val('');
			$('#txtCurp').val('');
			limpiarCamposPrevalidacion();
		}
		else if (valNombreTrab == 0) {// prioridad 25
			$('#txtNss').val('');
			$('#txtCurp').val('');
			var codigoError = "P11";// prioridad 25
			codError = codigoError;
			limpiarCamposPrevalidacion();
			obtenerMensajeErrorPrevalidador(codigoError);
		}
	}
}



// MENSAJES DE ERRORES
function mensajesErrores(motivoRechazo) {
	motivoRechazo = motivoRechazo.trim();

	if (motivoRechazo != "" && motivoRechazo != "0") {
		limpiarCamposPrevalidacion();
		obtenerMensajeErrorPrevalidador(motivoRechazo);
	}
}

function validaAntiguedadTraspaso(traspasoPrevioRendimientos, valRendimMenor) {
	var codigoError = "";
	var fechaActualTrasp = "";
	var banderaMensajePromotor = 99;
	//var banderaMensajePromotor = obtenerBandMensajePromotor();
	
	var sfechaActual = "";
	var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0'); //Enero es 0
	var yyyy = today.getFullYear();
	var sDay = "";
	var sMes = "";

	today = yyyy + '-' + mm + '-' + dd;
	
	sfechaActual = today;
	
	
	if (traspasoPrevioRendimientos == 1 || valRendimMenor == 1 )
	{
		if (sFecha1TrasPrevio != '' && sFecha1TrasPrevio != '1900-01-01' )
		{

			fechaActualTrasp = sFecha1TrasPrevio;
			validaTranscursoFechasAnio(fechaActualTrasp,sfechaActual);
			
			if(bError){
				codigoError = "P17";
			}
			
		}else if (sFecha2TrasPrevio != '' && sFecha2TrasPrevio != '1900-01-01' ){
			
			fechaActualTrasp = sFecha2TrasPrevio;
			validaTranscursoFechasAnio(fechaActualTrasp,sfechaActual);
			
			if(bError){
				codigoError = "P17";
			}
			
		}else if (sFecha3TrasPrevio != '' && sFecha3TrasPrevio != '1900-01-01' ){
			
			fechaActualTrasp = sFecha3TrasPrevio;
			validaTranscursoFechasAnio(fechaActualTrasp,sfechaActual);
			
			if(bError){
				codigoError = "P17";
			}
			
		}else if (sFecha4TrasPrevio != '' && sFecha4TrasPrevio != '1900-01-01' ){
			
			fechaActualTrasp = sFecha4TrasPrevio;
			validaTranscursoFechasAnio(fechaActualTrasp,sfechaActual);
			
			if(bError){
				codigoError = "P17";
			}
			
		}else if (sFecha5TrasPrevio != '' && sFecha5TrasPrevio != '1900-01-01' ){
			
			fechaActualTrasp = sFecha5TrasPrevio;
			validaTranscursoFechasAnio(fechaActualTrasp,sfechaActual);
			
			if(bError){
				codigoError = "P17";
			}
			
		}else{
			
		}
		
		if(bError){
			BanderaAntiguedadTraspaso = true;
			bError = false;
			limpiarCamposPrevalidacion();
			obtenerMensajeErrorPrevalidador(codigoError);
		}else if (banderaMensajePromotor == 0 && errorMessages.length == 0) {
			MensajePromotor = 1;
			bError = false;
			BanderaAntiguedadTraspaso = true;
			limpiarCamposPrevalidacion();
			obtenerMensajeErrorPrevalidador('MVT');
		}else{
			
		}
		
		
	}else{
		
	}


}




function validaTranscursoFechasAnio(fechaActualTrasp, fechaActual){
	
	
	    var date1 = new Date(fechaActualTrasp);
		var date2 = new Date(fechaActual);

		var Difference_In_Time = date2.getTime() - date1.getTime();
  
		var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
	
		if(Difference_In_Days>365)
		{
			//Trancurrio un año o mas
			console.log("transcurrio un año");
		}else{
			//no ha transcurrido
			codigoError = "P17";
			bError = true;
			var banderaMensajePromotor = obtenerBandMensajePromotor();
		}
	
	
	
}





function validaVigenciaPromotor(valCurpFuncionarioAdministradora) {
	if (valCurpFuncionarioAdministradora == 0) {
		$('#txtNss').val('');
		$('#txtCurp').val('');
		var codigoError = "P18";
		limpiarCamposPrevalidacion();
		obtenerMensajeErrorPrevalidador(codigoError);
	}
}

//Al recibir respuesta de procesar, habilitar campos y colocar opción Traspaso IMSS/Traspaso ISSSTE en campo Tipo de Constancia
function habilitaCamposPrevalidador(sValRespuesta) {
	var valRespuesta = sValRespuesta;

	if (valRespuesta == "OK") {
		desboqueoCajasdeTexto();
	}
}

function respuestaPrevalidador(data) {
	folioPrevalidador = data['folioSolicitud'];
	numeroEmpleado = data['numeroEmpleado'];

	datos =
	{
		opcion: 66,
		data
	}

	$.ajax(
		{
			data: datos,
			success: function (data) {
				if (data.respondioServicio == 1) {
					datos = { folio: folioPrevalidador };
					obtenerRespuestaPrevalidador(datos, manipularRespuestaPrevalidador);
				}
				else {
					if (contadorRespuesta <= 3) {
						var datos =
						{
							folioSolicitud: folioPrevalidador,
							numeroEmpleado: numeroEmpleado
						};

						var tiempoEspera = 10000;
						contadorRespuesta++;
						setTimeout(function () {
							respuestaPrevalidador(datos);
						}, tiempoEspera);
					}
					else {
						$('.content-loader').hide();
						bPrevalNoExitoso = true;
						contadorRespuesta = 0;
						respuestaPreval = true;
						mensaje = "Promotor, el servicio de Prevalidación en Línea por el momento no está ";
						mensaje += "disponible";
						mostrarMensaje(mensaje, sTitle);
					}
				}
			}
		}
	);
}

function validarAforeCedentePrevalidador(claveAforeCedente) {
	var sTipoConstancia = $("#cboTipoConstancia");

	if (claveAforeCedente != "") {
		claveAforeCedente = parseInt(claveAforeCedente);
	}

	if (claveAforeCedente == "" || claveAforeCedente == 0) {
		if ((sTipoConstancia.val() == 27 || tipoAfiliacion == 27) && (valProcesarNssTrabajador == 0 || valProcesarNssTrabajador == 1)) {
			esRegistro = 1;
			contadorRespuesta = 0;
			sTipoConstancia.val(26);
			habilitarCampos = false;
			sTipoConstancia.trigger("change");
			$("#txtCurp").attr("disabled", false);
		}
		else if (sTipoConstancia.val() == 1 || tipoAfiliacion == 1 || sTipoConstancia.val() == 527 || tipoAfiliacion == 527) {
			contadorRespuesta = 0;
			sTipoConstancia.val(33);
			habilitarCampos = false;
			sTipoConstancia.trigger("change");
			$("#txtCurp").attr("disabled", false);
		}

		sVerificacionEstatus = "";
		iSelloVerificacion = 0;

		if (valProcesarNssTrabajador != 2) {
			var codigoError = "P19";
			obtenerMensajeErrorPrevalidador(codigoError);
		}
	}
}

function limpiarCamposPrevalidacion() {
	if(bP16){
		$("#txtNss").val("");
	}else{
		
	$("#txtNss").val("");
	$("#txtCurp").val("");
	$("#txtNombre").val("");
	$("#cboExcepciones").val(-1);
	$("#txtApellidoMatero").val("");
	$("#txtApellidoPaterno").val("");
	$("#txtNss").attr("disabled", true);
	$("#txtNombre").attr("disabled", true);
	$("#txtApellidoMatero").attr("disabled", true);
	$("#txtApellidoPaterno").attr("disabled", true);
	$("#cboTipoConstancia").val(-1).trigger("change");
	
	}
}

function obtenerRespuestaPrevalidador(data, getDataPrevalidador = null) {
	folioRespuestaPrevalidador = data.folio;

	datos =
	{
		opcion: 69,
		data
	}

	$.ajax(
		{
			data: datos,
			success: function (data) {
				if (data.estatus == 1) {
					if (getDataPrevalidador) {
						getDataPrevalidador(data);
					} else {
						console.log('No se esta recibiendo el callback getDataPrevalidador');
					}
				}
				else {
					if (contadorRespuesta <= 3) {
						var datos = { folio: folioRespuestaPrevalidador };

						var tiempoEspera = 10000;
						contadorRespuesta++;
						setTimeout(function () {
							obtenerRespuestaPrevalidador(datos, manipularRespuestaPrevalidador);
						}, tiempoEspera);
					}
					else {
						$('.content-loader').hide();
						respuestaPreval = true;
						contadorRespuesta = 0;
						var codigoError = "P00";
						obtenerMensajeErrorPrevalidador(codigoError);
						var mensaje = errorMessages[0];
						mostrarMensaje(mensaje, sTitle);
					}
				}
			}
		}
	);

	return respuestaPrevalidador;
}

function showMessages(data = null, validaFolioConstanciaImplicaciones = null) {
	var message = "";

	if (errorMessages.length > 0) {
		messages = true;
		validaFolioConstanciaImplicaciones(data);
		respuestaPreval = true;
		habilitarCampos = true;
		$('#bRenapoSol').attr('disabled', false);
		$('#bRenapoTitular').attr('disabled', false);
		document.getElementById("bRenapoSol").classList.add("habilitarImg");
		document.getElementById("bRenapoTitular").classList.add("habilitarImg");
		var title = "";


		if (MensajePromotor == 1) {
			mostrarMensajeProm(errorMessages, title);
		} else {
			mostrarMensaje(errorMessages, title);
		}

		$("#btnConsultaCurp").attr("disabled", false);
	}
	else {
		messages = false;
		validaFolioConstanciaImplicaciones(data);
	}

	tipoAfiliacion = "";
	valProcesarNssTrabajador = "";
	return messages;
}

function bloquearCamposPrevalidacion() {
	if (prevalExitosa) {
		$("#txtNss").attr("disabled", true);
		$("#txtCurp").attr("disabled", true);
		$("#txtNombre").attr("disabled", true);
		$("#btnConsultaCurp").removeClass("habilitarBtn");
		$("#btnConsultaCurp").addClass("deshabilitarBtn");
		$("#btnConsultaCurp").attr("disabled", true);
		$("#cboTipoConstancia").attr("disabled", true);
		$("#txtApellidoMatero").attr("disabled", true);
		$("#cboTipoSolicitante").attr("disabled", true);
		$("#txtApellidoPaterno").attr("disabled", true);

		if ($("#cboExcepciones").val() != -1) {
			$("#cboExcepciones").attr("disabled", true);
		}
		else {
			$("#cboExcepciones").attr("disabled", false);
		}
	}
	else {
		if ($("#cboTipoConstancia").val() == 26 || $("#cboTipoConstancia").val() == 27) {
			if ($("#cboTipoConstancia").val() == 27) {
				$("#cboTipoSolicitante").attr("disabled", true);

				if (afterPrevalidacion) {
					afterPrevalidacion = false;
					$("#txtNss").attr("disabled", true);
					$("#txtCurp").attr("disabled", true);
					$("#txtNombre").attr("disabled", true);
					$("#btnConsultaCurp").attr("disabled", true);
					$("#txtApellidoMatero").attr("disabled", true);
					$("#txtApellidoPaterno").attr("disabled", true);
				}
			}
			else {
				$("#txtNss").attr("disabled", false);
			}
		}
		else {
			$("#txtNss").attr("disabled", true);

			if ($("#cboTipoConstancia").val() == 1 || $("#cboTipoConstancia").val() == 527) {
				$("#cboTipoSolicitante").attr("disabled", true);

				if (afterPrevalidacion) {
					afterPrevalidacion = false;
					$("#txtCurp").attr("disabled", true);
					$("#txtNombre").attr("disabled", true);
					$("#btnConsultaCurp").attr("disabled", true);
					$("#txtApellidoMatero").attr("disabled", true);
					$("#txtApellidoPaterno").attr("disabled", true);
				}
				else {
					if ($("#txTelCelular").val() != "") {
						boqueoCajasdeTexto();
						if (BanderaEstatus != 7 && BanderaEstatus != 8) {
							$('#cboTipoComprobante').attr('disabled', false);
							$('#cboEstadoCivil').attr('disabled', false);
						}
					} else {
						$("#txtCurp").attr("disabled", false);
					}

				}
			}
		}

		if ($("#cboExcepciones").val() != -1) {
			$("#cboExcepciones").attr("disabled", true);
		}
		else {
			$("#cboExcepciones").attr("disabled", false);
		}

		$("#cboTipoConstancia").attr("disabled", true);
	}
}

function prevalidacionExitosa(data) {
	if (messages == false) {
		habilitaCamposPrevalidador(data.sValRespuesta);

		if (habilitarCampos) {
			sTitle = " ";
			prevalExitosa = true;
			contadorRespuesta = 0;
			habilitarCampos = false;
			afterPrevalidacion = true;
			//sMensaje = "Promotor: Favor de colocar su dedo indice sobre el sensor";
			bloquearCamposPrevalidacion();
			//mostrarMensaje(sMensaje, sTitle);
			mostrarmensaje2();//790
		}
	}
}

function inicializarVariables() {
	iRetorno = 0;
	field = [];
	errorFields = {};
	iRespuestaConsulta = 0;
	numeroEmpleado = 0;
	contadorRespuesta = 0;
	iFolioConstanciaImplicaciones = "";
	folioPrevalidador = 0;
	dataRegistrarPrevalidador = 0;
	folioRespuestaPrevalidador = 0;
	errorMessages = [];
}

function manipularRespuestaPrevalidador(data) {
	var sValRespuesta = data.sValRespuesta;
	var iValMarcas = parseInt(data.iValMarcas);
	var iValFolioConstanciaImplicaciones = parseInt(data.iValFolioConstanciaImplicaciones);
	var valNumTrasp36meses = parseInt(data.iValNumTraspTreintaseisMeses);
	var valCitActiva = parseInt(data.iValCitactiva);
	var iValRegimenPensionario = parseInt(data.iValRegimenPensionario);
	var iValNssTrabajador = parseInt(data.iValNssTrabajador);
	var iValCurpTrabajador = parseInt(data.iValCurpTrabajador);
	var iValApellidoPaterno = parseInt(data.iValApellidoPaterno)
	var iValApellidoMaterno = parseInt(data.iValApellidoMaterno);
	var iValNombreTrab = parseInt(data.iValNombreTrab);
	var traspasoPrevioRendimientos = parseInt(data.traspasopreviorendimientos);
	var valRendimMenor = parseInt(data.iValRendimmenor);
	var valCurpFuncionarioAdministradora = parseInt(data.valcurpfuncionarioadministradora);
	aforeCedente = data.cveenttransbd.trim();
	var motivoRechazo = data.motrec;
	var sCodigoError = data.sCodigoError;
	sFecha1TrasPrevio = data.fecha1traspasoprevio;
	sFecha2TrasPrevio = data.fecha2traspasoprevio;
	sFecha3TrasPrevio = data.fecha3traspasoprevio;
    sFecha4TrasPrevio = data.fecha4traspasoprevio;
    sFecha5TrasPrevio = data.fecha5traspasoprevio;
	var datos =
	{
		'iValFolioConstanciaImplicaciones': iValFolioConstanciaImplicaciones,
		'valNumTrasp36meses': valNumTrasp36meses,
		'valCitActiva': valCitActiva
	};
	var dataHabilitar = { 'sValRespuesta': sValRespuesta };
	respuestaPreval = true;
	bPrevalidacionEnCurso = false;
	$('.content-loader').hide();
	mensajesErrores(motivoRechazo);//prioridad 1 AL 6
	validarAforeCedentePrevalidador(aforeCedente);//prioridad 7
	validaVigenciaPromotor(valCurpFuncionarioAdministradora);//prioridad 8
	validaAntiguedadTraspaso(traspasoPrevioRendimientos, valRendimMenor);//prioridad 12,13
	validaRegimenPensionario(iValRegimenPensionario); //prioridad 14
	validaCurpyNssBDNSAR(iValNssTrabajador, iValCurpTrabajador);//prioridad 16,17,18,19,20
	validaApellidosyNombre(iValApellidoPaterno, iValApellidoMaterno, iValNombreTrab);//prioridad 21,22,23.25,26,27
	validaMarca(iValMarcas);//prioridad 15
	showMessages(datos, validaFolioConstanciaImplicaciones);//prioridad 9,10,11
	prevalidacionExitosa(dataHabilitar);
	$('#bAceptar').attr('disabled', true);
	document.getElementById("bAceptar").classList.remove("habilitarImg");
	document.getElementById("bAceptar").classList.add("deshabilitarImg");
}

function obtenerMensajeErrorPrevalidador(codigo) {
	codigoErrorPrevalidador = codigo;

	datos =
	{
		opcion: 70,
		cCodigo: codigo
	};

	$.ajax(
		{
			data: datos,
			success: function (data) {
				if (data.estatus == 1) {
					var mensaje = data.mensaje;

					if (errorMessages == 0) {
						errorMessages.push(mensaje);
						if(iTipoAfiliacion == 1 || iTipoAfiliacion == 27 || iTipoAfiliacion == 527){
							bPrevalNoExitoso=true;
						}
					}
					
		
					if (bPrioridad)
					{

					}
					else{
						if (bP18){
							   
						}else{
								mostrarMensaje(mensaje, sTitle);
							}
					}
					
				}
				else {
					if (contadorRespuesta <= 3) {
						var tiempoEspera = 10000;
						contadorRespuesta++;
						setTimeout(function () {
							obtenerMensajeErrorPrevalidador(codigoErrorPrevalidador);
						}, tiempoEspera);
					}
					else {
						$('.content-loader').hide();
						respuestaPreval = true;
						bPrevalNoExitoso = true;
						contadorRespuesta = 0;
						mensaje = "No se pudo obtener los mensajes correspondientes al proceso de prevalidación";
						mostrarMensaje(mensaje, sTitle);
					}
				}
			}
		}
	);
}

function ejecutaPrevalidacion() {
	datos =
	{
		opcion: 68
	};

	$.ajax(
		{
			data: datos,
			success: function (data) {
				var servicioActivo = parseInt(data.estatus) == 1 ? true : false;
				if (servicioActivo) {
					bPrevalidacionEnCurso = data.estatus == 1 ? true : false;
					//if(OSName == "Android"){ /* REVISAR MOVIL */
					//	bPrevalidacionEnCurso = false;
					//}
				} else {
					bPrevalidacionEnCurso = false;
				}
			}
		}
	);
}

//funcion para saber el estatus de enrolamiento del empleado
function validarenrolamiento(iEmpleado) {
	var estatusEnrolamiento = 0;
	var cadena = '';
	cadena += "opcion=13&empleado=" + iEmpleado;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				estatusEnrolamiento = datos.valor;
			}
		}
	);
	return estatusEnrolamiento;
}

//Esta funcion es para actualizar la tabla de solconstancia
/*1.- El folioenrolamiento = 0 por que aun no tengo un folio de enrolamiento.
2.- El estatusconsulta puede ser igual a 'RE' o 'OK'.
3.- El estatusexpedien puede ser 00 No existe, 01 Incompleto, 02 Temporal, 03 En transito, 05 Permanente.
Es de la consulta a la base de datos y pertenece al empleado(consulta)
4.- El estatusenrol puede ser 00 No existe enrolamiento, 01 Existe enrolamiento y 02 Existe enrolamiento en transito.
Es de la consulta a la base de datos y pertenece al empledo (cosnulta)
5.- El diagconsulta. Dato que pertenece a la base de datos y lo obtenemos de la consulta del empleado.
6.- El estatusverificaciontrab puede ser 'RE' o 'OK'. parte del cliente.
7.- El diagverificaciontrab. tambien es parte del trabajador.
8.- El selloverificacion. Tambien lo obtenemos del tabrajador.*/
function actualizaSolConstancia() {
	var cadena = '';
	cadena += "opcion=19&folioaActualizar=" + folio + "&Cstatus=" + sConsultaEstatus + "&Cstatusexpe=" + sConsultaExpediente;
	cadena += "&Cstatusenrol=" + sConsultaEnrolamiento + "&Cdiagnostico=" + sConsultaDiagnostico + "&Vstatus=" + sVerificacionEstatus;
	cadena += "&Vdiagnostico=" + sVerificacionDiganostico + "&Vsello=" + iSelloVerificacion;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				respuestaC = datos.iValorConsulta;
				marcarLog('actualiza sol constancia '+respuestaC);
				if (parseInt(datos.iValorConsulta) == 1) {
					if (iTipoSolicitante == 1) {
						if (enrolamientopermanente) {
							
							if (bGestorexito){
								validarHuella();
								subirHuellas();

							}else{
								var arresp = verificaExpedientePermanente();
								if ((iTipoAfiliacion != 26 && iTipoAfiliacion != 33) && (arresp['estatusenrol'] == '05' || arresp['estatusenrol'] == '08')) {
									validarHuella();
									subirHuellas();
								}
								else {
									iOpcion = ENROLAR_TRABAJADOR;
									marcarLog("Se asigna bandera enrolamiento proceso principal");									  
									sTitle = "";
									//sMensaje = 'Promotor: inició el proceso de enrolamiento.';
									//mostrarMensaje(sMensaje, sTitle);
									iCodEjecutarEnrol = 'EET';
									mostrarmensaje2();
								}
							}
						}
						else {
							iOpcion = ENROLAR_TRABAJADOR;
							marcarLog("Se asigna bandera enrolamiento proceso principal");										 
							sTitle = "";
							//sMensaje = 'Promotor: inició el proceso de enrolamiento.';
							//mostrarMensaje(sMensaje, sTitle);
							iCodEjecutarEnrol = 'EET';
							mostrarmensaje2();
						}
					}
					else if (((guardarSolConstancia && checkTrabManosLesionadas != 2 && checkTrabSinManos != 2) && (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) && (iTipoSolicitante != 1))) {
						validarHuella();
						subirHuellas();
					}
					else {
						generarFolioEnrolamiento();
					}
				}
				else {
					if(cActualiza < 3)
					{
						cActualiza++;
						if(cActualiza == 3)
						{
							
						}
						else
						{
							actualizaSolConstancia();
						}
					}
				}
			}
		}
	);
}

function obtenerenlace() {
	var cadena = '';
	cadena += "opcion=20";
	//Se vueleve a tomar la curp del trabajador en caso de que el promotor se equivoque e ingrese o modifique una nueva
	sCurpTrabajador = $('#txtCurp').val();
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				var sTipoSolConstancia = $('#cboTipoConstancia').val();
				if (sTipoSolConstancia == 1 || sTipoSolConstancia == 527) {
					sTipoSolConstancia = 27;
				}
				var enlace = datos.dato + '?tiposolicitud=' + sTipoSolConstancia + '&curp=' + sCurpTrabajador + '&foliosolicitud=' + folio + '&empleado=' + iEmpleado;
				if (datos.dato != '') {
					//Se hace un reeplace 3 veces para reemplazar los 3 & de la liga
					var enlaceLog = enlace.replace("&", "_");
					enlaceLog = enlaceLog.replace("&", "_");
					enlaceLog = enlaceLog.replace("&", "_");
					marcarLog("CURP: " + sCurpTrabajador + " - EJECUTARA LA LIGA DE ENROLAMIENTO: " + enlaceLog);//logs
					//Se incializa la variable para su uso posterior
					iCodEjecutarEnrol = '';
				    iOpcion = ENROLAR_TRABAJADOR;
					marcarLog("Se asigna bandera enrolamiento proceso secundario");						
					opcionejecuta(enlace);
				}
			}
		}
	);
}

function consultaEstatusenrol() {
	var cadena = '';
	//Se obtiene la curp de la caja de texto para posteriormente enviarserla, esto en caso de que el pormotor aya cambiado la curp
	sCurpTrabajador = $('#txtCurp').val();
	cadena += 'opcion=21&curp=' + sCurpTrabajador + '&folioservicioafore=' + iFolioSol;
	marcarLog('Cadena consulta Enrolamiento --> ' + cadena);
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				var regreso = datos.iEstatusEnrolamiento;
				marcarLog('consultaEstatusenrol  CURP = ' + sCurpTrabajador + ' FOLIO = ' + iFolioSol + ' ESTATUSENROLAMIENTO = ' + datos.iEstatusEnrolamiento + ' SELLOVERIFICACION = ' + iSelloVerificacion + ' CodigoEjecuta = ' + iCodEjecutarEnrol);  //logs

				//datos.iEstatusEnrolamiento = 3;
				if ((datos.iEstatusEnrolamiento == 3) && (iSelloVerificacion > 0)) {
					$('.content-loader').hide();
					validarRespuestaFolio();
				}
				else if (datos.iEstatusEnrolamiento == 9) {
					$('.content-loader').hide();
					sTitle = "";
					sMensaje = "Promotor: Se canceló el enrolamiento, favor de volverlo a intentar.";
					marcarLog("CURP: " + sCurpTrabajador + " - SE LE CANCELÓ EL ENROLAMIENTO");//logs
					//Inicializamos la variable para que solamente entre al flujo que le corresponde y usarla posteriormente
					iCodEjecutarVideo == '';
					//Ejecutar Enrolamiento Trabajador(EET)
					iCodEjecutarEnrol = 'EET';
					mostrarMensajeCancelEnrol(sMensaje, sTitle);
					$('#bGrabacion').attr('disabled', true);
					document.getElementById("bGrabacion").classList.add("deshabilitarImg");
				}
				else if (datos.iEstatusEnrolamiento == 0) {
					//Inicializamos la variable para que solamente entre al flujo que le corresponde usarla posteriormente
					$('.content-loader').hide();
					iCodEjecutarVideo = '';
					sMensaje = "Promotor: Ocurrio un problema al guardar el enrolamiento favor de intentarlo de nuevo.";
					marcarLog("CURP: " + sCurpTrabajador + " - LE OCURRIO UN PROBLEMA AL GUARDAR EL ENROLAMIENTO");//logs
					iCodEjecutarEnrol = 'EET';
					mostrarMensaje(sMensaje, sTitle);
					$('#bGrabacion').attr('disabled', true);
					document.getElementById("bGrabacion").classList.add("deshabilitarImg");
				}
			}
		}
	);
}

//validacion del teclado
function validarKeypressCurp(event) {
	if (!(arrCaracteresCurp.indexOf(event.which) > -1)) {
		if (!(event.which >= 65 && event.which <= 90 || event.which >= 97 && event.which <= 122 || event.which >= 48 && event.which <= 57)) {
			event.preventDefault();
		}
	}
}

function validarKeypressNombre(event) {
	if (!(arrCaracteresNombre.indexOf(event.which) > -1)) {
		if (!(keyCode >= 65 && keyCode <= 90 || keyCode >= 97 && keyCode <= 122 || keyCode == 32 || keyCode == 39 || keyCode >= 45 && keyCode <= 47 || keyCode == 95 || keyCode >= 152 && keyCode <= 154 || keyCode == 129 || keyCode == 132 || keyCode == 137 || keyCode == 139 || keyCode == 142 || keyCode == 148)) {
			event.preventDefault();
		}
	}
}

function validarKeypressApellidos(event) {
	if (!(arrCaracteresApellidos.indexOf(event.which) > -1)) {
		if (!(keyCode >= 65 && keyCode <= 90 || keyCode >= 97 && keyCode <= 122 || keyCode == 32 || keyCode == 39 || keyCode >= 45 && keyCode <= 47 || keyCode == 95 || keyCode >= 152 && keyCode <= 154 || keyCode == 129 || keyCode == 132 || keyCode == 137 || keyCode == 139 || keyCode == 142 || keyCode == 148)) {
			event.preventDefault();
		}
	}
}

function validarKeypressSoloNumeroCons(event) {
	if ((event.which < 48 && event.which > 58) && event.which != 0)
		event.preventDefault();
}


function validarKeypressAlfanumericoCons(event) {
	if (!(arrCaracteresPermitidosCons.indexOf(event.which) > -1)) {
		if (!(event.which >= 65 && event.which <= 90 || event.which >= 97 && event.which <= 122 || event.which >= 48 && event.which <= 57)) {
			event.preventDefault();
		}
	}
}

function validarKeypressNumCasa(event) {
	if (!(arrCaracteresNumCasa.indexOf(event.which) > -1)) {
		if (!(event.which >= 65 && event.which <= 90 || event.which >= 97 && event.which <= 122 || event.which >= 48 && event.which <= 57)) {
			event.preventDefault();
		}
	}
}

function llamadoErrores(diagnostico) {
	var cadena = '';
	cadena += 'opcion=23&diagnostico=' + diagnostico;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				var sMensajeBiometrico = data.mensajeretorno;

				if (data.mensajeretorno != 'NA' && data.mensajeretorno != 'N/A') {
					mostrarMensaje(sMensajeBiometrico, "");
				}
				else {
					$('.content-loader').hide();
					sMensajeBiometrico = '[' + diagnostico + ']' + '  Promotor: Favor de comunicarte con mesa de ayuda';
					mostrarMensaje(mensaje, "");
				}
			}
		});
}

function actualizaEnrolHuellaServicio(iFolioConsulta, iFolioVerificacion, sCurpTrabajador) {
	var cadena = '';
	var sEstatusRetorno = 0;
	//Obtenemos la curp de la caja de texto en caso de que el promotor se haya equivocado y este tenga que coregirla se mande la nueva curp
	sCurpTrabajador = $('#txtCurp').val();
	cadena += 'opcion=24&curp=' + sCurpTrabajador + '&folioOperacionC=' + iFolioConsulta + '&folioOperacionV=' + iFolioVerificacion;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				datos = data;
				sEstatusRetorno = datos.sEstatus;
			}
		}
	);
	return sEstatusRetorno;
}

function anularBotonDerecho(e) {
	if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2)) {
		sTitle = "";
		sMensaje = "Promotor por seguridad no puede usar esta opcion.";
		mostrarMensaje(sMensaje, sTitle);
		return false;
	}
}

function bloquearUI() {
	//eliminamos si existe un div ya bloqueando
	//desbloquearUI();
	var tiempo = 300000;

	$(".content-loader").show();

	//TimeOut para cuando no se obtiene respuesta de los servicios
	setTimeout(function () {

		desbloquearUI();

	}, tiempo);
}

function desbloquearUI() {
	// eliminamos el div que bloquea pantalla
	$(".content-loader").hide();

	if (iKeyxinfBancoppel == 0) {
		$('.content-loader').hide();
		sFlujoExtraccion = 1;
		sTitle = "";
		sMensaje = "Promotor: Ah\u00CD un problema con la comunicaci\u00F3n con BanCoppel, Favor de realizar la captura normalmente.";
		mostrarMensaje(sMensaje, sTitle);
	}

}

//Metodo para mostrar la IFE/INE o direccion del cliente bancoppel en un dialog
function mostrarIfeDireccion() {
	if (sEstatusImagen == 1 || sEstatusImagen == '1') {
		var sHtml = '';
		sHtml = "<table  class=\"jsgrid\" align=\"center\">";
		sHtml += "<tr>";
		sHtml += "<td> <img src=\"data:image/png;base64," + imagenR + "\"  width=\"300px\"></img> </td>";
		sHtml += "<td> <img src=\"data:image/png;base64," + imagenA + "\"  width=\"300px\"></img> </td>";
		sHtml += "</tr>";
		sHtml += "<tr>";
		sHtml += "<td><button id=\"btnAceptar\" class=\"ui-button ui-widget ui-state-default\" onclick=\"aceptarIFE();\">&nbsp;&nbsp;Aceptar&nbsp;&nbsp;</button></td>";
		sHtml += "<td><button id=\"btnOmitir\" class=\"ui-button ui-widget ui-state-default\" onclick=\"omitirIFE();\">&nbsp;&nbsp;Omitir&nbsp;&nbsp;</button></td>";
		sHtml += "</tr>";
	}
	else
		iFlagIdentificacion = 1;

	sHtml += "<tr>";
	if (iDireccionMostrar == 1 && sEstatusImagen == 1) {
		sHtml += "<td> <input type=\"radio\"  id =\"rbnDireccionone\" name=\"rbnDireccion\" value=\"001\" checked> " + sPrimeraDireccion + " </td>";
	}
	else if (iDireccionMostrar == 2 && sEstatusImagen == 1) {
		sHtml += "<td> <input type=\"radio\"  id =\"rbnDirecciontwo\" name=\"rbnDireccion\" value=\"002\" checked> " + sSegundaDireccion + " </td>";
	}
	else if (iDireccionMostrar == 3) {
		sHtml += "<td><input type=\"radio\"  id =\"rbnDireccionone\" name=\"rbnDireccion\" value=\"001\"> " + sPrimeraDireccion + "</td>";
		sHtml += "<td><input type=\"radio\"  id =\"rbnDirecciontwo\" name=\"rbnDireccion\" value=\"002\"> " + sSegundaDireccion + "</td>";
	}
	sHtml += "</tr>";
	sHtml += "</table>";

	$('#divIfeDireccoin').hide(0);
	$('#divIfeDireccoin').dialog
		({
			autoOpen: false,
			resizable: false,
			width: '50%',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {

				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html(sHtml);

			},
			buttons:
			{
				"Aceptar": function () {
					$(this).dialog("close");
					if (iFlagIdentificacion != 0) {
						if (iDireccionMostrar != 4) {
							if (document.getElementById('rbnDireccionone').checked) {
								sDireccionElegida = $('#rbnDireccionone').val();
								iDireccionMostrar = 1;
								validardireccionseleccionada(sDireccionElegida);
							}
							else if (document.getElementById('rbnDirecciontwo').checked) {
								sDireccionElegida = $('#rbnDirecciontwo').val();
								iDireccionMostrar = 2;
								validardireccionseleccionada(sDireccionElegida);
							}
							else
								validardireccionseleccionada(sDireccionElegida);
						}
						else {
							validardireccionseleccionada(sDireccionElegida);
						}
					}
					else {
						$('.content-loader').hide();
						opcionMensaje = "Promotor: Favor de aceptar u omitir la IFE o INE";
						Muestramensajenoseleccionado(opcionMensaje);
					}
				},
				"Omitir": function () {
					$(this).dialog("close");
					if (iFlagIdentificacion != 0) {
						if (iDireccionMostrar != 4) {
							$('.content-loader').hide();
							opcionMensaje = "Promotor: Seguro que quiere omitir la eleccion de una direcci\u00F3n";
							MuestramensajeCancelacion(opcionMensaje);
						}
						else {
							validardireccionseleccionada(sDireccionElegida)
						}
					}
					else {
						$('.content-loader').hide();
						opcionMensaje = "Promotor: Favor de aceptar u omitir la Identificacion";
						Muestramensajenoseleccionado(opcionMensaje);
					}
				}
			}
		});
	$("#divIfeDireccoin").siblings('div.ui-dialog-titlebar').remove();
	$('#divIfeDireccoin').dialog("open");

}

function validardireccionseleccionada(sDireccionElegida) {
	if (sDireccionElegida != '' || iDireccionMostrar == 4) {
		//Se manda llamar al metodo que llenara todos los campos de la constancia
		if (iFlagIdentificacion == 1) {
			actualizarflagclientebcpl(iKeyxinfBancoppel);
		}
		llenaformulariodatosBancoppel(aInformacionBancoppel);
	}
	else {
		$('.content-loader').hide();
		opcionMensaje = "Promotor: Favor de elegir la direcci\u00F3n del cliente";
		Muestramensajenoseleccionado(opcionMensaje);
	}
}

//Metodos para saber si se omitio la INE o IFE que se muestra en un mensaje
function aceptarIFE() {
	iFlagIdentificacion = 2;
}

function omitirIFE() {
	iFlagIdentificacion = 1;
}

//Metodo para actualizar el campo flagidentificacion de la auclientebancoppel
function actualizarflagclientebcpl(iFoliobcpl) {
	var cadena = "";
	cadena += "opcion=30&keyxbancoppel=" + iFoliobcpl;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				//No se hace nada
			}
		});
}

//FIN DE IMPLENTACION DE ANDRES

function grabarLogJs(sConsultaEstatus, sConsultaDiagnostico, sVerificacionEstatus, sVerificacionDiganostico, sConsultaExpediente, sConsultaEnrolamiento, iSelloVerificacion, sTipoConstancia) {
	var cadena = '';

	cadena += 'opcion=25&Cstatus=' + sConsultaEstatus + '&Cdiagnostico=' + sConsultaDiagnostico + '&Vstatus=' + sVerificacionEstatus + '&Vdiagnostico=' + sVerificacionDiganostico;
	cadena += '&Cstatusexpe=' + sConsultaExpediente + '&Cstatusenrol=' + sConsultaEnrolamiento + '&Vsello=' + iSelloVerificacion + '&tipoconstancia=' + sTipoConstancia;

	$.ajax(
		{
			data: cadena,
			success: function (data) {
				//No se realiza ninguna accion
			}
		}
	);
}

//funciones para el uso de curp por RENAPO
function consultarRenapo(iEmpleado) {
	var cLiga = "";
	var iProceso = 1;
	var iSubProceso = 8;

	if (iFolAfore == "") {
		iFolAfore = 0;
	}
	var sParametros =
		"opcion=36&opcionRenapo=" + 1 + "&folioservicioafore=" + iFolAfore;
	var liga;
	marcarLog("entro a obtener la liga de renapo con parametros " + sParametros);

	$.ajax({
		data: sParametros,
		success: function (datos) {
			if (datos.estatus == 1) {
				liga = datos.cLiga;
			}

			$.ajax({
				url: liga,
				dataType: "html",
				success: function (data) {
					$("#ren").html(data);
					iniDivConsultaRenapo(detonarServicioRenapo, folio, iEmpleado, iProceso, iSubProceso);
				}
			});
		}
	});
}


function consultarRenapoSol(iEmpleado) {
	var cLiga = "";
	var iProceso = 1;
	var iSubProceso = 462;
	cLiga = obtenerLigaRenapo(1, iFolAfore);
	marcarLog("entro a abrir la pantalla de renapo: " + cLiga);
	validaLiga(cLiga);

	$.ajax({
		url: cLiga,
		dataType: "html",
		success: function (data) {
			$("#ren").html(data);
			iniDivConsultaRenapo(detonarServicioRenapoSol, folio, iEmpleado, iProceso, iSubProceso);
		}
	});
}

function validaLiga(cLiga) {
	var cadena = '';
	var bAux = false;
	cadena += 'opcion=38&liga=' + cLiga;
	$.ajax(
		{
			data: cadena,
			success: function (data) {
				if (data.bandera == 1) {
					bAux = true;
				}
			}
		});
	return bAux;
}

function detonarServicioRenapo() {
	//si se ejecuta el servicio de renapo  se limpian las variables
	bEsRenapo = false;//renapo es falso
	iErrorRenapo = -1;// error de renapo es -1
	var divRespuestaRenapo 	= document.getElementById('divRespuestaRenapo');
	var addRespuesta = '';
	
	iFolAfore = arrDatosRenapo[0];
	var iError = arrDatosRenapo[1];
	habilitaCampos();

	if (iError == 1) 
	{
		bloquearBtnRenapo();
	}

	if (iFolAfore > 0) 
	{
		var sParametros = "opcion=33&folioservicioafore=" + iFolAfore;
		$.ajax
		({
			data: sParametros,
			success: function (data) {
				respuesta = data;

				if (!bTerceraFigura) {
					$('#btnConsultaCurp').attr('disabled', false);
				}

				$('#bRenapoSol').attr('disabled', false);
				document.getElementById("bRenapoSol").classList.add("habilitarImg");

				iErrorRenapo = respuesta['iRespuestaRen'];
				$("#txtCurp").val(respuesta['cCurp'].trim());
				$("#txtApellidoPaterno").val(respuesta['cApellidopaterno'].trim());
				$("#txtApellidoMatero").val(respuesta['cApellidomaterno'].trim());
				$("#txtNombre").val(respuesta['cNombres'].trim());

				//variables para constancia impresion
				sNombreRenapo = respuesta['cNombres'].trim();
				sApePatRenapo = respuesta['cApellidopaterno'].trim();
				sApeMatRenapo = respuesta['cApellidomaterno'].trim();
				sCurpRenapo = respuesta['cCurp'].trim();

				if (iErrorRenapo == 0) {//si es ok con datos
					bEsRenapo = true;
					bBanderaRenapo = true;

					//Folio 539 Hace la consulta al webservice de bancoppel
					if($('#cboTipoSolicitante').val()==1){
						esperaConsultaWSBancoppel(sCurpRenapo);
						
					}

					//se bloquean datos de renapo por respuesta OK + DATOS
					boqueoCajasdeTexto();

					if ($('#cboTipoConstancia').val() == 1 || $('#cboTipoConstancia').val() == 33 || $('#cboTipoConstancia').val() == 527) {
						$("#txtNss").attr("disabled", true);
					}
					else {
						$('#txtNss').attr('disabled', false);
					}
					//Valida Renapo Titular o Beneficiario para mostrar la Marca RENAPO
					if(iTipoSolicitante == 1)
					{
						divRespuestaRenapo.innerHTML = '';
						addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagen/RenapoExitoso.png" width = "132" height = "100"   VSPACE="10"></input>';
						divRespuestaRenapo.innerHTML = addRespuesta;
						iRenapo = 1;
					}
					else
					{
						divRespuestaRenapo.innerHTML = '';
						addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagen/Renapo.png" width = "132" height = "100"   VSPACE="10"></input>';
						divRespuestaRenapo.innerHTML = addRespuesta;
					}
					/*if(iTipoSolicitante == 1)
					{
						$('#btnConsultaCurp').attr('disabled', false);
					}*/

				}
				else if (iErrorRenapo == 1) {//extrajo datos desde rensolicitudescurp
					$('#txtCurp').attr('disabled', false);
					if (iTipoAfiliacion != 33)
						$('#txtNss').attr('disabled', false);
					if (sCurpRenapo == '') {//si no trae curp la consulta fue por datos
						$('#txtApellidoPaterno').attr('disabled', false);
						$('#txtApellidoMatero').attr('disabled', false);
						$('#txtNombre').attr('disabled', false);
					}
					else {
						bBanderaRenapo = false;

						//Folio 539 Muestra la pantalla de captura de la llave de datos Bancoppel cuando falla renapo
						if($('#cboTipoSolicitante').val()==1){
							mostrarCapturaDatosBancoppel();
						}

						$('#cboTipoConstancia').change(function () {
							if ($('#cboTipoConstancia').val() == 27 || $('#cboTipoConstancia').val() == 1) {
								$('#txtNss').attr('disabled', false);
								$('#txtCurp').attr('disabled', false);
								$('#txtApellidoPaterno').attr('disabled', false);
								$('#txtApellidoMatero').attr('disabled', false);
								$('#txtNombre').attr('disabled', false);
							}
							else {
								$('#txtCurp').attr('disabled', false);
								$('#txtApellidoPaterno').attr('disabled', true);
								$('#txtApellidoMatero').attr('disabled', true);
								$('#txtNombre').attr('disabled', true);
							}
						});
						divRespuestaRenapo.innerHTML = '';
						addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagen/Renapo.png" width = "132" height = "100"   VSPACE="10"></input>';
						divRespuestaRenapo.innerHTML = addRespuesta;
					}
				}
				else {
					bBanderaRenapo = false;

					//Folio 539 Muestra la pantalla de captura de la llave de datos Bancoppel cuando falla renapo
					if($('#cboTipoSolicitante').val()==1){
						mostrarCapturaDatosBancoppel();
					}

					$('#cboTipoConstancia').change(function () {
						if ($('#cboTipoConstancia').val() == 27 || $('#cboTipoConstancia').val() == 1) {
							$('#txtNss').attr('disabled', false);
							$('#txtCurp').attr('disabled', false);
							$('#txtApellidoPaterno').attr('disabled', false);
							$('#txtApellidoMatero').attr('disabled', false);
							$('#txtNombre').attr('disabled', false);
						}
						else {
							$('#txtCurp').attr('disabled', false);
							$('#txtApellidoPaterno').attr('disabled', true);
							$('#txtApellidoMatero').attr('disabled', true);
							$('#txtNombre').attr('disabled', true);
						}
					});
					divRespuestaRenapo.innerHTML = '';
					addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagen/Renapo.png" width = "132" height = "100"   VSPACE="10"></input>';
					divRespuestaRenapo.innerHTML = addRespuesta;
				}
			}
		});
	}
	else {
		$('#txtCurp').attr('disabled', false);
	}
}

function detonarServicioRenapoSol() {

	//si se ejecuta el servicio de renapo  se limpian las variables
	bEsRenapoSol = false;//renapo es falso
	iErrorRenapoSol = -1;// error de renapo es -1
	var divRespuestaRenapo 	= document.getElementById('divRespuestaRenapo');
	var addRespuesta = '';
	
	iFolAforeSol = arrDatosRenapo[0];
	var iErrorSol = arrDatosRenapo[1];
	habilitaCampos();
	if (iErrorSol == 1) 
	{
		bloquearBtnRenapo();
	}

	if (iFolAforeSol > 0) 
	{
		var sParametros = "opcion=33&folioservicioafore=" + iFolAforeSol;
		$.ajax
		({
			data: sParametros,
			success: function (data) {
				respuesta = data;

				iErrorRenapoSol = respuesta['iRespuestaRen'];
				$("#txtCurp2").val(respuesta['cCurp'].trim());
				sApellidoPaternoBenef = respuesta['cApellidopaterno'].trim();
				sApellidoMaternoBenef = respuesta['cApellidomaterno'].trim();
				sNombreBeneficiario = respuesta['cNombres'].trim();
				sCurpRenapoSol = $('#txtCurp2').val();;
				if (iErrorRenapoSol == 0) {//si es ok con datos
					bEsRenapoSol = true;
					if(bBanderaRenapo)
					{
						divRespuestaRenapo.innerHTML = '';
						addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagen/RenapoExitoso.png" width = "132" height = "100"></input>';
						divRespuestaRenapo.innerHTML = addRespuesta;
						iRenapo = 1;
					}
					//se bloquean datos de renapo por respuesta OK + DATOS
					$('#txtCurp2').attr('disabled', true);
					$('#btnConsultaCurp').attr('disabled', false);
				}
				else if (iErrorRenapoSol == 1) {//extrajo datos desde rensolicitudescurp
					if (sCurpRenapoSol == '') {//si no trae curp la consulta fue por datos
						$('#txtCurp2').attr('disabled', false);
					}
					else {
						$('#txtCurp2').attr('disabled', false);
					}
					divRespuestaRenapo.innerHTML = '';
					addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagen/Renapo.png" width = "132" height = "100"></input>';
					divRespuestaRenapo.innerHTML = addRespuesta;
					capturarDatosSolicitante();
				}
				else {
					$('#txtCurp2').attr('disabled', false);
					divRespuestaRenapo.innerHTML = '';
					addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagen/Renapo.png" width = "132" height = "100"></input>';
					divRespuestaRenapo.innerHTML = addRespuesta;
					capturarDatosSolicitante();
				}
			}
		});
	}
	else {
		capturarDatosSolicitante();
		$('#txtCurp2').attr('disabled', false);
		//$('#btnConsultaCurp').attr('disabled', false);
	}
}
function bloquearBtnRenapo() {
	//desabilitamos el boton
	$('#btnRenapo').attr("disabled", true);

	//corre el tiempo en milisegundos para volver a habilitar el boton
	setTimeout(function () {
		$('#btnRenapo').button("enable");
	}, 300000);
}

function obtenerLigaRenapo(opcionLiga, iFolAfore) {
	if (iFolAfore == "") {
		iFolAfore = 0;
	}

	var sParametros = "opcion=36&opcionRenapo=" + opcionLiga + "&folioservicioafore=" + iFolAfore;
	var liga;
	marcarLog('entro a obtener la liga de renapo con parametros ' + sParametros);
	$.ajax({
		data: sParametros,
		success: function (datos) {
			if (datos.estatus == 1) {
				liga = datos.cLiga;
			}
		}
	});
	return liga;
}

function obtenerLigaRenapoSol(opcionLiga, iFolAforeSol) {
	if (iFolAforeSol == "") {
		iFolAforeSol = 0;
	}

	var sParametros = "opcion=36&opcionRenapo=" + opcionLiga + "&folioservicioafore=" + iFolAforeSol;
	var liga;
	marcarLog('entro a obtener la liga de renapo con parametros ' + sParametros);
	$.ajax({
		data: sParametros,
		success: function (datos) {
			if (datos.estatus == 1) {
				liga = datos.cLiga;
			}
		}
	});
	return liga;
}

function generarFormatoRenapo(iFolAfore) {
	if (iFolAfore > 0) {
		var cLiga = obtenerLigaRenapo(2, iFolAfore);
		$.ajax
			(
				{
					url: cLiga,
					success: function (data) {
						//GENERO FORMATO RENAPO
					}
				}
			);
	}
}

function generarFormatoRenapoSol(iFolAforeSol) {
	if (iFolAforeSol > 0) {
		var cLiga = obtenerLigaRenapoSol(2, iFolAforeSol);
		$.ajax
			(
				{
					url: cLiga,
					success: function (data) {
						//GENERO FORMATO RENAPO
					}
				}
			);
	}
}

//funcion que actualiza el folio en la tabla de renapo
function actualizaFolioRenapo(folio, iFolAfore) {
	var sParametros = "";
	sParametros = "opcion=35&folioConst=" + folio + "&folioservicioafore=" + iFolAfore;

	$.ajax({
		data: sParametros,
		success: function (data) {
			if (data.iRespuesta == 1) {
				//se realizo correcta la actualizacion
				marcarLog('se realizo correcta la actualizacion del folio renapo : ' + iFolAfore);
			}
		}
	});
}

function actualizaFolioRenapoSol(folio, iFolAforeSol) {
	var sParametros = "";
	sParametros = "opcion=35&folioConst=" + folio + "&folioservicioafore=" + iFolAforeSol;

	$.ajax({
		data: sParametros,
		success: function (data) {
			if (data.iRespuesta == 1) {
				//se realizo correcta la actualizacion
				marcarLog('se realizo correcta la actualizacion del folio renapo : ' + iFolAforeSol);
			}
		}
	});
}

function consultaRespuestaRenapo(iFolioAfore) {
	var cadena = '';
	iRespuestaRenapoTitular = 0;
	cadena += "opcion=32&folioservicioafore=" + iFolioAfore;
	$.ajax
		({
			data: cadena,
			success: function (data) {
				respuesta = data;
				try {
					if (respuesta.estatus == 1) {
						iRespuestaRenapo = respuesta.codigoerrorrenapo;
						iRespuestaRenapoTitular = respuesta.codigoerrorrenapo;
						iTipoError = respuesta.tipoerror;
						sDecsEroor = respuesta.descerror;

						if (iRespuestaRenapo == 0) {/* Mensaje de exito */
							sTitle = "Mensaje";
							mensaje = "Consulta Exitosa";
							iRespuestaRenapoTitular = 1;
							mostrarMensaje(mensaje, sTitle);
						}
						else if (iRespuestaRenapo > 0) {/* ocurrio un error al generar la curp dentro del WS*/
							sTitle = "ERROR";
							mensaje = sDecsEroor;
							mostrarMensaje(mensaje, sTitle);

							//llamar funcion de accion del error
							//accionErrorRenapo(iRespuestaRenapo,iTipoError);
						}
						else {/* error al ejecutar la consulta de respuesta */
							sTitle = "ERROR";
							mensaje = "Error al consultar la respuesta";
							/* muestra este mensaje cuando la funcion regresa vacio*/
							mostrarMensaje(mensaje, sTitle);

						}
					}
					else {
						if (iVueltaRenapo < 1) {
							iVueltaRenapo++;
							consultarenapo();
						}
						else {
							iVueltaRenapo = 0;
							Title = "ERROR";
							mensaje = respuesta.MensajeDescrip;
							mostrarMensaje(mensaje, sTitle);
						}

					}
				}
				catch (err) {
					sTitle = "";
					sMensaje = err.toString();
					mostrarMensaje(sMensaje, sTitle);
				}
			}
		});
}

function consultaRespuestaRenapoSol(iFolioAforeSol) {
	var cadena = '';
	cadena += "opcion=32&folioservicioafore=" + iFolioAforeSol;
	$.ajax
		({
			data: cadena,
			success: function (data) {
				respuesta = data;
				try {
					if (respuesta.estatus == 1) {
						iRespuestaRenapo = respuesta.codigoerrorrenapo;
						iTipoError = respuesta.tipoerror;
						sDecsEroor = respuesta.descerror;

						if (iRespuestaRenapo == 0) {/* Mensaje de exito */
							sTitle = "Mensaje";
							mensaje = "Consulta Exitosa"
							mostrarMensaje(mensaje, sTitle);

						}
						else if (iRespuestaRenapo > 0) {/* ocurrio un error al generar la curp dentro del WS*/
							sTitle = "ERROR";
							mensaje = sDecsEroor;
							mostrarMensaje(mensaje, sTitle);
							capturarDatosSolicitante();

						}
						else {/* error al ejecutar la consulta de respuesta */
							sTitle = "ERROR";
							mensaje = "Error al consultar la respuesta";
							/* muestra este mensaje cuando la funcion regresa vacio*/
							mostrarMensaje(mensaje, sTitle);
							capturarDatosSolicitante();

						}
					}
					else {
						if (iVueltaRenapo < 1) {
							iVueltaRenapo++;
							consultarenapo();
						}
						else {
							iVueltaRenapo = 0;
							Title = "ERROR";
							mensaje = respuesta.MensajeDescrip;
							mostrarMensaje(mensaje, sTitle);
							capturarDatosSolicitante();
						}

					}
				}
				catch (err) {
					sMensaje = err.toString();
					mostrarMensaje(sMensaje, "");
					capturarDatosSolicitante();
				}
			}
		});
}

function consultarenapo() {
	setTimeout(function () {
		consultaRespuestaRenapo(iFolioAfore);
	}, 5000);
}

function consultarenapoSol() {
	setTimeout(function () {
		consultaRespuestaRenapo(iFolioAforeSol);
	}, 5000);
}

function activarCampos() {
	$('#cboTipoTelefono').attr('disabled', false);
	$('#txtTelContacto').attr('disabled', false);
	$('#cboCompTelefCont').attr('disabled', false);
}

function validarRegistroRenapo() {
	var band = false;
	var cCurpTrabajador = "";
	var sParametros = "";

	//obtenemos la curp del campo en la constancia ya que puede que este en blanco
	cCurpTrabajador = $('#txtCurp').val();

	//validamos la curp ya que si esta en blanco no necesitamos hacer nada
	if (cCurpTrabajador.length > 0) {
		var iEstruturacurp = validarEstructuraCurp(cCurpTrabajador);
		if (iEstruturacurp == 1) {
			//el campo curp si estaba lleno
			sParametros = "opcion=34&curp=" + cCurpTrabajador;
			$.ajax({
				data: sParametros,
				success: function (dato) {
					if (dato.iRespuesta == 1) {
						//se encontro la curp en la tabla
						band = true;
					}
					else {
						//no se encontro la curp en la tabla
						band = false;
					}
				}
			});
			//devolvemos la variable
			return band;
		}
	}
	else {
		//el campo curp esta en blanco devolvemos un false
		band = false;
		return band;
	}
}

function validarRegistroRenapoSol() {
	var band = false;
	var cCurpTrabajador = "";
	var sParametros = "";

	//obtenemos la curp del campo en la constancia ya que puede que este en blanco
	cCurpTrabajador = $('#txtCurp2').val();

	//validamos la curp ya que si esta en blanco no necesitamos hacer nada
	if (cCurpTrabajador.length > 0) {
		var iEstruturacurp = validarEstructuraCurp(cCurpTrabajador);
		if (iEstruturacurp == 1) {
			//el campo curp si estaba lleno
			sParametros = "opcion=34&curp=" + cCurpTrabajador;
			$.ajax({
				data: sParametros,
				success: function (dato) {
					if (dato.iRespuesta == 1) {
						//se encontro la curp en la tabla
						band = true;
					}
					else {
						//no se encontro la curp en la tabla
						band = false;
					}
				}
			});
			//devolvemos la variable
			return band;
		}
	}
	else {
		//el campo curp esta en blanco devolvemos un false
		band = false;
		return band;
	}
}

function deshabilitaCampos() {
	//$('#AltaUnica').attr('disabled', true);
	if (OSName == "Android") {
		document.getElementById("btnConsultaCurp").classList.add("deshabilitarBtn");
		//document.getElementById("AltaUnica").classList.add("deshabilitarBtn");

		$('#bCancelarModulo').attr('disabled', true);
		document.getElementById("bCancelarModulo").classList.add("deshabilitarImg");

	} else {

		$('#bCancelarModulo').attr('disabled', true);
		document.getElementById("bCancelarModulo").classList.add("deshabilitarImg");

	}

}

function habilitaCampos() {
	/* Folio 539
	if ($('#cboTipoConstancia').val() == 33) {
		$('#AltaUnica').attr('disabled', true);
		document.getElementById("AltaUnica").classList.add("deshabilitarBtn");
	} else {
		$('#AltaUnica').attr('disabled', false);
		document.getElementById("AltaUnica").classList.add("habilitarBtn");
	}*/

	if (OSName == "Android") {

		document.getElementById("btnConsultaCurp").classList.add("habilitarBtn");
		//document.getElementById("AltaUnica").classList.add("habilitarBtn");

		$('#bCancelarModulo').attr('disabled', false);
		document.getElementById("bCancelarModulo").classList.add("habilitarImg");

	} else {

		$('#bCancelarModulo').attr('disabled', false);
		document.getElementById("bCancelarModulo").classList.add("habilitarImg");
	}
}

function marcarLog(cTexto) {
	var sParametros = '';
	sParametros = 'opcion=37&descripcion=' + cTexto;

	$.ajax({
		data: sParametros,
		success: function (dato) {

		}
	});
}

function validarListaNegraTelefonoCel() {
	var cadena = '';
	var bRetorna = false;
	var numerocel = $('#txTelCelular').val();
	var curp = $('#txtCurp').val();
	var sMensajeTelefono1 = "";
	var iIdentificador = 0;

	$.ajax(
		{
			data:
			{
				opcion: 29,
				numerocel: numerocel,
				//telcontacto: telcontacto ,
				empleado: iEmpleado,
				curp: curp
				//empleado: empleado

			},
			success: function (data) {
				sMensajeTelefono1 = data.mensaje;
				iIdentificador = data.identificador;
				if (iIdentificador == 0 || iIdentificador == 4) {
					validarListaNegraTelefonofijo();
					bRetorna = true;
				}
				else {
					sTitle = "AVISO";
					sMensaje = (sMensajeTelefono1);
					mensajeAdvertencia(sMensaje, sTitle);
				}
			},
			error: function (a, b, c) {
				sTitle = " ";
				sMensaje = " Ocurrio un error al realizar las validaciones de los telefonos  ";
				mensajeAdvertencia(sMensaje, sTitle);
			}
		});
	return bRetorna;
}

function mensajeAdvertencia(sMensaje) {
	$("#divMensaje").html("");
	$("#divMensaje").append("<p style='text-align: justify;'><table><tr><td><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span></td><td>" + sMensaje + "</td></tr><table></p>");
	$("#divMensaje").dialog({
		resizable: false,
		height: 250,
		width: 500,
		modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1500px'); },
		title: "ADVERTENCIA",
		buttons: {
			"Aceptar": function () {
				$(this).dialog("close");
			},
		}
	});
}

function validarListaNegraTelefonofijo() {

	var cadena = '';
	var bRetorna = false;
	var telcontacto = $('#txtTelContacto').val();
	var curp = $('#txtCurp').val();
	var sMensajeTelefono2 = "";
	var iIdentificador = 0;

	$.ajax({
		data: {
			opcion: 39,
			telcontacto: telcontacto,
			empleado: iEmpleado,
			curp: curp
		},
		success: function (data) {
			sMensajeTelefono2 = data.mensaje;
			iIdentificador = data.identificador;

			if (iIdentificador == 0 || iIdentificador == 4 || telcontacto == '') {
				bRetorna = true;
			}
			else {
				sTitle = "AVISO";
				sMensaje = (sMensajeTelefono2);
				mensajeAdvertencia(sMensaje, sTitle);
			}

		},
		error: function (a, b, c) {
			sTitle = " ";
			sMensaje = " Ocurrio un error al realizar las validaciones de los telefonos  ";
			mensajeAdvertencia(sMensaje, sTitle);
		}
	});
	return bRetorna;
}

/**
 * CAPOTURAAFILIAICON AGRAGAR ESTADO CIVIL
 */
//Funcion para obtener el catalogo de Esatdo Civil y llenarlo en la pagina como opciones
function cargacatalogoestadocivil() {
	var datos = Array();
	$.ajax(
		{
			data: { opcion: 40 },
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = eval(data);
				//si es caso 1, crea la opcion, caso diferente, es un error y se debe de mostrar el mensaje
				switch (datos.estatus) {
					case 1:
						sHtml = "<option value = -1>SELECCIONE ...</option>";
						for (var i = 0; i < datos.registros.length; i++) {
							//sHtml += ("<option value =" + datos.registros[i].numcomp + ">" + datos.registros[i].desccomp.toUpperCase() +"</option>");
							sHtml += ("<option id='edocivil_" + datos.registros[i].clavec + "' value =" + datos.registros[i].clavec + ">" + datos.registros[i].desccomp.toUpperCase() + "</option>");
						}
						$('#cboEstadoCivil').empty();
						$('#cboEstadoCivil').html(sHtml);
						break;
					default:
						mostrarMensaje("as:" + datos.descripcion, sTitle);
						break;
				}
			}
		});
}

//METODO PARAACTUALIZAR ESTADO CIVIL
function actualizarestadocivil(sCBOEstadoCivil) {
	var cadena = '';
	cadena += "opcion=41&folioactualizaestadocivil=" + folio + "&estadocivil=" + sCBOEstadoCivil;

	$.ajax({
		data: cadena,
		success: function (data) {
			//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
			datos = data;
			$('#cboEstadoCivil').attr('disabled', true);
		}
	});
}

//
function validaTelefonoCelular() {
	var cadena = '';
	var bRetorna = false;
	var sNumeroCelular = $('#txTelCelular').val();
	var iEstatusCelular = 0;
	cadena += "opcion=42&numerocel=" + sNumeroCelular;

	$.ajax(
		{
			data: cadena,
			success: function (data) {
				iEstatusCelular = data.irespuesta;
				if (iEstatusCelular == 0) {
					//se mandan ejecutar los servicios
					sTitle = "AVISO";
					sMensaje = "El número de teléfono capturado pertenece a telefonía fija. Favor de capturar un número de teléfono celular.";
					mensajeAdvertencia(sMensaje, sTitle);
					//se manda llamar el metodo para saber cuando se obtuvo respuesta de los servicios
				}
				else if (iEstatusCelular == 1) {
					bRetorna = true;
				}
			}
		});

	return bRetorna;
}

function validarregistroprevio(curptrab) {
	var arrdatos = Array();
	$.ajax(
		{
			data: { opcion: 43, curp: curptrab },
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				arrdatos = eval(data);
				//si es caso 1, crea la opcion, caso diferente, es un error y se debe de mostrar el mensaje
			}
		});
	llenardatostrabajador(arrdatos);
}

function llenardatostrabajador(datos) {
	/*********************************** */

	var nombre = $('#txtNombre').val();
	var apeido = $('#txtApellidoPaterno').val();
	if (!bBanderaRenapo || (apeido == '' && nombre == '')) {
		if (datos.apellidopaterno != "") {
			$('#txtApellidoPaterno').val(datos.apellidopaterno);
		}

		if (datos.apellidomaterno != "") {
			$('#txtApellidoMatero').val(datos.apellidomaterno);
		}

		if (datos.nombre != "") {
			$('#txtNombre').val(datos.nombre);
		}
	}
	/*********************************** */
	if (datos.nombre != '' && datos.apellidopaterno != '') {
		/*Folio 539 Actualiza el estatus de la bitacora por que hay solicitud rechazada*/
		guardaBitacoraRespuestaBancoppel(2,'');

		$('#txtNss').val(datos.nss);
		$('#txTelCelular').val(datos.celular);
		$('#txtClienteCoppel').val(datos.numclientecop);
		$('#txtTelContacto').val(datos.telofonodecontacto);
		/*539
		$('#AltaUnica').attr('disabled', true);*/
		/*-------------------------------------------------*/
		/*
		$('#txtCalle').val(datos.calle);
		$('#txtNumExt').val(datos.numeroexterior);
		$('#txtNumInt').val(datos.numerointerior);
		$('#txtCodPostal').val(datos.codpostal);
		$('#txtdesColonia').val(datos.colonia);
		$('#txtdesMunicipio').val(datos.municipio);
		$('#txtdesEstado').val(datos.estado);
		$('#txtdesPais').val(datos.pais);

		$('#txtColonia').val(datos.icolonia);
		$('#txtDelMun').val(datos.imunicipio);
		$('#txtEstado').val(datos.iestado);
		$('#txtPais').val(datos.ipais);*/
		/*SELECTS*/
		if (datos.comprobante != 1) {
			$('#dtpFechaComprobante').val(datos.fechacomprobante);
		}

		$('#cboTipoComprobante').val(datos.comprobante);
		$('#cboCompTelef').val(datos.companiacel);
		$('#cboCompTelefCont').val(datos.companiatelefono);
		$('#cboTipoTelefono').val(datos.tipotelefono);
		$('#cboEstadoCivil').val(datos.estadocivil);

	}
}

function validaExcepcion(value) {
	if (value == 0) {
		iExcepcion = value;
	}
	else {
		iExcepcion = value;
	}
}

function grabarVideo() {
	if (banderaContinuarV == 1 && validarRespuestaFolioVideo()) {
		var newHtml = "<div class='alerta'>";
		newHtml += "<p>";
		newHtml += "<label id = 'lvideo'>¿PROMOTOR, EL TRABAJADOR CUENTA CON ALGUNA IMPOSIBILIDAD DEL HABLA PARA REALIZAR LA <br> GRABACIÓN DE VIDEO?</label>" + "<br><br>";
		newHtml += "<td><select id='cboexcepcionesAudio' name='cboexcepcionesAudio' class ='comboBoxVistaExcep' onChange='validaExcepcion(value)'>";
		newHtml += "<option value = '0'>NINGUNA</option>";
		newHtml += "<option value = '1'>NO SABE LEER</option>";
		newHtml += "<option value = '2'>GRIPE</option>";
		newHtml += "<option value = '3'>TOS</option>";
		newHtml += "<option value = '4'>TARTAMUDEZ</option>";
		newHtml += "<option value = '5'>MUDEZ</option>";
		newHtml += "<option value = '6'>AFÓNICO</option>";
		newHtml += "<option value = '7'>OTRA ENFERMEDAD</option>";
		newHtml += "</select></td>";
		newHtml += "</p></div>";

		$('#divMensaje').html(newHtml);
		$("#divMensaje").dialog({
			resizable: false,
			height: 350,
			width: 350,
			modal: true,
			open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '750px'); },
			title: "EXCEPCIONES DE VIDEO",
			buttons: {
				"ACEPTAR": function () {
					$(this).dialog("close");
					grabarExcepcion();
					var sCurp = $('#txtCurp').val();
					verificarEnrolamiento(folio);
					//respExcepcion = 1;
					//nombrevideo = sCurp+folio+claveOPeracion;
					if (respExcepcion == 1) {
						if (iExcepcion == 0) {
							if (iTipoAfiliacion == 26 || iTipoAfiliacion == 33 || iTipoAfiliacion == 35) {
								if (iTipoSolicitante == 2) {
									valordialogo = 3;
								} else if (iTipoSolicitante == 3) {

									valordialogo = 6;
								} else if (iTipoSolicitante == 4) {

									valordialogo = 5;
								} else if (iTipoSolicitante == 5) {

									valordialogo = 6;
								}
								else {
									valordialogo = 2;
								}
							}
							else if (iTipoAfiliacion == 27 || iTipoAfiliacion == 1 || iTipoAfiliacion == 527) {
								valordialogo = 1;
							}
							else {
								sMensaje = "Favor de capturar un tipo de afiliación";
								mostrarMensaje(sMensaje, "");

								return false;
							}


						}
						else {
							if (iTipoSolicitante == 2 || iTipoSolicitante == 3 || iTipoSolicitante == 4)
								valordialogo = 7;
							else
								valordialogo = 4;
						}
						iExcepcion = 0;
						obtenerDialogos(valordialogo);
						nombreVideoPublica = sCurp + folio + claveOPeracion + '.' + cFormato;
						nombrevideo = sCurp + folio + claveOPeracion;
						if (OSName == "Android") {
							var sTextoVideo = buscarTextoVideo(valordialogo);
							setTimeout(function () {
								Android.llamaComponenteVideo(nombreVideoPublica, sTextoVideo, iTiempoMinimo, iTiempoMaximo);
							}, 2000);

						} else {
							subirvolumenaudio();
							var sHtml = '<iframe src=\"' + "indexGrabacionVideo.html?numeroempleado=" + iEmpleado + "&" + "folio=" + folio + "&" + "valordialogo=" + valordialogo + "&" + "nombrevideo=" + nombrevideo + "&tiposolicitante=" + iTipoSolicitante + '\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>';
							$("#capturaVideo").empty();
							$("#capturaVideo").html(sHtml);
							$('#capturaVideo').dialog('open');
						}
					}
					else {
						errorExcepcion();
					}
				},
				"CANCELAR": function () {
					$(this).dialog("close");
				}
			}
		});
	}
}

function errorExcepcion() {

	var sMensaje = "ERROR AL REGISTRAR LA EXCEPCIÓN DEL VIDEO, INTENTE DE NUEVO";
	var newHtml = "<div class='alerta'>";
	newHtml += "<p>";
	newHtml += sMensaje;
	newHtml += "</p></div>";

	$('#divMensaje').html(newHtml);
	$("#divMensaje").dialog({
		resizable: false,
		height: 230,
		width: 320,
		modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1200px'); },
		title: "AVISO",
		buttons: {
			"ACEPTAR": function () {
				$(this).dialog("close");
			}
		}
	});
}

function cancelar() {
	var sMensaje = "SE CANCELA LA CAPTURA DE DATOS PARA AFILIACIÓN, ¿DESEA CONTINUAR?";
	var newHtml = "<div class='alerta'>";
	newHtml += "<p>";
	newHtml += sMensaje;
	newHtml += "</p></div>";

	$('#divMensaje').html(newHtml);
	$("#divMensaje").dialog({
		resizable: false,
		height: 230,
		width: 320,
		modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1200px'); },
		title: "AVISO",
		buttons: {
			"ACEPTAR": function () {
				$(this).dialog("close");
				if (OSName == "Android") {
					cerrarNavegador();
				} else {
					window.close();
				}
			},
			"CANCELAR": function () {
				$(this).dialog("close");
			}
		}
	});
}

function validarExpediente(curp, nss) {
	var bResp = false;
	var arrExpediente = new Array();
	$.ajax
		({
			data: { opcion: 44, curp: curp },
			success: function (data) {
				arrExpediente = eval(data);
				var iErrorExp = arrExpediente.error;

				if (arrExpediente.mensaje != nss && arrExpediente.error == 0) {
					iErrorExp = -1;
				}
				validarErrorCaseSolExp(iErrorExp, arrExpediente.mensaje, arrExpediente.redireccionar);
			}
		});
}

function validarErrorCaseSolExp(error, mensaje, redireccionar) {

	//mostrarMensaje(error,"");
	var bResp = false;
	switch (parseInt(error)) {
		case -2: //Mostrar Mensaje de Error, Si ya Esta Afiliado con nosotros o se encuentra en otro porceso
			sMensaje = mensaje;
			mostrarMensaje(sMensaje, "");
			break;
		case -1:
			//no dejamos avanzar el flujo
			//MuestraDialogoMensajeURL("El NSS capturado para esa CURP, es distinto al que capturaste en la Solicitud de la Constancia para Registro o Traspaso.",redireccionar);
			break;
		case 0: //Continuar Flujo Normal
			//bResp = true;
			var mensaje = 'Promotor el trabajador ya cuenta con expediente de identificación, favor de continuar con la Solicitud de Afiliación';
			MensajeExpediente(mensaje);
			break;
		case 5://No cuenta con Expediente Independiente valido

			sTipoSol = $('#cboTipoConstancia').val();
			if (sTipoSol == 1 || sTipoSol == 527) {
				sTipoSol = 27;
			}
			redireccionar = redireccionar.replace("{0}", iEmpleado); //noPromotor
			redireccionar = redireccionar.replace("{1}", folio); //SolicitudFolio
			redireccionar = redireccionar.replace("{2}", sTipoSol);//Tipo Servicio
			redireccionar = redireccionar + "&renapo=" + iRenapo;
			if (OSName == "Android") {
				Android.llamaComponenteEnlace(redireccionar);
				setTimeout(function () {
					Android.terminarSolConst("1");
				}, 4000);
			} else {
				location.href = redireccionar;
			}
			break;

		default:
			MensajeExpediente("Ocurrio una excepción al validar los prerequisitos, no se puede continuar con el proceso de afiliación.");
			break;
	}
}

function capturarCodigoAutenticacion() {
	var newHtml = "<div class='alerta'>";
	//newHtml += "<p>";
	newHtml += "<table>";
	newHtml += "<tr>";

	if (!bTerceraFigura) {
		newHtml += "<label class= 'labelCodigo'>Promotor, favor de solicitar el Código de Autenticación proporcionado al Trabajador para su captura.</label>";
		newHtml += "<label class= 'labelCodigo'>Si el Trabajador aún no recibe el Código de Autenticación, favor de continuar con la afiliación.<br><br><br></label>";
	}
	else {
		newHtml += "<label class= 'labelCodigo'>Promotor, favor de solicitar el Código de Autenticación proporcionado al Trabajador para su captura.</label>";
		newHtml += "<label class= 'labelCodigo'>Si el Trabajador aún no recibe el Código de Autenticación, favor de continuar con la afiliación.<br><br><br></label>";
	}

	newHtml += "<td><label>Código de Autenticación:</label></td>";
	newHtml += "<td><input type = text id = 'codAut' maxlength = '7' class = 'entradaTextoCodAut text-uppercase' ></input></td>";
	newHtml += "</tr>";
	newHtml += "</table>";
	newHtml += "</div>";

	$('#codigoAut').html(newHtml);
	$("#codigoAut").dialog({
		resizable: false,
		closeOnEscape: false,
		//height:350,
		width: 415,
		modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1000px'); },
		title: "CAPTURA DE CÓDIGO DE AUTENTICACIÓN",
		buttons: {
			"CONTINUAR": function () {
				var codAu = $('#codAut').val().toUpperCase();
				if (codAu == '') {
					$(this).dialog("close");
					var curp = $('#txtCurp').val();
					var nss = $('#txtNss').val();
					validarExpediente(curp, nss);
				}
				else {
					var longCodigo = codAu.length;
					if (longCodigo < 7) {
						mensaje = 'El Código de Autenticación no cumple con la longitud requerida';
						mostrarMensajeIneVal(mensaje);
					}
					else {
						validarcodigoautenticacion(codAu);
					}
				}
			}
		}
	});
}

function validarcodigoautenticacion(codAu) {
	var curpTitular = $("#txtCurp").val();
	iFolioSol = obtenerRecuperacionFolioConstancia(curpTitular);
	datos =
	{
		opcion: 77,
		folioConst: iFolioSol,
		codigoine: codAu
	};

	$.ajax(
		{
			data: datos,
			success: function (data) {
				if (data.validacion == 1) {
					$('#codigoAut').dialog("close");

					if ((iTipoAfiliacion == 26 || iTipoAfiliacion == 33) && bTerceraFigura) {
						var curp = $('#txtCurp2').val();
					}
					else {
						var curp = $('#txtCurp').val();
					}
					var nss = $('#txtNss').val();
					validarExpediente(curp, nss);
				}
				else if (data.validacion == -5) {
					mensaje = 'El Código de Autenticación no corresponde a este trámite';
					mostrarMensajeIneVal(mensaje);
					$('#codAut').val('');
					$('#codAut').focus();
				}
				else if (data.validacion == -3) {
					mensaje = 'El Código de Autenticación capturado no se encuentra vigente';
					mostrarMensajeIneVal(mensaje);
					$('#codAut').val('');
					$('#codAut').focus();
				}
			}
		});
}

function actualizarSolEstatusExpiden(iFolioSol, curpTitular) {

	datos = { opcion: 87, folioConst: iFolioSol, curp: curpTitular };

	$.ajax({
		data: datos,
		success: function (data) {
		}
	});
}

function obtenerRecuperacionFolioConstancia(curp) {
	datos =
	{
		opcion: 78,
		curp: curp
	};

	var iFolioR = 0

	$.ajax(
		{
			data: datos,
			success: function (data) {
				if (data.estatus == 1) {
					iFolioR = data.folio;
				}
			}
		});
	return iFolioR;
}

function ActualizarExpedientePermante(iFolioSol, curpTitular) {
	datos =
		{
			opcion: 88,
			folioConst: iFolioSol, 
			curp: curpTitular
		};
		
	var iRes = 0

	$.ajax(
		{
			data: datos,
			success: function (data) {
				if (data.estatus == 1) {
					iRes = data.actualizaestatus;
				}
			}
		});
	return iRes;
}

function mostrarMensajeIneVal(mensaje) {
	$("#mensCodigoAut").dialog
		({
			title: 'Captura de Datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"ACEPTAR": function () {
					$(this).dialog("close");
				}
			}

		});
}

function bloquearModal() {
	$('.content-loader').show();
}

function desbloquearModal() {
	$('.content-loader').hide();
}

//funcion para desplegar un mensaje
function abrirExpediente(mensaje, redireccionar) {

	var sMensaje = mensaje;
	var newHtml = "<div class='alerta'>";
	newHtml += "<p>";
	newHtml += sMensaje;
	newHtml += "</p></div>";

	$('#divMensaje').html(newHtml);
	$("#divMensaje").dialog({
		resizable: false,
		height: 220,
		width: 350,
		modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '750px'); },
		title: "EXPEDIENTE DE IDENTIFICACIÓN",
		buttons: {
			"ACEPTAR": function () {
				$(this).dialog("close");
				if (OSName == "Android") {
					Android.llamaComponenteEnlace(redireccionar);
					setTimeout(function () {
						Android.terminarSolConst("1");
					}, 4000);
				} else {
					location.href = redireccionar;
				}
			},
			"CANCELAR": function () {
				$(this).dialog("close");
			}
		}
	});
}

function grabarExcepcion() {
	$.ajax
		({
			data: { opcion: 45, excepcion: iExcepcion, folioConst: folio, empleado: iEmpleado },
			success: function (data) {
				datos = data;
				if (datos.respuesta == 1) {
					respExcepcion = 1;
				}
				else {
					respExcepcion = 0;
				}
			}
		});
}

function obtenerestatusVideo() {
	$.ajax
		({
			data: { opcion: 50, folioConst: folio },
			success: function (data) {
				datos = data;
				if (datos.estatus == 1) {
					$('#bGrabacion').attr('disabled', true);
					$('#bDigitalizar').attr('disabled', false);
					var mensajeVideo = 'Video Guardado con Éxito';
					MensajeGrabacion(mensajeVideo);
				}
				else {
					var mensajeVideo = 'No se completo la Grabación del Video, intente nuevamente';
					MensajeGrabacion(mensajeVideo);
				}
			}
		});
}

function MuestraDialogoGrabacion(mensaje) {
//mostrarCombos(false);
$("#divMensaje").dialog
	({
		title: 'Grabación de Video',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position: { my: "center", at: "center" },//Posicionar el dialog en el centro
		open: function (event, ui) {
		$(".ui-dialog-titlebar-close", ui.dialog).hide();
		$(this).html("<p>" + mensaje + "</p>");
		},
		buttons:
		{

		"SI": function () {
		$(this).dialog("close");
		$('#capturaVideo').dialog("close");
		var sHtml = '<iframe src=\" "></iframe>';
		$("#capturaVideo").empty();
		$("#capturaVideo").html(sHtml);
		$("#capturaVideo").hide();
		//obtenerestatusVideo();
		iEstatusEnrolVideo = 99;
		//LLAMAR FUNCION QUE ACTUALIZA EL ESTATUS DE LA TABLA ENROLTRABAJADORES EN 99
		actualizarEstatusEnrolamiento(iFolioEnrolamiento, iEstatusEnrolVideo);
		opcionVideoP = 1;
		opcionejecutaPublicaVideo();
		},

		"NO": function () {
		$(this).dialog("close");
		$('#capturaVideo').dialog( "open" ); //790  
		$("#capturaVideo").show();          //790
		}
		}
	});
}

function MensajeGrabacion(mensaje) {
	$("#divMensaje").dialog
		({
			title: 'Grabación de Video',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"ACEPTAR": function () {
					$(this).dialog("close");
				}
			}
		});
}

function MensajeExpediente(mensaje) {
	$("#divMensaje").dialog
		({
			title: 'Expediente de Identificación',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"ACEPTAR": function () {


					$(this).dialog("close");
					if (OSName == "Android") {
						cerrarNavegador();
					} else {
						window.close();
					}

				}
			}
		});
}

function validarHuella() {
	//SI SE TRATA DE UN INDEPENDIENTE NO SE CAPTURA NSS POR LO CUAL SE MANDA UN VALOR DEFAULT 00000000000
	let curpValidaH = $('#txtCurp').val();
	let nssValidaH = $('#txtNss').val();
	if (sTipoSolicitud == 33 || traspasoIssste || traspasoNoAfiliado) {
		sParametros = curpValidaH + " " + RUTA_LOCAL_HUELLAS + " " + iEmpleado + " " + '00000000000';
	} else {
		sParametros = curpValidaH + " " + RUTA_LOCAL_HUELLAS + " " + iEmpleado + " " + nssValidaH;
	}

	sRuta = "C:\\SYS\\PROGS\\SOLCONSTANCIAINDICES.EXE";

	bloquearUIHuella();
	console.log("sRuta " + sRuta);
	console.log("sParametros " + sParametros);
	ejecutaWebServiceHuella(sRuta, sParametros);
}

//function bloquearUI
function bloquearUIHuella() {

	$('.content-loader').show();

}

//desbloqueamos la pantalla
function desbloquearUIHuella() {

	$('.content-loader').hide();

}

function ejecutaWebServiceHuella(sRuta, sParametros) {
	console.log("ejecutaWebServiceHuella ");

	if (OSName == "Android") {

		Android.llamaComponenteHuellas(8, iEmpleado, 1, 8, iEmpleado, sTipoSolicitud, sCurpTrabajador);

	} else {

		soapData = "",
			httpObject = null,
			docXml = null,
			iEstado = 0,
			sMensaje = "";
		sUrlSoap = "http://127.0.0.1:20044/";

		soapData =
			'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
			'<SOAP-ENV:Envelope' +
			' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"' +
			' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"' +
			' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"' +
			' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"' +
			' xmlns:ns2=\"urn:ServiciosWebx\">' +
			'<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">' +
			'<ns2:ejecutarAplicacion>' +
			'<inParam>' +
			'<Esperar>1</Esperar>' +
			'<RutaAplicacion>' + sRuta + '</RutaAplicacion>' +
			'<parametros>' + sParametros + '</parametros>' +
			'</inParam>' +
			'</ns2:ejecutarAplicacion>' +
			'</SOAP-ENV:Body>' +
			'</SOAP-ENV:Envelope>';

		httpObject = getHTTPObjectH();

		if (httpObject) {
			if (httpObject.overrideMimeType) {
				httpObject.overrideMimeType("false");
			}

			httpObject.open('POST', sUrlSoap, false); //-- no asincrono
			httpObject.setRequestHeader("Accept-Language", null);
			httpObject.onreadystatechange = function () {
				if (httpObject.readyState == 4 && httpObject.status == 200) {
					parser = new DOMParser();
					docXml = parser.parseFromString(httpObject.responseText, "text/xml");
					iEstado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;
					//Llamada a la función que recibe la respuesta del WebService
					respuestaWebServiceHuella(iEstado);
				}
				else {
					console.log(httpObject.status);
				}
			};
			httpObject.send(soapData);
		}
	}
}

function getHTTPObjectH() {
	var xhr = false;
	if (window.ActiveXObject) {
		try {
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				xhr = false;
			}
		}
	} else if (window.XMLHttpRequest) {
		try {
			xhr = new XMLHttpRequest();
		} catch (e) {
			xhr = false;
		}
	}
	return xhr;
}

/**
 * RESPUESTA WEBSERVICE
*/
function respuestaWebServiceHuella(iRespuesta) {

	desbloquearUIHuella();
	console.log("RESPUESTA APPLET: " + iRespuesta);
	var mensajeH = '';
	switch (iRespuesta.toString()) {
		case '1':
			if (iTipoAfiliacion != 26 && iTipoAfiliacion != 33) {
				validarRespuestaFolio();
			}
			$('#bGrabacion').attr('disabled', false);
			document.getElementById("bGrabacion").classList.add("habilitarImg");
			$('#bAceptar').attr('disabled', true);
			document.getElementById("bAceptar").classList.add("deshabilitarImg");
			banderaContinuarV = 1;
			loading(false);
			break;
		case '0':
		case '-3':
			mensajeH = 'Captura Cancelada, Intente Nuevamente';
			break;
		case '-1':
			mensajeH = 'Promotor tus Huellas no deben coincidir con el Trabajador';
			break;
		case '-4':
			$('#bGrabacion').attr('disabled', true);
			document.getElementById("bGrabacion").classList.add("deshabilitarImg");
			mensajeH = 'No se Encontró Dispositivo Conectado, favor de contactar a mesa de ayuda';
			break;
		case '-2':
		case '-5':
		case '-6':
		case '-7':
		case '-8':
		case '-9':
		case '-10':
		case '-11':
		case '-12':
			mensajeH = 'Error en la Captura de Huellas';
			break;
		case '-98':
			mensajeH = "Favor verificar, no se encontro el componente: " + sRuta + ".";
			console.log("RESPUESTA APPLET: " + iRespuesta + ". INSTALAR COMPONENTE: " + sRuta);
			break;
	}

	if (iRespuesta != 1) //si es exito, se desabilita el boton
	{
		MensajeValidacionHuella(mensajeH);
		$('#bGrabacion').attr('disabled', true);
		$('#bAceptar').attr('disabled', false);
	}

}

function MensajeValidacionHuella(mensaje) {
	//mostrarCombos(false);
	$("#divMensaje").dialog
		({
			title: 'Validacion Huella',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{

				"ACEPTAR": function () {
					$(this).dialog("close");
					validarHuella();

				}
			}
		});
}

function validarenrolamientoConstancia(curp) {
	var estado;
	$.ajax
		({
			data: { opcion: 51, curp: curp },
			success: function (data) {
				datos = data;
				if (datos.estado == 1) {
					estado = 1;

				}
				else {
					estado = 0;
				}
			}
		});

	return estado;
}

function obtenerClaveOperacion() {
	$.ajax
		({
			data: { opcion: 53 },
			success: function (data) {
				datos = data;
				claveOPeracion = datos.ClaveOP.trim();
			}
		});
}

function opcionejecutaPublicaVideo() {
	if (OSName == 'Android') {

		intentoPublicaVideo++;
		setTimeout(function () {
			Android.llamaComponenteVideo(nombreVideoPublica, sTextoVideo);
		}, 2000);

	} else {
		intentoPublicaVideo++;
		sParametrosPublica = opcionVideoP + ' ' + folio + ' ' + nombreVideoPublica + ' ' + ipCluster;
		var sRuta = 'C:\\sys\\progs\\PUBLICAVIDEO.EXE';

		ejecutaWebServicePublicaVideo(sRuta, sParametrosPublica);
		if (opcionVideoP == 2) {
			bloquearModal();
		}
	}
}

//EJECUTA WEBSERVICE
function ejecutaWebServicePublicaVideo(sRuta, sParametrosPublica) {
	console.log("Parametros EjecutaWebService son --> " + sParametrosPublica);

	soapData = "",
		httpObject = null,
		docXml = null,
		iestado = 0,
		sMensaje = "";
	sUrlSoap = "http://127.0.0.1:20044/";

	soapData =
		'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
		'<SOAP-ENV:Envelope' +
		' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"' +
		' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"' +
		' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"' +
		' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"' +
		' xmlns:ns2=\"urn:ServiciosWebx\">' +
		'<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">' +
		'<ns2:ejecutarAplicacion>' +
		'<inParam>' +
		'<Esperar>1</Esperar>' +
		'<RutaAplicacion>' + sRuta + '</RutaAplicacion>' +
		'<parametros><![CDATA[' + sParametrosPublica + ']]></parametros>' +
		'</inParam>' +
		'</ns2:ejecutarAplicacion>' +
		'</SOAP-ENV:Body>' +
		'</SOAP-ENV:Envelope>';

	httpObject = getHTTPObjectPublicaVideo();

	if (httpObject) {
		if (httpObject.overrideMimeType) {
			httpObject.overrideMimeType("false");
		}

		httpObject.open('POST', sUrlSoap, false); //-- NO ASINCRONO
		httpObject.setRequestHeader("Accept-Language", null);
		httpObject.onreadystatechange = function () {
			if (httpObject.readyState == 4 && httpObject.status == 200) {
				parser = new DOMParser();
				docXml = parser.parseFromString(httpObject.responseText, "text/xml");
				iestado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;

				if (iestado == '1') {
					respuestaWebServicePublicaVideo(iestado);
					if(!bResubirVideo)
					{
						$('#bDigitalizar').attr('disabled', false);
						document.getElementById("bDigitalizar").classList.add("habilitarImg");
						$('#bGrabacion').attr('disabled', true);
						document.getElementById("bGrabacion").classList.add("deshabilitarImg");
					}

				}
				else if ((iestado == '-1' || iestado == '-2' || iestado == '-4' || iestado == '-5' || iestado == '-6' || iestado == '-7' || iestado == '-9') && intentoPublicaVideo == 3) {
					iFlagNoEcontro = true;
					//desbloquearModal();
				}
				else if (iestado == '-3' || iestado == '-8' || iestado == '1') {
					iFlagNoEcontro = true;
					//desbloquearModal();
				} else if (iestado == '-1' || iestado == '-3' || iestado == '-10') {
					//desbloquearModal();
					iFlagNoEcontro = true;
				} else if (iestado == 6) {
					iFlagVideo = true;
				}
				console.log('EjecutaRespuestaWebService: La Respuesta es: ' + iestado);


				//LLAMADA A LA FUNCIÓN QUE RECIBE LA RESPUESTA DEL WEBSERVICE
				setTimeout(function () {
					respuestaWebServicePublicaVideo(iestado);
				}, 4000);

				//respuestaWebService(iestado, iOpcion);
			}
			else {
				console.log(httpObject.status);
			}
		};
		httpObject.send(soapData);
	}
	
	if(bResubirVideo)
		continuar();
}

function getHTTPObjectPublicaVideo() {
	var xhr = false;
	if (window.ActiveXObject) {
		try {
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				xhr = false;
			}
		}
	} else if (window.XMLHttpRequest) {
		try {
			xhr = new XMLHttpRequest();
		} catch (e) {
			xhr = false;
		}
	}
	return xhr;
}

//funcion que permite obtener la respuesta que arrojo el applet
//function respuestaApplet(iRespuesta)
function respuestaWebServicePublicaVideo(iRespuesta) {
	/*Respuestas del exe que publica el video
		EXITO = 1,
        NO_SE_ENCONTRO_EL_ARCHIVO       = -1, recuperable
        NO_CONECTO_BASE_DE_DATOS        = -2, recuperable
        NO_SE_ENCONTRO_DIRECTORIO       = -3, no recuperable
        PARAMETROS_INCORRECTOS          = -4, recuperable
        NO_ESTABLECIO_CONEXION_SERVIDOR = -5, recuperable
        EER_ENVIAR_ARCHIVO_FTP          = -6, recuperable
        ERR_NO_CONECTA_WEBSERVICE       = -7, recuperable
        NO_EXISTE_ARCHIVO_SERVIDOR      = -8, no recuperable
        ERR_MD5_NO_COINCIDE             = -9  recuperable
	*/
	if ((iRespuesta == '-1' || iRespuesta == '-2' || iRespuesta == '-4' || iRespuesta == '-5' || iRespuesta == '-6' || iRespuesta == '-7' || iRespuesta == '-9') && intentoPublicaVideo == 3) {
		iFlagNoEcontro = true;
		desbloquearModal();
	}
	else if (iRespuesta == '-3' || iRespuesta == '-8' || iRespuesta == '1') {
		iFlagNoEcontro = true;
		desbloquearModal();
	} else if (iRespuesta == '-10')			//790
	{
		desbloquearModal();
		iFlagNoEcontro = true;
	}
	console.log('EjecutaRespuestaWebService: La Respuesta es: ' + iRespuesta);
	switch (iRespuesta) {
		case '1'://guardo el video exitosamete en servidor
			actualizarEstatusVideo(iRespuesta);
			if (iEstatusEnrolVideo == -1) {
				iEstatusEnrolVideo = 0;
				actualizarEstatusEnrolamiento(iFolioEnrolamiento, iEstatusEnrolVideo);
				iEstatusEnrolVideo = -1;
			}
			else {
				iEstatusEnrolVideo = -1;
			}
			break;
		case '6':					//790
			iFlagVideo = true;
			break;
		default:

			break;

	}

	if (opcionVideoP == 2) {
		//valida si es un video recuperable y los intestos de publicacion
		if ((iRespuesta == '-1' || iRespuesta == '-2' || iRespuesta == '-4' || iRespuesta == '-5' || iRespuesta == '-6' || iRespuesta == '-7' || iRespuesta == '-9') && intentoPublicaVideo <= 3) {	//si los intentos de publicar son menor a 3 lo vuelve a intentar publicar
			if (intentoPublicaVideo < 3) {
				//MuestraDialogoMensajePublica('Promotor, se detectó un error, intenta de nuevo.');
				opcionejecutaPublicaVideo();
			}
			else {//si ya llego a los 3 intentos de publicar sin exito se convierte en archivo no recuperable y borra el archivo de la pc
				//actualiza el estatus de enrolamiento para validar que no tiene video grabado
				iEstatusEnrolVideo = 99;
				actualizarEstatusEnrolamiento(iFolioEnrolamiento, iEstatusEnrolVideo);
				//actualiza el estatus del video para verificar su flujo y estado del mismo
				actualizarEstatusVideo(iRespuesta);
				opcionVideoP = 1;
				opcionejecutaPublicaVideo();
				if (intentosGrabado < 3) {
					MensajeGrabacion('Promotor, se detectó un error, favor de realizar nuevamente la grabación de video.');
				}
				else {
					MensajeGrabacion('Promotor, se detectó un error, favor de contactar a Mesa de Ayuda.');
					intentosGrabado = 0;
				}

				intentoPublicaVideo = 0;

			}
		}//si es un video no recuperable actualiza los estatus de enrolamiemto y el estatus del video
		else if (iRespuesta == '-3' || iRespuesta == '-8') {
			iEstatusEnrolVideo = 99;
			actualizarEstatusEnrolamiento(iFolioEnrolamiento, iEstatusEnrolVideo);
			actualizarEstatusVideo(iRespuesta);
			opcionVideoP = 1;
			opcionejecutaPublicaVideo();

			if (intentosGrabado < 3) {
				MensajeGrabacion('Promotor, se detectó un error, favor de realizar nuevamente la grabación de video.');
			}
			else {
				MensajeGrabacion('Promotor, se detectó un error, favor de contactar a Mesa de Ayuda.');
				intentosGrabado = 0;
			}

		}

	}
	else {
		iEstatusEnrolVideo = -1;
	}
}

//obtenemos el formato del video para agregarlo al nombre del video y sea usado de parametro en el publicavideo.exe
function obtenerDialogos(valordialogo) {
	$.ajax
		({
			data: { opcion: 47, tipodialogo: valordialogo },
			success: function (data) {
				datos = data;
				if (datos.respuesta == 1) {
					cFormato = datos.formato;
				}
				else {
					sMensaje = "NO SE EJECUTO CORRECTAMENTE LA APLICACIÓN, INTENTE DE NUEVO";
					mostrarMensaje(sMensaje, "");
				}
			}
		});

}

//actualizar el estatus del video en base de datos
function actualizarEstatusVideo(iRespuesta) {
	$.ajax
		({
			data: { opcion: 55, folioConst: folio, estatusvideo: iRespuesta },
			success: function (data) {
				datos = data;
				if (datos.respuesta == 1) {
					banderaVideo = 1;
				}
				else {
					banderaVideo = 0;
				}
			}
		});
}

//funcion que verifica si el Trabajador cuenta con un Enrolamiento
function verificarEnrolamiento(folio) {
	var sCadena = '';
	//Arma la cadena que recibe el php
	sCadena = "opcion=57&folioConst=" + folio;

	$.ajax(
		{
			data: sCadena,
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso
				datos = data;

				if (datos.respuesta != '0') //El Trabajador cuenta con un enrolamiento activo
				{
					/*
						1.-Actualiza en el solconstancia el folioenrolamiento siempre y cuando no cuente con un enrolamiento permanente.
						2.-Si cuenta con enrolamiento permanente no actualiza el campo folioenrlamiento, se guardan los dedos indices en afotemplatecliente y si existe ahi retorna el 1.
						*Nota: Caso dos unicamente para TRASPASOS.
					*/
					iFolioEnrolamiento = datos.respuesta;
				}
			}
		}
	);
}

//funcion que actualiza el estatus del Enrolamiento del Trabajador
function actualizarEstatusEnrolamiento(iFolioEnrolamiento, iEstatusEnrolVideo) {
	var iEstatusVideo = 1;
	var sCadena = '';
	iFolioEnrolamiento = iFolioEnrolamiento != "" ? iFolioEnrolamiento : 0;
	//Arma la cadena que recibe el php
	sCadena = "opcion=58&folioEnrolamiento=" + iFolioEnrolamiento + "&estatus=" + iEstatusEnrolVideo + "&curp=" + $("#txtCurp").val();

	$.ajax
		({
			data: sCadena,
			success: function (data) {
				//pasa el arreglo data a datos, para su posterior uso
				datos = data;
				if (datos.respuesta == '1') //Respuesta que indica que se actualizo correctamente el estatus
				{
					if (iEstatusEnrolVideo == 0) {
						if (OSName == "Android") {
							iEstatusVideo = 2;
						}
						actualizarEstatusVideo(iEstatusVideo);
						if (banderaVideo == 1 || iflagvideo) {
							$('#bDigitalizar').attr('disabled', false);
							document.getElementById("bDigitalizar").classList.add("habilitarImg");
							$('#bGrabacion').attr('disabled', true);
							document.getElementById("bGrabacion").classList.add("deshabilitarImg");
						}
					}
					else {
						$('#bDigitalizar').attr('disabled', true);
						document.getElementById("bDigitalizar").classList.add("deshabilitarImg");
						$('#bGrabacion').attr('disabled', false);
						document.getElementById("bGrabacion").classList.add("habilitarImg");
					}
				}
			}
		});
}

//funcion para obtener la ip del servidor offline para usarse de parametro con el publicavideo.exe
function obtenerIpCluster() {
	$.ajax
		({
			data: { opcion: 56 },
			success: function (data) {
				datos = data;
				ipCluster = datos.ipcluster.trim();
			}
		});
}

function loading(bMostrar, sMensaje) // muestra el loading de procesando...
{
	//inicializamos el bloqueo de pantalla con sus respectivas opciones.
	if (bMostrar == true) {
		bloquearUIHuella();
	}
	else {
		desbloquearUIHuella();
	}
}

//funcion para obtener la ip del servidor offline para usarse de parametro con el publicavideo.exe
function guardarControlEnvio() {
	$.ajax
		({
			data: { opcion: 59, folioConst: folio, recepcionfolio: iRecepcion },
			success: function (data) {
				//datos = data;
				//ipCluster = datos.ipcluster.trim();
			}
		});
}

function actualizadatossolicitanterenapo() {
	$.ajax
		({
			data: { opcion: 60, folioConst: folio, nombreSolicitante: sNombreBeneficiario.toUpperCase(), apellidoPatSolicitante: sApellidoPaternoBenef.toUpperCase(), apellidoMatSolicitante: sApellidoMaternoBenef.toUpperCase() },
			success: function (data) {
				datos = data;
				if (datos.respuesta == 1) {
					marcarLog('SE GUARDO CORRECTAMENTE LOS DATOS DEL SOLICITANTE');
					sApellidoPaternoBenef = '';
					sApellidoMaternoBenef = '';
					sNombreBeneficiario = '';
				}
				else {
					marcarLog('ERROR AL GUARDAR LOS DATOS DEL SOLCITANTE');
				}
			}
		});
}

function ejecutaServicioFolio() {
	var servicioFolio = 9939;
	marcarLog('ejecucion de servicio 9939');
	$.ajax
		({
			data: { opcion: 61, serviciofolio: servicioFolio, folioConst: folio, empleado: iEmpleado },
			success: function (data) {
				/*datos = data;
				if (datos.respuesta == 1)
				{
					marcarLog('SE GUARDO CORRECTAMENTE LOS DATOS DEL SOLICITANTE');
				}
				else
				{
					marcarLog('ERROR AL GUARDAR LOS DATOS DEL SOLCITANTE');
				}
				*/
			}
		});
}

function capturarDatosSolicitante() {
	var newHtml = "<div class='alerta'>";
	//newHtml += "<p>";
	newHtml += "<table>";
	newHtml += "<tr>";
	newHtml += "<td><label>Apellido Paterno (*):&nbsp;</label></td>";
	newHtml += "<td><input type = text id = 'apPatBenef' class='entradaTextoBenef' required onkeypress ='validarKeypressApellidos(event)'></input></td>";
	newHtml += "</tr>";
	newHtml += "<tr>";
	newHtml += "<td><label>Apellido Materno:&nbsp;</label></td>";
	newHtml += "<td><input type = text id = 'apMatBenef' class='entradaTextoBenef' onkeypress ='validarKeypressApellidos(event)'></input></td>";
	newHtml += "</tr>";
	newHtml += "<tr>";
	newHtml += "<td><label>Nombres (*):&nbsp;</label>";
	newHtml += "<td><input type = text id = 'NomBenef' class='entradaTextoBenef' required onkeypress = 'validarKeypressNombre(event)'></input></td>";
	newHtml += "</tr>";
	newHtml += "</table>";
	newHtml += "<br>" + "<br>" + "<br>";
	newHtml += "<label style='marginBottom:-5%'>Datos Obligatorios(*)</label>";
	newHtml += "</div>";

	$('#divDatosBenef').html(newHtml);
	$('#apPatBenef,#apMatBenef,#NomBenef').bind('change', aMayusculas);
	$("#divDatosBenef").dialog({
		resizable: false,
		closeOnEscape: false,
		//height:350,
		width: 430,
		modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1000px'); },
		title: "Datos del Solicitante",
		buttons: {
			"ACEPTAR": function () {
				if ($('#apPatBenef').val() != '' && $('#NomBenef').val() != '') {
					sApellidoPaternoBenef = $('#apPatBenef').val();
					sApellidoMaternoBenef = $('#apMatBenef').val();
					sNombreBeneficiario = $('#NomBenef').val();
					$(this).dialog("close");
				}
			}
		}
	});
}

//function que manda ser llamada desde el boton cancelar
function MuestraDialogoSolicitante(mensaje) {

	$("#divMensaje").dialog
		({
			title: 'Solicitud de constancia',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"ACEPTAR": function () {
					$(this).dialog("close");
				}
			}
		});
}
//function que manda ser llamada desde el boton cancelar
function MuestraDialogoRespuestaFolio(mensaje) {

	$("#divMensaje").dialog
		({
			title: 'Captura de Datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"ACEPTAR": function () {
					$(this).dialog("close");
					if (respuestaProcesar == -2 || respuestaProcesar == -3) {
						reenvioservicio();
						loading(true);
					}
				}
			}
		});
}

function sistemaOperativo() {
	// Instruccion que permite conocer el sistema operativo de donde ingresa el usuario
	if (window.navigator.userAgent.indexOf("Windows NT 10.0") != -1) OSName = "Windows 10";
	if (window.navigator.userAgent.indexOf("Windows NT 6.2") != -1) OSName = "Windows 8";
	if (window.navigator.userAgent.indexOf("Windows NT 6.1") != -1) OSName = "Windows 7";
	if (window.navigator.userAgent.indexOf("Windows NT 6.0") != -1) OSName = "Windows Vista";
	if (window.navigator.userAgent.indexOf("Windows NT 5.1") != -1) OSName = "Windows XP";
	if (window.navigator.userAgent.indexOf("Windows NT 5.0") != -1) OSName = "Windows 2000";
	if (window.navigator.userAgent.indexOf("Mac") != -1) OSName = "Mac/iOS";
	if (window.navigator.userAgent.indexOf("X11") != -1) OSName = "UNIX";
	if (window.navigator.userAgent.indexOf("Linux") != -1) OSName = "Linux";
	if (window.navigator.appVersion.indexOf("Android") != -1) OSName = "Android";
}

function validarRespuestaFolio() {
	//PARA PRUEBAS CON TESTER PARA EMULACION
	//alert("validarRespuestaFolio: "+folio);
	$.ajax
		({
			data: { opcion: 62, folioConst: folio },
			success: function (data) {
				datos = data;

				marcarLog('validarRespuestaFolio  RESPUESTAPROCESAR--> ' + datos.respuesta + ' MENSAJE --> ' + datos.mensaje);  //logs

				if (datos.respuesta == 1) {
					$('#bGrabacion').attr('disabled', false);
					document.getElementById("bGrabacion").classList.add("habilitarImg");
					$('#bAceptar').attr('disabled', true);
					document.getElementById("bAceptar").classList.add("deshabilitarImg");
					banderaContinuarV = 1;
					loading(false);

				}
				else if (datos.respuesta == -2 || datos.respuesta == -3) {
					if (reenviofolio == false) {
						mensaje = 'No se ha obtenido respuesta del servicio del folio, Favor de esperar';
						MuestraDialogoRespuestaFolio(mensaje);
					}
				}
				else if (datos.respuesta == -1) {
					loading(false);
					mensaje = 'La solicitud ha sido rechazada por el<br>' + datos.mensaje;
					MuestraDialogoRespuestaFolio(mensaje);
					$('#bAceptar').attr('disabled', true);
				}
				respuestaProcesar = datos.respuesta;
			}
		});
}

function validarRespuestaFolioVideo() {
	var bRespuesta = false;
	$.ajax
		({
			data: { opcion: 62, folioConst: folio },
			success: function (data) {
				datos = data;

				marcarLog('validarRespuestaFolioVideo  RESPUESTAPROCESAR--> ' + datos.respuesta + ' MENSAJE --> ' + datos.mensaje);  //logs

				if (datos.respuesta == 1) {
					bRespuesta = true;
					loading(false);

				}
				else if (datos.respuesta == -2 || datos.respuesta == -3) {
					if (reenviofolio == false) {
						mensaje = 'No se ha obtenido respuesta del servicio del folio, Favor de esperar';
						MuestraDialogoRespuestaFolio(mensaje);
					}
				}
				else if (datos.respuesta == -1) {
					loading(false);
					mensaje = 'La solicitud ha sido rechazada por el<br>' + datos.mensaje;
					MuestraDialogoRespuestaFolio(mensaje);
				}
				respuestaProcesar = datos.respuesta;
			}
		});
	return bRespuesta;
}


function reenvioservicio() {
	reenviofolio = true;
	ejecutaServicioFolio();//invoco de nuevo el envio del folio porcesar
	tiempoFolio();

}

function tiempoFolio() {
	console.log("espera 10 segundos");
	setTimeout(function () {
		validarRespuestaFolio();
		if (respuestaProcesar == 1) {
			$('#bGrabacion').attr('disabled', false);
			document.getElementById("bGrabacion").classList.add("habilitarImg");
			$('#bAceptar').attr('disabled', true);
			document.getElementById("bAceptar").classList.add("deshabilitarImg");
			loading(false);
		}
		else if (respuestaProcesar == -2 || respuestaProcesar == -3) {
			tiempoFolio();
		}
	}, 10000);
}

function generarFolioEnrolamiento() {
	$.ajax
		({
			data: { opcion: 63, folioConst: folio, tipoconstancia: iTipoAfiliacion, curp: sCurpTrabajador, empleado: iEmpleado },
			success: function (data) {
				datos = data;

				if (datos.respuesta > 0) {
					loading(true);
					setTimeout(function () {
						validarRespuestaFolio();
					}, 5000);
				}
				else {
					var mensaje = 'Promotor, a ocurrido una excepción presione Aceptar para reintentar por favor';
					MuestraDialogoEnrolamiento(mensaje);
				}
			}
		});
}

//function que manda ser llamada desde el boton cancelar
function MuestraDialogoEnrolamiento(mensaje) {

	$("#divMensaje").dialog
		({
			title: 'Captura de Datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"ACEPTAR": function () {
					$(this).dialog("close");
					generarFolioEnrolamiento();
				}
			}
		});
}

function actualizarestatusprocesosolconstancia() {
	$.ajax
		({
			data: { opcion: 64, folioConst: folio },
			success: function (data) {
				datos = data;

				if (datos.respuesta == 1) {
					$('#bContinuar').attr('disabled', false);
				}
			}
		});
}

//Obtiene l valor da la variable pasada por url
function getQueryVariable(varGet) {

	if (OSName == 'Android') {
		varGet = varGet.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + varGet + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	} else {
		//Obtener la url compleata del navegador
		var sUrl = window.location.search.substring(1);
		//Separar los parametros junto con su valor
		var vars = sUrl.split("&");
		for (var i = 0; i < vars.length; i++) {
			//Obtener el parametro y su valor en arreglo de 2
			var par = vars[i].split("=");
			if (par[0] == varGet) // [0] nombre de variable, [1] valor
			{
				return par[1];
			}
		}
		return (false);
	}
}

//Cerrar Modal Mensaje
function cerrar() {
	divDlgMain.innerHTML = "";
	divDlgMain.style.display = 'none';
}

function obtenerBandMensajePromotor() {

	var respvalidarBandPrev = 0;

	datos =
	{
		opcion: 71
	};

	$.ajax(
		{
			data: datos,
			success: function (data) {
				if (data.estatus == 1) {
					respvalidarBandPrev = parseInt(data.valor);
				}
			}
		}
	);

	return respvalidarBandPrev;
}

function actualizarBandMensajePromotor(iFolSol, iValor) {

	var respBandMensajePromot = 0;

	datos =
	{
		opcion: 72,
		iFolioSolicitud: iFolSol,
		iValor: iValor
	};

	$.ajax(
		{
			data: datos,
			success: function (data) {
				if (data.estatus == 1) {
					respBandMensajePromot = data.respuesta;
				}
			}
		}
	);

	return respBandMensajePromot;
}

function indexarFormatoEnrolamiento() {

	var bRetorna = 0;
	var ipOffline = location.host;

	$.ajax(
		{
			data: { opcion: 607, iFolio: folio, iEmpleado: iEmpleado, documento: 'FAHD', ipOffline: ipOffline },
			success: function (data) {
				bRetorna = data;
			}
		});

	return bRetorna;
}

function preguntaIne() {
	banderapreguntarine = 1;
	banderamodal = 1;
	var sTipoConstancia = $('#cboTipoConstancia').val();
	numeroConsulta = 2;
	if ((sTipoConstancia == 27 || sTipoConstancia == 1 || sTipoConstancia == 527) && iTieneautBancoppel != 2) {		/*se cierra modal que abre la pagina de bancoppel. */
		divDlgMain = document.getElementById('divDlgFrame');
		divDlgMain.innerHTML = "";
		divDlgMain.style.display = 'none';
		mensaje = 'Promotor, ¿El Trabajador cuenta con IFE/INE?';
	}
	else {
		mensaje = 'Promotor, ¿El trabajador cuenta con sus dedos índice izquierdo e índice derecho e IFE/INE?';
	}
	$("#divIfe").dialog
		({
			title: 'Captura de Datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{

				"SI": function () {
					$(this).dialog("close");
					iOpcion = VALIDACION_INE;
					ligaIne = obtenerLigaIne();
					//alert(ligaIne);
					opcionejecuta(ligaIne);
					banderapreguntarine = 2;
					//banderapreguntarine = 0;
					banderamodal = 0;

				},
				"NO": function () {
					$(this).dialog("close");
					banderapreguntarine = 2;
					//banderapreguntarine = 0;
					banderamodal = 0;
					//var mensaje = "Promotor, al no realizar la Autenticación INE debes continuar con el traspaso solicitando al Trabajador la Constancia de Folio de Estado de Cuenta (CFEC BanCoppel) o el Estado de Cuenta de su Afore Actual.";
					cancelacionIne();
				}
			}
		});
}

function cancelacionIne() {
	banderapreguntarine = 2;
	var sTipoConstancia = $('#cboTipoConstancia').val();
	var mensaje = "";
	if (sTipoConstancia == 27 || sTipoConstancia == 1 || sTipoConstancia == 527) {
		/*se cierra modal que abre la pagina de bancoppel. */
		divDlgMain = document.getElementById('divDlgFrame');
		divDlgMain.innerHTML = "";
		divDlgMain.style.display = 'none';

		if (iTieneautBancoppel == 2) {
			mensaje = "Promotor, al no realizar la Autenticación INE debes continuar con el traspaso solicitando al Trabajador la Constancia de Folio de Estado de Cuenta (CFEC BanCoppel) o el Estado de Cuenta de su Afore Actual.";
		} else {
			mensaje = "Promotor, al no realizar la Autenticación debes continuar con el Traspaso solicitando al Trabajador el Estado de Cuenta de su Afore Actual";
		}

	} else {
		mensaje = "Promotor, al no realizar Autenticación INE no podrás continuar con la afiliación.";
	}

	$("#divIne").dialog
		({
			title: 'Captura de Datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{

				"ACEPTAR": function () {
					$(this).dialog("close");
				}
			}

		});
}

function preguntaBancoppel() {
	banderapreguntarine = 1;
	banderamodal = 2;
	ligaIne = obtenerLigaIne01(12);
	numeroConsulta = 2;
	mensaje = 'Promotor, ¿El Trabajador cuenta con sus dedos índice izquierdo e índice derecho?';
	$("#divBancoppel").dialog
		({
			title: 'Captura de Datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{

				"SI": function () {
					$(this).dialog("close");
					//console.log("entra btnFrame");
					//modalbancoppel(1);
					var myDlg = new dialog_frame(document.getElementById('divDlgFrame'));
					myDlg.FramePagina(80, 80, ligaIne);
					iOpcion = VALIDACION_BANCOPPEL;
					banderaautbancoppel = 0;
					banderapreguntarine = 0;
					banderapreguntarine = 2;
					banderamodal = 0;
				},
				"NO": function () {
					$(this).dialog("close");
					banderaautbancoppel = 0;
					banderapreguntarine = 0;
					banderapreguntarine = 2;
					banderamodal = 0;
					//var mensaje = "Promotor, al no realizar la Autenticación INE debes continuar con el traspaso solicitando al Trabajador la Constancia de Folio de Estado de Cuenta (CFEC BanCoppel) o el Estado de Cuenta de su Afore Actual.";
					cancelacionIne();
				}
			}
		});
}

function cancelacionRegistroTerceros(mensaje) {
	$("#divIne").dialog
		({
			title: 'Captura de Datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{

				"ACEPTAR": function () {
					$(this).dialog("close");
				}
			}

		});
}

function tomaHuellasIne() {
	folioIne = 6707802;
	//sParametros =  + "6707802 " + iEmpleado + " 1";
	sParametros = folioIne + " " + iEmpleado + " 1";
	if (OSName == "Windows XP") {
		sRuta = "C:\\sys\\mAfore\\ClientesAforeOL.exe";
	}
	else {
		sRuta = "C:\\sys\\mAfore\\ClientesAforeOL.exe";
	}

	//sMensajeBloqueo = "Validando Huellas del Trabajador...";

	//bloquearUIHuella(sMensajeBloqueo)
	ejecutaWebServiceHuellaIne(sRuta, sParametros);
}

function ejecutaWebServiceHuellaIne(sRuta, sParametros) {
	soapData = "",
		httpObject = null,
		docXml = null,
		iEstado = 0,
		sMensaje = "";
	sUrlSoap = "http://127.0.0.1:20044/";

	soapData =
		'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
		'<SOAP-ENV:Envelope' +
		' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"' +
		' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"' +
		' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"' +
		' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"' +
		' xmlns:ns2=\"urn:ServiciosWebx\">' +
		'<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">' +
		'<ns2:ejecutarAplicacion>' +
		'<inParam>' +
		'<Esperar>1</Esperar>' +
		'<RutaAplicacion>' + sRuta + '</RutaAplicacion>' +
		'<parametros>' + sParametros + '</parametros>' +
		'</inParam>' +
		'</ns2:ejecutarAplicacion>' +
		'</SOAP-ENV:Body>' +
		'</SOAP-ENV:Envelope>';

	httpObject = getHTTPObjectH();

	if (httpObject) {
		if (httpObject.overrideMimeType) {
			httpObject.overrideMimeType("false");
		}

		httpObject.open('POST', sUrlSoap, false); //-- no asincrono
		httpObject.setRequestHeader("Accept-Language", null);
		httpObject.onreadystatechange = function () {
			if (httpObject.readyState == 4 && httpObject.status == 200) {
				parser = new DOMParser();
				docXml = parser.parseFromString(httpObject.responseText, "text/xml");
				iEstado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;
				//Llamada a la función que recibe la respuesta del WebService
				respuestaWebServiceHuellaIne(iEstado);
			}
			else {
				console.log(httpObject.status);
			}
		};
		httpObject.send(soapData);
	}
}
/**
 * RESPUESTA WEBSERVICE
*/
function respuestaWebServiceHuellaIne(iRespuesta) {
	console.log("RESPUESTA APPLET CAPTURA INDICES INE: " + iRespuesta);
	var mensajeH = '';

	mensaje = 'Inició Validacion INE';
	iOpcion = VALIDACION_INE;
	ligaIne = obtenerLigaIne();
	opcionejecuta(ligaIne);
	cancelacionIne(mensaje);
}

function obtenerLigaIne() {
	var ligaObtIne;
	var pasoRenapo;
	var sTipoConstancia = $('#cboTipoConstancia').val();
	var tipohuella = 0; // tipo huella 0 es que no entro previamente por bancoppel, tipohuella = 1 es que previamente capturo las huellas en autenticacion bancoppel

	var curpT = (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) && $('#cboTipoSolicitante').val() != '1' ? $('#txtCurp2').val() : $('#txtCurp').val();

	//Folio 539 verifica si se obtuvo la curp por la captura de datos de bancoppel cuando falla renapo
	if(bandCapturaDatos==1){
		curpT = curpCapturaDatos;
	}

	/*Identificador de utileria:
		0 => Identifica que sera un tipo traspaso para ejecutar la validacion por INE.
		3 => Identifica que sera un tipo registro de terceras figuras para ejecutar la validacion INE.
	*/
	var esUtileria = (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) ? 3 : 0;

	if (bEsRenapo) {
		pasoRenapo = 1;
	}
	else {
		if (bEsRenapoSol && esUtileria == 3) {
			pasoRenapo = 1;
		}
		else {
			pasoRenapo = 0;
		}
	}

	datos =
	{
		opcion: 73,
		empleado: iEmpleado,
		curp: curpT,
		pasorenapo: pasoRenapo
	};

	$.ajax(
		{
			data: datos,
			success: function (data) {
				if (iTieneautBancoppel != 2 && (sTipoConstancia == 27 || sTipoConstancia == 1 || sTipoConstancia == 527)) {
					tipohuella = 1;
					ligaObtIne = data.liga + iTipoAfiliacion + '&utileria=' + esUtileria + '&huellas=' + tipohuella;
				} else {
					tipohuella = 0;
					ligaObtIne = data.liga + iTipoAfiliacion + '&utileria=' + esUtileria + '&huellas=' + tipohuella;
				}
			}
		}
	);

	return ligaObtIne;
}

function mostrarMensajeIne(mensaje) {
	$("#divIne").dialog
		({
			title: 'Captura de Datos',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			//closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{

				"ACEPTAR": function () {
					banderapreguntarine = 2;
					$(this).dialog("close");
					obtenerEstatusIne();
					entroine = 1;
				}
			}

		});
}

function obtenerEstatusIne() {
	banderapreguntarine = 2;
	var curpT = (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) && $("#cboTipoSolicitante").val() != "1" ? $('#txtCurp2').val() : $('#txtCurp').val();

	datos =
	{
		opcion: 74,
		curp: curpT
	};

	$.ajax(
		{
			data: datos,
			success: function (data) {
				if (parseInt(data.estatusine) == 5) {
					banderaIne = 0;
					estatusValIne = 1;
					folioIneVal = data.foliosoline;
					if (data.telefonoine != '' && data.telefonoine != undefined && data.companiatel != 0 && data.companiatel != -1 && data.companiatel != undefined) {
						$('#txTelCelular').val(data.telefonoine.trim());
						$('#cboCompTelef').val(data.companiatel);
						if (!bEsRenapo) {
							$('#bRenapoTitular').attr('disabled', true);
							document.getElementById("bRenapoTitular").classList.add("deshabilitarImg");
							$('#bRenapoSol').attr('disabled', true);
							document.getElementById("bRenapoSol").classList.add("deshabilitarImg");

							$('#txtApellidoPaterno').attr("disabled", false);
							$('#txtApellidoMatero').attr("disabled", false);
							$('#txtNombre').attr("disabled", false);

							if (iTipoAfiliacion == 26 || iTipoAfiliacion == 27 || iTipoAfiliacion == 527) //Si es un idependiente
								$("#txtNss").attr("disabled", false);
							$('#txtCurp').attr("disabled", true);
						}
						datosteline = true;
					}
				}
				else {
					//var mensaje = "Promotor, al no realizar la Autenticación INE debes continuar con el traspaso solicitando al Trabajador la Constancia de Folio de Estado de Cuenta (CFEC BanCoppel) o el Estado de Cuenta de su Afore Actual.";
					banderaIne = 0;
					cancelacionIne();
					datosteline = false;
				}
			}
		});
}

function actualizarFolioIne() {
	var curpT = (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) && $('#cboTipoSolicitante').val() != '1' ? $('#txtCurp2').val() : $('#txtCurp').val();
	$.ajax
		({
			data: { opcion: 75, folioConst: folio, curp: curpT, empleado: iEmpleado, foliosoline: folioIneVal },
			success: function (data) {
				datos = data;
				if (datos.actualizo == 1) {
					marcarLog('SE GUARDA EL FOLIO INTERNO EN TABLA DE CONTROL INE');
				}
				else {
					marcarLog('NO SE PUDO EJECUTAR LA ACTUALIZACIÓN DEL FOLIO EN CONTROL INE');
				}
			}
		});
}

function subirvolumenaudio() {
	iOpcion = SUBIR_VOLUMEN;
	opcionejecuta('');
}

function obtenerDialogos(valordialogo) {
	$.ajax
		({
			data: { opcion: 47, tipodialogo: valordialogo },
			success: function (data) {
				datos = data;
				if (datos.respuesta == 1) {
					cdialogoVideo = datos.dialogo.trim();
					iTiempoMinimo = datos.tiempominimo;
					iTiempoMaximo = datos.tiempomaximo;
					cFormato = datos.formato;
					iCalidad = datos.calidad;
					cIpModulo = datos.ipmodulo;

					switch (valordialogo) {
						case '1':
							cdialogoVideo = cdialogoVideo.replace("[Nombre]", cNombreTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoPat]", cApellidoPatTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoMat]", cApellidoMatTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[celulartrab]", iCelular);
							cdialogoVideo = cdialogoVideo.replace("[AforeCedente]", aforeActual);
							break;
						case '2':
							cdialogoVideo = cdialogoVideo.replace("[Nombre]", cNombreTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoPat]", cApellidoPatTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoMat]", cApellidoMatTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[celulartrab]", iCelular);
							break;
						case '3':
						case '5':
						case '6':
							cdialogoVideo = cdialogoVideo.replace("[nombrebeneficiario]", cNombreSolicitante);
							cdialogoVideo = cdialogoVideo.replace("[apellidoparbenef]", cApellidoPatSolicitante);
							cdialogoVideo = cdialogoVideo.replace("[apellidomatbenef]", cApellidoMatSolicitante);
							cdialogoVideo = cdialogoVideo.replace("[nombretrab]", cNombreTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[apellidopattrab]", cApellidoPatTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[apellidomattrab]", cApellidoMatTrabajador);
							break;
						case '4':
							cdialogoVideo = cdialogoVideo.replace("[NombreProm]", cNombrePromotor);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoPatProm]", cApellidoPatPromotor);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoMatProm]", cApellidoMatPromotor);
							cdialogoVideo = cdialogoVideo.replace("[NombreDeTrab]", cNombreTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoPaternoTrab]", cApellidoPatTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoMaternoTRab]", cApellidoMatTrabajador);
							cdialogoVideo = cdialogoVideo.replace("[celulartrab]", iCelular);
							break;
						case '7':
							var sSolicitante = ""

							if (iTipoSolicitante == 2)
								sSolicitante = "Beneficiario";
							else if (iTipoSolicitante == 3)
								sSolicitante = "Apoderado Legal";
							else if (iTipoSolicitante == 4)
								sSolicitante = "Curador";

							cdialogoVideo = cdialogoVideo.replace("[NombreProm]", cNombrePromotor);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoPatProm]", cApellidoPatPromotor);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoMatProm]", cApellidoMatPromotor);
							cdialogoVideo = cdialogoVideo.replace("[TipoSolicitante]", sSolicitante);
							cdialogoVideo = cdialogoVideo.replace("[NombreDeTrab]", cNombreSolicitante);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoPaternoTrab]", cApellidoPatSolicitante);
							cdialogoVideo = cdialogoVideo.replace("[ApellidoMaternoTRab]", cApellidoMatSolicitante);
							cdialogoVideo = cdialogoVideo.replace("[celulartrab]", iCelular);
							break;
					}
				}
				else {
					//alert('NO SE EJECUTO CORRECTAMENTE LA APLICACIÓN, INTENTE DE NUEVO');
				}
			}
		});
}

function moverFormatoEnrol() {
	$.ajax(
		{
			data: { opcion: 608, folio: folio },
			success: function (data) {
				bRetorna = data;
			}
		});

	return bRetorna;
}

function fntieneautenticacionine() {
	cCurp = (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) && $('#cboTipoSolicitante').val() != '1' ? $('#txtCurp2').val() : $('#txtCurp').val();

	datos =
	{
		opcion: 76,
		curp: cCurp,
		empleado: iEmpleado
	};

	$.ajax(
		{
			data: datos,
			success: function (data) {
				folioIneVal = parseInt(data.respuesta);
				if (folioIneVal > 0) {
					cuentaconAutIne = true;
					numeroConsulta = 2;
					estatusValIne = 1;
					banderaIne = 0;
					banderaautbancoppel = 0;
				}
				else {
					cuentaconAutIne = false;
					banderaautbancoppel = 0;

				}
			}
		});
}

function almacenaDispositivoAfiliacion() {
	$.ajax(
		{
			data: { opcion: 610, folio: folio, sistemaOperativo: OSName, tipoConexion: 1 },
			success: function (data) {
				bRetorna = data;
			}
		});

	return bRetorna;
}

function preguntaconsulta() {

	var sTipoConstancia = $('#cboTipoConstancia').val();
	var consval = consultarValidaciones();
	
	//Folio 539 Verifica que se haya seleccionado un cuentahabiente de la respuesta del WS de bancoppel y se haya guardado y tenga el vaolor cuentaefectiva
	if($('#cboTipoSolicitante').val()==1){
		if(Gcuentahabiente["CTA_EFECTIVA_ACTIVA"] != undefined){
			var cuentaefectiva = Gcuentahabiente["CTA_EFECTIVA_ACTIVA"];
		}
	}
	
	
	//Folio 539 Se agrego la condicion de la cuentaefectiva la cual debe estar en uno para proceder con la autenticacion bancoppel
	if ((consval == true && sTipoConstancia == 27 || sTipoConstancia == 1 || sTipoConstancia == 527) && OSName != 'Android') {
		if((banderabancoppel539 == true && cuentaefectiva == 1))
		{
			preguntaBancoppel();
			banderapreguntarine = 1;
		}
		else
		{	
			iTieneautBancoppel = 2;
			preguntaIne();
			banderapreguntarine = 1;
		}
	}
	else if ((consval == true && entroine == 0) || OSName == 'Android') {
		banderapreguntarine = 0;
		if($('#txtCurp2').val() == "" || $('#txtCurp').val() == "")
		{

		}else{
			preguntaIne();
			banderapreguntarine = 1;
		}
	}
}

function respuestabancoppel(respuesta) {
	//alert("Datos de respuesta->" + respuesta);
	console.log("Datos de respuesta-> " + respuesta);
	divDlgMain = document.getElementById('divDlgFrame');
	divDlgMain.innerHTML = "";
	divDlgMain.style.display = 'none';
}

function obtenerEstatusBancoppel() {
	estatusValBancoppel = 1;
	banderaautbancoppel = 0;
	banderapreguntarine = 2;
	if (iTelefonoautbancoppel != '' && iTelefonoautbancoppel != undefined && iCompaniabancoppel != 0 && iCompaniabancoppel != -1 && iCompaniabancoppel != undefined) {
		$('#txTelCelular').val(iTelefonoautbancoppel.trim());
		$('#cboCompTelef').val(iCompaniabancoppel);
		if (!bEsRenapo) {
			$('#txtApellidoPaterno').attr("disabled", false);
			$('#txtApellidoMatero').attr("disabled", false);
			$('#txtNombre').attr("disabled", false);

			if (iTipoAfiliacion == 26 || iTipoAfiliacion == 27 || iTipoAfiliacion == 527) //Si es un idependiente
				$("#txtNss").attr("disabled", false);
			$('#txtCurp').attr("disabled", true);
		}
		datostelbancoppel = true;
	}
	else {
		//var mensaje = "Promotor, al no realizar la Autenticación INE debes continuar con el traspaso solicitando al Trabajador la Constancia de Folio de Estado de Cuenta (CFEC BanCoppel) o el Estado de Cuenta de su Afore Actual.";
		banderaautbancoppel = 0;
		cancelacionIne();
		datostelbancoppel = false;
	}
}

function actualizarFolioBancoppel() {
	var curpT = (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) && $('#cboTipoSolicitante').val() != '1' ? $('#txtCurp2').val() : $('#txtCurp').val();
	var datos = new Array();
	var arrDatos = {
		'iFolio': folio,
		'cCurp': curpT,
		'iEmpleado': iEmpleado,
		'iFolioBanco': iFoliobancoppel
	}
	$.ajax
		({
			data: { opcion: 79, idcons: "actualizarfoliobancoppel", datoscaptura: arrDatos },
			success: function (data) {

				datos = (data);
				var respapi = 0;
				var respapi = datos.registros[0][0].respuesta;
				console.log("variable respapi ->" + respapi);
				if (respapi == 1) {
					marcarLog('SE GUARDA EL FOLIO INTERNO EN TABLA DE CONTROL INE');
				}
				else {
					marcarLog('NO SE PUDO EJECUTAR LA ACTUALIZACIÓN DEL FOLIO EN CONTROL INE');
				}
			}
		});
}

function exitoBancoppel(iFoliobanco, celular, compania) {
	iFoliobancoppel = iFoliobanco;
	iTelefonoautbancoppel = celular;
	iCompaniabancoppel = compania;
	console.log("iFoliobancoppel" + iFoliobancoppel + "iTelefonoautbancoppel ->" + iTelefonoautbancoppel + "iCompaniabancoppel ->" + iCompaniabancoppel);
	divDlgMain = document.getElementById('divDlgFrame');
	divDlgMain.innerHTML = "";
	divDlgMain.style.display = 'none';
	obtenerEstatusBancoppel();

}

function tieneautenticacionbancoppel() {
	cCurp = (iTipoAfiliacion == 26 || iTipoAfiliacion == 33) && $('#cboTipoSolicitante').val() != '1' ? $('#txtCurp2').val() : $('#txtCurp').val();
	var obtipmodulo = obtenerIPModulo();
	var consval = consultarValidaciones();
	var datos = new Array();
	var arrDatos =
	{
		"cCurp": cCurp,
		"iEmpleado": iEmpleado,
		"cIpmodulo": obtipmodulo
	};

	$.ajax(
		{
			data: { opcion: 79, idcons: "tieneautenticacionbancoppel", datoscaptura: arrDatos },
			success: function (data) {
				datos = (data);
				iTieneautBancoppel = datos.registros[0][0].respuesta;
				console.log(data);
				console.log("variable que valida la autenticacion bancoppel ->" + iTieneautBancoppel);
				console.log("ip del modulo l ->" + obtipmodulo);
				if (iTieneautBancoppel > 0 && iTieneautBancoppel != 2 && consval == true) {
					cuentaconAutBancoppel = true;
					numeroConsulta = 2;
					estatusValBancoppel = 1;
					banderaautbancoppel = 0;
				}
				else {
					cuentaconAutBancoppel = false;
				}
				if (consval == true && iTieneautBancoppel == 2 && banderapreguntarine == 0) {
					entroine = 0;
					preguntaIne();
					//banderapreguntarine = 2;
				}
			}
		});
}

function obtenerLigaIne01(iOpcionliga) {
	var ligaObtIne;
	var pasoRenapo;
	if (bEsRenapo) { pasoRenapo = 1; } else { pasoRenapo = 0; }
	var ipModulo = obtenerIPModulo();
	var arrDatos = {
		'empleado': iEmpleado,
		'curp': $('#txtCurp').val(),
		'pasoRenapo': pasoRenapo,
		'ipModulo': ipModulo,
		'iAutenticacion': iOpcionliga
	}
	$.ajax({
		data: { opcion: 79, idcons: "obtenerLigaIne01", datoscaptura: arrDatos },
		success: function (data) {
			ligaObtIne = data["registros"].liga;
		}
	});

	return ligaObtIne;
}

function obtenerIPModulo() {
	var ipModulo = '';
	$.ajax({
		data: { opcion: 80 },
		success: function (data) {
			ipModulo = data;
		}
	});
	return ipModulo;
}

function modalAutenticacion() {
	if (banderamodal == 1) {
		$("#divIfe").dialog("close");
	}
	else if (banderamodal == 2) {
		$("#divBancoppel").dialog("close");
	}
}


function verificaExpedientePermanente() {
	var arrdatos = Array();
	$.ajax(
		{
			async:false,
			cache:false,
			data: { opcion: 81, folioservicioafore: iFolioConsulta },
			success: function (data) {

				arrdatos = eval(data);
			}
		});
	return arrdatos;
}

function mensajeVerExpIden(sMensajeVerExpIden) {
	$('.content-loader').hide();
	$("#divMensaje").dialog
		({
			title: '',
			autoOpen: true,
			resizable: false,
			width: '700',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + sMensajeVerExpIden + "</p>");
			},
			buttons:
			{
				"Aceptar": function () {
					$(this).dialog("close");
					//location.href = "/menuafore/indexMenu.html?empleado="+iEmpleado
					window.close();
				}

			}

		});
}

function subirHuellas() {

	sRuta = "C:\\SYS\\PROGS\\SUBIRARCHIVOFTPSVR.EXE";
	iOpcion = SUBIR_ARCHIVO;
	iOpcionHuella = PUBLICA_HUELLAS_IZQ;
	sCurp = sCurpRenapo == "" ? $('#txtCurp').val() : sCurpRenapo;

	nombreArchivoIzq = sCurp + "_" + iEmpleado + "I.txt";
	nombreArchivoDer = sCurp + "_" + iEmpleado + "D.txt";

	sParametros = nombreArchivoIzq + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_ENROLAMIENTO + " " + location.host + " 0";

	console.log("sRuta " + sRuta);
	console.log("sParametros " + sParametros);

	if (OSName == "Android" && bBanderaHuellaMovil) {

		//Generar Archivo  Huella Izquierda
		generarArchivoHuellas(nombreArchivoIzq, TemplateIzqTF);

		//Generar Archivo Huella Derecha
		generarArchivoHuellas(nombreArchivoDer, TemplateDerTF);

		//Guardar Huellas en BD
		guardarHuellasIndices(nombreArchivoIzq, nombreArchivoDer, sCurpTrabajador, iEmpleado);

	} else {

		ejecutaWebService(sRuta, sParametros, iOpcion);
	}
}

function publicahuellasizq(parametros) {
	iContHuellas++;
	iOpcion = SUBIR_ARCHIVO;
	iOpcionHuella = PUBLICA_HUELLAS_IZQ;
	ejecutaWebService(sRuta, parametros, iOpcion);
}

function publicahuellasder(parametros) {
	iContHuellas++;
	iOpcion = SUBIR_ARCHIVO;
	iOpcionHuella = PUBLICA_HUELLAS_DER;
	ejecutaWebService(sRuta, parametros, iOpcion);
}

function guardarHuellasIndices(nombreArchivoIzq, nombreArchivoDer, sCurpTrabajador, iEmpleado) {

	$.ajax(
		{
			data: { opcion: 82, nombrearchivoizquierdo: nombreArchivoIzq, nombrearchivoderecho: nombreArchivoDer, curpTrabajador: sCurpTrabajador, empleado: iEmpleado },
			success: function (data) {
				if (data.respuesta == 1) {
					marcarLog('se grabaron las huellas exitosamente');
					limpiarHuellasServicio(nombreArchivoIzq, nombreArchivoDer);
				}
				else if (data.respuesta == -1) {
					marcarLog('metodo -> guardarHuellasIndices , No existe el folio en solconstancia');
				}
				else {
					marcarLog('error al ejecutar el metodo -> guardarHuellasIndices');
				}
			}
		});
}

function limpiarHuellasServicio(nombreArchivoIzq, nombreArchivoDer) {
	$.ajax(
		{
			data: { opcion: 83, nombrearchivoizquierdo: nombreArchivoIzq, nombrearchivoderecho: nombreArchivoDer },
			success: function (data) {
				if (data.estatus == 1) {
					marcarLog('se borraron las huellas');
				}
				else {
					marcarLog('error al guardar las huellas');
				}
			}
		});
}

function verificaEjecucionServicio() {
	var servicioActivo = false;
	$.ajax(
		{
			data: { opcion: 84 },
			success: function (data) {
				servicioActivo = data.estatus == 1 ? true : false;
				enrolamientopermanente = data.enrolamientopermanente == 1 ? true : false;
			}
		});

	return servicioActivo;
}

function elminarinformacionServicio() {
	$.ajax(
		{
			data: { opcion: 85 },
			success: function (data) {
			}
		});
}

function obtenerEstatusAfiliacion() {

	var sCurp = $("#txtCurp").val();
	var iestatusproceso = 0;

	$.ajax({
		data: { opcion: 86, curp: sCurp },
		success: function (data) {
			/********DATOS DE RETORNO**********
			***********************************
			** 1 -> Paso de enrolamiento.
			** 2 -> Paso de captura de video.
			** 3 -> Paso de Digitalización.
			***********************************
			*/
			iestatusproceso = data.iestatusproceso;
			switch (iestatusproceso) {
				case 1:
					//SE BLOQUEAN LOS BOTONES
					$('#bGrabacion').attr('disabled', true);
					document.getElementById("bGrabacion").classList.add("deshabilitarImg");
					$('#bDigitalizar').attr('disabled', true);
					document.getElementById("bDigitalizar").classList.add("deshabilitarImg");
					$('#bContinuar').attr('disabled', true);
					document.getElementById("bContinuar").classList.add("deshabilitarImg");
					//SE EJECUTA EL ENROLAMIENTO
					iOpcion = ENROLAR_TRABAJADOR;
					sTitle = "";
					//sMensaje = 'Promotor: inició el proceso de enrolamiento.';
					//mostrarMensaje(sMensaje, sTitle);
					iCodEjecutarEnrol = 'EET';
					mostrarmensaje2();
					break;
				case 2:
					//SE BLOQUEAN BOTONES
					$('#bDigitalizar').attr('disabled', true);
					document.getElementById("bDigitalizar").classList.add("deshabilitarImg");
					$('#bContinuar').attr('disabled', true);
					document.getElementById("bContinuar").classList.add("deshabilitarImg");
					//SE DESBLOQUEA EL BOTON DEL VIDEO PARA SU CAPTURA
					$('#bGrabacion').attr('disabled', false);
					document.getElementById("bGrabacion").classList.add("habilitarImg");
					break;
				case 3:
					//SE BLOQUEAN BOTONES
					$('#bAceptar').attr('disabled', true);
					$('#bAceptar').removeClass("habilitarImg").addClass("deshabilitarImg");
					$('#bGrabacion').attr('disabled', true);
					$('#bGrabacion').removeClass("habilitarImg").addClass("deshabilitarImg");
					//SE DESBLOQUEA BOTON PARA DIGITALIZAR
					$('#bDigitalizar').attr('disabled', false);
					$('#bDigitalizar').removeClass("deshabilitarImg").addClass("habilitarImg");
					break;
				case 4:
					//SE BLOQUEAN BOTONES
					$('#bAceptar').attr('disabled', true);
					$('#bAceptar').removeClass("habilitarImg").addClass("deshabilitarImg");
					$('#bGrabacion').attr('disabled', true);
					$('#bGrabacion').removeClass("habilitarImg").addClass("deshabilitarImg");
					$('#bDigitalizar').attr('disabled', false);
					$('#bDigitalizar').removeClass("habilitarImg").addClass("deshabilitarImg");
					//SE DESBLOQUEA BOTON PARA DIGITALIZAR
					$('#bContinuar').attr('disabled', false);
					$('#bContinuar').removeClass("deshabilitarImg").addClass("habilitarImg");
					break;
				case 5:                    //SE BLOQUEAN LOS BOTONES 
					$('#bGrabacion').attr('disabled', true);
					document.getElementById("bGrabacion").classList.add("deshabilitarImg");
					$('#bDigitalizar').attr('disabled', true);
					document.getElementById("bDigitalizar").classList.add("deshabilitarImg");
					$('#bContinuar').attr('disabled', true);
					document.getElementById("bContinuar").classList.add("deshabilitarImg");
					//SE CAPTURAN INDICES                   
					validarHuella();
					subirHuellas();
					break;
				default:
					break;
			}
		}
	});
}
//aqui comienza cdigo para modal envia correo o mensaje***************************************//////////////////////////////////////

function envíoCartaObtenciónFCT() {
	var cMensaje = "";
	var pMensaje = '';
	var sradiocorreo = "'radiocorreo'"
	var sradiocelular = "'radiocelular'"
	cMensaje += '<div id="divCont" class="modal-content">';
	cMensaje += '<div class="modal-header">';
	cMensaje += '<div><p>Promotor, indica al trabajador que debe solicitar su Folio de Conocimiento de Traspaso para realizar su trámite</p></div>';
	cMensaje += '<input id="radiocorreo" type="radio" name="click" onClick="muestra_oculta(' + sradiocorreo + ')" checked>';
	cMensaje += 'Correo Electronico &nbsp;&nbsp;&nbsp;';
	cMensaje += '<input id="radiocelular" type="radio" name="click" onClick="muestra_oculta(' + sradiocelular + ')"> Celular';
	cMensaje += '</div>';

	cMensaje += '<div id="divBody" class="modal-body" style="background-color:rgb(241,245,248);">';
	cMensaje += '<div id="divCorreo"class=""	 style="background-color:rgb(241,245,248);">';
	cMensaje += '<label>Correo:&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;</label>';
	//cMensaje += '<input id="txtCorreoCarta" size="30" maxlength="99" class="txtSombraRojo" onchange="validaSiEsVacio()" type="text"style="witdh:160.95;height:27" /> &nbsp;@';
	cMensaje += '<input id="txtCorreoCarta" size="30" maxlength="99" class="txtSombraRojo" onchange="validaSiEsVacio()" type="text"style="margin-left: 11px;" /> &nbsp;@';
	cMensaje += '<select id="cbxDominioCorreo" class="custom-select " style="margin-bottom:5px;" onchange="validaSiEsOtro()">';
	cMensaje += '<option selected value="0">Seleccione un dominio...</option>';
	cMensaje += '<option value="1">gmail.com</option>';
	cMensaje += '<option value="2">outlook.com</option>';
	cMensaje += '<option value="3">outlook.es</option>';
	cMensaje += '<option value="4">hotmail.com</option>';
	cMensaje += '<option value="5">mail.com</option>';
	cMensaje += '<option value="6">yahoo.com</option>';
	cMensaje += '<option value="7">coppel.com</option>';
	cMensaje += '<option value="8">aforecoppel.com</option>';
	cMensaje += '<option value="9">bancoppel.com</option>';
	cMensaje += '<option value="10">otros</option>';
	cMensaje += '</select>';
	cMensaje += "<label id='labelMensajeErrorEnvioCarta' style='font-size:9; color:#FF0000;'></label>";
	cMensaje += '<input id="dominio" size="20" maxlength="99" class=" show1" type="text" onchange="validaSiEsOtro()"style="margin-left: 1px; witdh:160.95;height:27"/>';
	cMensaje += '<br>';
	cMensaje += '<label>Confirmar:&nbsp;&nbsp;&nbsp;</label>';
	cMensaje += '<input id="txtCorreoCarta2" size="30" maxlength="99" class="txtSombraRojo" onchange="validaSiEsVacio2()"style="margin-left: 5px;" type="text" /> &nbsp;@';
	cMensaje += '<select id="cbxDominioCorreo2"  class="custom-select" onchange="validaSiEsOtroConfir()">';
	cMensaje += '<option selected value="0">Seleccione un dominio...</option>';
	cMensaje += '<option value="1">gmail.com</option>';
	cMensaje += '<option value="2">outlook.com</option>';
	cMensaje += '<option value="3">outlook.es</option>';
	cMensaje += '<option value="4">hotmail.com</option>';
	cMensaje += '<option value="5">mail.com</option>';
	cMensaje += '<option value="6">yahoo.com</option>';
	cMensaje += '<option value="7">coppel.com</option>';
	cMensaje += '<option value="8">aforecoppel.com</option>';
	cMensaje += '<option value="9">bancoppel.com</option>';
	cMensaje += '<option value="10">otros</option>';
	cMensaje += '</select>';
	cMensaje += '<input id="dominio2" size="20" maxlength="99" class="show1" type="text" onchange="validaSiEsOtroConfir()"/>';
	cMensaje += "<label id='labelMensajeErrorEnvioCartaConfir' style='font-size:9; color:#FF0000;'></label>";
	cMensaje += '<br>';
	cMensaje += '<br>';
	cMensaje += '&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;';
	cMensaje += '&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;';
	cMensaje += '<button id="btnCancelarCorreo" type="Button" style="background-color: rgb(78,147,206);" class="btn btn-primary"';
	cMensaje += 'onclick="cerrarModal()">Cancelar';
	cMensaje += '</button>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;';
	cMensaje += '<button id="btnEnvioCorreo" type="button" style="background-color: rgb(78,147,206);" class="btn btn-primary"';
	cMensaje += 'onclick="enviaCorreo()">Enviar</button>';
	cMensaje += '</div>';

	cMensaje += '<div id="divCelu"  class="show1" style="background-color:rgb(241,245,248);">';
	cMensaje += '<label>Celular:&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</label>';
	cMensaje += '<input style="margin-left: 35px"id="txtCelular" size="30" maxlength="10" class="txtSombraRojo " onchange="verificaCofetel()" type="text" style="margin-bottom:5px;margin-left:2px;"/>';
	cMensaje += "<label id='labelMensajeErrorEnvioCartaCel' style='font-size:9; color:#FF0000;'></label>";
	cMensaje += '<br>';
	cMensaje += '<label>Confirmación : &nbsp;&nbsp;&nbsp;</label>';
	cMensaje += '<input id="txtCelularConfir" size="30" maxlength="10" class="txtSombraRojo" onchange="compararNumero()" type="text" style=" margin-botton:5px;"/>';
	cMensaje += "<label id='labelMensajeErrorEnvioCartaCelConfir' style='font-size:9; color:#FF0000;'></label>";
	cMensaje += '<br>';
	cMensaje += '<br>';
	cMensaje += '&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;';
	cMensaje += '&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;';
	cMensaje += '<button id="btnCancelarMensaje"type="Button" style="background-color: rgb(78,147,206);" class="btn btn-primary"';
	cMensaje += 'onclick="cerrarModal()">Cancelar';
	cMensaje += '</button>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;';
	cMensaje += '<button id="btnEnvioMensaje"type="button" style="background-color: rgb(78,147,206);" class="btn btn-primary"';
	cMensaje += 'onclick="enviarmensaje()">Enviar</button>';
	cMensaje += '</div>';
	cMensaje += '</div>';
	cMensaje += '</div>';
	//cMensaje = 'hola1';
	//console.log(cMensaje);
	let t = { 'text': cMensaje };
	var fFuncionEn = "enviaCorreo();";
	var fFuncionCa = "";
	ModalMensajeFuncion(cMensaje);
	$('#btnCancelarMensaje').prop("disabled", false);
	$('#btnCancelarCorreo').prop("disabled", false);
	$('#btnEnvioMensaje').prop("disabled", true);
	$('#btnEnvioCorreo').prop("disabled", true);
	//catalogoDominioCorreo();
	cambiaSombraTxt("txtCorreoCarta");
	cambiaSombraTxt("cbxDominioCorreo");
}

function cerrarModal() {
	var entrega = 3;
	var tipo = '';
	var adicional = '';
	var ipremotoservidor = obtenerIPServidor();
	var numero = '';
	insertarTipoEntregaCorreoTelefono(entrega, numero, ipremotoservidor);
	detonapythonenviocorreo(3, adicional);
	$("#myModal").dialog('close');
	// impimir aqui
	let curp = $("#txtCurp").val();
	//$("#btnCancelarCorreo").attr("data-print-resource-uri", "http://10.27.142.242/fct/MABL951014HSLRNS00.pdf");
	//$("#btnCancelarCorreo").attr("data-print-resource-type", "application/pdf");
	//$("#myModal").html('<iframe src="http://10.27.142.242/salidas/constanciaimplicacionestraspaso/0.pdf" width="100%" style="height:100%"></iframe>');
	//printButton = document.getElementById('btnCancelarCorreo');
	let url = "../../fct/" + curp + ".pdf";
	ModalPdfFct('<iframe src="' + url + '" width="100%" style="height:100%"></iframe>');
	//window.print('<iframe src="'+url+'" width="100%" style="height:100%"></iframe>');
	//printFrame = document.getElementById('printf');
	//printBlob('printf', printButton.getAttribute('data-print-resource-uri'), printButton.getAttribute('data-print-resource-type'));
}

function muestra_oculta(id) {
	var divCo = document.getElementById("divCorreo").innerHTML;
	var divCe = document.getElementById("divCelu").innerHTML;
	if (id === "radiocorreo") {
		$('#txtCelular').removeClass("txtSombraAZul");
		$('#divCorreo').removeClass('show1');
		$('#divCelu').addClass('show1');
		//console.log("removilaclaseazul");
		$('#txtCelular').addClass("txtSombraRojo");
		//$('#divCont').removeClass('marger');
		$("#txtCelular").val('');
		$("#txtCelularConfir").val('');
	}
	if (id === "radiocelular") {
		$('#txtCorreoCarta').removeClass("txtSombraAZul");
		//$('#divCont').addClass('marger');
		$('#divCorreo').addClass('show1');
		$('#divCelu').removeClass('show1');
		$("#txtCorreoCarta2").val('');
		$("#txtCorreoCarta").val('');
		$('#txtCorreoCarta').addClass("txtSombraRojo");
	}
	$('#btnCancelarMensaje').prop("disabled", false);
	$('#btnCancelarCorreo').prop("disabled", false);
	$('#btnEnvioMensaje').prop("disabled", true);
	$('#btnEnvioCorreo').prop("disabled", true);
}

function verificarTelefono() {
	var numero = $("#txtCelular").val();
	var numeroLada = numero.substring(0, 6);
	var resul;
	$.ajax(
		{
			data: { opcion: 303, cel: numero, celLada: numeroLada },
			success: function (data) {
				console.log(data);
				if (data.irespuesta === 1) {
					resul = 1;
				} else {
					resul = 0;
				}
			}
		});

	return resul;
}

function ValidarDominio(cEmail) {

	var bRegresa = false;

	sDominio = cEmail;

	//("sDominio: " + sDominio);

	if ((sDominio == 'e-correo.com.mx') || (sDominio == 'ecorreo.com.mx') || (sDominio == 'e-correo.com') || (sDominio == 'ecorreo.com')) {
		bRegresa = false;
	}
	else {
		bRegresa = true;
	}

	return bRegresa;
}

function ValidarDominioUsados(cEmail) {

	//alert("entro en el metodo");
	var bRegresa = false;

	var sDominio = cEmail.toUpperCase();
	//alert("sDominio: " + sDominio);

	if ((sDominio == 'HOTMAIL.COM') || (sDominio == 'GMAIL.COM') || (sDominio == 'YAHOO.COM') || (sDominio == 'OUTLOOK.COM') || (sDominio == 'OUTLOOK.ES') || (sDominio == 'AFORECOPPEL.COM') || (sDominio == 'LIVE.COM')) {
		bRegresa = true;
	}
	else {
		bRegresa = false;
	}

	return bRegresa;
}

function catalogoDominioCorreo() {
	$("#cbxDominioCorreo").append("<option value='2'>Seleccione un dominio...</option><option value='1'>Otro</option><option value='3'>tres</option>");
}

function validaSiEsVacio() {
	validaSiEsOtro();
	cambiaSombraTxt("txtCorreoCarta");

}

function validaSiEsVacio2() {
	cambiaSombraTxt("txtCorreoCarta2");
	validaSiEsOtroConfir();
	//validaSiEsOtro();

}

function verificaCofetel() {
	var respue = verificarTelefono();
	$('#labelMensajeErrorEnvioCartaCel').html("");

	if (respue != 1) {
		//console.log("entre con 0");
		$('#labelMensajeErrorEnvioCartaCel').html("“Numero no es valido ante cofetel”");
		$('#txtCelular').removeClass("txtSombraAzul");
		$('#txtCelular').addClass("txtSombraRojo");
	} else {
		//console.log("entre con 1");
		$('#txtCelular').removeClass("txtSombraRojo");
		$('#txtCelular').addClass("txtSombraAzul");
	}
}

function compararNumero() {

	var numero = $("#txtCelular").val();
	var numero2 = $("#txtCelularConfir").val();

	$('#labelMensajeErrorEnvioCartaCelConfir').html("");
	$('#btnEnvioMensaje').prop("disabled", true);
	if (numero === '' || numero2 === '') {
		$('#txtCelularConfir').removeClass("txtSombraAZul");
		$('#txtCelularConfir').addClass("txtSombraRojo");
		return;
	}
	if (numero != numero2) {
		$('#txtCelularConfir').removeClass("txtSombraAZul");
		$('#labelMensajeErrorEnvioCartaCelConfir').html("“PROMOTOR: Número Celular no coincide, favor de	revisar los campos”");
		$('#txtCelularConfir').addClass("txtSombraRojo");
		$('#txtCelular').addClass("txtSombraRojo");
		$("#txtCelular").val('');
		$("#txtCelularConfir").val('');
	} else if (numero === numero2) {
		$('#txtCelularConfir').removeClass("txtSombraRojo");
		$('#txtCelularConfir').addClass("txtSombraAzul");
		$('#btnEnvioMensaje').prop("disabled", false);
	}
}

function enviarmensaje() {
	var numero = $("#txtCelular").val();
	var entrega = 1;
	var ipremotoservidor = obtenerIPServidor();
	insertarTipoEntregaCorreoTelefono(entrega, numero, ipremotoservidor);
	detonapythonenviocorreo(2, numero);
	cambiarURLMC();
}

function insertarTipoEntregaCorreoTelefono(entrega, correoTelefono, ipremotoservidor) {
	var tipoEntrega = entrega;
	var tipoCorreoTelefono = correoTelefono;
	var curptrabajado = $('#txtCurp').val();
	if (OSName == "Android") {
		iEmpleado = getQueryVariable('empleado');
	} else {
		iEmpleado = getQueryVariable("empleado");
	}
	var resultado = '';
	$.ajax(
		{
			data: { opcion: 507, tipoEntrega: tipoEntrega, cCurptrabajado: curptrabajado, tipoCorreoTelefono: tipoCorreoTelefono, empleado: iEmpleado },
			success: function (data) {
				console.log(data);
				if (data.irespuesta === 1) {
					resultado = 'exito';
				} else {
					resultado = 'no se realizo la consulta';
				}
				console.log(resultado);
			}
		});
}

function obtenerIPServidor() {
	var bResp = '';
	var arrIP = new Array();

	$.ajax
		({
			data: { opcion: 511 },
			success: function (data) {
				/*async: false,
				cache: false,
				url: 'php/constanciaafiliacion.php',
				type: 'POST',
				dataType: 'json',
				data:{opcion:511},
				success: function(data)*/
				arrIP = eval(data);
				bResp = arrIP.IP;
			}
		});

	return bResp;
}

$('#txtCorreoCarta2').keydown(function () {
	//console.log('sisooyama')
	if (correo === correo2 && dominio === dominio2 && correo != '' && correo2 != '') {
		$('#txtCorreoCarta').prop("disabled", true);
		$('#txtCorreoCarta2').prop("disabled", true);
		$('#cbxDominioCorreo').prop("disabled", true);
		//console.log('aqui2');
		$('#btnEnvioCorreo').prop("disabled", false);
	} else {
		$('#labelMensajeErrorEnvioCartaConfir').html("“Promotor: correo electrónico no coincide,favor de revisar los campos.”");
		$('#txtCorreoCarta').prop("disabled", false);
		$('#txtCorreoCarta2').prop("disabled", false);
		$('#cbxDominioCorreo').prop("disabled", false);
		$("#txtCorreoCarta2").val('');
		$("#dominio2").val('');
		$('#btnEnvioCorreo').prop("disabled", true);
		$('#cbxDominioCorreo').prop("disabled", false);
	}
});

function verificarcorreos() {
	$('#labelMensajeErrorEnvioCartaConfir').html("");
	var verifica = $("#cbxDominioCorreo :selected").text();
	var verifica2 = $("#cbxDominioCorreo2 :selected").text();
	if (verifica === 'otros' && verifica2 === 'otros') {
		var correo = $("#txtCorreoCarta").val();
		var correo2 = $("#txtCorreoCarta2").val();
		var dominio = $("#dominio").val();
		var dominio2 = $("#dominio2").val();
	} else {
		var correo = $("#txtCorreoCarta").val();
		var correo2 = $("#txtCorreoCarta2").val();
		var dominio = $("#cbxDominioCorreo :selected").text();
		var dominio2 = $("#cbxDominioCorreo2 :selected").text();

	}
	if (correo === correo2 && dominio === dominio2 && correo != '' && correo2 != '') {
		$('#txtCorreoCarta').prop("disabled", true);
		$('#txtCorreoCarta2').prop("disabled", true);
		$('#cbxDominioCorreo').prop("disabled", true);
		//console.log('aqui2');
		$('#btnEnvioCorreo').prop("disabled", false);
		$('#txtCorreoCarta2').removeClass("txtSombraRojo");
		$('#txtCorreoCarta2').addClass("txtSombraAzul");
	} else {
		$('#labelMensajeErrorEnvioCartaConfir').html("“Promotor: correo electrónico no coincide,favor de revisar los campos.”");
		$('#txtCorreoCarta').prop("disabled", false);
		$('#txtCorreoCarta2').prop("disabled", false);
		$('#cbxDominioCorreo').prop("disabled", false);
		$("#txtCorreoCarta2").val('');
		$("#dominio2").val('');
		$('#btnEnvioCorreo').prop("disabled", true);
		$('#cbxDominioCorreo').prop("disabled", false);
		$('#txtCorreoCarta2').removeClass("txtSombraAzul");
		$('#txtCorreoCarta2').addClass("txtSombraRojo");
	}
}

function enviaCorreo() {
	var verifica = $("#cbxDominioCorreo :selected").text();
	if (verifica === 'otros') {
		var correo = $("#txtCorreoCarta").val() + '@' + $("#dominio").val();
	} else {
		var correo = $("#txtCorreoCarta").val() + '@' + $("#cbxDominioCorreo :selected").text();
	}
	//Falta mandar la CURP	
	var entrega = 2;
	var ipremotoservidor = obtenerIPServidor();
	insertarTipoEntregaCorreoTelefono(entrega, correo, ipremotoservidor);
	detonapythonenviocorreo(1, correo);
	cambiarURLMC();
}

function detonapythonenviocorreo(opcionejecuta, tipoEnvio) {
	var parametroadicional = tipoEnvio;
	var curp = '';
	var cNombreCompleto = $('#txtNombre').val() + ' ' + $('#txtApellidoPaterno').val() + ' ' + $('#txtApellidoMatero').val();
	var iClaveConsar = sClaveConsar;
	var cadenaDatos = '';
	curp = $('#txtCurp').val();
	cadenaDatos = 'opcionejecuta=' + opcionejecuta + '&curp=' + curp + '&parametroadicional=' + tipoEnvio + '&cNombreCompleto=' + cNombreCompleto + '&iClaveConsar=' + iClaveConsar;
	$.ajax({
		data: cadenaDatos,
		url: 'php/detonarpyton.php',
		success: function (data) {
		}
	});
}

function cambiaSombraTxt(elemento) {
	if (document.getElementById(elemento).classList.contains("txtSombraAzul")) {

		switch (elemento) {
			case "txtCorreoCarta":
				if (document.getElementById(elemento).value == "") {
					document.getElementById(elemento).classList.remove("txtSombraAzul");
					document.getElementById(elemento).classList.add("txtSombraRojo");
				} else {
					var band = 0;
					var correo = document.getElementById("txtCorreoCarta").value;
					var validacionesCorreo = ['ValidarCaracteres', 'ValidarPrimerCaracter', 'ValidarUltimoCaracter', 'ValidarPuntosSeguidos'];
					for (var i = 0; i < validacionesCorreo.length; i++) {
						if (window[validacionesCorreo[i]](correo) == false) {
							band = 1;
							break;
						}
					}
					if (band == 1) {
						document.getElementById(elemento).classList.remove("txtSombraAzul");
						document.getElementById(elemento).classList.add("txtSombraRojo");
					}
				}
				break;
			case "txtCorreoCarta2":
				if (document.getElementById(elemento).value == "") {
					document.getElementById(elemento).classList.remove("txtSombraAzul");
					document.getElementById(elemento).classList.add("txtSombraRojo");
				} else {
					var band = 0;
					var correo = document.getElementById("txtCorreoCarta").value;
					var validacionesCorreo = ['ValidarCaracteres', 'ValidarPrimerCaracter', 'ValidarUltimoCaracter', 'ValidarPuntosSeguidos'];
					for (var i = 0; i < validacionesCorreo.length; i++) {
						if (window[validacionesCorreo[i]](correo) == false) {
							band = 1;
							break;
						}
					}
					if (band == 1) {
						document.getElementById(elemento).classList.remove("txtSombraAzul");
						document.getElementById(elemento).classList.add("txtSombraRojo");
					}
				}
				break;
		}

	} else if (document.getElementById(elemento).classList.contains("txtSombraRojo")) {
		switch (elemento) {
			case "txtCorreoCarta":
				if (document.getElementById(elemento).value != "") {
					var band = 0;
					var correo = document.getElementById("txtCorreoCarta").value;
					var validacionesCorreo = ['ValidarCaracteres', 'ValidarPrimerCaracter', 'ValidarUltimoCaracter', 'ValidarPuntosSeguidos'];
					for (var i = 0; i < validacionesCorreo.length; i++) {
						if (window[validacionesCorreo[i]](correo) == false) {
							band = 1;
							break;
						}
					}
					if (band != 1) {
						document.getElementById(elemento).classList.remove("txtSombraRojo");
						document.getElementById(elemento).classList.add("txtSombraAzul");
					}
				}
				break;
			case "txtCorreoCarta2":
				if (document.getElementById(elemento).value != "") {
					var band = 0;
					var correo = document.getElementById("txtCorreoCarta").value;
					var validacionesCorreo = ['ValidarCaracteres', 'ValidarPrimerCaracter', 'ValidarUltimoCaracter', 'ValidarPuntosSeguidos'];
					for (var i = 0; i < validacionesCorreo.length; i++) {
						if (window[validacionesCorreo[i]](correo) == false) {
							band = 1;
							break;
						}
					}
					if (band != 1) {
						document.getElementById(elemento).classList.remove("txtSombraRojo");
						document.getElementById(elemento).classList.add("txtSombraAzul");
					}
				}
				break;
		}
	}
}

function ValidarCaracteres(cEmail) {

	var bRegresa = false;

	// convert to a string for performing string comparisons.
	cEmail += "";

	// Loop through length of string and test for any alpha numeric 
	// characters
	for (i = 0; i < cEmail.length; i++) {
		if ((cEmail.charAt(i) == "&") || (cEmail.charAt(i) == "'") || (cEmail.charAt(i) == ",") || (cEmail.charAt(i) == '"') || (cEmail.charAt(i) == ":") || (cEmail.charAt(i) == ";") ||
			(cEmail.charAt(i) == "!") || (cEmail.charAt(i) == "+") || (cEmail.charAt(i) == "=") || (cEmail.charAt(i) == "/") || (cEmail.charAt(i) == "\\") || (cEmail.charAt(i) == "(") ||
			(cEmail.charAt(i) == ")") || (cEmail.charAt(i) == "<") || (cEmail.charAt(i) == ">") || (cEmail.charAt(i) == " ") || (cEmail.charAt(i) == "@")) {
			bRegresa = false;
			break;
		}
		else {
			bRegresa = true;
		}
	}

	return bRegresa;
}

function ValidarPrimerCaracter(cEmail) {

	var bRegresa = false;
	var cInicial = cEmail.substring(0, 1);

	if ((cInicial == "@") || (cInicial == ".") || (cInicial == "-") || (cInicial == "_") || (cInicial == "%")) {
		bRegresa = false;
	}
	else {
		bRegresa = true;
	}

	return bRegresa;
}

function ValidarUltimoCaracter(cEmail) {

	var bRegresa = false;

	var cFinal = cEmail.slice(cEmail.length - 1);

	if ((cFinal == "@") || (cFinal == ".") || (cFinal == "-") || (cFinal == "_")) {
		bRegresa = false;
	}
	else {
		bRegresa = true;
	}

	return bRegresa;
}

function ValidarPuntosSeguidos(cEmail) {

	var bRegresa = false;
	var icont = 0;

	// convert to a string for performing string comparisons.
	cEmail += "";

	for (i = 0; i < cEmail.length; i++) {
		if (cEmail.charAt(i) == ".") {
			icont++;

			if (icont > 1) {
				bRegresa = false;
				break;
			}
		}
		else {
			icont = 0;
			bRegresa = true;
		}
	}

	return bRegresa;
}

function validaSiEsOtro() {
	$('#labelMensajeErrorEnvioCarta').html("");
	var valida = $("#cbxDominioCorreo :selected").text();
	var correo = $("#txtCorreoCarta").val()
	if (valida === 'otros') {
		$('#dominio').removeClass('show1');
		$('#cbxDominioCorreo').addClass('show1');
		var dominio = $("#dominio").val().toUpperCase();
		if (dominio > 40) {
			$('#labelMensajeErrorEnvioCarta').html("“El correo electrónico no cumple con estructura válida.”");
		}
		if (correo.length > 99) {
			$('#labelMensajeErrorEnvioCarta').html("“El correo electrónico no cumple con estructura válida.”");
		}

	} else {
		$('#dominio').addClass('show1');
		$('#cbxDominioCorreo').removeClass('show1');
		var dominio = $("#cbxDominioCorreo :selected").text().toUpperCase();
		validarCorreoDominio(dominio, correo);
	}
}

function validaSiEsOtroConfir() {
	$('#labelMensajeErrorEnvioCarta').html("");
	var valida = $("#cbxDominioCorreo2 :selected").text();
	var correo = $("#txtCorreoCarta2").val()
	if (valida === 'otros') {
		$('#dominio2').removeClass('show1');
		$('#cbxDominioCorreo2').addClass('show1');
		var dominio = $("#dominio2").val().toUpperCase();
		if (dominio > 40) {
			$('#labelMensajeErrorEnvioCarta').html("“El correo electrónico no cumple con estructura válida.”");
		}
		if (correo.length > 99) {
			$('#labelMensajeErrorEnvioCarta').html("“El correo electrónico no cumple con estructura válida.”");
		}
	} else {
		$('#dominio2').addClass('show1');
		$('#cbxDominioCorreo2').removeClass('show1');
		var dominio = $("#cbxDominioCorreo2 :selected").text().toUpperCase();
		validarCorreoDominio(dominio, correo);
	}
	let vicente = $("#cbxDominioCorreo2").val();
	if (vicente === '0') {
		return;
	} else {
		verificarcorreos();
	}
}

//Metodo que valida si se selecciono "otro" en el combo dominio
function validarCorreoDominio(dominio, correo) {
	$('#labelMensajeErrorEnvioCarta').html("");
	if (dominio === 'HOTMAIL.COM') {
		var primeraLetra = correo.substring(0, 1);
		console.log(primeraLetra);
		if ((primeraLetra == 1) || (primeraLetra == 2) || (primeraLetra == 3) || (primeraLetra == 4) || (primeraLetra == 5) ||
			(primeraLetra == 6) || (primeraLetra == 7) || (primeraLetra == 8) || (primeraLetra == 9) || (primeraLetra == 0) && correo.length > 64) {
			$('#labelMensajeErrorEnvioCarta').html("El correo electrónico no cumple con estructura válida.");

		}
	}
	if (dominio === 'GMAIL.COM') {
		if (correo.length > 30) {
			$('#labelMensajeErrorEnvioCarta').html("El correo electrónico no cumple con estructura válida.");
		}
	}
	if (dominio === 'YAHOO.COM') {
		if (correo.length > 30) {
			$('#labelMensajeErrorEnvioCarta').html("El correo electrónico no cumple con estructura válida.");

		}
	}
	if (dominio === 'OUTLOOK.COM') {
		if (correo.length > 64) {
			$('#labelMensajeErrorEnvioCarta').html("El correo electrónico no cumple con estructura válida.");

		}
	}
	if (dominio === 'OUTLOOK.COM') {
		if (correo.length > 64) {
			$('#labelMensajeErrorEnvioCarta').html("El correo electrónico no cumple con estructura válida.");

		}
	}
	if (dominio === 'OUTLOOK.ES') {
		if (correo.length > 64) {
			$('#labelMensajeErrorEnvioCarta').html("El correo electrónico no cumple con estructura válida.");

		}
	}
	if (dominio === 'COPPEL.COM') {
		if (correo.length > 30) {
			$('#labelMensajeErrorEnvioCarta').html("El correo electrónico no cumple con estructura válida.");

		}
	}
	if (dominio === 'AFORECOPPEL.COM') {
		if (correo.length > 30) {
			$('#labelMensajeErrorEnvioCarta').html("El correo electrónico no cumple con estructura válida.");

		}
	}
	if (dominio === 'BANCOPPEL.COM') {
		if (correo.length > 30) {
			$('#labelMensajeErrorEnvioCarta').html("El correo electrónico no cumple con estructura válida.");
		}
	}
}

/////////////////**********************************//////////////////////////// */
//Mensaje y Detonar una Funcion Creada
function ModalMensajeFuncion(cMensaje) {
	///var myDlg = new dialog_boot(document.getElementById('myModal'));
	//myDlg.mensajefuncion(cMensaje);
	$("#myModal").dialog
		({
			title: sTitle,
			autoOpen: true,
			resizable: false,
			width: '750',
			height: 'auto',
			modal: true,
			buttons: '',
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + cMensaje + "</p>");
			}
		});
}

function ModalErrorFct(cMensaje) {
	//var myDlg = new dialog_boot(document.getElementById('divMensajeErrorFct'));
	//myDlg.mensajeErrorFct(cMensaje);
	$("#divMensaje").dialog
		({
			title: sTitle,
			autoOpen: true,
			resizable: false,
			width: '700',
			height: 'auto',
			modal: true,
			buttons: '',
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + cMensaje + "</p>");
			}
		});
}

function ModalPdfFct(cMensaje) {
	//var myDlg = new dialog_boot(document.getElementById('divMensajeErrorFct'));
	//myDlg.mensajeErrorFct(cMensaje);
	$("#myModalPDF").dialog
		({
			title: sTitle,
			autoOpen: true,
			resizable: false,
			width: '1000',
			height: '1000',
			modal: true,
			buttons: {
				"Cerrar": function () {
					cambiarURL();
				}
			},
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + cMensaje + "</p>");
			},
			closeText: ''
		});
}

window.addEventListener('afterprint', (event) => {
	console.log('After print');
});

window.onafterprint = (event) => {
	console.log('After print');
	cambiarURL();
};

//Folio 891 --------------listo
function envioctdimg() {
	var firmafuente = 0;
	if (iFirmaSolicitada == 1 || iFirmaSolicitada == 2) {
		firmafuente = 1;
	}
	$.ajax(
		{
			data: {
				opcion: 92,
				//folioafiliacion:iFolioAfore,
				folioafiliacion: iFolioAfore,
				firma: firmafuente
			},
			success: function (data) {
				//alert(data.mensaje);
				console.log("Success envioctdimg" + data.mensaje);
			}
		});
}

//Folio 891 Funcion que manda a llamar una funcion para guardar datos en la tabla para generar el acuse de la carta de derechos --listo
function guardarDatosCartaDerechos() {
	//var folio = iFolioAfore;
	var folio = iFolioSol;
	var opcionacuse = 'HUELLA';
	if (iFirmaSolicitada == 1 || iFirmaSolicitada == 2) {
		opcionacuse = 'FIRMA';
	}
	var scurp = document.getElementById("txtCurp").value;
	var nombreCompleto = document.getElementById("txtNombre").value + ' ' + document.getElementById("txtApellidoPaterno").value + ' ' + document.getElementById("txtApellidoMatero").value

	$.ajax({
		data: {
			opcion: 93,
			curp: scurp,
			NombreCompleto: nombreCompleto,
			ifoliosol: folio,
			sOpcionAcuse: opcionacuse
		},
		success: function (data) {
			var resp = data;
			if (resp.estatus == 1) {
				console.log("Inserto");
			} else {
				console.log("No Inserto");
			}
		}
	});
}

function mensajesDeErrores(mensaje) {
	$(".content-loader").hide();
	$("#divMensaje").dialog({
		title: "",
		autoOpen: true,
		resizable: false,
		width: "350",
		height: "auto",
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position: { my: "center", at: "center", of: $("body"), within: $("body") }, //Posicionar el dialog en el centro
		open: function (event, ui) {
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>" + mensaje + "</p>");
		},
		buttons: {
			Aceptar: function () {
				$(this).dialog("close");
			}
		}
	});
}

function mostrarMensajeCierreSesion() {
	$(".content-loader").hide();
	$("#divMensaje").dialog({
		title: "",
		autoOpen: true,
		resizable: false,
		width: "350",
		height: "auto",
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position: { my: "center", at: "center", of: $("body"), within: $("body") }, //Posicionar el dialog en el centro
		open: function (event, ui) {
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p> PROMOTOR: La sesión expiró. Favor de iniciar sesión nuevamente </p>");
		},
		buttons: {
			Aceptar: function () {
				$(this).dialog("close");
				cerrarSesion();
			}
		}
	});
}

function cerrarSesion() {
	if (OSName == 'Android') {
		Android.cerrarSesion();
	} else {
		ligaMenuAfore();
	}
}

function ligaMenuAfore() {
	$.ajax({
		data: {
			opcion: 611
		},
		success: function (data) {
			bRetorna = data.respuesta;
			$.ajax({
				data: { opcion: 612, codigoEmpleado: codigoEmpleado },
				success: function (data) {
					location.href = bRetorna;
				}
			});
		}
	});
}

//FOLIO 539 Funcion que muestra el loader al momento de hacer el comsumo del WS
function esperaConsultaWSBancoppel(sCurpRenapo)
{//metodo para esperar los 5 seg y despues revisar respuesta de renapo
$('.content-loader').show();
	setTimeout(function (){
		 if($('#cboTipoConstancia').val() == 1)
		 {
			 $("#txtNss").attr("disabled", true);
		 }
		consultaWebServiceBancoppel(sCurpRenapo);
		
	},3000);
}

//FOLIO 539 Funcion que ejecuta el web service de bancoppel
function consultaWebServiceBancoppel(curp){
	//debugger;
	var llaveDatos = obtieneLlaveDatosBancoppel(curp);
	if(llaveDatos == null){
		$('.content-loader').hide();
		return;
	}
	var generoDatos = 0;

	try{
		if(llaveDatos.genero=='H'){
			generoDatos = 1;
		}else{
			generoDatos = 2;
		}
		
		$.ajax(
			{
				async: false,
				cache: false,
				data:{
					opcion:91,
					nombres:llaveDatos.nombres,
					appaterno:llaveDatos.apellidopaterno,
					apmaterno:llaveDatos.apellidomaterno,
					fechanac:llaveDatos.fechanacimiento,
					genero:generoDatos,
					entidad:llaveDatos.identidadfederativa
				},
				 url: 'php/constanciaafiliacion.php',
				 type: 'POST',
				 dataType: 'json',
				 success: function(data)
				{
					
					$('.content-loader').hide();
					var resp = data;
					if(resp == null)
					{
						banderabancoppel539 = true;
					   //mostrarMensajegenerico("No se encontraron registros con la informacion proporcionada,continuar con validacion manual","Servicios Bancoppel");
					}
					else
					{
						banderabancoppel539 = true;
						if(resp[0]==null){
							fecha=llaveDatos.fechanacimiento;
							var fechanacimiento = fecha.substr(0,fecha.indexOf(' '));
			
							var generoLlave="";
							if(llaveDatos.genero=='H'){
								generoLlave = 1;
							}else{
								generoLlave = 2;
							}
			
							if(llaveDatos.curp==resp.curp){
								if(llaveDatos.nombres==resp.nombres){
									if(llaveDatos.apellidopaterno==resp.apellidopaterno){
										if(llaveDatos.apellidomaterno==resp.apellidomaterno){
											if(fechanacimiento==resp.fechadenacimiento){
												if(llaveDatos.identidadfederativa==resp.entidaddenacimiento){
													if(generoLlave==resp.genero){
														$("#txTelCelular").val(resp.telefono1);
														$("#txtClienteCoppel").val(resp.nodeclientecoppel);
														$("#cboEstadoCivil").val(resp.estadocivil);
														$("#txtTelContacto").val(resp.telefono2);
													}else{
														
													}
												}
											}
										}
									}
								}
							}
		
						}else{
							//Llena la tabla de los cuentahabientes
							tablaCuentahabientesBancoppel(resp);
						}
					}
					//Valida que el Webservice haya respondido solo con 1 registro
					
				},
				error: function(a, b, c){
					mostrarMensajegenerico("Ocurrio un error al consultar los datos con Bancoppel","Servicios Bancoppel");
				},
				beforeSend: function(){}
			});
			$('.content-loader').hide();
	}catch(e){
		console.log(e);
		
		$('.content-loader').hide();
	}
	validarCNNS();
	
}

//Folio 539 Consulta en base de datos el folio de la solicitud a renapo para sacar la informacion para formar la llave de datos bancoppel
function obtieneLlaveDatosBancoppel(vCurp){
	var respuesta=null;
	$.ajax(
		{
			async: false,
			cache: false,
			data:{
				opcion: 89,
				curp:vCurp
			},
			 url: 'php/constanciaafiliacion.php',
			 type: 'POST',
			 dataType: 'json',
			 success: function(data)
			{
				if(data.estatus==1){
					respuesta=data.respuesta[0];
				}else{
					console.log("Error al obtener la llave de datos" + data.descripcion);
				}
			},
			error: function(a, b, c){
				alert("Error" + a + b + c);
			}
		});
	return respuesta;
}

//FOLIO 539 Modal para mostrar la captura de datos Bancoppel
function mostrarMensajeBancoppel(mensaje, sTitle)
{
  $('.content-loader').hide();
  $("#divBancoppelCapturaDatos").dialog
	({
		title: sTitle,
		autoOpen: true,
		resizable: false,
		width: '600',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "top",at: "top+5%",of: window,within: window},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			
			if (Array.isArray(mensaje))
			{
				var message = ""

				for (i = 0; i <= mensaje.length; i++)
				{
					if (mensaje[i] == undefined)
					{
						mensaje.splice(i, 1);
						continue;
					}

					message += mensaje[i] + "<br/> <br/>";
				}

				$(this).html("<p>"+message+"</p>");
			}
			else
			{
				$(this).html("<p>"+mensaje+"</p>");
			}
		},

		//Folio 539 boton aceptar para determinar los datos correspondinetes
		buttons: [{
			id:"btnaceptardtos",
			text: "Aceptar",
			click: function() {
				$(this).dialog("close");
				 setTimeout(ventanaEmergente(sCurpRenapo), 3000)
				bandCapturaDatos = 1;
			}
		}
		]
	});
	//Folio 539 atributo para desactivar el boton aceptar
	$("#btnaceptardtos").attr('disabled','disabled');
	  $( "#txtNombrebancoppel,#txtApellidoPbancoppel,#txtApellidoMbancoppel,#txtFechaNbancoppel,#inputgenerobancoppel,#inputentnacimientobancoppel" ).change(function() {
		revisardatos();
	  });

	  //Folio 539 Datepicker para la captura de datos bancoppel
		$("#txtFechaNbancoppel").datepicker({ 
			dateFormat: 'dd-mm-yy',
			changeYear: true,
			showButtonPanel: true,
			yearRange: '1900:2020',
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá']
		  });

		  $('#txtNombrebancoppel').on('input', function () { 
			this.value = this.value.replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ ]/g,'');
		  });

		  $('#txtApellidoPbancoppel').on('input', function () { 
			this.value = this.value.replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ ]/g,'');
		  });

		  $('#txtApellidoMbancoppel').on('input', function () { 
			this.value = this.value.replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ ]/g,'');
		  });

	//Folio 539 quitar el aviso de llenado
	$('#avisoLlenadobancoppel').css('display');
}

//Folio 539 Funcion que muestra la pantalla para la captura de datos para la llave bancoppel
function mostrarCapturaDatosBancoppel(){
	var entidades = obtenerEntidadNacimiento();
	var long = entidades.length;
	var opciones='';
	
	for(var i=0;i<long;i++){
		opciones=opciones+'<option value="'+entidades[i].identidadnacimiento+'">'+entidades[i].descripcion+'</option>';
	}

	var mensaje='<section id="seccionOpciones"><form><div class="form-row"><label for="inputState">Llene correctamente los campos.</label><div class="form-group"><div class="form-group"><label for="inputState">Nombre(s):</label><input type="text" id="txtNombrebancoppel" placeholder="*Obligatorio" class="form-control"><br><label for="inputState">Apellido Paterno:</label><input type="text" id="txtApellidoPbancoppel" placeholder="*Obligatorio" class="form-control"><br><label for="inputState">Apellido Materno:</label><input type="text" id="txtApellidoMbancoppel" placeholder="*Obligatorio" class="form-control"><br><label for="inputState">Fecha de Nacimiento:</label><input type="date" id="txtFechaNbancoppel" class="form-control"><br><label for="inputState">Genero:</label><select id="inputgenerobancoppel" class="form-control"><option selected>...</option><option>Hombre</option><option>Mujer</option></select><br><label for="inputState">Entidad de Nacimiento:</label><select id="inputentnacimientobancoppel" class="form-control"><option selected>...</option>'+opciones+'</select><br><div class="form-row text-center" id="avisoLlenadobancoppel" style="color:red;">Favor de llenar los campos para continuar.<div class="form-group">';
	
	mostrarMensajeBancoppel(mensaje,"Captura de datos Bancoppel");
}

//Folio 539 Modal para mostrar la tabla con los cuentahabientes que responde el webService de Bancoppel
function mostrarTabla(mensaje, sTitle, cuentahabientesBcpl)
{


  //Metodo llamado 1 es igual a Captura de datos y el 2 es igual al flujo normal
  $('.content-loader').hide();
  $("#divTablaClientesBcpl").dialog
	({
		title: sTitle,
		autoOpen: true,
		resizable: false,
		width: '890',
		height: '650',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "top",at: "top+5%",of: window,within: window},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			
			if (Array.isArray(mensaje))
			{
				var message = ""

				for (i = 0; i <= mensaje.length; i++)
				{
					if (mensaje[i] == undefined)
					{
						mensaje.splice(i, 1);
						continue;
					}

					message += mensaje[i] + "<br/> <br/>";
				}

				$(this).html("<p>"+message+"</p>");
			}
			else
			{
				$(this).html("<p>"+mensaje+"</p>");
			}
		},
		buttons:
		[{
			id:"btnAceptarBcpl",
			text: "Aceptar",
			click: function() {
				if(bandCapturaDatos==0 || bandCapturaDatos == 1){
					var idRadio=null;
					if($('input:radio[name=radio]:checked').attr('id')==null){
						$('#errorRadio').css('display', 'block');
						//document.getElementById('errorRadio').scrollIntoView();
						var myElement = document.getElementById('errorRadio');
						var topPos = myElement.offsetTop;
						document.getElementById('divTablaClientesBcpl').scrollTop = topPos;
					}else{
						idRadio = $('input:radio[name=radio]:checked').attr('id');
						var estadoCivil = 176;
						switch(cuentahabientesBcpl[idRadio]['ESTADO_CIVIL']){
							case 'S': estadoCivil = 169;
							break;
							case 'C': estadoCivil = 170;
							break;
							case 'V': estadoCivil = 171;
							break;
							case 'D': estadoCivil = 172;
							break;
							case 'U': estadoCivil = 173;
							break;
							case 'S': estadoCivil = 174;
							break;
							case 'N': estadoCivil = 175;
							break;
							default: estadoCivil = 176;
							break;
						}

						if(bBanderaRenapo == true)
						{
							
						}
						else
						{
							$("#txtCurp").val(cuentahabientesBcpl[0]['CURP']);
							$("#txtApellidoPaterno").val(cuentahabientesBcpl[0]['APELL_PATERNO']);
							$("#txtApellidoMatero").val(cuentahabientesBcpl[0]['APELL_MATERNO']);
							$("#txtNombre").val(cuentahabientesBcpl[0]['NOMBRES']);
						}
						
						
						if(JSON.stringify(cuentahabientesBcpl[idRadio]['TELEFONO_MOVIL']) != '{}')
						{
							$("#txTelCelular").val(cuentahabientesBcpl[idRadio]['TELEFONO_MOVIL']);
							
						}
						if(JSON.stringify(cuentahabientesBcpl[idRadio]['NUMERO_CTE_COPPEL']) != '{}')
						{
							
							$("#txtClienteCoppel").val(cuentahabientesBcpl[idRadio]['NUMERO_CTE_COPPEL']);
						}
					
						
						$("#cboEstadoCivil").val(estadoCivil);
						if(JSON.stringify(cuentahabientesBcpl[idRadio]['TELEFONO_FIJO']) != '{}')
						{
							$("#txtTelContacto").val(cuentahabientesBcpl[idRadio]['TELEFONO_FIJO']);
						}
						desboqueoCajasdeTexto();
						
						if($('#cboTipoConstancia').val() == 1)
						{
							$("#txtNss").attr("disabled", true);
						}
						

						//$("#cboTipoComprobante").val('5');
						//$("#dtpFechaComprobante").val($.datepicker.formatDate('yy/mm/dd', new Date()));

						cuentaHabienteBancoppel = cuentahabientesBcpl[idRadio];

						$(this).dialog( "close" );
					}
				}else{
					if($('input:radio[name=radio]:checked').attr('id')==null){
						$('#errorRadio').css('display', 'block');
						//document.getElementById('errorRadio').scrollIntoView();
						var myElement = document.getElementById('errorRadio');
						var topPos = myElement.offsetTop;
						document.getElementById('divTablaClientesBcpl').scrollTop = topPos;
					}else{
						idRadio = $('input:radio[name=radio]:checked').attr('id');
						curpCapturaDatos = cuentahabientesBcpl[idRadio]['CURP'];
						preguntaIne();
						
						$(this).dialog( "close" );
					}
				}

				idRadio = $('input:radio[name=radio]:checked').attr('id');
				Gcuentahabiente = cuentahabientesBcpl[idRadio];
				
				$("#txTelCelular").attr("disabled", true);
						$("#cboCompTelef").attr("disabled", true);
						$("#cboTipoComprobante").attr("disabled", true);
						$("#cboEstadoCivil").attr("disabled", true);
						$("#cboTipoTelefono").attr("disabled", true);
						$("#txtTelContacto").attr("disabled", true);
						$("#cboCompTelefCont").attr("disabled", true);
						$("#txtClienteCoppel").attr("disabled", true);
						
				
			}
		},
			{
				id:"btnCancelarBcpl",
				text: "Cancelar",
				click: function() {
					modalCancelarBcpl("¿Deseas cancelar la compartición de datos de BanCoppel?","Aviso");
				}
			}
		]	
	});
	$("#btnAceptarBcpl").attr('disabled','disabled');

	var long = cuentahabientesBcpl.length;
	var idsRadio ="";
	
	for(var i=0;i<long;i++){
		if(i==0){
			idsRadio="#"+i;
		}else{
			idsRadio=idsRadio+","+"#"+i;
		}
	}

	$(idsRadio).change(function() {
		$('#errorRadio').css('display', 'none');
		$("#btnAceptarBcpl").removeAttr('disabled');
	  });
}

//Folio 539 tabla cuando WebService de Bancoppel responde con mas de 1 cuentahabientes
function tablaCuentahabientesBancoppel(cuentahabientesBcpl){
	
	
	
	retornoarr = cuentahabientesBcpl;
	var long = cuentahabientesBcpl.length;
	var tablahtml='<span id="errorRadio" style="color:red; font-size:14px;">Favor de seleccionar un cuentahabiente.</span>';
	
	for(var i=0; i<long; i++){
		
		var appaterno = cuentahabientesBcpl[i]["APELL_PATERNO"];
		var apmaterno = cuentahabientesBcpl[i]["APELL_MATERNO"];
		var nombres = cuentahabientesBcpl[i]["NOMBRES"];
		var fechanacimiento = cuentahabientesBcpl[i]["FECHA_NAC"];
		var genero = cuentahabientesBcpl[i]["GENERO"];
		var entidad = cuentahabientesBcpl[i]["ENTIDAD_NAC"];
		var curp = cuentahabientesBcpl[i]["CURP"];
		var imagen = cuentahabientesBcpl[i]["ID_ANVERSO"];
		imagen = JSON.stringify(imagen);
		
		//if(imagen !="{}"){
		//	tablahtml=tablahtml+'<div class="container" style="height: 300px; border-radius:5px; border:1px solid #4297d7; margin-bottom:10px; padding: 10px; display: flex; flex-wrap: wrap; width: 830px;"><div style="width: 100%; margin-bottom:-20px;"><input type="radio" id="'+i+'" name="radio" style="margin-right:10px;"><label for="">Cuentahabiente encontrado</label></div><div style="display: flex;height: 220px; width: 58%; margin-right: 2%; padding: 5px; padding-left: 20px;"><div style="display:inline;"><label style="display: block; margin-bottom: 10px;">Nombre(s)</label><label style="display: block; margin-bottom: 10px;">Apellido Paterno</label><label style="display: block; margin-bottom: 10px;">Apellido Materno</label><label style="display: block; margin-bottom: 10px;">Fecha de Nacimiento</label><label style="display: block; margin-bottom: 10px;">Genero</label><label style="display: block; margin-bottom: 10px;">Entidad de Nacimiento</label><label style="display: block;">CURP</label></div><div style="display: inline; padding-left: 15px;"><input id="txtNombresBcplWS'+i+'" value="'+nombres+'" style="display: block; margin-bottom: 5px; width: 230px;" disabled><input id="txtApellidoPaternoBcplWS'+i+'" value="'+appaterno+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled><input id="txtApellidoMaternoBcplWS'+i+'" value="'+apmaterno+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled><input id="txtFechaNacimientoBcplWS'+i+'" value="'+fechanacimiento+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled><input id="txtGeneroBcplWS'+i+'" value="'+genero+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled><input id="txtEntidadNacimientoBcplWS'+i+'" value="'+entidad+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled><input id="txtCurpBcplWS'+i+'" value="'+curp+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled></div></div><div style="height: 200px; width: 40%;"><img style="display:block;height: 185px;width: 310px;" id="base64image" src="data:image/jpeg;base64,'+imagen+'"/></div></div>';
		if(imagen !="{}"){
			tablahtml=tablahtml+'<div class="container" style="height: 300px; border-radius:5px; border:1px solid #4297d7; margin-bottom:10px; padding: 10px; display: flex; flex-wrap: wrap; width: 770px;"><div style="width: 100%; margin-bottom:-20px;"><input type="radio" id="'+i+'" name="radio" style="margin-right:10px;"><label for="">Cuentahabiente encontrado</label></div><div style="display: flex;height: 220px; margin-right: 2%; padding: 5px; padding-left: 20px;"><div style="display:inline;"><label style="display: block; margin-bottom: 10px;">Nombre(s)</label><label style="display: block; margin-bottom: 10px;">Apellido Paterno</label><label style="display: block; margin-bottom: 12px;">Apellido Materno</label><label style="display: block; margin-bottom: 12px;">Fecha de Nacimiento</label><label style="display: block; margin-bottom: 12px;">Genero</label><label style="display: block; margin-bottom: 12px;">Entidad de Nacimiento</label><label style="display: block;">CURP</label></div><div style="display: inline; padding-left: 15px;"><input id="txtNombresBcplWS'+i+'" value="'+nombres+'" style="display: block; margin-bottom: 5px; width: 500px;" disabled><input id="txtApellidoPaternoBcplWS'+i+'" value="'+appaterno+'" style="display: block; margin-bottom: 7px; width: 100%;" disabled><input id="txtApellidoMaternoBcplWS'+i+'" value="'+apmaterno+'" style="display: block; margin-bottom: 7px; width: 100%;" disabled><input id="txtFechaNacimientoBcplWS'+i+'" value="'+fechanacimiento+'" style="display: block; margin-bottom: 7px; width: 100%;" disabled><input id="txtGeneroBcplWS'+i+'" value="'+genero+'" style="display: block; margin-bottom: 7px; width: 100%;" disabled><input id="txtEntidadNacimientoBcplWS'+i+'" value="'+entidad+'" style="display: block; margin-bottom: 7px; width: 100%;" disabled><input id="txtCurpBcplWS'+i+'" value="'+curp+'" style="display: block; margin-bottom: 7px; width: 100%;" disabled></div></div></div>';
		}else{
			tablahtml=tablahtml+'<div class="container" style="height: 300px; border-radius:5px; border:1px solid #4297d7; margin-bottom:10px; padding: 10px; display: flex; flex-wrap: wrap; width: 830px;"><div style="width: 100%; margin-bottom:-20px;"><input type="radio" id="'+i+'" name="radio" id="" style="margin-right:10px;"><label for="">Cuentahabiente encontrado</label></div><div style="display: flex;height: 220px; width: 58%; margin-right: 2%; padding: 5px; padding-left: 20px;"><div style="display:inline;"><label style="display: block; margin-bottom: 10px;">Nombre(s)</label><label style="display: block; margin-bottom: 10px;">Apellido Paterno</label><label style="display: block; margin-bottom: 10px;">Apellido Materno</label><label style="display: block; margin-bottom: 10px;">Fecha de Nacimiento</label><label style="display: block; margin-bottom: 10px;">Genero</label><label style="display: block; margin-bottom: 10px;">Entidad de Nacimiento</label><label style="display: block;">CURP</label></div><div style="display: inline; padding-left: 15px;"><input id="txtNombresBcplWS'+i+'" value="'+nombres+'" style="display: block; margin-bottom: 5px; width: 230px;" disabled><input id="txtApellidoPaternoBcplWS'+i+'" value="'+appaterno+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled><input id="txtApellidoMaternoBcplWS'+i+'" value="'+apmaterno+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled><input id="txtFechaNacimientoBcplWS'+i+'" value="'+fechanacimiento+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled><input id="txtGeneroBcplWS'+i+'" value="'+genero+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled><input id="txtEntidadNacimientoBcplWS'+i+'" value="'+entidad+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled><input id="txtCurpBcplWS'+i+'" value="'+curp+'" style="display: block; margin-bottom: 5px; width: 100%;" disabled></div></div><div style="height: 200px; width: 40%;"><img height="185px" width="310" src="imagen/ine3.png"></div></div>';
		}
		
	}

		if(long == 1)
	{
		var estadoCivil = 176;
		switch(cuentahabientesBcpl[0]['ESTADO_CIVIL']){
			case 'S': estadoCivil = 169;
			break;
			case 'C': estadoCivil = 170;
			break;
			case 'V': estadoCivil = 171;
			break;
			case 'D': estadoCivil = 172;
			break;
			case 'U': estadoCivil = 173;
			break;
			case 'S': estadoCivil = 174;
			break;
			case 'N': estadoCivil = 175;
			break;
			default: estadoCivil = 176;
			break;
		}
		
		if(bBanderaRenapo == true)
		{
			
		}
		else
		{
			$("#txtCurp").val(cuentahabientesBcpl[0]['CURP']);
			$("#txtApellidoPaterno").val(cuentahabientesBcpl[0]['APELL_PATERNO']);
			$("#txtApellidoMatero").val(cuentahabientesBcpl[0]['APELL_MATERNO']);
			$("#txtNombre").val(cuentahabientesBcpl[0]['NOMBRES']);
		}

	
		
		
		if(JSON.stringify(cuentahabientesBcpl[0]['TELEFONO_MOVIL']) != '{}')
		{
			$("#txTelCelular").val(cuentahabientesBcpl[0]['TELEFONO_MOVIL']);
			
		}
		if(JSON.stringify(cuentahabientesBcpl[0]['NUMERO_CTE_COPPEL']) != '{}')
		{
			
			$("#txtClienteCoppel").val(cuentahabientesBcpl[0]['NUMERO_CTE_COPPEL']);
		}
	
		
		$("#cboEstadoCivil").val(estadoCivil);
		if(JSON.stringify(cuentahabientesBcpl[0]['TELEFONO_FIJO']) != '{}')
		{
			$("#txtTelContacto").val(cuentahabientesBcpl[0]['TELEFONO_FIJO']);
		}
		
		
		Gcuentahabiente = cuentahabientesBcpl[0];
	}
	else
	{
		mostrarTabla(tablahtml,"Cuentahabientes encontrados",cuentahabientesBcpl);
	}
}

//Folio 539 deshabilitar boton aceptar de Ventana Emergente
function revisardatos(){
		
	if($("#txtNombrebancoppel").val() != '' && $("#txtApellidoPbancoppel").val() != '' && $("#txtFechaNbancoppel").val() != '' && $("#txtApellidoMbancoppel").val() != '' )
	{
		
		if($("#inputgenerobancoppel option:selected").val() != '...' && $("#inputentnacimientobancoppel option:selected").val() != '...')
		{
			$("#btnaceptardtos").removeAttr('disabled');
			document.getElementById("avisoLlenadobancoppel").style.display = "none";
			//$("#avisoLlenadobancoppel").val(value='');
		}
		else
		{
			//Mantiene habilitado el boton por si no se cumple lo anterior
			$("#btnaceptardtos").attr('disabled','disabled');
			//Mantiene habilitado el texto de aviso por si no se cumple lo anterior
			document.getElementById("avisoLlenadobancoppel").style.display = "block";
		}
	}
	else{
		$("#btnaceptardtos").attr('disabled','disabled');
		document.getElementById("avisoLlenadobancoppel").style.display = "block";
	}
  
}

//Folio 539 Muestra modal de cancelar tabla cuentahabientes bancoppel
function modalCancelarBcpl(mensaje, sTitle)
{
  $('.content-loader').hide();
  $("#divMensajeCancelar").dialog
	({
		title: sTitle,
		autoOpen: true,
		resizable: false,
		width: '320',
		height: '180',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "top",at: "top+20%",of: window,within: window},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			
			if (Array.isArray(mensaje))
			{
				var message = ""

				for (i = 0; i <= mensaje.length; i++)
				{
					if (mensaje[i] == undefined)
					{
						mensaje.splice(i, 1);
						continue;
					}

					message += mensaje[i] + "<br/> <br/>";
				}

				$(this).html("<p>"+message+"</p>");
			}
			else
			{
				$(this).html("<p>"+mensaje+"</p>");
			}
		},
		buttons:
		[{
			id:"btnSiBcpl",
			text: "SI",
			click: function() {
					$('#divTablaClientesBcpl').dialog('close')
					$(this).dialog( "close" );
					if($("#cboTipoConstancia").val()==26){
						$("#btnConsultaCurp").removeAttr('disabled');
					}else{
						$("#btnConsultaCurp").attr('disabled','disabled');
						preguntaIne();
					}
					
				}
		},
			{
				id:"btnNoBcpl",
				text: "NO",
				click: function() {
					$(this).dialog("close");
				}
			}
		]	
	});
}

//FOLIO 539 Funcion que ejecuta el web service de bancoppel para traer datos al llenado
function ventanaEmergente(){
	//debugger;
	var nombresVentana = document.getElementById("txtNombrebancoppel").value;
	var paternoVentana = document.getElementById("txtApellidoPbancoppel").value;
	var maternoVentana = document.getElementById("txtApellidoMbancoppel").value;
	var nacimientoVentana = document.getElementById("txtFechaNbancoppel").value;
	var generoVentana = document.getElementById("inputgenerobancoppel").value;
	var entNacimientoVentana = document.getElementById("inputentnacimientobancoppel").value;
	if(generoVentana == 'Hombre'){
		generoVentana = 1;
	}else{
		generoVentana = 2;
	}
	$.ajax(
		{
			async: false,
			cache: false,
			data:{
				opcion:91,
				nombres:nombresVentana,
				appaterno:paternoVentana,
				apmaterno:maternoVentana,
				fechanac:nacimientoVentana,
				genero:generoVentana,
				entidad:entNacimientoVentana
			},
			 url: 'php/constanciaafiliacion.php',
			 type: 'POST',
			 dataType: 'json',
			 success: function(data)
			 {
				// debugger;
				 respuesta=data;
				 if(respuesta == null)
				 {
					//mostrarMensajegenerico("No se encontraron registros con la informacion proporcionada,continuar con validacion manual","Servicios Bancoppel");
					banderabancoppel539 = true;
				 }
				 else
				 {
					banderabancoppel539 = true;
					tablaCuentahabientesBancoppel(data);
				 }

			 },
			 error: function(a, b, c){
				 console.log("Error" + a + b + c);
			 }
		 });
		 
		
}

//FOlIO 539 Guarda los datos obtenidos en el WS de bancoppel en la tabla bitacorarespuestabancoppel
function guardaBitacoraRespuestaBancoppel(opcionbcpl,cuentaHabienteBancoppel){
	var obtienecurp = cuentaHabienteBancoppel["CURP"];
	if(obtienecurp == '')
	{
		obtienecurp = $("#txtCurp").val();
	}
	iFolioSol = obtenerRecuperacionFolioConstancia(obtienecurp);
	var dfechaconsulta = '';
	var dhoraconsulta = '';
	 
	var dtienda = 1;
	var dempleado = getQueryVariable("empleado");
	var didanverso = JSON.stringify(cuentaHabienteBancoppel["ID_ANVERSO"]);
	
	if(opcionbcpl == 1 && didanverso != "")
	{
		didanverso = didanverso.split(',');
		didanverso = didanverso[1];
	}
	var didreverso = cuentaHabienteBancoppel["ID_REVERSO"];
	var dcurp = cuentaHabienteBancoppel["CURP"];
	var dappaterno = cuentaHabienteBancoppel["APELL_PATERNO"];
	var dapmaterno = cuentaHabienteBancoppel["APELL_MATERNO"];
	var dnombres = cuentaHabienteBancoppel["NOMBRES"];
	var drfc = cuentaHabienteBancoppel["RFC"];
	var dfechanac = cuentaHabienteBancoppel["FECHA_NAC"];
	var dentidad = cuentaHabienteBancoppel["ENTIDAD_NAC"];
	var dgenero = cuentaHabienteBancoppel["GENERO"];
	var dnacionalidad = cuentaHabienteBancoppel["NACIONALIDAD"];
	var dedoCivil = cuentaHabienteBancoppel["ESTADO_CIVIL"];
	var dnumClienteCoppel = cuentaHabienteBancoppel["NUMERO_CTE_COPPEL"];
	var dnumClienteBancoppel = cuentaHabienteBancoppel["NUMERO_CTE_BANCO"];
	var dnvlEstudio = cuentaHabienteBancoppel["ESCOLARIDAD"];
	var dprofesion = cuentaHabienteBancoppel["OCUPACION_PROF"];
	var dgiroNegocio = cuentaHabienteBancoppel["ACTIVIDAD"];
	var dtelefono1 = cuentaHabienteBancoppel["TELEFONO_MOVIL"];
	var dtelefono2 = cuentaHabienteBancoppel["TELEFONO_FIJO"];
	var dcorreo = cuentaHabienteBancoppel["EMAIL"];
	var dcalle = cuentaHabienteBancoppel["CALLE"];
	var dnumExterior = cuentaHabienteBancoppel["NUM_EXT"];
	var dnumInterior = cuentaHabienteBancoppel["NUM_INT"];
	var dcodigoPostal = cuentaHabienteBancoppel["CP"];
	var dcolonia = cuentaHabienteBancoppel["COLONIA"];
	var dciudad = cuentaHabienteBancoppel["CIUDAD"];
	var ddelegacion = cuentaHabienteBancoppel["DELEGACION_MUN"];
	var destado = cuentaHabienteBancoppel["ESTADO"];
	var dpais = cuentaHabienteBancoppel["PAIS"];
	var dcalleCobranza = cuentaHabienteBancoppel["CALLE1"];
	var dnumExteriorCobranza = cuentaHabienteBancoppel["NUM_EXT1"];
	var dnumInteriorCobranza = cuentaHabienteBancoppel["NUM_INT1"];
	var dcodigoPostalCobranza = cuentaHabienteBancoppel["CP1"];
	var dcoloniaCobranza = cuentaHabienteBancoppel["COLONIA1"];
	var dciudadCobranza = cuentaHabienteBancoppel["CIUDAD1"];
	var ddelegacionCobranza = cuentaHabienteBancoppel["DELEGACION_MUN1"];
	var destadoCobranza = cuentaHabienteBancoppel["ESTADO1"];
	var dpaisCobranza = cuentaHabienteBancoppel["PAIS1"];
	var dcalleLaboral = cuentaHabienteBancoppel["CALLE2"];
	var dnumExteriorLaboral = cuentaHabienteBancoppel["NUM_EXT2"];
	var dnumInteriorLaboral = cuentaHabienteBancoppel["NUM_INT2"];
	var dcodigoPostalLaboral = cuentaHabienteBancoppel["CP2"];
	var dcoloniaLaboral = cuentaHabienteBancoppel["COLONIA2"];
	var dciudadLaboral = cuentaHabienteBancoppel["CIUDAD2"];
	var ddelegacionLaboral = cuentaHabienteBancoppel["DELEGACION_MUN2"];
	var destadoLaboral = cuentaHabienteBancoppel["ESTADO2"];
	var dpaisLaboral = cuentaHabienteBancoppel["PAIS2"];
	var dapPaternoRef1 = cuentaHabienteBancoppel["APELL_PATERNO_REF1"];
	var dapMaternoRef1 = cuentaHabienteBancoppel["APELL_MATERNO_REF1"];
	var dnombresRef1 = cuentaHabienteBancoppel["NOMBRE_REF1"];
	var dapPaternoRef2 = cuentaHabienteBancoppel["APELL_PATERNO_REF2"];
	var dapMaternoRef2 = cuentaHabienteBancoppel["APELL_MATERNO_REF2"];
	var dnombresRef2 = cuentaHabienteBancoppel["NOMBRE_REF2"];
	var dctaTradicionalVigente = cuentaHabienteBancoppel["CTA_EFECTIVA_ACTIVA"];
	var dctaNomina = cuentaHabienteBancoppel["CTA_NOMINA"];

	var respuesta=null;
	$.ajax(
		{
			async: false,
			cache: false,
			data:{
				opcion: 94,
				fechaconsulta: dfechaconsulta,
				horaconsulta: dhoraconsulta,
				folioafiliacion: iFolioSol,
				tienda: dtienda,
				empleado: dempleado,
				idanverso: didanverso,
				idreverso: didreverso,
				sCurp: dcurp,
				appaterno: dappaterno,
				apmaterno: dapmaterno,
				nombres: dnombres,
				rfc: drfc,
				fechanac: dfechanac,
				entidad: dentidad,
				genero: dgenero,
				nacionalidad: dnacionalidad,
				edocivil: dedoCivil,
				numclientecoppel: dnumClienteCoppel,
				numclientebancoppel: dnumClienteBancoppel,
				nvlestudio: dnvlEstudio,
				profesion: dprofesion,
				gironegocio: dgiroNegocio,
				telefono1: dtelefono1,
				telefono2: dtelefono2,
				correo: dcorreo,
				calle: dcalle,
				numexterior: dnumExterior,
				numinterior: dnumInterior,
				codigopostal: dcodigoPostal,
				colonia: dcolonia,
				ciudad: dciudad,
				delegacion: ddelegacion,
				estado: destado,
				pais: dpais,
				callecobranza: dcalleCobranza,
				numexteriorcobranza: dnumExteriorCobranza,
				numinteriorcobranza: dnumInteriorCobranza,
				codicopostalcobranza: dcodigoPostalCobranza,
				coloniacobranza: dcoloniaCobranza,
				ciudadcobranza: dciudadCobranza,
				delegacioncobranza: ddelegacionCobranza,
				estadocobranza: destadoCobranza,
				paiscobranza: dpaisCobranza,
				callelaboral: dcalleLaboral,
				numexteriorlaboral: dnumExteriorLaboral,
				numinteriorlaboral: dnumInteriorLaboral,
				codigopostallaboral: dcodigoPostalLaboral,
				colonialaboral: dcoloniaLaboral,
				ciudadlaboral: dciudadLaboral,
				delegacionlaboral: ddelegacionLaboral,
				estadolaboral: destadoLaboral,
				paislaboral: dpaisLaboral,
				appaternoref1: dapPaternoRef1,
				apmaternoref1: dapMaternoRef1,
				nombresref1: dnombresRef1,
				appaternoref2: dapPaternoRef2,
				apmaternoref2: dapMaternoRef2,
				nombresref2: dnombresRef2,
				ctatradicionalvigente: dctaTradicionalVigente,
				ctanomina: dctaNomina,
				opcionbcpl:opcionbcpl
			},
			 url: 'php/constanciaafiliacion.php',
			 type: 'POST',
			 dataType: 'json',
			 success: function(data)
			{
				respuesta=data;
				if(respuesta.estatus==1){
					respuesta=data;
				}else{
					console.log("Error al guarda la bitacora: "+respuesta.Error);
				}
			},
			error: function(a, b, c){
				console.log("Error" + a + b + c);
			}
		});
	return respuesta;
}

//FOlIO 539 Obtiene las entidades para la captura de datos
function obtenerEntidadNacimiento()
{
	var respuesta = '';
	$.ajax
    ({
		async: false,
		cache: false,
		url: 'php/constanciaafiliacion.php',
		type: 'POST',
		dataType: 'json',
		data:{opcion:90},
		success: function(data)
		{
			if(data.estatus == 1){
				respuesta = data.respuesta;
			}else{
				console.log("Error al obtener entidad Nacimiento: "+data.descripcion);
			}
		},
        error: function(a, b, c){
			sMensaje= "error entidad nacimiento ajax " + a + " " + b + " " + c;
			mostrarMensaje(sMensaje,"");
		},
	});
	validarCNNS();
	return respuesta;
}

function mostrarMensajegenerico(mensaje, sTitle)
{
  $('.content-loader').hide();
  $("#divMensaje").dialog
	({
		title: sTitle,
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();

			if (Array.isArray(mensaje))
			{
				var message = ""

				for (i = 0; i <= mensaje.length; i++)
				{
					if (mensaje[i] == undefined)
					{
						mensaje.splice(i, 1);
						continue;
					}

					message += mensaje[i] + "<br/> <br/>";
				}

				$(this).html("<p>"+message+"</p>");
			}
			else
			{
				$(this).html("<p>"+mensaje+"</p>");
			}
		},
		buttons:
			{
				"Aceptar" : function()
				{
					$(this).dialog( "close" );
					 
				}
			}
	});
}
//Folio 539
function bloquearcamposine()
{
	$("#txTelCelular").attr("disabled", true);
	$("#cboCompTelef").attr("disabled", true);
	$("#cboTipoComprobante").attr("disabled", true);
	$("#cboEstadoCivil").attr("disabled", true);
	$("#cboTipoTelefono").attr("disabled", true);
	$("#txtTelContacto").attr("disabled", true);
	$("#cboCompTelefCont").attr("disabled", true);
	$("#txtClienteCoppel").attr("disabled", true);
}

function verificajfuse()
{
	var respuesta = '';
	
	$.ajax
    ({
		async: false,
		cache: false,
		url: 'php/constanciaafiliacion.php',
		type: 'POST',
		dataType: 'json',
		data:{
		opcion:96,
		folioservicioafore: iFolioConsulta
		},
		success: function(data)
		{
			respuesta = data.enviada;
		},
        error: function(a, b, c){
			
		},
	});

	return respuesta;
}

function aceptarsecundario()
{
	$("#lentitud").hide();
	iOpcion = ENROLAR_TRABAJADOR;
	marcarLog("Se asigna bandera enrolamiento proceso falla solconstancia");									  
	sTitle = "";
	//sMensaje = 'Promotor: inició el proceso de enrolamiento.';
	//mostrarMensaje(sMensaje, sTitle);
	iCodEjecutarEnrol = 'EET';
	mostrarmensaje2();
}