update caterroresprevalidador 
set mensaje = 'Error 188. Promotor, debes verificar la CURP del Trabajador, si est&aacute; 
				capturada correctamente favor de comunicarte con Mesa de Ayuda para que se genere
				un reporte de incidencia.' 
where codigo = '188';


update caterroresprevalidador 
set mensaje = 'Promotor, el Trabajador requiere el Folio de Conocimiento de Traspaso.'
where codigo = 'P16';

update caterroresprevalidador 
set mensaje = 'Promotor, el Trabajador se encuentra en R&eacute;gimen Pensionario en su Afore actual, no podr&aacute;s realizar su solicitud.'
where codigo = 'P14';

update caterroresprevalidador
set mensaje = 'Promotor, el Trabajador cuenta con una marca operativa en proceso, no podr&aacute;s realizar su solicitud. Favor de indicarle al trabajador que deber&aacute; aclararlo en su Afore actual.'
where codigo = 'P13';

update caterroresprevalidador set mensaje = 'Promotor, el Trabajador no se puede Traspasar, ya que a&uacute;n no cuenta con 1 a&ntilde;o de antig&uuml;edad en su Afore actual.' 
where codigo = 'MVT';

