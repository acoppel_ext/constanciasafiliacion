-- Function: fnobtenerrespuestagestordactilar(integer, character, integer)

-- DROP FUNCTION fnobtenerrespuestagestordactilar(integer, character, integer);

CREATE OR REPLACE FUNCTION fnobtenerrespuestagestordactilar(
    integer,
    character,
    integer)
  RETURNS SETOF tpobtenerrespuestagestordactilar AS
$BODY$

DECLARE
	iFoliosolicitud     ALIAS FOR $1;
	cCurptrabajador		ALIAS FOR $2;
	iOpcion				ALIAS FOR $3;
	
	
	--Declaracion de variables 
	resp        	tpobtenerrespuestagestordactilar;
	iSelloVolun	        BIGINT;
	iRespuesta			INTEGER;
	iExiste				INTEGER;
	cDiagnostico		CHARACTER(30);
	cResultadoVerificacion CHARACTER(2);
	cMensaje			CHARACTER(300);
	cCurppromotor		CHARACTER(18);
	cResultado CHARACTER(2);
	cDiag		CHARACTER(30);
	iSello        BIGINT;
	iFoliosol	BIGINT;

	
BEGIN
	-- Iniciualizacion de variables 
    iRespuesta = 0;
	iSelloVolun = 0;
    iExiste = 0;
	cDiagnostico = '';
	cResultadoVerificacion = '';
	cMensaje = '';
	cCurppromotor ='';
	cResultado='';
	cDiag ='';
	iSello = 0;
	iFoliosol = 0;
	
	IF (iOpcion = 1 ) THEN 
	-- Se revisa la ejecucion del gestro a 4 hullas 
		IF EXISTS (SELECT 'OK' FROM tbrelacionhuellasenviar WHERE folioservicio = iFoliosolicitud AND tipooperacion = '0620' LIMIT 1) THEN 

		-- si se ejecuto se obtiene el sello de verificacion
				SELECT  dig.resultadoverificacion, dig.diagnosticos, dig.selloverificacion,mae.curppromotor
				INTO cResultadoVerificacion, cDiagnostico, iSelloVolun,cCurppromotor
				FROM tbmaeenrolhuellasserviciosuv AS mae
				INNER JOIN tbdetenrolhuellasserviciosuv AS det ON (mae.foliopaquete = det.foliopaquete)
				INNER JOIN diagnosticosserviciossuv AS dig ON (mae.folioservicio = dig.folioservicio)
				WHERE mae.foliosolicitud = iFoliosolicitud and mae.curptrabajador= cCurptrabajador ORDER BY dig.fechaalta DESC LIMIT 1;
		
				IF ( cDiagnostico != '' OR cDiagnostico IS NOT NULL) THEN -- si el diagnostico esta vacio no entra a sacar la descripcion
				
					SELECT mensaje INTO cMensaje FROM catdiagprocesarbiometricos WHERE codigo = cDiagnostico ;
				
				END IF;

				IF EXISTS ( SELECT 'OK' FROM identificartraspasodactilar WHERE curptrabajador = cCurptrabajador AND foliosolicitud = iFoliosolicitud LIMIT 1 ) THEN
				
					IF(iSelloVolun > 0 AND cResultadoVerificacion = '01') THEN 
					
						UPDATE identificartraspasodactilar SET fechaalta = NOW(), resultadooperacion= cResultadoVerificacion,diagnosticos = cDiagnostico , selloverificaciondactilar = iSelloVolun WHERE curptrabajador = cCurptrabajador AND foliosolicitud = iFoliosolicitud;
					
						GET DIAGNOSTICS iExiste = ROW_COUNT;
				
						IF iExiste > 0 THEN		
							iRespuesta = 1;
						ELSE
							iRespuesta = 0;	      
						END IF;

						resp.banderadiag =iRespuesta;
					
					ELSE 
					
					INSERT INTO identificartraspasodactilar (foliosolicitud,curptrabajador,curpempleado,tipooperacion,identificador,resultadooperacion,diagnosticos,selloverificaciondactilar) VALUES (iFoliosolicitud,cCurptrabajador,cCurppromotor,'0620','48 - Dactilar',cResultadoVerificacion,cDiagnostico,iSelloVolun);
					
					GET DIAGNOSTICS iExiste = ROW_COUNT;
				
						IF iExiste > 0 THEN		
							iRespuesta = 1;
						ELSE
							iRespuesta = 0;	      
						END IF;

						resp.banderadiag =iRespuesta;
					
					END IF;
					
				ELSE 
					
					INSERT INTO identificartraspasodactilar (foliosolicitud,curptrabajador,curpempleado,tipooperacion,identificador,resultadooperacion,diagnosticos,selloverificaciondactilar) VALUES (iFoliosolicitud,cCurptrabajador,cCurppromotor,'0620','48 - Dactilar',cResultadoVerificacion,cDiagnostico,iSelloVolun);
					
						GET DIAGNOSTICS iExiste = ROW_COUNT;
				
						IF iExiste > 0 THEN		
							iRespuesta = 1;
						ELSE
							iRespuesta = 0;	      
						END IF;

						resp.banderadiag =iRespuesta;
				END IF;
				
					resp.selloverificacionbiometricavoluntram = iSelloVolun;
					resp.diagnosticovoluntram = trim(cDiagnostico);
					resp.Mensaje = trim(cMensaje);
					resp.resultadoverificacionvoluntram = trim(cResultadoVerificacion);
		ELSE
		
				resp.Mensaje ='No se encontro registro del consumo del gestor a 4 huellas';
		
		END IF;
	
	ELSIF (iOpcion = 2 ) THEN 
	
		IF EXISTS (SELECT 'OK' FROM solconstancia WHERE curp = cCurptrabajador) THEN
            
                UPDATE solconstancia SET validacionine = 3, enrolpermanente = 1, expedientepermanente = 1 WHERE curp = cCurptrabajador AND folio = iFoliosolicitud;
				
					GET DIAGNOSTICS iExiste = ROW_COUNT;
				
						IF iExiste > 0 THEN		
							iRespuesta = 1;
						ELSE
							iRespuesta = 0;	      
						END IF;

						resp.banderadiag =iRespuesta;
		ELSE 
		
				resp.banderadiag =iRespuesta;
				
		END IF;
		
	ELSIF (iOpcion = 3 ) THEN 
	
		IF EXISTS (SELECT * FROM tbmaeenrolhuellasserviciosuv WHERE curptrabajador = cCurptrabajador AND tipooperacion = '0620' LIMIT 1) THEN
			SELECT foliosolicitud FROM tbmaeenrolhuellasserviciosuv
			INTO iFoliosol
			WHERE tipooperacion = '0620' AND curptrabajador = cCurptrabajador ORDER BY fechaalta DESC LIMIT 1;

			UPDATE tbmaeenrolhuellasserviciosuv SET foliosolicitud = iFoliosolicitud WHERE foliosolicitud = iFoliosol AND curptrabajador = cCurptrabajador;
			UPDATE tbrelacionhuellasenviar SET folioservicio = iFoliosolicitud WHERE folioservicio = iFoliosol AND tipooperacion = '0620';
			UPDATE identificartraspasodactilar SET foliosolicitud = iFoliosolicitud WHERE foliosolicitud = iFoliosol AND tipooperacion = '0620';
				
					GET DIAGNOSTICS iExiste = ROW_COUNT;
				
						IF iExiste > 0 THEN		
							iRespuesta = 1;
						ELSE
							iRespuesta = 0;	      
						END IF;

						resp.banderadiag =iRespuesta;
		ELSE 
		
				resp.banderadiag =iRespuesta;
				
		END IF;			
	
	END IF;
	
	RETURN NEXT resp;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION fnobtenerrespuestagestordactilar(integer, character, integer)
  OWNER TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fnobtenerrespuestagestordactilar(integer, character, integer) TO public;
GRANT EXECUTE ON FUNCTION fnobtenerrespuestagestordactilar(integer, character, integer) TO sysaforeglobal;