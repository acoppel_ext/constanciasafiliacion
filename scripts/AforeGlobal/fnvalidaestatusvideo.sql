--FUNCTION fnvalidaestatusvideo(integer);

--DROP FUNCTION fnvalidaestatusvideo(integer);
CREATE OR REPLACE FUNCTION fnvalidaestatusvideo(folioAfiliacion INTEGER)
/***************************************************************************************************
Autor: Jesús Gabriel Vázquez Sánchez 97837211
Fecha: 14/05/2020
BD: AforeGlobal
Descripción: Valida que el estatus al subir el vídeo a la base de datos de video sea 1 o 2,
             de no ser así se obtiene el nombre del vídeo para cargarlo de nuevo a la base de datos
***************************************************************************************************/
RETURNS TEXT AS
$BODY$
BEGIN
    IF EXISTS(SELECT '*' FROM colstatusguardado WHERE folio = folioAfiliacion AND status IN(1,2))THEN
        RETURN '1';
    ELSE
        RETURN (SELECT nombrearchivo FROM colstatusguardado WHERE folio = folioAfiliacion);
    END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION fnvalidaestatusvideo(INTEGER)
  OWNER TO sysaforeglobal;