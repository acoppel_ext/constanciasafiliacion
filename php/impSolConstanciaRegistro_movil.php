<?php
include("clases/fpdf.php");
include("../../fpdi/fpdi.php");
include_once("clases/CMetodoGeneral.php");
include_once("clases/global.php");
include_once('clases/Capirestconstanciaafiliacion.php');
date_default_timezone_set('America/Mazatlan');

$controlador = "CtrlconstanciaAfiliacion";

define("APP_DWSQ", "/sysx/progs/gsoap/wshuellas4mas1Afore/nbis/bin/dwsq");
define("RUTA_SALIDA_HLLA", "/sysx/progs/web/entrada/huellas/afiliacion/");
$objGn = new CMetodoGeneral();
$arrErr = array();
$arrResp = array();

$fechaServidor = obtenerFechaServidor();
$arrDatosImpres  = array('folio' => $_POST["folio"],
							'tipo' => $_POST["tipo"],
							'fecha' => $fechaServidor,
							'nombre' => $_POST["nombre"],
							'curp' => $_POST["curp"],
							'telcelular' => $_POST["telcelular"],
							'telfijo' => $_POST["telfijo"],
							'domicilio' => $_POST["domicilio"],
							'lugarsolicitud' => $_POST["lugarsolicitud"],
							'opcionenvio' => 'C',
							'promotor' => $_POST["promotor"],
							'cveconsar' => $_POST["cveconsar"],
							'municipio' => $_POST["municipio"],
							'estado' => $_POST["estado"],
							'ifirma' => $_POST["ifirma"]
							);

generarSolConstanciaTraspaso($arrDatosImpres);

function generarSolConstanciaTraspaso($arrDatosImpresion)
{
		//376.1
		consultarHuellas($arrDatosImpresion["curp"], $arrDatosImpresion["folio"]);
		
		global $objGn;
		$objGn->grabarLogx("INICIA GENERACION DE PDF");
		$arrResp = array();
		$respuesta = new stdClass();
		$pdf = new FPDF();
		$pdf->AliasNbPages();
		$pdf->AddPage('P','Letter');
		$iColIni = 10;
		$iRenIni = 10;
		$iCol = $iColIni;
		$iRen = $iRenIni;
		$iAltoRen = 5;
		$iAltoLogo = 70;
		$IRectangulo = 35;
		$iPosRectY = 60;
		$iBand = 0;
		$sTexto = '';
		$iCveOperacion = -1;
		$sImagneSolConstancia = "";
		$sRutaAbsoluta = "";
		$sRutaImagenNombreTrabajador = '/sysx/progs/web/entrada/firmas/SCTR_'.$arrDatosImpresion["folio"].'_NTRAB.JPG'; 
		$sRutaImagenFirmaTrabajador = '/sysx/progs/web/entrada/firmas/SCTR_'.$arrDatosImpresion["folio"].'_FTRAB.JPG'; 
		$sRutaImagenFirmaPromotor = '/sysx/progs/web/entrada/firmas/SCTR_'.$arrDatosImpresion["folio"].'_FPROM.JPG';
		//376.1
		$sRutaImagenHuellaTrabajador = '/sysx/progs/web/entrada/huella/afiliacion/SCTR_'.$arrDatosImpresion["folio"].'_HTRAB.jpg';
		$sRutaImagenHuellaPromotor 	= '/sysx/progs/web/entrada/huella/afiliacion/SCTR_'.$arrDatosImpresion["folio"].'_HPROM.jpg';

		
		$iPidActual = getmypid();
		
		$arrResp = obtenerSieforeTrabajador($arrDatosImpresion['curp']);

		if($arrResp["estatus"] == OK___)
		{
			$siefore = $arrResp['siefore']; unset($arrResp);

			//Decodificar el arreglo para mostrar correctamente los acentos
			foreach ($arrDatosImpresion as $key=>$valor)
			{
				$arrDatosImpresion[$key] = utf8_decode($arrDatosImpresion[$key]);
			}
			//Establecer margenes izquierdo, arriba, derecho
			$pdf->SetMargins(30, 25 ,30);

			$pdf->SetFont('Arial','B',12);
			$pdf->Cell( 200, $iAltoRen, utf8_decode("SOLICITUD DE CONSTANCIA PARA ".$arrDatosImpresion['tipo']) , 0, 0, 'C');
			$pdf->Ln();

			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial','',10);
			$sTexto = $arrDatosImpresion['municipio'].', '.$arrDatosImpresion['estado'].', A '.$arrDatosImpresion['fecha'];
			//$pdf->Cell(-5);//DAR UNA MARGEN DE 5 PUNTOS A LA DERECHA
			$pdf->Cell( 150, $iAltoRen, $sTexto , 0, 0, 'R');
			//$pdf->Cell( 150, $iAltoRen, $arrDatosImpresion['fecha'] , 0, 0, 'R');
			$pdf->Ln(20);

			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(5);
			$pdf->Cell(150, $iAltoRen, utf8_decode('DATOS DEL TRABAJADOR') , 0, 0, 'L');
			$pdf->Ln();

			$pdf->SetFont('Arial','',8);
			$pdf->Cell(30, $iAltoRen, "Nombre :", 1, 0, 'R');
			$pdf->Cell(120, $iAltoRen, $arrDatosImpresion['nombre'], 1, 0, 'L');
			$pdf->Ln();

			$pdf->Cell(30, $iAltoRen, "CURP :", 1, 0, 'R');
			$pdf->Cell(120, $iAltoRen, $arrDatosImpresion['curp'], 1, 0, 'L');
			$pdf->Ln();

			$pdf->Cell(30, $iAltoRen, utf8_decode("Teléfono Celular :"), 1, 0, 'R');
			$pdf->Cell(120, $iAltoRen, $arrDatosImpresion['telcelular'], 1, 0, 'L');
			$pdf->Ln();

			if($arrDatosImpresion["ifirma"] == 1)
			{
			$pdf->Cell(30, $iAltoRen, utf8_decode("Teléfono Fijo :"), 1, 0, 'R');
			$pdf->Cell(120, $iAltoRen, $arrDatosImpresion['telfijo'], 1, 0, 'L');
			$pdf->Ln();
			}
			else
			{
			$pdf->Cell(30, $iAltoRen, utf8_decode("Teléfono Fijo* :"), 1, 0, 'R');
			$pdf->Cell(120, $iAltoRen, "", 1, 0, 'L');
			$pdf->Ln();	
			}

			$pdf->Cell(30, $iAltoRen, "Domicilio :", 0, 0, 'R');
			$pdf->MultiCell(110, $iAltoRen, $arrDatosImpresion['domicilio'],0,'L');

			$pdf->Rect(30, $iPosRectY , 30, $iAltoRen*2 );
			$pdf->Rect(60, $iPosRectY, 120, $iAltoRen*2 );

			$pdf->SetY(75);
			$pdf->Cell(75, $iAltoRen, "Lugar de la Solicitud :", 1, 0, 'C');
			$pdf->SetX(120);
			$pdf->Cell(60, $iAltoRen, utf8_decode("Opciones de envío :"), 1, 0, 'C');
			$pdf->Ln();

			$pdf->MultiCell(75, $iAltoRen, $arrDatosImpresion["lugarsolicitud"], 1, 'C',false);
			$pdf->SetX(120);

			$pdf->Rect(144, 80.5 , 4, 4 );
			$pdf->Rect(166.5, 80.5, 4, 4 );
			
			$pdf->SetFont('Arial','B',8);
			if($arrDatosImpresion["opcionenvio"] == 'C')
			{
				$pdf->Text(145,83.5,"X");
			}
			else
			{
				$pdf->Text(167.5,83.5,"X");	
			}
			$pdf->SetXY(120, $pdf->GetY()-($iAltoRen*2));
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(60, $iAltoRen, "Celular              Domicilio", 1, 0, 'C');
			$pdf->Ln(15);
				

			if($arrDatosImpresion["ifirma"] != 1)
			{
				if ($arrDatosImpresion['tipo'] == 'TRASPASO')
				{
					$pdf->Cell(150, $iAltoRen, 'Por medio de la presente, yo',0,'J');
					$pdf->Image('/sysx/progs/web/plantillas/plconstanciasafiliacion/Numero.JPG',68,93,5,5,'JPG');
					$pdf->Cell(150, $iAltoRen, ',',0,'D');
					$pdf->Ln();
					$pdf->Cell(150, $iAltoRen, 'manifiesto que se lleve a cabo la Solicitud de la Constancia para Traspaso a la Afore COPPEL',0,'J');
					$pdf->Ln(14);
				}
				else
				{
					$pdf->Cell(150, $iAltoRen, 'Por medio de la presente, yo',0,'J');
					$pdf->Image('/sysx/progs/web/plantillas/plconstanciasafiliacion/Numero.JPG',68,93,5,5,'JPG');
					$pdf->Cell(150, $iAltoRen, ',',0,'D');
					$pdf->Ln();
					$pdf->Cell(150, $iAltoRen, 'manifiesto que se lleve a cabo la Solicitud de la Constancia para Registro a la Afore COPPEL',0,'J');
					$pdf->Ln(14);
				}
			}else
			{	
				if ($arrDatosImpresion['tipo'] == 'TRASPASO')
				{
				$pdf->Image('/sysx/progs/web/plantillas/plconstanciasafiliacion/Numero.JPG',68,95,3,3,'JPG');
				$pdf->MultiCell(150, $iAltoRen, 'Por medio de la presente, yo                                                            , manifiesto que se lleve a cabo la Solicitud de la Constancia para Traspaso a la Afore COPPEL',0,'J');
				$pdf->Ln(15);
				}
				else
				{
				$pdf->Image('/sysx/progs/web/plantillas/plconstanciasafiliacion/Numero.JPG',68,95,3,3,'JPG');
				$pdf->MultiCell(150, $iAltoRen, 'Por medio de la presente, yo                                                            , manifiesto que se lleve a cabo la Solicitud de la Constancia para Registro a la Afore COPPEL',0,'J');
				$pdf->Ln(15);
				}
			}
		
			/*Agregar imagen de rendimentoss*/
			switch ( $siefore ) {
				case 1:
					$pdf->Image('/sysx/progs/web/entrada/constanciasafiliacion/IMGRENDIMIENTOS/sb1.jpg',68,108,80,NULL,'GIF');
					break;
				case 2:
					$pdf->Image('/sysx/progs/web/entrada/constanciasafiliacion/IMGRENDIMIENTOS/sb2.jpg',68,108,80,NULL,'GIF');
					break;
				case 3:
					$pdf->Image('/sysx/progs/web/entrada/constanciasafiliacion/IMGRENDIMIENTOS/sb3.jpg',68,108,80,NULL,'GIF');
					break;
				case 4:
					$pdf->Image('/sysx/progs/web/entrada/constanciasafiliacion/IMGRENDIMIENTOS/sb4.jpg',68,108,80,NULL,'GIF');
					break;
				
				default:
					# code...
					break;
			}
			
			//Posicionar la cabeza de escritura de abajo hacia arriba 7 cm
			$pdf->SetY(-70);
			$pdf->Cell(70, $iAltoRen, "AGENTE PROMOTOR", 1, 0, 'C');
			$pdf->SetX(115);
			$pdf->Cell(70, $iAltoRen, "TRABAJADOR", 1, 0, 'C');
			$pdf->Ln();

			$pdf->Rect(30, 214.5 , 70, $iAltoRen * 5);
			$pdf->Rect(115, 214.5 , 70, $iAltoRen * 5);

			$pdf->SetY(-45);
			$pdf->Cell(70, $iAltoRen, "FIRMA", 0, 0, 'C');

			$pdf->SetX(115);
			$pdf->Cell(70, $iAltoRen, "FIRMA", 0, 0, 'C');
			$pdf->Ln();

			$pdf->Cell(70, $iAltoRen, $arrDatosImpresion["promotor"], 0, 0, 'C');
			$pdf->SetX(115);
			$pdf->Cell(70, $iAltoRen, $arrDatosImpresion["nombre"], 0, 0, 'C');

			$pdf->Ln();
			$pdf->Cell(70, $iAltoRen, $arrDatosImpresion["cveconsar"], 0, 0, 'C');

			$pdf->Ln();
			$pdf->Image('/sysx/progs/web/plantillas/plconstanciasafiliacion/Numero.JPG',24,248,7,7,'JPG');
			$pdf->Cell(50, $iAltoRen, utf8_decode("Los campos deben ser llenados con puño y letra del Trabajador." ),0,0,'L');

			$pdf->Ln();
			$pdf->Cell(50, $iAltoRen-3, utf8_decode("* En su caso." ),0,0,'L');
				
			$rutaPdf = "/sysx/progs/web/salida/constanciasafiliacion/".$iPidActual."SolConstanciaRegistro".$arrDatosImpresion['curp'].".pdf";
			$rutaPdfTemp = "/sysx/progs/web/salida/constanciasafiliacion/".$arrDatosImpresion["folio"]."_".$iPidActual."SolConstanciaRegistro".$arrDatosImpresion['curp']."_TEMP.pdf";
			$respuesta->rutaPdf = "../salida/constanciasafiliacion/".$iPidActual."SolConstanciaRegistro".$arrDatosImpresion['curp'].".pdf";
			$respuesta->rutaPdfTemp = "/sysx/progs/web/salida/constanciasafiliacion/".$arrDatosImpresion["folio"]."_".$iPidActual."SolConstanciaRegistro".$arrDatosImpresion['curp']."_TEMP.pdf";
			$sRutaAbsoluta = $iPidActual."SolConstanciaRegistro".$arrDatosImpresion['curp'].".pdf";
			
			try
			{
				$objGn->grabarLogx("VALIDA CAMPO FIRMA ".$arrDatosImpresion["ifirma"]);
				switch ($arrDatosImpresion["ifirma"])
				{
					case 0: //NO HAY SIGNPAD
						$pdf->Output($rutaPdf);
						$objGn->grabarLogx("Se guarda y muestra PDF");
						$respuesta->descestatus = 'EXITO';
						$respuesta->estado = 1;
						break;
					case 2: //CON HUELLA
						$pdf->Output($rutaPdf);
						$objGn->grabarLogx("Se guarda y muestra PDF");
						$respuesta->descestatus = 'EXITO';
						$respuesta->estado = 1;
						break;
					case 1: //CON FIRMA DIGITAL
						$pdf->Output($rutaPdf);
						//copy($rutaPdf, $rutaPdfTemp);						
						$objGn->grabarLogx("Se actualizo campo firmasolconstancia [impSolConstanciaRegistro.php]");

						$pdf = new FPDI();
						$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/constanciasafiliacion/'.$sRutaAbsoluta);
						$objGn->grabarLogx("Se Abre PDF");
						$tplIdx = $pdf->importPage(1);
						$pdf->addPage();
						$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);	
						
						//376.1			
						if (file_exists('/sysx/progs/web/entrada/huellas/afiliacion/SCTR_'.$arrDatosImpresion["folio"].'_HTRAB.jpg'))
						{	
							$pdf->Image('/sysx/progs/web/entrada/huellas/afiliacion/SCTR_'.$arrDatosImpresion["folio"].'_HTRAB.jpg',169, 220.5, 15, 18, 'JPG');
							$iBand = 1;			
						}
						else
						{						
							$pdf->SetFont('Arial','B', 7);
							$pdf->SetTextColor(0,0,0);
							$pdf->SetXY(169, 232);
							$pdf->Write(0, "Sin huella");
						}										
						if (file_exists('/sysx/progs/web/entrada/huellas/afiliacion/SCTR_'.$arrDatosImpresion["folio"].'_HPROM.jpg'))
						{	
							$pdf->Image('/sysx/progs/web/entrada/huellas/afiliacion/SCTR_'.$arrDatosImpresion["folio"].'_HPROM.jpg', 84, 220.5 , 15, 18, 'JPG');
							$iBand = 1;
						}
						else
						{						
							$pdf->SetFont('Arial','B', 7);
							$pdf->SetTextColor(0,0,0);
							$pdf->SetXY(84.3, 232);
							$pdf->Write(0, "Sin huella");
						}

						$objGn->grabarLogx("Se agregan las huellas al PDF");

						if($iBand == 1)
						{
							$pdf->Output($rutaPdf);
							copy($rutaPdf, $rutaPdfTemp);
							$pdf = new FPDI();
							
							$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/constanciasafiliacion/'.$sRutaAbsoluta);
							$tplIdx = $pdf->importPage(1);
							$pdf->addPage();
							$pdf->useTemplate($tplIdx, null, null, 0, 0, true);	

						}

						if( $arrDatosImpresion['tipo'] == 'TRASPASO')
						{
							$pdf->Image('/sysx/progs/web/entrada/firmas/SCTR_'.$arrDatosImpresion["folio"].'_NTRAB.JPG',72,93,50,10,'JPG');
							$pdf->SetFont('Arial','',8);
							$pdf->SetXY(30,95);
							$pdf->MultiCell(150, $iAltoRen, 'Por medio de la presente, yo                                                            , manifiesto que se lleve a cabo la Solicitud de la Constancia para Traspaso a la Afore COPPEL',0,'J');
						}
						else 
						{
							$pdf->Image('/sysx/progs/web/entrada/firmas/SCTR_'.$arrDatosImpresion["folio"].'_NTRAB.JPG',72,93,50,10,'JPG');
							$pdf->SetFont('Arial','',8);
							$pdf->SetXY(30,95);
							$pdf->MultiCell(150, $iAltoRen, 'Por medio de la presente, yo                                                            , manifiesto que se lleve a cabo la Solicitud de la Constancia para Registro a la Afore COPPEL',0,'J');
						}
						
						$pdf->Image('/sysx/progs/web/entrada/firmas/SCTR_'.$arrDatosImpresion["folio"].'_FTRAB.JPG', 118, 215, 50, 20, 'JPG');
						$pdf->Image('/sysx/progs/web/entrada/firmas/SCTR_'.$arrDatosImpresion["folio"].'_FPROM.JPG', 35, 215, 50, 20, 'JPG');
						
						$objGn->grabarLogx("Se agregan las firmas al PDF");
						
						//SE GUARDA PDF Y SE MUESTRA EN EL NAVEGADOR
						$pdf->Output($rutaPdf, 'F');
						$objGn->grabarLogx("Se guarda y muestra PDF");
											
						$pdf = new FPDI();

						$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/constanciasafiliacion/'.$sRutaAbsoluta);
						$tplIdx = $pdf->importPage(1);
						$pdf->addPage();
						$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
						
						$arrResp = $objGn->actualizarfirmapublicacion($arrDatosImpresion["folio"], 1);
						
						if($arrResp["estado"] == OK___)
						{
							$respuesta->descestatus = 'EXITO';
							$respuesta->estado = 1;	
						}
						else
						{
							$objGn->grabarLogx("No se pudo actualizar la firma en base de datos");
							$respuesta->descestatus = 'Error';
							$respuesta->estado = 1;	
						}
						
						
						break;
					default:
						break;
				}
			}
			
			catch (Exception $Ex) 
			{
				$objGn->grabarLogx("Se borran las imagenes del servidor. [revisar fuente]");
				//SE BORRAN FIRMAS DEL SERVIDOR
				/*
					20151025: Euduardo Osuna
					se comenta el borrado hasta definir en sistemas que se hace con los archivos
				*/
				/*array_map('unlink', glob("/sysx/progs/web/entrada/firmas/SCTR_".$arrDatosImpresion["folio"]."_NTRAB.JPG"));
				array_map('unlink', glob("/sysx/progs/web/entrada/firmas/SCTR_".$arrDatosImpresion["folio"]."_FTRAB.JPG"));
				array_map('unlink', glob("/sysx/progs/web/entrada/firmas/SCTR_".$arrDatosImpresion["folio"]."_FPROM.JPG"));*/
				
				$respuesta->descestatus = $Ex->getMessage();
				$respuesta->estado = ERR__;
			}
		}
		else
		{
			
			$respuesta->descestatus = $arrResp['descripcion'];
			$respuesta->estado = $arrResp['estatus'];
		}

		echo json_encode($respuesta);
}

function obtenerSieforeTrabajador($curp)
{
	$objAPI = new Capirestconstanciaafiliacion();

	$arrResp = array( );
	$arrResp['estatus'] = ERR__;

	$arrData = array(
		'curp' => $curp,
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('obtenersiefore', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			$resultAPI = json_decode($resultAPI,true);
			$arrResp['siefore'] = (int)$resultApi['siefore'];
			$arrResp['estatus'] = OK___;
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}

	return $arrResp;
}

function obtenerFechaServidor()
{
	$fecha = "";

	date_default_timezone_set('America/Mazatlan');
	setlocale(LC_TIME, 'spanish');
	$sDia = strtoupper(strftime("%A"));
	$fecha =  strtoupper(strftime("%d de %B de %Y"));
	return utf8_encode($fecha);
}

function obtenerClaveOperacion($iFolio)
{
	$objAPI = new Capirestconstanciaafiliacion();

	$arrDatos = array();
	$arrDatos['clave'] = 0;
	$arrDatos['respuesta'] = 0;
	$arrDatos['estado'] = -1;

	$arrData = array(
		'iFolio' => $iFolio,
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('obtenerClaveOperacionImp', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			foreach($resultAPI["registros"] as $reg)
			{
				$arrDatos['estado'] = 1;
				$arrDatos["clave"] = $reg['fnobtenerdoctosdigitalizacion'];
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}

	return $arrDatos;
}

function consultarHuellas($sCurpTrab, $iFolio)
{
	/*Retorno: 0=Sin huellas el prom y trab, 1= existen ambas huellas, 2=Sin hlla prom, 3= sin hlla trab, 4=Error*/
	$objAPI = new Capirestconstanciaafiliacion();

	$sImgHlla = "";
	$sArchivoWsq = "";
	$sArchivoRaw = "";
	$sArchivoJpg = "";
	$sArchivoTmp = "";
	$sComando = "";
	$sAncho = "";
	$sAlto = "";
	$iRet = 1;
	$bProm = false;
	$bTrab = false;
	$iIDServicio = 9910;
	$cTipoOperacion = '0401';
	$iContador = 0;

	$arrData = array(
		'sCurpTrab' => $sCurpTrab,
		'iFolio' => $iFolio,
		'iIDServicio' => $iIDServicio,
		'cTipoOperacion' => $cTipoOperacion
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('consultarHuellas', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			$resultAPI = json_decode($resultAPI,true);
			foreach($resultAPI["registros"] as $reg)
			{			
				CLogImpresion::escribirLog("foreach");
				$sImgHlla=base64_decode($reg['templateafiliacion']);
				
				if((int)$reg['tipopersona']==2)
				{			
					CLogImpresion::escribirLog("Entra a crear la huella del promotor");					
					$sArchivoJpg=RUTA_SALIDA_HLLA."SCTR_".$iFolio."_HPROM.jpg";
					
					file_put_contents($sArchivoJpg, $sImgHlla); 
					
					$bProm=true;
					
				}
				else if((int)$reg['tipopersona']==3)
				{							
				
					$sArchivoJpg=RUTA_SALIDA_HLLA."SCTR_".$iFolio."_HTRAB.jpg";
					
					file_put_contents($sArchivoJpg, $sImgHlla); 
					$bTrab=true;
				}
			}//END FOREACH
			if($bTrab && $bProm)
				$iRet=1;		
			else if(!$bTrab && $bProm)
				$iRet=3;
			else if($bTrab && !$bProm)
				$iRet=2;
			else
				$iRet=0;
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}

	return $iRet;
}
?>