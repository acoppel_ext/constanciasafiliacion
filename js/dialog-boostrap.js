/***************************************************************
* Modal Base
***************************************************************/
/*
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
Launch demo modal
</button>

<!-- Modal -->
<div class="bootbox modal fade" id="myModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Modal -->
*/

/***************************************************************
* Ventanas Modales
***************************************************************/
function dialog_boot(myModal)
{
    var divModal=myModal;

    this.espera=function(sTitulo)
	{
        $("#myModal").modal();
		divModal.innerHTML="";
		if(sTitulo == 'undefined') sTitulo = '';
		var sHtml = 
        '<div class="modal-dialog"> ' +
            '<div class="modal-content"> ' +
                '<div class="modal-header"> ' +
                    '<h4 class="modal-title">' + sTitulo + '</h4> ' +
                '</div> ' +
                '<div class="modal-body"> ' +
                    '<div class="progress progress-striped active"> ' +
                        '<div class="progress-bar progress-bar-blue" style="width: 100%"></div> ' +
                    '</div> ' +
                ' </div> ' +
            '</div>' +
        '</div>';
		divModal.innerHTML=sHtml;
	};

    this.mensaje=function(sTitulo,sMensaje)
	{
        $("#myModal").modal();
		divModal.innerHTML="";
        if(sTitulo == 'undefined') {
            sTitulo = '';
        }
		var sHtml = 
        '<div class="modal-dialog"> ' +
            '<div class="modal-content"> ' +
                '<div class="modal-header"> ' +
                    '<button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button> ' +
                    '<h4 class="modal-title">' + sTitulo + '</h4> ' +
                '</div> ' +
                '<div class="modal-body"> ' + sMensaje + ' </div> ' +
                '<div class="modal-footer"> ' +
                    '<button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button> ' +
                '</div> ' +
            '</div>' +
        '</div>';
        divModal.innerHTML=sHtml;
	};

    this.mensajecerrar=function(sTitulo,sMensaje)
	{
        $("#myModal").modal();
		divModal.innerHTML="";
		if(sTitulo == 'undefined') sTitulo = '';
		var sHtml = 
        '<div class="modal-dialog"> ' +
            '<div class="modal-content"> ' +
                '<div class="modal-header"> ' +
                    '<button type="button" class="bootbox-close-button close" onclick="closeNavegador();" aria-hidden="true">×</button> ' +
                    '<h4 class="modal-title">' + sTitulo + '</h4> ' +
                '</div> ' +
                '<div class="modal-body"> ' + sMensaje + ' </div> ' +
                '<div class="modal-footer"> ' +
                    '<button type="button" class="btn btn-primary" onclick="closeNavegador();">Aceptar</button> ' +
                '</div> ' +
            '</div>' +
        '</div>';
		divModal.innerHTML=sHtml;
	};

    this.mensajecerrarpreg=function(sTitulo,sMensaje)
	{
        $("#myModal").modal();
		divModal.innerHTML="";
		if(sTitulo == 'undefined') sTitulo = '';
		var sHtml = 
        '<div class="modal-dialog"> ' +
            '<div class="modal-content"> ' +
                '<div class="modal-header"> ' +
                    '<button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button> ' +
                    '<h4 class="modal-title">' + sTitulo + '</h4> ' +
                '</div> ' +
                '<div class="modal-body"> ' + sMensaje + ' </div> ' +
                '<div class="modal-footer"> ' +
                    '<button type="button" class="btn btn-warning" data-dismiss="modal">No</button> ' +
                    '<button type="button" class="btn btn-primary" onclick="closeNavegador();">Si</button> ' +
                '</div> ' +
            '</div>' +
        '</div>';
		divModal.innerHTML=sHtml;
	};

    this.mensajeErrorFct=function(cMensaje)
	{

        $("#divMensajeErrorFct").modal();
        divModal.innerHTML="";
        var sHtml = '';
		//if(sTitulo == 'undefined') sTitulo = '';
		
        sHtml += '<div class="modal-dialog modal-lm">';
        sHtml += cMensaje;
        divModal.innerHTML=sHtml;
	};


    this.mensajefuncion=function(cMensaje)
	{

        $("#myModal").modal();
        divModal.innerHTML="";
        var sHtml = '';
		//if(sTitulo == 'undefined') sTitulo = '';
		
        sHtml += '<div class="modal-dialog modal-lm">';
        sHtml += cMensaje;
        divModal.innerHTML=sHtml;
	};

    this.mensajefuncionpreg=function(sTitulo,sMensaje,sFuncion)
	{
        $("#myModal").modal();
		divModal.innerHTML="";
		if(sTitulo == 'undefined') sTitulo = '';
		var sHtml = 
        '<div class="modal-dialog"> ' +
            '<div class="modal-content"> ' +
                '<div class="modal-header"> ' +
                    '<button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button> ' +
                    '<h4 class="modal-title">' + sTitulo + '</h4> ' +
                '</div> ' +
                '<div class="modal-body"> ' + sMensaje + ' </div> ' +
                '<div class="modal-footer"> ' +
                    '<button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button> ' +
                    '<button type="button" class="btn btn-primary" onclick="' + sFuncion + '">Aceptar</button> ' +
                '</div> ' +
            '</div>' +
        '</div>';
		divModal.innerHTML=sHtml;
	};

    this.mensajefuncionpregcerrar=function(sTitulo,sMensaje,sFuncion)
	{
        $("#myModal").modal();
		divModal.innerHTML="";
		if(sTitulo == 'undefined') sTitulo = '';
		var sHtml = 
        '<div class="modal-dialog"> ' +
            '<div class="modal-content"> ' +
                '<div class="modal-header"> ' +
                    '<button type="button" class="bootbox-close-button close" onclick="closeNavegador();" aria-hidden="true">×</button> ' +
                    '<h4 class="modal-title">' + sTitulo + '</h4> ' +
                '</div> ' +
                '<div class="modal-body"> ' + sMensaje + ' </div> ' +
                '<div class="modal-footer"> ' +
                    '<button type="button" class="btn btn-warning" onclick="closeNavegador();">Cancelar</button> ' +
                    '<button type="button" class="btn btn-primary" onclick="' + sFuncion + '">Aceptar</button> ' +
                '</div> ' +
            '</div>' +
        '</div>';
		divModal.innerHTML=sHtml;
	};

    this.mensajepdf=function(sTitulo,sMensaje,sFuncion,sNombreBoton)
	{
        $("#myModal").modal();
		divModal.innerHTML="";
		if(sTitulo == 'undefined') sTitulo = '';
		var sHtml = 
        '<div class="modal-dialog modalPDF" style="width: 80%;"> ' +
            '<div class="modal-content"> ' +
                '<div class="modal-header"> ' +
                    '<h4 class="modal-title">' + sTitulo + '</h4> ' +
                '</div> ' +
                '<div class="modal-body" style="height: 750px;"> ' + sMensaje + ' </div> ' +
                '<div class="modal-footer"> ' +
                    '<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="' + sFuncion + '"> ' + sNombreBoton + '</button> ' +
                '</div> ' +
            '</div>' +
        '</div>';
		divModal.innerHTML=sHtml;
	};
}

//Mostrar PDF y mostrar modales sobre este.
function dialog_bootPDF(myModal)
{
    var divModal=myModal;
    this.mensajepdf=function(sTitulo,sMensaje,sFuncion,sNombreBoton)
	{
        divModal.style.display = "block";
		divModal.innerHTML="";
		if(sTitulo == 'undefined') sTitulo = '';
		var sHtml = 
        '<div class="modal-dialog modalPDF" style="width: 80%;"> ' +
            '<div class="modal-content"> ' +
                '<div class="modal-header"> ' +
                    '<h4 class="modal-title">' + sTitulo + '</h4> ' +
                '</div> ' +
                '<div class="modal-body" style="height: 750px;"> ' + sMensaje + ' </div> ' +
                '<div class="modal-footer"> ' +
                    '<button id="id-btn-accion" type="button" class="btn btn-primary" data-dismiss="modal" onclick="' + sFuncion + '"> ' + sNombreBoton + '</button> ' +
                '</div> ' +
            '</div>' +
        '</div>';
		divModal.innerHTML=sHtml;
	};

    this.mensajepdf2=function(sTitulo,sMensaje,sFuncion1,sNombreBoton1,sFuncion2,sNombreBoton2)
	{
        divModal.style.display = "block";
		divModal.innerHTML="";
		if(sTitulo == 'undefined') sTitulo = '';
		var sHtml = 
        '<div class="modal-dialog modalPDF" style="width: 80%;"> ' +
            '<div class="modal-content"> ' +
                '<div class="modal-header"> ' +
                    '<h4 class="modal-title">' + sTitulo + '</h4> ' +
                '</div> ' +
                '<div class="modal-body" style="height: 750px;"> ' + sMensaje + ' </div> ' +
                '<div class="modal-footer"> ' +
                    '<button type="button" class="btn btn-warning" data-dismiss="modal" onclick="' + sFuncion2 + '"> ' + sNombreBoton2 + '</button> ' +
                    '<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="' + sFuncion1 + '"> ' + sNombreBoton1 + '</button> ' +
                '</div> ' +
            '</div>' +
        '</div>';
		divModal.innerHTML=sHtml;
	};
}

/***************************************************************
* Acciones
***************************************************************/
function closeNavegador()
{
	//firefox
	if(navigator.appName.indexOf('Netscape') >= 0 )
	{
		//NAVEGADOR FIREFOX
		javascript:window.close();
	}
	else 
	{
		if(navigator.appName.indexOf('Microsoft') >= 0)
		{//internet explorer
			var ventana = window.self;
			ventana.opener = window.self;
			ventana.close();
		}
	}
}