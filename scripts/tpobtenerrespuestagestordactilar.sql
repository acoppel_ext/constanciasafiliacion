-- Type: tpobtenerrespuestagestordactilar

-- DROP TYPE tpobtenerrespuestagestordactilar;

CREATE TYPE tpobtenerrespuestagestordactilar AS
   (selloverificacionbiometricavoluntram bigint,
    diagnosticovoluntram character(30),
	mensaje character(500),
    resultadoverificacionvoluntram character(2),
    banderadiag integer);
ALTER TYPE tpobtenerrespuestagestordactilar
  OWNER TO sysaforeglobal;
