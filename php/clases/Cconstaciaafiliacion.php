<?php
include_once ('clases/global.php');
include_once ('clases/CMetodoGeneral.php');
include_once('ccuentas/validarcliente.php');
include_once ('clases/CServicioBusTramites.php');
include_once ('clases/Capirestconstanciaafiliacion.php');
//include_once("JSON.php");
//var numeroFolioSolConstancia = 0;

$controlador = "CtrlconstanciaAfiliacion";

class Cconstancia
{
	//Funcion que almacenara en la BD los datos del formulario una vez que pasaran correctamente las diferentes validaciones previas.
	//Recibe como parametro los datos a insertar
	//Retorna 1 si se inserto correctamente, 0 si se presento un problema el cual no pudiera insertar los datos.
	public static function  AlmacenaDatosConstacia($iFolioSolicitud,$iEmpleado,$iTipoConstancia,$sCurp,$sAppaterno,$sApmaterno,$sNombre,$sNss,$sNumeroCell,$iCompaniaCel,$sTipoComprobante,$sFechaComprobante,$sNumeroCliente,$iTipoTelefono,$sTelefonoContacto,$iCompTel,$sCalle,$sNumExt,$sNumInt,$sCodPost,$iColonia,$iDelegacion,$iEstado,$iPais,$sLugarSol,$sDesCol,$sDesMun,$sDesEdo,$sDesPais,$iFirma,$dExpediente,$dEnrolamiento,$dVerificacion,$sVerificacion,$vVerificacion,$iFolioBcpl,$sCurpSolicitante,$iTiposolicitante,$iFolioConstanciaImplicaciones, $aforeCedente, $tipoTraspaso, $folioPrevalidador,$iTipoExcepcion,$iFolioConocimientoTraspaso)
	{
		$numerofolio=$iFolioSolicitud;
		$arrData = array();
		$arrData['iEmpleado'] = $iEmpleado;
		$arrData['iTipoConstancia'] = $iTipoConstancia;
		$arrData['sCurp'] = $sCurp;
		$arrData['sAppaterno'] = $sAppaterno;
		$arrData['sApmaterno'] = $sApmaterno;
		$arrData['sNombre'] = $sNombre;
		$arrData['sNss'] = $sNss;
		$arrData['sNumeroCell'] = $sNumeroCell;
		$arrData['iCompaniaCel'] = $iCompaniaCel;
		$arrData['sTipoComprobante'] = $sTipoComprobante;
		$arrData['sTelefonoContacto'] = $sTelefonoContacto;
		$arrData['iTipoTelefono'] = $iTipoTelefono;
		$arrData['iCompTel'] = $iCompTel;
		$arrData['sCalle'] = $sCalle;
		$arrData['sNumExt'] = $sNumExt;
		$arrData['sNumInt'] = $sNumInt;
		$arrData['sCodPost'] = $sCodPost;
		$arrData['iColonia'] = $iColonia;
		$arrData['iDelegacion'] = $iDelegacion;
		$arrData['iEstado'] = $iEstado;
		$arrData['iPais'] = $iPais;
		$arrData['sLugarSol'] = $sLugarSol;
		$arrData['sDesCol'] = $sDesCol;
		$arrData['sDesMun'] = $sDesMun;
		$arrData['sDesEdo'] = $sDesEdo;
		$arrData['sDesPais'] = $sDesPais;
		$arrData['iFirma'] = $iFirma;
		$arrData['dExpediente'] = $dExpediente;
		$arrData['dEnrolamiento'] = $dEnrolamiento;
		$arrData['dVerificacion'] = $dVerificacion;
		$arrData['vVerificacion'] = $vVerificacion;
		$arrData['iFolioBcpl'] = $iFolioBcpl;
		$arrData['iFolioImplicacionTraspaso'] = $iFolioConstanciaImplicaciones;
		$arrData['aforeCedente'] = $aforeCedente;
		$arrData['tipoTraspaso'] = $tipoTraspaso;
		$arrData['iFolioConocimientoTraspaso'] = $iFolioConocimientoTraspaso;
		$arrDatos = array("respuesta"=>0, "curpPromotor"=>"", "descripcion"=>"", "respValDomTien"=>0, "respComtelpromae"=>0,
					"respTel"=>0, "respValTel"=>0, "respValTelTienda"=>0, "estatus"=>0, "inserto"=>0, "folio"=>0);
		$arrCliente = array();
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$bandConta = false;
		$bandFecComp = false;
		$iTienda = 0;
		$curpcol = '';

		//Funcion que permite obtner la siefore
		$iSiefore = Cconstancia::obtenersiefore($sCurp);

		$arrData['iSiefore'] = $iSiefore;

		//funccion que permite obtener la tienda
		$iTienda = Cconstancia::obtenertienda();

		if($iTienda==0){

			$arrTienda = array(
				'iEmpleado' => $iEmpleado
			);

			$iTienda = $objAPI->consumirApi('obtenerTiendaEmpleado', $arrTienda, $GLOBALS["controlador"]);
		}

		$arrData['iTienda'] = $iTienda;
		$objGn->grabarLogx("PARAM iTienda :".$iTienda );
		$mensaje = "Tienda: ".$iTienda;
		//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//Regresa la curp del promotor en caso de que sea un promotor activo
		$objGn->grabarLogx("PARAM EMPLEADO :".$iEmpleado );
		$arrDatos =  $objGn->obtenerCurpPromotor($iEmpleado);
		$sPromotorvalido = $arrDatos['curpPromotor'];
		$mensaje = "Es un Promotor Valido: ".$arrDatos['curpPromotor'];
		//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//Regresa 1 si el domicilio proporcionado pertenece a una tienda
		//regresa 0 si el domicilio no pertenece a una tienda
		$iValDomicilio = Cconstancia::validarDomicilioTiendas($sCalle,$sNumExt,$sCodPost,$sDesCol,$sDesMun,$sDesEdo);
		$arrDatos['respValDomTien'] = $iValDomicilio;
		$mensaje = "Compara Domicilio contra tiendas: ".$iValDomicilio;
		//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);



		if ($sTipoComprobante == 2 || $sTipoComprobante == 3 || $sTipoComprobante == 4 || $sTipoComprobante == 5 || $sTipoComprobante == 6)
		{
			//Validar Tipo de comprobante solo aplica para casos 2(CFE),3(AGUA),5(BANCO),6(TIENDA COPPEL)
				//Regresa 1 cuando la el comprobante de domicilio Si esta vigent
				//Regresa 2 cuando la el comprobante de domicilio No esta vigente
			$iRespCom = Cconstancia::validafechacomprobante($sFechaComprobante);
			$arrDatos['respacomp'] = $iRespCom;
			$mensaje = "Valida Vigencia del comprobante: ".$arrDatos['respacomp'];
			//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);


			//Indica que es un estado de cuenta departamental y debe de validar el numero de cliente
			if ($sTipoComprobante == 6)
			{
				$arrCliente = validarcliente($sNumeroCliente);
				$arrDatos['respcalidaclientecartera'] = $arrCliente['estatus'];
				$mensaje = "Es cliente: ".$arrCliente['estatus']." Nombre:".$arrCliente['descripcion'] . " Error: ".$arrCliente['error'];
			//				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
			else
			{
				//como es un comprobante diferente a tienda departamental CFE,AGUA,Predial,EctaBancario
				//Se pone en uno
				$arrCliente['estatus'] = 1;
			}

			if ($iRespCom == 1 && $arrCliente['estatus'] == 1)
			{
				$bandFecComp =  true;
			}
		}
		else
		{
			//Se pone en 1 directamente cuando el comprobante es IDENTIFICACION INE
			//SE asume que su fecha es valida, debido a que para este comprobante no se VALIDA LA FECHA
			$iRespCom = 1;
			//Cuando el tipo es INE se le pone la fecha de comprobante 1900-01-01 ya que este campo es obligatorio
			$sFechaComprobante = '1900-01-01';
			//Se pone en true, puesto que el tipo de comprobante que se eligio es la CREDENCIAL INE
			//Y no aplica la validacion de vigencia
			$bandFecComp = true;

		}

		//Valida el telefono contra la tabla promaepromotor de informix
			//Devuelve 1 --Si pertenece a un promotor y este no debe de capturarse
			//Devuelve 0 --Si no pertenece a un promotor y debe de capturarse
		$iResComTelPromae = Cconstancia::validadtelPromotores($sNumeroCell);
		$arrDatos['respComtelpromae'] = $iResComTelPromae;
		$mensaje = "compara telefono Trabajador promaepromotor:".$iResComTelPromae;
		//		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);


		//manda llamar al metodo para validar que la curp pertenesca al telefono, de no ser asi
		// se le manda la telefono, curp y opcion(1-tel trabajador, 2- tel contacto)
			//Regresa 0 si no entro por ningun camino
			//Regresa 1 Indica que si hay un registro con ese telefono  y diferente curp por lo tanto no  debe permitir su captura
			//Regresa 2 Indica que No hay un registro con ese curp y telefono, por lo tanto ese telefono le corresponde ala curp y debe de continuar el proceso
		$op = 1;
		$iResValTel = Cconstancia::validadtelefonoSolConstancias($sNumeroCell,$sCurp,$op);
		$arrDatos['respTel'] = $iResValTel;
		$mensaje = "Valida Numero telefono contra solContactos:".$arrDatos['respTel'];
		//		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);


		//Valida el tipo de telefono si es un telefono real o falso
			//Devuelve 1 si es un telefono valido
			//Devuelve 0 si es un telefono invalido
		$iResValTelefono = Cconstancia::validaNumeroTelefono($sNumeroCell);
		$arrDatos['respValTel'] = $iResValTelefono;
		$mensaje = "Valida Numero telefono para ver si es numero valido:".$arrDatos['respValTel'];
		//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//Valida el telefono del trabajador contra los telefonos de la tabla catTelefonosDeTiendas
			//Regresa 1- Si el Telefono proporcionado le pertenece a uan tienda o zapateria
			//Regresa 0- Si el Telefono No le pertenece a la Tienda.
		$iResValTeleTienda = Cconstancia::validadtelefonoTelTiendas($sNumeroCell);
		$arrDatos['respValTelTienda'] = $iResValTeleTienda;
		$mensaje = "Valida Numero telefono vs tienda:".$arrDatos['respValTelTienda'];
			//	$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);


		//Se aplica la validacion de validarlo siempre y cuando la variable traiga algo
		if ($sTelefonoContacto != '')
		{

			//Valida el telefono Contacto contra la tabla promaepromotor de informix
			//Devuelve 1 --Si pertenece a un promotor y este no debe de capturarse
			//Devuelve 0 --Si no pertenece a un promotor y debe de capturarse
			$iResComTelContPromae = Cconstancia::validadtelPromotores($sTelefonoContacto);
			$arrDatos['respComContProm'] = $iResComTelContPromae;
			$mensaje = "compara telefono Contacto promaepromotor:".$iResComTelContPromae;
			//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);


			//manda llamar al metodo para validar que la curp pertenesca al telefono, de no ser asi
			// se le manda la telefono, curp y opcion(1-tel trabajador, 2- tel contacto)
				//Regresa 0 si no entro por ningun camino
				//Regresa 1 Indica que si hay un registro con ese telefono  y diferente curp por lo tanto no  debe permitir su captura
				//Regresa 2 Indica que No hay un registro con ese curp y telefono, por lo tanto ese telefono le corresponde ala curp y debe de continuar el proceso
			$op = 2;
			$iResValTelCont = Cconstancia::validadtelefonoSolConstancias($sTelefonoContacto,$sCurp,$op);
			$arrDatos['respTelCon'] = $iResValTelCont;
			$mensaje = "Valida Numero contacto contra solContactos:".$arrDatos['respTelCon'];
			//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);


			//Valida el tipo de telefono
				//Devuelve 1 si es un telefono valido
				//Devuelve 0 si es un telefono invalido
			$iResValNunlCont = Cconstancia::validaNumeroTelefono($sTelefonoContacto);
			$arrDatos['respValTelCont'] = $iResValNunlCont;
			$mensaje = "Valida Numero contacto para ver si es numero valido:".$arrDatos['respValTelCont'];
			//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

			//Valida el telefono del trabajador contra los telefonos de la tabla catTelefonosDeTiendas
			//Regresa 1- Si el Telefono proporcionado le pertenece a uan tienda o zapateria
			//Regresa 0- Si el Telefono No le pertenece a la Tienda.
			$iResValTeleContTienda = Cconstancia::validadtelefonoTelTiendas($sTelefonoContacto);
			$arrDatos['respValTelContTienda'] = $iResValTeleContTienda;
			$mensaje = "Valida Numero Cont vs tienda:".$arrDatos['respValTelContTienda'];
			//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

			//Bandera que se utiliza para en caso de que se proporcione el telefono del contacto del cliente
			//Este le aplique las mismas validaciones que al celular del trabajador
			// y solamente si es valido los tres filtros esta se activara, lo que indica que es un numero valido
			//para la captura
			if ( $iResComTelContPromae ==0 && $iResValTelCont == 2 && $iResValNunlCont== 1&& $iResValTeleContTienda ==0 )
			{
				$bandConta = true;
			}
		}
		else
		{
			//Pone en vacio la variable para su insercion
			$sTelefonoContacto = '';
			//Se pone la bandera en true, indicando que no se proporciono el numero de celular del contacto
			//Y por lo tanto no se aplica las validaciones a este numero y se activa a true, para su almacenamiento
			$bandConta = true;
		}


		try
		{
			$bandera_caracter='';
			//$arrDatos['estatus'] = OK___;
			$arrDatos['inserto'] =  0;
			$objGn->grabarLogx(' iResComTelPromae '.$iResComTelPromae);																					//esta es tru por que no ingreso contacto
			//iResValTeleTienda 0 iResComTelPromae

			if ($iRespCom != 0 && $iResValTel !=1 && $iResValTelefono ==1 && $iResValTeleTienda ==0  && $iValDomicilio ==2 && $bandConta == true && $bandFecComp == true && $iResComTelPromae == 0 && $sPromotorvalido !='' )
			{
				if ($numerofolio > 0){
					$arrDatos['folio'] =  $numerofolio;
					$arrData['numerofolio'] =  $numerofolio;
					}else{
						$arrResp= Cconstancia::obtenernumerofolio();
						$numerofolio = $arrResp;
						$arrDatos['folio'] =  $numerofolio;
						$arrData['numerofolio'] =  $numerofolio;
					}

				//SE APLICA INSTRUCCION DE TRATADO CORRECTO DE LAS Ñ
				/*$sAppaterno = utf8_decode($sAppaterno);
				$sApmaterno = utf8_decode($sApmaterno);
				$sNombre = utf8_decode($sNombre);
				$sCalle = utf8_decode($sCalle);*/

				//SE AGREGO ESTA VALIDACION PARA LAS AFILIACIONES QUE PRESENTAN ' EN SU NOMBRE/APELLIDOS LAS CUALES SE ENVIARAN CON * PARA QUE LE PERMITA CONTINUAR CON LA AFILIACION 9721206
				//nombre
				$bandera_caracter = strpos($sNombre,"'");
				if ($bandera_caracter!= '')
				{
					$sNombre = str_replace("'", "*",$sNombre);
				}

				//apellidopaterno
				$bandera_caracter = strpos($sAppaterno,"'");
				if ($bandera_caracter!= '')
				{
					$sAppaterno = str_replace("'", "*",$sAppaterno);
				}

				//apellidomaterno
				$bandera_caracter = strpos($sApmaterno,"'");
				if ($bandera_caracter!= '')
				{
					$sApmaterno = str_replace("'", "*",$sApmaterno);
				}
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				if ($sTipoComprobante ==-1)
				{
					$arrDatos['inserto'] =  0;
					$arrDatos['respTipoComprobante'] =  $sTipoComprobante;
					$mensaje =  "No se pudo obtener el tipo de comprobante $sTipoComprobante";
					$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
				}
				else
				{
					$objGn->grabarLogx('Siefore -> ['.$iSiefore.']');
					//En caso de que se abra la conexion, forma la consulta a ejecutar
					$cSql = "select fn_grabar_solconstancia03 from fn_grabar_solconstancia03
							($numerofolio,$iEmpleado,$iSiefore, $iTipoConstancia,'$sCurp','$sAppaterno','$sApmaterno','$sNombre','$sNss','$sNumeroCell',$iCompaniaCel,'$sTipoComprobante','$sFechaComprobante','$sNumeroCliente',
							$iTipoTelefono,'$sTelefonoContacto',$iCompTel,
							'$sCalle','$sNumExt','$sNumInt','$sCodPost',$iColonia,$iDelegacion,$iEstado,$iPais,$sLugarSol,$iTienda,$iFirma,$iFolioBcpl,'$sCurpSolicitante',$iTiposolicitante,'$iFolioConstanciaImplicaciones', '$aforeCedente', $tipoTraspaso, $iTipoExcepcion,$folioPrevalidador,'$iFolioConocimientoTraspaso');";

					$objGn->grabarLogx( '[' . __FILE__ . ']' . $cSql);

					$arrayGuardar['numerofolio'] = $numerofolio;
					$arrayGuardar['iEmpleado'] = $iEmpleado;
					$arrayGuardar['iSiefore'] = $iSiefore;
					$arrayGuardar['iTipoConstancia'] = $iTipoConstancia;
					$arrayGuardar['sCurp'] = $sCurp;
					$arrayGuardar['sAppaterno'] = $sAppaterno;
					$arrayGuardar['sApmaterno'] = $sApmaterno;
					$arrayGuardar['sNombre'] = $sNombre;
					$arrayGuardar['sNss'] = $sNss;
					$arrayGuardar['sNumeroCell'] = $sNumeroCell;
					$arrayGuardar['iCompaniaCel'] = $iCompaniaCel;
					$arrayGuardar['sTipoComprobante'] = $sTipoComprobante;
					$arrayGuardar['sFechaComprobante'] = $sFechaComprobante;
					$arrayGuardar['sNumeroCliente'] = $sNumeroCliente;
					$arrayGuardar['iTipoTelefono'] = $iTipoTelefono;
					$arrayGuardar['sTelefonoContacto'] = $sTelefonoContacto;
					$arrayGuardar['iCompTel'] = $iCompTel;
					$arrayGuardar['sCalle'] = $sCalle;
					$arrayGuardar['sNumExt'] = $sNumExt;
					$arrayGuardar['sNumInt'] = $sNumInt;
					$arrayGuardar['sCodPost'] = $sCodPost;
					$arrayGuardar['iColonia'] = $iColonia;
					$arrayGuardar['iDelegacion'] = $iDelegacion;
					$arrayGuardar['iEstado'] = $iEstado;
					$arrayGuardar['iPais'] = $iPais;
					$arrayGuardar['sLugarSol'] = $sLugarSol;
					$arrayGuardar['iTienda'] = $iTienda;
					$arrayGuardar['iFirma'] = $iFirma;
					$arrayGuardar['iFolioBcpl'] = $iFolioBcpl;
					$arrayGuardar['sCurpSolicitante'] = $sCurpSolicitante;
					$arrayGuardar['iTiposolicitante'] = $iTiposolicitante;
					$arrayGuardar['iFolioImplicacionTraspaso'] = $iFolioConstanciaImplicaciones;
					$arrayGuardar['aforeCedente'] = $aforeCedente;
					$arrayGuardar['tipoTraspaso'] = $tipoTraspaso;
					$arrayGuardar['iTipoExcepcion'] = $iTipoExcepcion;
					$arrayGuardar['folioPrevalidador'] = $folioPrevalidador;
					$arrayGuardar['iFolioConocimientoTraspaso'] = $iFolioConocimientoTraspaso;

					$resultAPI = $objAPI->consumirApi('AlmacenaDatosConstacia', $arrayGuardar, $GLOBALS["controlador"]);

					//Verifica que se haya ejecutado correctamente

					if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
					{
						$arrDatos['inserto'] =  1;
						$mensaje =  "Se Almaceno los datos correctamente";
						$arrDatos['descripcion'] = $mensaje;
						$arrDatos['folio'] =  $numerofolio;
						$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

					}
					else
					{
						$arrDatos['inserto'] =  0;
						$mensaje =  "No se almacenaron los datos";
						$arrDatos['descripcion'] = $mensaje;
						$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

						if(isset($resultAPI["tipo"]))
							header('HTTP/1.1 401 Unauthorized');
						else
							header('HTTP/1.1 409 Conflict');

						return "[opcion 1] Error al consultar la API [Datos] " + json_encode($resultAPI);
					}
				}
			}
			else
			{
				$arrDatos['inserto'] =  0;
				$mensaje =  "No se ejecuto la consulta";
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		    $arrDatos['inserto'] = 0;

		}
		return $arrDatos;
	}

	/*Funcion que permite consultar el catalogo de los diferentes tipos de comprobantes*/
	public static function ConsultacatalogoComprobante()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();

		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('ConsultacatalogoComprobante',$arrDatos,$GLOBALS["controlador"]);

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				
				$arrDatos = $resultAPI;
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 2] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	/*Funcion que permite consultar el catalogo de los diferentes tipos solicitantes
	No recibe parametros y retorna el arreglo con los datos de los solicitantes*/

	public static function ConsultacatalogoSolicitante()
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrDatos = array();

		try
		{
			$resultAPI = $objAPI->consumirApi('ConsultacatalogoSolicitante', $arrDatos, $GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				
				$arrDatos = $resultAPI;
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 46] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	/*Funcion que permite consultar el catalogo de los diferentes tipos de compañias de celulares
	No recibe parametros y retorna el arreglo con los datos de la compañia de celulares*/
	public static function ConsultacatalogoCompaCelulares()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();

		//manda llamar al metodo donde se obtiene la ip, bd, usuario y password
		//se llenan variables define para su posterior uso
		//Cconstancia::getOpenedConecctionPostgre();
		try
		{
			$resultAPI = $objAPI->consumirApi('ConsultacatalogoCompaCelulares',$arrDatos,$GLOBALS["controlador"]);
			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				
				$arrDatos['registros'] = $resultAPI['registros'];
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 3 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//metodo que permite consultar en la tabla sol
	public static function Consultacurp($sCurp,$iTipoConstancia)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrCurp = array(
				'sCurp' => $sCurp
		);

		//Ejecuta la API
		$resultAPI = $objAPI->consumirApi('Consultacurp',$arrCurp,$GLOBALS["controlador"]);

		//Verifica que se haya ejecutado correctamente
		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			//indicador que asigna estatus 1, osea correctamente y su descripcion
			$arrDatos['estado'] = 1;
			$arrDatos['descripcion'] = "EXITO";
			

			foreach($resultAPI['registros'] as $reg)
			{
				$arrDatos['registros'][]=array_map('trim', $reg);
				$arrDatos['fecha'] = $reg['fecha'];
				$arrDatos['curp'] = $reg['curp'];
				$arrDatos['estatusvig'] = $reg['estatus'];
				$arrDatos['diagnostico'] = trim($reg['diagnostico']);
				$objGn->grabarLogx("4");

				//En caso de ser rechazada, se va a obtener el numero de dias partiendo de un control pharcode
				if ($reg['estatus']  == 2)
				{
					//Obtener numero de dias
					$iVigencia = Cconstancia::obtenervidencia($iTipoConstancia);
					$arrDatos['diasvigencia'] = $iVigencia;
				}
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 4 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}
		return $arrDatos;
	}

	public static function ConsultaRechazoCurp ($sCurp)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrCurp = array(
				'sCurp' => $sCurp
		);

		$resultAPI = $objAPI->consumirApi('ConsultaRechazoCurp',$arrCurp,$GLOBALS["controlador"]);
		//Verifica que se haya ejecutado correctamente
		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			//indicador que asigna estatus 1, osea correctamente y su descripcion
			$arrDatos['estado'] = 1;
			$arrDatos['descripcion'] = "EXITO";
			

			foreach($resultAPI['registros'] as $reg)
			{
				$arrDatos['registros'][]=array_map('trim', $reg);
				$arrDatos['estatus'] = (int)$reg['estatus'];
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 5 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}
		return $arrDatos;
	}

	//Funcion que permite cargar el catalogo de lugar de la solicitud
	public static function ConsultacatalogoLugarSolicitud()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrData = array();
		//$iRespuesta = 0;
		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('ConsultacatalogoLugarSolicitud',$arrData,$GLOBALS["controlador"]);

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				

				foreach($resultAPI['registros'] as $reg)
				{
					$arrDatos['registros'][]=array_map('trim', $reg);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 6 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//funcion que permite validar la fecha del comprobante contra la fecha del servidor
	public static function validafechacomprobante($sFechaComprobante)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrData = array(
				'sFechaComprobante' => $sFechaComprobante
		);

		$iRespuesta = 0;

		$resultAPI = $objAPI->consumirApi('validafechacomprobante',$arrData,$GLOBALS["controlador"]);
		//Verifica que se haya ejecutado correctamente
		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			//indicador que asigna estatus 1, osea correctamente y su descripcion
			$arrDatos['estatus'] = OK___;
			$arrDatos['descripcion'] = "EXITO";
			
			$arrDatos['registros'] = $resultAPI['registros'];

			//$arrDatos['fecha'] = $resulSet['sfechaalta'];
			foreach($resultAPI['registros'] as $reg)
			{
				$arrDatos['registros'][]=array_map('trim', $reg);
				$iRespuesta = $reg['respuesta'];
			}
		}
		else
		{
			if(isset($resultAPI["tipo"])){
				header('HTTP/1.1 401 Unauthorized');
				return "[opcion ] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}	
		}
		return $iRespuesta;
	}

	//funcion que permite validar el numero de telefono para ver si es real o ficticio
	public static function validaNumeroTelefono($sNumeroCell)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		//declara un arreglo
		$arrDatos = array();

		$iRespuesta = 0;

		$telefono =  substr($sNumeroCell, 0,6) ;

		$arrData = array(
		'sNumeroCell' => $telefono
		);

		//Ejecuta la consulta
		$resultAPI = $objAPI->consumirApi('validaNumeroTelefono',$arrData,$GLOBALS["controlador"]);
		//Verifica que se haya ejecutado correctamente
		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			//indicador que asigna estatus 1, osea correctamente y su descripcion
			$arrDatos['estatus'] = OK___;
			$arrDatos['descripcion'] = "EXITO";
			

			//$arrDatos['fecha'] = $resulSet['sfechaalta'];
			foreach($resultAPI['registros'] as $reg)
			{
				$iRespuesta = $reg['respuesta'];
			}
		}
		else
		{
			if(isset($resultAPI["tipo"])){
				header('HTTP/1.1 401 Unauthorized');
				return "[opcion validaNumeroTelefono] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		return $iRespuesta;
	}

	//funcion que permite validar el numero telefonico contra la solcontancias y esta no le pertenesca a otra curp
	public static function validadtelefonoSolConstancias($sTel,$sCurp,$op)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrData['sTel'] = $sTel;
		$arrData['sCurp'] = $sCurp;
		$arrData['op'] = $op;
		//$iRespuesta = 0;
		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('validadtelefonoSolConstancias',$arrData,$GLOBALS["controlador"]);

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				

				foreach($resultAPI['registros'] as $reg)
				{
					$arrDatos['registros'][]=array_map('trim', $reg);
					$iRespuesta = (int)$reg['respuesta'];
					$objGn->grabarLogx('Resultado de la API para mandar a respTel-> ['.$iRespuesta.']');
				}
			}
			else
			{
				if(isset($resultAPI["tipo"])){
					header('HTTP/1.1 401 Unauthorized');
					return "[opcion validadtelefonoSolConstancias] Error al consultar la API [Datos] " + json_encode($resultAPI);
				}	
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $iRespuesta;
	}

	//funcion que permite validar el numero telefonico contra la tabla catTelefonosDeTiendas.
	// esta retorna un 1 cuando existe y un 0 cuando no existe en la tabla catTelefonosDeTiendas
	public static function validadtelefonoTelTiendas($sTel)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		//declara un arreglo
		$arrDatos = array();
		$arrData = array(
				'sTel' => $sTel
		);

		$iRespuesta = 0;

		//Ejecuta la consulta
		$resultAPI = $objAPI->consumirApi('validadtelefonoTelTiendas',$arrData,$GLOBALS["controlador"]);

		//Verifica que se haya ejecutado correctamente
		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			//indicador que asigna estatus 1, osea correctamente y su descripcion
			$arrDatos['estatus'] = OK___;
			$arrDatos['descripcion'] = "EXITO";
			

			foreach($resultAPI['registros'] as $reg)
			{
				$iRespuesta = $reg['respuesta'];
			}
		}
		else
		{
			if(isset($resultAPI["tipo"])){
				header('HTTP/1.1 401 Unauthorized');
				return "[opcion validadtelefonoTelTiendas] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}

		return $iRespuesta;
	}

	//funcion que abre conexion a informix para validar la informacion contra los de los promotores
	public static function validadtelPromotores($sTel)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrData['sTel'] = $sTel;
		//$iRespuesta = 0;
		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('validadtelPromotores',$arrData,$GLOBALS["controlador"]);

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				

				foreach($resultAPI['registros'] as $reg)
				{
					$iRespuesta = $reg['IRESPUESTA'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"])){
					header('HTTP/1.1 401 Unauthorized');
					return "[opcion validadtelPromotores] Error al consultar la API [Datos] " + json_encode($resultAPI);
				}
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $iRespuesta;
	}

	/*
	Funcion que permite validar la direccion proporcionada por el cliente contra el catalogo
	de direcciones de tiendas y zapaterias coppel canada
		Recibe como parametro:
		-La calle del trabajador
		-Numero exterior del trabajador
		-codigo postal del trabajador
		-Nombre de colonia, muinicipio, estado del trabajador
		Regresa  la funcion un 1 si encontro un domicilio con los parametros proporcionados,
				indicando que ese domicilio le pertenece a una tienda o zapateria.
		Regresa 0 el domicilio proporcionado no le pertenece a una tienda.
	*/

	public static function validarDomicilioTiendas($sCalle,$sNumExt,$sCodPost,$sDesCol,$sDesMun,$sDesEdo)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrData = array();

		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('validarDomicilioTiendas',$arrData,$GLOBALS["controlador"]);

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				

				foreach($resultAPI as $reg)
				{
					$arrDatos['registros'][]=array_map('trim', $reg);
					$iRespuesta = (int)$reg['respuesta'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"])){
					header('HTTP/1.1 401 Unauthorized');
					return "[opcion ] Error al consultar la API [Datos] " + json_encode($resultAPI);
				}				
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $iRespuesta;
	}

	//Funcion que permite obtener el numero de dias de vigencia apartir de un control
	//Recibe el tipo de solicitud
	//Retorna el numero de dias de vigencia dependiendo el tipo de solicitud
	public static function obtenervidencia($tiposolicitud)
	{

		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrData = array();

		$iRespuesta = 0;
		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('obtenervidencia',$arrData,$GLOBALS["controlador"]);

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				

				foreach($resultAPI as $reg)
				{
					$arrDatos['registros'][]=array_map('trim', $reg);
					$iRespuesta = (int)$reg['tipo4'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion obtenervidencia] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $iRespuesta;
	}

	//Genera el numero de folio con el cual se insertara en la base de datos
	public static function obtenernumerofolio()
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrData = array();
		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('obtenernumerofolio',$arrData,$GLOBALS["controlador"]);

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				

				foreach($resultAPI['registros'] as $reg)
				{
					$iRespuesta = $reg['numerofolio'];
					$arrDatos['respuesta'] = (int)$iRespuesta;
				}
			}
			else
			{
				if(isset($resultAPI["tipo"])){
					header('HTTP/1.1 401 Unauthorized');
					return "[opcion 15 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
				}	
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $iRespuesta;
	}

	public static function obtenersiefore($sCurp)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrData['sCurp'] = $sCurp;
		$iRespuesta = 0;
		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('obtenersiefore',$arrData,$GLOBALS["controlador"]);

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				
				$iRespuesta = (int)$resultAPI['siefore'];
				$objGn->grabarLogx('Siefore -> ['.$iRespuesta.'] Result API  Siefore-> ['.$resultAPI['siefore'].']');
			}
			else
			{
				if(isset($resultAPI["tipo"])){
					header('HTTP/1.1 401 Unauthorized');
					return "[opcion ] Error al consultar la API [Datos] " + json_encode($resultAPI);
				}
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $iRespuesta;
	}

	public static function obtenerestatusempleado($iEmpleado)
	{

		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrData['iEmpleado'] = $iEmpleado;

		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('obtenerestatusempleado',$arrData,$GLOBALS["controlador"]);

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['descripcion'] = "EXITO";
				
				$arrDatos['valor'] = (int)$resultAPI['valor'];
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 13] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//funcion para retornar la curp del empleado que esta enrolando
	public static function obtenercurpempleado($iEmpleado)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();
		$arrCurp = array(
				'iEmpleado' => $iEmpleado
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenercurpempleado");

			$resultAPI = $objAPI->consumirApi('obtenercurpempleado',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg)
				{
					$arrDatos["curpE"] = trim($reg['curp']);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 14] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
			
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//Funcion que permite obtener la clave consar y el nombre completo del promotor
	public static function obtenerdatospromotor($iEmpleado)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
    	$arrDatos = array();
		$arrCurp = array(
				'iEmpleado' => $iEmpleado
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerdatospromotor");

			$resultAPI = $objAPI->consumirApi('obtenerdatospromotor',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				//$arrDatos['registros'] = $resultAPI['registros'];

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos["nombre"] = html_entity_decode($key['nombre']);
					$arrDatos["claveconsar"] = trim($key['claveconsar']);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 7 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//Funcion que permite validar si hay un registro en la tabla colsolicitudes con esa curp y ciertos criterios
	public static function consultacolsolicitudes($sCurp)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array('curp' => '');
		$arrCurp = array(
			'sCurp' => $sCurp
		);

		try
		{
			$objGn->grabarLogx("Inicia API Rest");

			$resultAPI = $objAPI->consumirApi('consultacolsolicitudes',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos["curp"] = trim($key['curp']);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');

				return $arrDatos;
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//CAMBIO: TAMBIEN REVISA EL TIPO DE SOLICITUD QUE TIENE GUARDADO EN BD.
	//SI ES INDEPENDIENTE O ISSSTE, QUE PERMITA AFILIAR UN IMSS
	public static function consultatipoconstancia($sCurp)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array('tiposolicitud' => 0);
		$arrCurp = array(
			'sCurp' => $sCurp
		);

		try
		{

			$objGn->grabarLogx("Inicia API Rest");

			$resultAPI = $objAPI->consumirApi('consultatipoconstancia',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos["tiposolicitud"] = trim($key['tiposolicitud']);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				

				return $arrDatos;
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function obtenertienda()
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

	  	$arrDatos = array();
		$iNumeroTienda=0;

		try
		{
			$ipTienda = Cconstancia::obtIpTienda();
			$arrCurp = array(
			'ipTienda' => $ipTienda
			);

			$resultAPI = $objAPI->consumirApi('obtenertienda',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				
				foreach($resultAPI["registros"] as $reg  => $key)
				{
					if($key['tipo3']>0)
					{
						$arrDatos['estatus'] = 1;
						$arrDatos['descripcion'] = "EXITO";
						$arrDatos["iTienda"] = $key['tipo3'];
						$iNumeroTienda = $key['tipo3'];
					}
					else
					{
						$arrDatos['estatus'] = ERR__;
						$arrDatos['error'] = '[hardcode] no se encontro tienda asignada en tabla de control Afore.';
						$arrDatos['descripcion'] = "TIENDA NO ESTA REGISTRADA EN CONTROL AFORE";
					}
				}
			}
			else
			{
				if(isset($resultAPI["tipo"])){
					header('HTTP/1.1 401 Unauthorized');
					return "[opcion obtenertienda] Error al consultar la API [Datos] " + json_encode($resultAPI);
				}
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $iNumeroTienda;
	}

	public static function obtIpTienda()
	{
		$ipCliente = "";

	  if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) )

		$ipCliente = $_SERVER['HTTP_X_FORWARDED_FOR'];

	  else

		$ipCliente = $_SERVER['REMOTE_ADDR'];

	  return $ipCliente;
	}

	//Funcion que permite validar la informacion de la tabla solconstancia apartir de la curp y el estatus proceso.
	public static function consultainformacionsolconstancia($sCurp,$iEstatusProceso)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos['descolonia'] = '';
		$arrDatos['desmunicipio'] = '';
	 	$arrDatos['desestado'] = '';

		$arrCurp = array(
				'scurp' => $sCurp,
				'iEstatusProceso' => $iEstatusProceso,
		);

		try
		{
		  	$objGn->grabarLogx("Inicia API Rest");

			$resultAPI = $objAPI->consumirApi('consultainformacionsolconstancia',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos['tipoconstancia']	= trim($key['tipoconstancia']);
					$arrDatos['folio']	= trim($key['folio']);
					$arrDatos['numempleado']	= trim($key['numempleado']);
					$arrDatos['tipoconstancia']	= trim($key['tipoconstancia']);
					$arrDatos['curp']	= trim($key['curp']);

					$arrDatos['appaterno']	= utf8_encode(trim($key['appaterno']));
					$arrDatos['apmaterno']	= utf8_encode(trim($key['apmaterno']));
					$arrDatos['nombres']	= utf8_encode(trim($key['nombres']));

					$arrDatos['nss']	= trim($key['nss']);
					$arrDatos['telefonocelular']	= trim($key['telefonocelular']);
					$arrDatos['compatelefonica']	= trim($key['compatelefonica']);
					$arrDatos['tipocomprobante']	= trim($key['tipocomprobante']);
					$arrDatos['fechacomprobante']	= trim($key['fechacomprobante']);
					$arrDatos['noclientecoppel']	= trim($key['noclientecoppel']);
					$arrDatos['tipotelefono']	= trim($key['tipotelefono']);
					$arrDatos['telefonocontacto']	= trim($key['telefonocontacto']);
					$arrDatos['comptelefonicacontacto']	= trim($key['comptelefonicacontacto']);
					$arrDatos['calle']	= utf8_encode( trim($key['calle']));
					$arrDatos['numexterior']	= trim($key['numexterior']);
					$arrDatos['numinterior']	= trim($key['numinterior']);
					$arrDatos['codigopostal']	= trim($key['codigopostal']);
					$arrDatos['colonia']	= trim($key['colonia']);
					$arrDatos['delegmunic']	= trim($key['delegmunic']);
					$arrDatos['estado']	= trim($key['estado']);
					$arrDatos['pais']	= trim($key['pais']);
					$arrDatos['lugarsolicitud']	= trim($key['lugarsolicitud']);
					$arrDatos['tipotraspaso']	= $key['idtipotraspaso'];
					$arrDatos['estadocivil']	= $key['estadocivil'];

					if ($arrDatos['codigopostal'] !='' && $arrDatos['codigopostal']>0)
					{
						$var = Cconstancia::consultainformaciondireccionsolconstancia($arrDatos['codigopostal'],$arrDatos['colonia'],$arrDatos['delegmunic'],$arrDatos['estado']);

						$arrDatos['descolonia'] = $var['descolonia'];
						$arrDatos['desmunicipio'] = $var['desmunicipio'];
						$arrDatos['desestado'] = $var['desestado'];
					}
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 10 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//Funcion que permite validar la informacion de la tabla solconstancia apartir de la curp y el estatus proceso.
	public static function consultainformaciondireccionsolconstancia($sCodigopostal,$idColonia,$idDelegacion,$idEstado)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array();
		$arrCurp = array(
				'sCodigopostal' => $sCodigopostal,
				'idColonia' => $idColonia,
				'idDelegacion' => $idDelegacion,
				'idEstado' => $idEstado
		);

		try
		{
			$objGn->grabarLogx("Inicia API Rest");

			$resultAPI = $objAPI->consumirApi('consultainformaciondireccionsolconstancia',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos['descolonia']	= trim($key['descolonia']);
					$arrDatos['desmunicipio']	= trim($key['desmunicipio']);
					$arrDatos['desestado']	= trim($key['desestado']);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion consultainformaciondireccionsolconstancia] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}

    	return $arrDatos;
	}

	public static function consultaInvocacionServicio($sCurp)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrCurp = array(
				'sCurp' => $sCurp
		);

		try
		{

			$objGn->grabarLogx("Inicia API Rest");

			$resultAPI = $objAPI->consumirApi('consultaInvocacionServicio',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['registros'] = $resultAPI['registros'];

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos['estatuscontingente']	= trim($key['estatus']);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 12] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//Metodo para ejecutar el servicio de consulta biometrica
	public static function ejecutarAplicacionConstanciaConsulta($sCurpEmpleado, $sCurpTrabajador, $tPersona, $idServicio, $idServidor, $idOperacion)
	{
		//se crea arreglo a retornar
		$arrResp = array();

		//se crea objeto para invocar los metodos de la clase
		$objGn = new CMetodoGeneral();

		//se declaran varibles en vacio y 0 por si falla la incacion tenga algo por default
		$arrResp->descripcionRespuesta= '';
		$arrResp->folioServicioAfore = 0;
		$arrResp->respondioServicio = 0;

		//se obtine la fecha del servidor
		date_default_timezone_set('America/Mazatlan');
		setlocale(LC_TIME, 'spanish');

		//Se crea array a mandar
		$array = array(
			    'identificationIDControlIdentification' => $idOperacion,
			    'identificationIDUnoContentSummary' => 0, //SMS
			    'recordCategoryTypeContentSummaryTypeUno' => 2,
			    'identificationIDDosContentSummary' => 1,
			    'recordCategoryTypeContentSummaryTypeDos' => 14,
			    'recordCategoryTypeDescriptiveTextRecord' => 2,
			    'identificationIDDescriptiveTextRecord' => 0,
			    'curpEmpleado' => $sCurpEmpleado,
			    'curpTrabajador' => $sCurpTrabajador,
			    'recordCategoryCodeFingerPrintImage' => 14,
			    'identificationIDFingerPrintImageRecord' => 1,
				'tieneHuella' => 1
			    );

		//Crear XML a enviar de parametros
		$xml = $objGn->obtenerXML($array);

		$mensaje = "XML:".$xml;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//Ejecutar el Servicio
		$arrResp = $objGn->consumirServicioEjecutarAplicacion($idServicio, $idServidor, $xml);

		//pasa la respuesta obtenida del metodo anterior
		$mensaje = $arrResp->descripcionRespuesta;
		$folioServicioAfore = $arrResp->folioServicioAfore;
		$respuestaservicio = $arrResp->respondioServicio;
		return $arrResp;
	}

	//Metodo para ejecutar el servicio de verificacion biometrica
	public static function ejecutarAplicacionConstanciaVerificacion($sCurpEmpleado, $sCurpTrabajador, $tPersona, $idServicio, $idServidor, $idOperacion, $Seleccionado, $Motivo, $codigo)
	{
		//se crea arreglo a retornar
		$arrResp = array();

		//se crea objeto para invocar los metodos de la clase
		$objGn = new CMetodoGeneral();

		//se declaran varibles en vacio y 0 por si falla la incacion tenga algo por default
		$arrResp->descripcionRespuesta= '';
		$arrResp->folioServicioAfore = 0;
		$arrResp->respondioServicio = 0;
		$arrResp->respondioEI = 0;

		//se obtine la fecha del servidor
		date_default_timezone_set('America/Mazatlan');
		setlocale(LC_TIME, 'spanish');

		//Se crea array a mandar
		$array = array(
			    'identificationIDControlIdentification' => $idOperacion,
			    'identificationIDUnoContentSummary' => 0, //SMS
			    'recordCategoryTypeContentSummaryTypeUno' => 2,
			    'identificationIDDosContentSummary' => 1,
			    'recordCategoryTypeContentSummaryTypeDos' => 14,
			    'recordCategoryTypeDescriptiveTextRecord' => 2,
			    'identificationIDDescriptiveTextRecord' => 0,
			    'curpEmpleado' => $sCurpEmpleado,
			    'curpTrabajador' => $sCurpTrabajador,
			    'recordCategoryCodeFingerPrintImage' => 14,
			    'identificationIDFingerPrintImageRecord' => 1,
			    'tieneHuella' =>$Seleccionado,
			    'motivoSinHuella' =>$Motivo,
			    'fingerMissingCode' =>$codigo
			    );

		//Crear XML a enviar de parametros
		$xml = $objGn->obtenerXML($array);

		$mensaje = "XML:".$xml;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//Ejecutar el Servicio
		$arrResp = $objGn->consumirServicioEjecutarAplicacion($idServicio, $idServidor, $xml);

		//pasa la respuesta obtenida del metodo anterior
		$mensaje = $arrResp->descripcionRespuesta;
		$folioServicioAfore = $arrResp->folioServicioAfore;
		$respuestaservicio = $arrResp->respondioServicio;
		$respuestaEI = $arrResp->respondioEI;

		$mensajelog = "Mensaje:".$mensaje.'FolioAfore:'.$folioServicioAfore."RespServicioInv:".$respuestaservicio."RespServicioEI:".$respuestaEI;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensajelog);

		return $arrResp;
	}

	//Metodo para obtener los estatus, diagnsticos, y el sello de verificacion de las consultas biometricas.
	public static function consultadatosbiometricos($consultafolio, $verificacionFolio)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array();
		$arrCurp = array(
				'consultafolio' => $consultafolio,
				'verificacionFolio' => $verificacionFolio
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('consultadatosbiometricos',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos['sConsulEstatus'] = trim($key['cestatus']);
					$arrDatos['sConsulDiagnostico'] = trim($key['cdiagnosticos']);
					$arrDatos['sVerifiEstatus'] = trim($key['vestatus']);
					$arrDatos['sVerifiDiagnostico'] = trim($key['vdiagnosticos']);
					$arrDatos['sEstatusVerifica'] = trim($key['cestatusexpediente']);
					$arrDatos['sEnrolamiento'] = trim($key['cestatusenrolamiento']);
					$arrDatos['iSelloVerificacion'] = $key['vselloverificacion'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				
				$arrDatos['estatus'] = ERR__;
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function actualizarSolConstancia($folioactualizar,$estatusconsulta, $estatusexpedienteconsulta, $estatusenrolamientoconsulta, $diagnosticoconsulta, $estatusverificacion, $diagnosticoverificacion, $selloverificacion)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("iValorConsulta"=>0);
		$arrCurp = array(
				'folioactualizar' => $folioactualizar,
				'estatusconsulta' => $estatusconsulta,
				'estatusexpedienteconsulta' => $estatusexpedienteconsulta,
				'estatusenrolamientoconsulta' => $estatusenrolamientoconsulta,
				'diagnosticoconsulta' => $diagnosticoconsulta,
				'estatusverificacion' => $estatusverificacion,
				'diagnosticoverificacion' => $diagnosticoverificacion,
				'selloverificacion' => $selloverificacion
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('actualizarSolConstancia',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos['iValorConsulta'] = $key['valor'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 19 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//Metodo para obtener el enlace de la pagina del enrolamiento trabajador
	public static function obtenerenlace()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$ipTienda = '';

		$iOpcion = 5;
		$iFolio = 0;
		$iTestigo = 0;
		$iNumTestigo = 0;
		$ipTienda = Cconstancia::obtIpTienda();

		$arrDatos = array();

		$arrCurp = array(
				'iOpcion' => $iOpcion,
				'iFolio' => $iFolio,
				'iTestigo' => $iTestigo,
				'iNumTestigo' => $iNumTestigo,
				'ipTienda' => $ipTienda
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('obtenerenlace',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				//$arrDatos['registros'] = $resultAPI['registros'];

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos['dato'] = $key['dato'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 20] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//Metodo para obtener el estatus enrolamiento
	public static function obtenerestatusEnrolamiento($sCurp, $iFolioAfore)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("iEstatusEnrolamiento"=>0);
		$arrCurp = array(
			"sCurp"=>$sCurp,
			"iFolioAfore"=>$iFolioAfore
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('obtenerestatusEnrolamiento',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos['iEstatusEnrolamiento'] = $key['valor'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 21] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//Metodo para obtener los errores de los diagnosticos de las consultas biometricas
	public static function dianosticoErrores($diagnostico)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("mensajeretorno"=>'');
		$arrCurp = array(
				"diagnostico"=>$diagnostico
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('dianosticoErrores',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos['mensajeretorno'] = $key['valor'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 23] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function obtenerRespuestaEnrolHuellaServicio($folioOperacionC, $folioOperacionV, $sCurp, $idServicioC, $idServicioV, $idOperacionC, $idOperacionV)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrDatos = array("sEstatus"=>'');
		$arrApi = array(
					"folioOperacionC"  => $folioOperacionC,
					"folioOperacionV"  => $folioOperacionV,
					"sCurp"  => $sCurp,
					"idServicioC"  => $idServicioC,
					"idServicioV"  => $idServicioV,
					"idOperacionC"  => $idOperacionC,
					"idOperacionV" => $idOperacionV);

		try
		{
			$objGn->grabarLogx("Ejecucion de API Rest");

			//Se ejecuta la API para obtener una respuesta en rol huella servicio

			$resultAPI = $objAPI->consumirApi("obtenerRespuestaEnrolHuellaServicio",$arrApi,$GLOBALS["controlador"]);

			

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI['registros'] as $reg => $key)
				{
					$arrDatos["sEstatus"] = $key['dato'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 24] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function grabarLogJs($estatusconsulta, $diagnosticoconsulta, $estatusverificacion, $diagnosticoverificacion, $estatusexpedienteconsulta, $estatusenrolamientoconsulta, $selloverificacion, $iTipoConstancia)
	{
		$cSql = '';
		$objGn = new CMetodoGeneral();
		$arrDatos = array();
		$arrDatos['estatus'] = _DEFAULT_;

		$cSql = "grabarLogJs-> Estatus Consulta: $estatusconsulta, Diagnostico Consulta: $diagnosticoconsulta, Estatus Verificacion: $estatusverificacion, Diagnostico Verificacion: $diagnosticoverificacion, Estatus Expediente: $estatusexpedienteconsulta, Estatus Enrolamiento: $estatusenrolamientoconsulta, Sello Verificacion: $selloverificacion, Tipo de Solicitud: $iTipoConstancia";

		$objGn->grabarLogx($cSql);

		if($cSql != '')
		{
			$arrDatos['estatus'] = OK___;
			$arrDatos['descripcion'] = "EXITO";
		}
		else
		{
			$arrDatos['estatus'] = ERR__;
		}

		return $arrDatos;
	}

	//Metodo para obtener la respuesta de los servicios de BanCoppel mediante el folio que nos regresa la aplicacion
	//HTCONSULTABCPL.EXE
	public static function obtenermensajerespuestaAU($folioretorno)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("iOperacionBancoppel"=>0, "sCodigoBancoppel"=>'', "sMensajeBancoppel"=>'');

		$arrApi['folioretorno'] = (int)$folioretorno;

		try
		{
			//graba en Log ejecucion de apo rest
			$objGn->grabarLogx("Ejecucion de API Rest");

			//Se lanza la peticion a la API Rest para obtener los datos
			$resultAPI = $objAPI->consumirApi('obtenermensajerespuestaAU',$arrApi,$GLOBALS["controlador"]);
			

			//Se verifica si no hay errores al consultar la API
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['estatus'] = OK___;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($resultAPI['registros'] as $resultado => $key)
				{
					$arrDatos["iOperacionBancoppel"] = $key['tipooperacion'];
					$arrDatos["sCodigoBancoppel"] = trim($key['codigo']);
					$arrDatos["sMensajeBancoppel"] = $key['mensaje'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 27] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//Metodo para obtener la informacion del cliente Bancoppel, una vez terminado el consumo del servicio de BanCoppel y que todo
	//Salio bien
	public static function obteneriformacionclientebancoppel($keyxbancoppel)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi['keyxbancoppel'] = (int)$keyxbancoppel;

		$arrDatos = array("curpinfbancoppel" => '',"nombreinfbancoppel" => '',"appinfbancoppel" => '',"apminfbancoppel" => '',"telefonoinfbancoppel" => '',"numclienteinfbancoppel" => ''
		,"telcontactoinfbancoppel" => '',"calleinfbancoppel" => '',"nexteriorinfbancoppel" => '',"ninteriorinfbancoppel" => '',"codpostalinfbancoppel" => '',"coloniainfbancoppel" => ''
		,"delmuninfbancoppel" => '',"estadoinfbancoppel" => '',"ciudadinfbancoppel" => '',"calle1infbancoppel" => '',"nexterior1infbancoppel" => '',"ninterior1infbancoppel" => ''
		,"codpostal1infbancoppel" => '',"colonia1infbancoppel" => '',"delmun1infbancoppel" => '',"estado1infbancoppel" => '',"ciudad1infbancoppel" => '',
		"iIdentificadorColonia"=>0,"iIdentificadorMuni"=>0,"iIdentificadorEstado"=>0,"iIdentificadorColonia1"=>0,"iIdentificadorMuni1"=>0,"IdentificadorColonia1"=>0,"anverso"=>'',"reverso"=> '');

		try
		{
			//graba en Log
			$objGn->grabarLogx("Ejecucion de API Rest");

			//Se lanza la consulta para la API rest
			$resultAPI = $objAPI->consumirApi('obteneriformacionclientebancoppel',$arrApi,$GLOBALS["controlador"]);
			

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos["curpinfbancoppel"] = trim($key['curp']);
					$arrDatos["nombreinfbancoppel"] = trim($key['nombre']);
					$arrDatos["appinfbancoppel"] = trim($key['app']);
					$arrDatos["apminfbancoppel"] = trim($key['apm']);
					$arrDatos["telefonoinfbancoppel"] = trim($key['telefonocelular']);
					$arrDatos["numclienteinfbancoppel"] = trim($key['numclientecoppel']);

					//Datos del contacto
					$arrDatos["telcontactoinfbancoppel"] = trim($key['telcontacto']);

					//Direccion del cliente
					$arrDatos["calleinfbancoppel"] = trim($key['calle']);
					$arrDatos["nexteriorinfbancoppel"] = trim($key['nexterior']);
					$arrDatos["ninteriorinfbancoppel"] = trim($key['ninterior']);
					$arrDatos["codpostalinfbancoppel"] = trim($key['codpostal']);
					$arrDatos["coloniainfbancoppel"] = trim($key['colonia']);
					$arrDatos["delmuninfbancoppel"] = trim($key['delmun']);
					$arrDatos["estadoinfbancoppel"] = trim($key['estado']);
					$arrDatos["ciudadinfbancoppel"] = trim($key['ciudad']);


					//Direccion auxiliar del cliente
					$arrDatos["calle1infbancoppel"] = trim($key['calle1']);
					$arrDatos["nexterior1infbancoppel"] = trim($key['nexterior1']);
					$arrDatos["ninterior1infbancoppel"] = trim($key['ninterior1']);
					$arrDatos["codpostal1infbancoppel"] = trim($key['codpostal1']);
					$arrDatos["colonia1infbancoppel"] = trim($key['colonia1']);
					$arrDatos["delmun1infbancoppel"] = trim($key['delmun1']);
					$arrDatos["estado1infbancoppel"] = trim($key['estado1']);
					$arrDatos["ciudad1infbancoppel"] = trim($key['ciudad1']);

					$arrDatos["iIdentificadorColonia"] = $key['idcolonia'];
					$arrDatos["iIdentificadorColonia1"] = $key['idcolonia1'];

					$arrDatos["iIdentificadorMuni"] = $key['iddelmun'];
					$arrDatos["iIdentificadorMuni1"] = $key['iddelmun1'];

					$arrDatos["iIdentificadorEstado"] = $key['idestado'];
					$arrDatos["iIdentificadorEstado1"] = $key['idestado1'];

					$arrDatos["anverso"] = $key['idanverso'];
					$arrDatos["reverso"] = $key['idreverso'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 28] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function actualizarflagclientebcpl($keyxbancoppel)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("estatus" => 0);

		$arrApi['keyxbancoppel'] = (int)$keyxbancoppel;

		try
		{
			$objGn->grabarLogx("Ejecucion de API Rest");

			//Se hace la peticion a la API Rest para hacer la consulta
			$resultAPI = $objAPI->consumirApi('actualizarflagclientebcpl',$arrApi,$GLOBALS["controlador"]);
			

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg => $key)
				{
					$arrDatos['estatus'] = (int)$key["iretorono"];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 30] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function validacion($keyxbancoppel)
	{
		$sImagen = "";
		$sArchivo = "";
		$iAncho = 0;
		$iAlto = 0;
		$arrResp = array("estatus"=>0, "descripcion"=> '');
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$sRutaImagenes = "/sysx/progs/web/constanciasafiliacion/imagen/";

		$arrApi['keyxbancoppel'] = (int)$keyxbancoppel;

		try
		{
			//graba en Log
			$objGn->grabarLogx("Ejecucion de API Rest");

			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('validacion',$arrApi,$GLOBALS["controlador"]);
			

			//Verifica que se haya ejecutado correctamente
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				foreach($resultAPI['registros'] as $reg => $key)
				{
					$arrResp["anverso"] = $key['idanverso'];
					$arrResp["reverso"] = $key['idreverso'];
				}

				if ($arrResp["anverso"] != '' && $arrResp["reverso"] != '')
				{
					$sArchivo = "imagen"."_"."1".".tif";
					$sImagen = base64_decode($arrResp["anverso"]);
					$fs = fopen('../imagen/'. $sArchivo , "w");
					fwrite($fs, $sImagen);
					fclose($fs);
					$image = new Imagick($sRutaImagenes . $sArchivo);
					$image->setImageFormat("png");

					$iAlto = 650;
					$iAncho = 1020;

					$imgRectInteres = clone($image);
					$imgRectInteres->cropImage($iAncho,$iAlto,0,0);
					$imgRectInteres->resizeImage($iAncho/4,$iAlto/4,Imagick::FILTER_BOX,1);
					$imgRectInteres->writeImage($sRutaImagenes . "imagen" .'_'. "1" . '.png');

					$imgRectInteres->clear();

					$sArchivo = "imagen"."_"."2".".tif";
					$sImagen = base64_decode($arrResp["reverso"]);
					$fs = fopen('../imagen/'. $sArchivo , "w");
					fwrite($fs, $sImagen);
					fclose($fs);
					//$image = new Imagick();
					$image = new Imagick($sRutaImagenes . $sArchivo);
					$image->setImageFormat("png");
					$iAlto = 650;
					$iAncho = 1020;

					$imgRectInteres = clone($image);
					$imgRectInteres->cropImage($iAncho,$iAlto,0,0);
					$imgRectInteres->resizeImage($iAncho/4,$iAlto/4,Imagick::FILTER_BOX,1);
					$imgRectInteres->writeImage($sRutaImagenes . "imagen" .'_'. "2" . '.png');

					$arrResp['estatus'] = 1;
					$arrResp['descripcion'] = "EXITO";
				}
			}
			else
			{
				$arrResp['estatus'] = -1;
				$arrResp['descripcion'] = "ANVERSO Y REVERSO VACIOS";

			   if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 31] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}

		return $arrResp;
	}

	/***** consulta la respuesta del servicio de RENAPO *****/
	public static function consultaRespuestaRenapo($iFolioAfore)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		//SE DECLARAN LOS ARREGLOS
		$arrApi = array();
		$arrDatos = array("estatus"=>0,
					  "MensajeDescrip"=>"",
					  "tipoerror"=>-1,
					  "codigoerrorrenapo"=>-1,
					  "descerror"=>""
					   );

		$arrApi['iFolioAfore'] = (int)$iFolioAfore;

		try
		{
			$objGn->grabarLogx("Ejecucion de API Rest");
			//Se hace la peticion a la API para obtener los datos
			$resultAPI = $objAPI->consumirApi('consultaRespuestaRenapo',$arrApi,$GLOBALS["controlador"]);
			

			//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//INDICADOR QUE ASGNA ESTATUS 1, OSEA QUE ES CORRECTO Y SU DESCRIPCION
				$arrDatos['estatus'] = 1;
				$arrDatos['MensajeDescrip'] = "EXITO";

				foreach($resultAPI["registros"] as $reg => $key)
				{
					$arrDatos['tipoerror']= $key["idu_tipoerror"];
					$arrDatos['codigoerrorrenapo']= $key["idu_codigoerrorrenapo"];
					$arrDatos['descerror']= trim($key["mensaje"]);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 32] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function consultarDatosRenapo($iFolioAfore)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//SE DECLARA UN ARREGLO
		$arrApi = array();
		$arrDatos = array(	"cCurp"				=> "",
							"cApellidopaterno"	=> "",
							"cApellidomaterno"	=> "",
							"cNombres"			=> "",
							"iRespuestaRen"		=> -1
						);

		$arrApi['iFolioAfore'] = (int)$iFolioAfore;

		try
		{
			$objGn->grabarLogx("Ejecucion de API Rest");
			$resultAPI = $objAPI->consumirApi('consultarDatosRenapo',$arrApi,$GLOBALS["controlador"]);
			

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				foreach($resultAPI["registros"] AS $registro => $key)
				{
					$arrDatos['iRespuestaRen'] = $key['icoderror'];
					$arrDatos['cCurp'] = $key['curp'];
					//SE APLICA INSTRUCCION DE TRATADO CORRECTO DE LAS Ñ
					$arrDatos['cApellidopaterno'] = strpos(strtoupper($key['apellidopaterno']), 'Ñ')===false ? $key['apellidopaterno'] : utf8_decode($key['apellidopaterno']);
					$arrDatos['cApellidomaterno'] = strpos(strtoupper($key['apellidomaterno']), 'Ñ')===false ? $key['apellidomaterno'] : utf8_decode($key['apellidomaterno']);
					$arrDatos['cNombres'] = strpos(strtoupper($key['nombres']), 'Ñ')===false ? $key['nombres'] : utf8_decode($key['nombres']);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 33] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function validarCurpenReanpo($sCurp)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		//se declara el array
		$arrApi = array();
		$arrApi['sCurp'] = $sCurp;

		//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE
		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('validarCurpenReanpo',$arrApi,$GLOBALS["controlador"]);

		

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['iRespuesta'] = $reg['respuesta'];
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 34] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function actualizarFolRenapo($iFolioCons, $iFolioAfore)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		//se declara el array
		$arrApi['iFolioCons'] = $iFolioCons;
		$arrApi['iFolioAfore'] = $iFolioAfore;

		//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE
		$resultAPI = $objAPI->consumirApi('actualizarFolRenapo',$arrApi,$GLOBALS["controlador"]);

		

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['iRespuesta'] = (int)$reg['respuesta'];
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 35] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function obtenerLigaRenapo($iOpcionRenapo,$iFolioAfore)
	{

		$objGn = new CMetodoGeneral();
		$iPmodulo = $objGn->getRealIP();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		//se declara el array
		$arrApi['iOpcionRenapo'] = $iOpcionRenapo;
		$arrApi['iFolioAfore'] = $iFolioAfore;
		$arrApi['iPmodulo'] = $iPmodulo;

		//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE
		$resultAPI = $objAPI->consumirApi('obtenerLigaRenapo',$arrApi,$GLOBALS["controlador"]);

		

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['cLiga'] = trim($reg['liga']);

			}
			$objGn->grabarLogx('retorno la liga se contectara a: '.$arrDatos['cLiga']);
			$arrDatos['estatus'] = 1;
			$arrDatos['MensajeDescrip'] = "EXITO";
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 36] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function validarListaNegraTelefonoCel($sNumeroCell, $iEmpleado, $sCurp)
	{

		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrCurp = array(
				'sNumeroCell' => $sNumeroCell,
				'iEmpleado' => $iEmpleado,
				'sCurp' => $sCurp
		);

	 	$arrDatos['mensaje'] ='';

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('validarListaNegraTelefonoCel',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos['mensaje'] = $key['mensaje'];
					$arrDatos['identificador'] = $key['identificador'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 29] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function validarListaNegraTelefonofijo($sTelefonoContacto, $iEmpleado, $sCurp)
	{

		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		//$arrTelefonos = array($sNumeroCell=>"0");
    	$arrCurp = array(
				'sTelefonoContacto' => $sTelefonoContacto,
				'iEmpleado' => $iEmpleado,
				'sCurp' => $sCurp
		);
	  	$arrDatos['mensaje'] ='';

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('validarListaNegraTelefonofijo',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				//$arrDatos['registros'] = $resultAPI['registros'];

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos['mensaje'] = $key['mensaje'];
					$arrDatos['identificador'] = $key['identificador'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 39] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//OPCION 39
	/*Funcion que permite consultar el catalogo de Estado Civil*/
	public static function Consultacatalogoestadocivil()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();

		try
		{
			//graba en Log
			$objGn->grabarLogx("Ejecucion de API Rest");

			$resultAPI = $objAPI->consumirApi('Consultacatalogoestadocivil',$arrDatos,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['registros'] = $resultAPI['registros'];
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 40] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//OPCION 40
	/*Funcion que permite actualizar de Estado Civil*/
	public static function ActualizarEstadoCivil( $ifolioactualizaestadocivil, $iestadocivil)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi['ifolioactualizaestadocivil'] = (int)$ifolioactualizaestadocivil;
		$arrApi['iestadocivil'] = (int)$iestadocivil;

		$arrDatos = array("ifolioactualizaestadocivil","iestadocivil");
		try
		{
			//Verifica que se haya ejecutado correctamente
			$resultAPI = $objAPI->consumirApi('ActualizarEstadoCivil',$arrApi,$GLOBALS["controlador"]);
			

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = OK___;
				$arrDatos['descripcion'] = "EXITO";

			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 41] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//Opcion 42
	//funcion que permite validar el numero de telefono celular si es movil o fijo.
	public static function validaTelefonoCelular($sNumeroCell)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi['sNumeroCell'] = $sNumeroCell;

		$arrDatos = array("sNumeroCell");
		try
		{
			//graba en Log
			$objGn->grabarLogx("Incio API Rest validaTelefonoCelular");

			//Verifica que se haya ejecutado correctamente
			$resultAPI = $objAPI->consumirApi('validaTelefonoCelular',$arrApi,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				
				$objGn->grabarLogx("resultAPI validaTelefonoCelular");
				foreach($resultAPI['registros'] AS $reg)
				{
					$arrDatos['irespuesta'] = $reg['iestatuscelular'];
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = OK___;
					$arrDatos['descripcion'] = "EXITO";
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 42] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//FOLIO 94.M_1_PI
	public static function validarregistroprevio($cCurp)
	{

		$objApi = new Capirestconstanciaafiliacion();
		$arrData = array ('cCurp' => $cCurp);

		$arrDatos = array(
				'apellidopaterno'		=> '',
				'apellidomaterno'		=> '',
				'nombre'				=> '',
				'nss' 					=> '',
				'celular'				=> '',
				'companiacel'			=> '0',
				'comprobante'			=> '0',
				'fechacomprobante' 		=> '1900-01-01',
				'numclientecop'			=> '0',
				'tipotelefono'			=> '0',
				'telofonodecontacto'	=> '',
				'companiatelefono'		=> '0',
				'calle'					=> '',
				'numeroexterior'		=> '',
				'numerointerior'		=> '',
				'codpostal'				=> '',
				'colonia'				=> '',
				'municipio'				=> '',
				'estado'				=> '',
				'pais'					=> '',
				'lugarsolicitud'		=> 0,
				'estadocivil'			=> 0,
				'icolonia'				=> 0,
				'imunicipio'			=> 0,
				'iestado'				=> 0,
				'ipais'					=> 0
			);

			try
			{
				$resultAPI = $objApi->consumirApi('validarregistroprevio',$arrData,$GLOBALS["controlador"]);

				

				if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
				{
					foreach($resultAPI AS $reg)
					{
						$arrDatos['apellidopaterno']		= utf8_encode(trim($reg['apellidopaterno']));
						$arrDatos['apellidomaterno']		= utf8_encode(trim($reg['apellidomaterno']));
						$arrDatos['nombre']					= utf8_encode(trim($reg['nombre']));
						$arrDatos['nss'] 					= utf8_encode(trim($reg['nss']));
						$arrDatos['celular']				= utf8_encode(trim($reg['celular']));
						$arrDatos['companiacel']			= utf8_encode(trim($reg['companiacel']));
						$arrDatos['comprobante']			= utf8_encode(trim($reg['comprobante']));
						$arrDatos['fechacomprobante'] 		= utf8_encode(trim($reg['fechacomprobante']));
						$arrDatos['numclientecop']			= utf8_encode(trim($reg['numclientecop']));
						$arrDatos['tipotelefono']			= utf8_encode(trim($reg['tipotelefono']));
						$arrDatos['telofonodecontacto']		= utf8_encode(trim($reg['telofonodecontacto']));
						$arrDatos['companiatelefono']		= utf8_encode(trim($reg['companiatelefono']));
						$arrDatos['calle']					= utf8_encode(trim($reg['calle']));
						$arrDatos['numeroexterior']			= utf8_encode(trim($reg['numeroexterior']));
						$arrDatos['numerointerior']			= utf8_encode(trim($reg['numerointerior']));
						$arrDatos['codpostal']				= utf8_encode(trim($reg['codpostal']));
						$arrDatos['colonia']				= utf8_encode(trim($reg['colonia']));
						$arrDatos['municipio']				= utf8_encode(trim($reg['municipio']));
						$arrDatos['estado']					= utf8_encode(trim($reg['estado']));
						$arrDatos['pais']					= utf8_encode(trim($reg['pais']));
						$arrDatos['lugarsolicitud']			= utf8_encode(trim($reg['lugarsolicitud']));
						$arrDatos['estadocivil']			= utf8_encode(trim($reg['estadocivil']));
						$arrDatos['icolonia']				= (int)$reg['icolonia'];
						$arrDatos['imunicipio']				= (int)$reg['imunicipio'];
						$arrDatos['iestado']				= (int)$reg['iestado'];
						$arrDatos['ipais']					= (int)$reg['ipais'];
					}
				}
				else
				{
					if(isset($resultAPI["tipo"]))
						header('HTTP/1.1 401 Unauthorized');
					else
						header('HTTP/1.1 409 Conflict');

					return "[opcion 43] Error al consultar la API [Datos] " + json_encode($resultAPI);
				}
			}
			catch (Exception $e)
			{
				header('HTTP/1.1 500 Internal Server Error');
				$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
				return $mensaje;
			}
		return ($arrDatos);
	}

	public static function registrarDatosPrevalidador($data)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
    		'apellidoPaterno' => $data["apellidoPaterno"],
			'apellidoMaterno' => $data["apellidoMaterno"],
			'nombres' => $data["nombres"],
			'curpTrabajador' => $data["curpTrabajador"],
			'curpPromotor' => $data["curpPromotor"],
			'nss' => $data["nss"],
			'selloVerificacion' => $data["selloVerificacion"],
			'numeroEmpleado' => $data["numeroEmpleado"]
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest registrarDatosPrevalidador");

			$resultAPI = $objAPI->consumirApi('registrarDatosPrevalidador',$arrData,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos["sValRespuesta"] = "OK";
					$arrDatos["folioSolicitud"] = $key['foliosolicitud'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');

				$arrDatos['estatus'] = ERR__;
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function respuestaPrevalidador($idServidor, $datos)
	{
		$idServicio = 9941;
		$objGn = new CMetodoGeneral();

		$arrData = array(
			'folioSolicitud' => $datos['folioSolicitud'],
			'numeroEmpleado' => $datos['numeroEmpleado']
		);

		//Crear XML a enviar de parametros
		$dataXML = $objGn->obtenerXML($arrData);

		$mensaje = "XML:".$dataXML;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//Ejecutar el Servicio
		$arrResp = $objGn->consumirServicioEjecutarAplicacion($idServicio, $idServidor, $dataXML);

		//pasa la respuesta obtenida del metodo anterior
		$mensaje = $arrResp->descripcionRespuesta;
		$folioServicioAfore = $arrResp->folioServicioAfore;
		$respuestaservicio = $arrResp->respondioServicio;

		return $arrResp;
	}

	// PETICION 72.1 Desarrollador: Ralbery Rodriguez*/
	public static function obtenerRespuestaPrevalidador($data)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();
		$arrCurp = array(
			'iFolio' => $data['folio']
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerRespuestaPrevalidador");

			$resultAPI = $objAPI->consumirApi('obtenerRespuestaPrevalidador',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos["sValRespuesta"] = "OK";
					$arrDatos["iValMarcas"] = $key['valmarcas'];
					$arrDatos["iValFolioConstanciaImplicaciones"] = $key['valfoliosconstanciasimplicaciones'];
					$arrDatos["iValRegimenPensionario"] = $key['valregimenpensionario'];
					$arrDatos["iValNumTraspTreintaseisMeses"] = $key['valnumtrasptreintaseismeses'];
					$arrDatos["iValCitactiva"] = $key['valcitactiva'];
					$arrDatos["iValRendimmenor"] = $key['valrendimmenor'];
					$arrDatos["traspasopreviorendimientos"] = $key['traspasopreviorendimientos'];
					$arrDatos["valcurpfuncionarioadministradora"] = $key['valcurpfuncionarioadministradora'];
					$arrDatos["foliosolicitud"] = $key['foliosolicitud'];
					$arrDatos["iValCurpTrabajador"] = $key['valcurptrabajador'];
					$arrDatos["iValNssTrabajador"] = $key['valnsstrabajador'];
					$arrDatos["iValApellidoPaterno"] = $key['valapellidopaterno'];
					$arrDatos["iValApellidoMaterno"] = $key['valapellidomaterno'];
					$arrDatos["iValNombreTrab"] = $key['valnombretrab'];
					$arrDatos["cveenttransbd"] = $key['cveenttransbd'];
					$arrDatos["motrec"] = $key['motrec'];
					$arrDatos["folioservicioafore"] = $key['folioservicioafore'];
					$arrDatos["clavepeticionservicio"] = $key['clavepeticionservicio'];
					$arrDatos["sCodigoError"] = $key['codigo_error'];
					$arrDatos["curp_promotor"] = $key['curp_promotor'];
					$arrDatos["fecha1traspasoprevio"] = $key['fecha1traspasoprevio'];
					$arrDatos["fecha2traspasoprevio"] = $key['fecha2traspasoprevio'];
					$arrDatos["fecha3traspasoprevio"] = $key['fecha3traspasoprevio'];
					$arrDatos["fecha4traspasoprevio"] = $key['fecha4traspasoprevio'];
					$arrDatos["fecha5traspasoprevio"] = $key['fecha5traspasoprevio'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				
				$arrDatos['estatus'] = ERR__;
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function obtenerSelloPrevalidador($sCurpEmpleado, $sCurpTrabajador, $tPersona, $idServicio, $idServidor, $idOperacion)
	{
		//se crea arreglo a retornar
		$arrResp = array();

		//se crea objeto para invocar los metodos de la clase
		$objGn = new CMetodoGeneral();
		/*$idOperacion = '0501';
		$idServicio = 9911;
		$tPersona = 2;*/
		//se declaran varibles en vacio y 0 por si falla la incacion tenga algo por default
		$arrResp->descripcionRespuesta= '';
		$arrResp->folioServicioAfore = 0;
		$arrResp->respondioServicio = 0;

		//se obtine la fecha del servidor
		date_default_timezone_set('America/Mazatlan');
		setlocale(LC_TIME, 'spanish');

		//Se crea array a mandar
		$array = array(
			    'identificationIDControlIdentification' => $idOperacion,
			    'identificationIDUnoContentSummary' => 0, //SMS
			    'recordCategoryTypeContentSummaryTypeUno' => 2,
			    'identificationIDDosContentSummary' => 1,
			    'recordCategoryTypeContentSummaryTypeDos' => 14,
			    'recordCategoryTypeDescriptiveTextRecord' => 2,
			    'identificationIDDescriptiveTextRecord' => 0,
			    'curpEmpleado' => $sCurpEmpleado,
			    'curpTrabajador' => $sCurpTrabajador,
			    'recordCategoryCodeFingerPrintImage' => 14,
			    'identificationIDFingerPrintImageRecord' => 1,
				'tieneHuella' => 1
			    );

		//Crear XML a enviar de parametros
		$xml = $objGn->obtenerXML($array);

		$mensaje = "XML:".$xml;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//Ejecutar el Servicio
		$arrResp = $objGn->consumirServicioEjecutarAplicacion($idServicio, $idServidor, $xml);

		return $arrResp;
	}

	public static function ejecutaPrevalidacion()
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array('estatus' => 0);

		try
		{
			$objGn->grabarLogx("Inicio API Rest ejecutaPrevalidacion");

			$resultAPI = $objAPI->consumirApi('ejecutaPrevalidacion',$arrData,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos["estatus"] = $key['estatus'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 68] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}
	//FUNCION QUE PERMITE VALIDAR EL EXPEDIENTE DE IDENTIFICACIÓN
	public static function validarExpediente($sCurp,$sIpRemoto)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi = array('sCurp' => $sCurp,
						'sIpRemoto' => $sIpRemoto
		);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('validarExpediente',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["error"] 	= $reg["error"];
				$arrDatos["mensaje"] = TRIM($reg["mensaje"]);
				$arrDatos["redireccionar"] 	= TRIM($reg["redireccionar"]);
			}
			$objGn->grabarLogx("[Error: ] -->".$arrDatos["error"]." [Mensaje: ] -->".$arrDatos["mensaje"]." [Redireccionar: ] -->".$arrDatos["redireccionar"]);
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion ] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function grabarExcepcion($excepcion,$iFolioCons,$iEmpleado)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi = array('excepcion' => $excepcion,
						'iFolioCons' => $iFolioCons,
						'iEmpleado' => $iEmpleado
		);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('grabarExcepcion',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] 	= $reg["irespuesta"];
			}
			$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["respuesta"] 	= 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 45] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}
		return $arrDatos;
	}

	public static function ConsultarDialogoVideo($TipoDialogo)
	{
        //CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$iPmodulo = $objGn->getRealIP();

		$arrApi = array('TipoDialogo' => $TipoDialogo);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('ConsultarDialogoVideo',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["tiempominimo"] = $reg["tiempominimo"];
				$arrDatos["tiempomaximo"] = $reg["tiempomaximo"];
				$arrDatos["formato"] = $reg["formato"];
				$arrDatos["calidad"] = $reg["calidad"];
				$arrDatos["dialogo"] = $reg["dialogo"];
				$arrDatos["ipmodulo"] = $iPmodulo;
				$arrDatos["respuesta"] 	= 1;
			}

			$objGn->grabarLogx("se obtuvieron el tiempó minimo, maximo y dialogo");
			$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["respuesta"] 	= 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 47] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function ObtenerUrlsVideo()
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$iPmodulo = $objGn->getRealIP();

		$arrApi = array('iPmodulo' => $iPmodulo);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('ObtenerUrlsVideo',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["urlgrabado"] = $reg["urlgrabado"];
				$arrDatos["urlguardado"] = $reg["urlguardado"];
				$arrDatos["respuesta"] 	= 1;
			}

			$objGn->grabarLogx("se obtuvieron la url de grabado y la url de guardado del video");
			$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["respuesta"] 	= 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 48] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function fnobtenerDatosCapturaVideo($iFolioCons, $iEmpleado)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi = array('iFolioCons' => $iFolioCons,
						'iEmpleado' => $iEmpleado
		);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('fnobtenerDatosCapturaVideo',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["tipoconstancia"] = $reg["tipoconstancia"];
				$arrDatos["ciudad"] = $reg["ciudad"];
				$arrDatos["nombrepromotor"] = $reg["nombrepromotor"];
				$arrDatos["apellidopaternoprom"] = $reg["apellidopaternoprom"];
				$arrDatos["apellidomaternoprom"] = $reg["apellidomaternoprom"];
				$arrDatos["claveconsar"] = $reg["claveconsar"];
				$arrDatos["nombretrabajador"] = $reg["nombretrabajador"];
				$arrDatos["apellidopaternotrab"] = $reg["apellidopaternotrab"];
				$arrDatos["apellidomaternotrab"] = $reg["apellidomaternotrab"];
				$arrDatos["curptrabajador"] = $reg["curptrabajador"];
				$arrDatos["telefonocelular"] = $reg["telefono"];
				$arrDatos["aforeactual"] = $reg["aforeactual"];
				$arrDatos["nombresolicitante"] 	= $reg["nombresolicitante"];
				$arrDatos["apellidopaternosol"] = $reg["apellidopaternosol"];
				$arrDatos["apellidomaternosol"] = $reg["apellidomaternosol"];
				$arrDatos["curpsolicitante"] = $reg["curpsolicitante"];
				$arrDatos["dia"] = $reg["dia"];
				$arrDatos["mes"] = $reg["mes"];
				$arrDatos["anio"] = $reg["anio"];
				$arrDatos["respuesta"] = 1;
			}

			$objGn->grabarLogx("se obtuvieron los datos del grabador de video");
			$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["respuesta"] 	= 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 49] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}
		return $arrDatos;
	}

	public static function obtenerEstatusVideo($iFolioCons)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi = array('iFolioCons' => $iFolioCons);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('obtenerEstatusVideo',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["estatus"] = $reg["estatus"];
			}
			if($arrDatos["estatus"] == 1)
			{
				$objGn->grabarLogx("se obtuvo el estatus del video grabado");
				$objGn->grabarLogx("[estatus: ] -->".$arrDatos["estatus"]);
			}
			else
			{
				$objGn->grabarLogx("no se pudo obtuvo el estatus del video grabado");
				$objGn->grabarLogx("[estatus: ] -->".$arrDatos["estatus"]);
			}
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["respuesta"] 	= 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 50] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function validarenrolamientoConstancia($sCurp)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi = array('sCurp' => $sCurp);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('validarenrolamientoConstancia',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["estado"] = $reg["estado"];
			}
			if($arrDatos["estado"] == 1)
			{
				$objGn->grabarLogx("se obtuvo el estado del enrolamiento");
				$objGn->grabarLogx("[estado: ] -->".$arrDatos["estado"]);
			}
			else
			{
				$objGn->grabarLogx("no se pudo obtuvo el estado del enrolamiento");
				$objGn->grabarLogx("[estatus: ] -->".$arrDatos["estatus"]);
			}
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["estado"] = 0;

		    if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 51] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function guardarVideoAfiliacion($iFolioCons,$nombreVideo)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$iPmodulo = $objGn->getRealIP();

		$arrApi = array('iFolioCons' => $iFolioCons,
						'nombreVideo' => $nombreVideo,
						'iPmodulo' => $iPmodulo,
	    );

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('guardarVideoAfiliacion',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $reg["respuesta"];
			}
			if($arrDatos["respuesta"] == 1)
			{
				$objGn->grabarLogx("se genero el registro del video");
				$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
			}
			else
			{
				$objGn->grabarLogx("no se pudo generar el registro del video");
				$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
			}
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["respuesta"] = 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 52] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function obtenerClaveOperacion()
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$iPmodulo = $objGn->getRealIP();

		$arrApi['iPmodulo'] = $iPmodulo;

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('obtenerClaveOperacion',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["ClaveOP"] = trim($reg["claveop"]);
				$arrDatos["respuesta"] 	= 1;

			}
			$objGn->grabarLogx("se obtuvó la clave de operación");
			$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["respuesta"] 	= 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 53] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function obtenerestatusfinado($iFolioCons)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi['iFolioCons'] = $iFolioCons;

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('obtenerestatusfinado',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["esfinado"] = $reg["estatus"];

			}
			$objGn->grabarLogx("[esfinado: ] -->".$arrDatos["estatus"]);
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 54] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function actualizarestatusvideo($iFolioCons, $estatusvideo)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi = array('iFolioCons' => $iFolioCons,
						'estatusvideo' => $estatusvideo
	    );

		$objGn->grabarLogx("Ejecucion de API Rest [actualizarestatusvideo]");
		$objGn->grabarLogx("iFolioCons: ".$arrApi['iFolioCons']." estatusvideo: ".$arrApi['estatusvideo']);

		$resultAPI = $objAPI->consumirApi('actualizarestatusvideo',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $reg["respuesta"];
			}
			if($arrDatos["respuesta"] == 1)
			{
				$objGn->grabarLogx("se actualizó el estatus del video");
				$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
			}
			else
			{
				$objGn->grabarLogx("no se pudo actualizar el estatus del video");
				$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
			}
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["respuesta"] = 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 55] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function obtenerIpCluster()
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$iPmodulo = $objGn->getRealIP();

		$arrApi['iPmodulo'] = $iPmodulo;

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('obtenerIpCluster',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["ipcluster"] 	= trim($reg["ipoffline"]);
				$arrDatos["respuesta"] 	= 1;

			}
			$objGn->grabarLogx("se obtuvó la ip de offline");
			$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["respuesta"] 	= 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 56] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function verificarEnrolamiento($iFolioSolicitud)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrDatos['estatus'] = _DEFAULT_;

		$arrApi['iFolioSolicitud'] = $iFolioSolicitud;

		$objGn->grabarLogx("Ejecucion de API Rest");
		$objGn->grabarLogx($iFolioSolicitud);

		$resultAPI = $objAPI->consumirApi('verificarEnrolamiento',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $reg['respuesta'];

			}
			$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 57] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}
	//funcion que actualiza el estatus del Enrolamiento del Trabajador
	public static function actualizarEstatusEnrolamiento($iFolioEnrolamiento,$iEstatusEnrolamiento,$sCurp)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi = array('iFolioEnrolamiento' => $iFolioEnrolamiento,
						'iEstatusEnrolamiento' => $iEstatusEnrolamiento,
						'sCurp' => $sCurp
	    );

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('actualizarEstatusEnrolamiento',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $reg["respuesta"];
			}
			if($arrDatos["respuesta"] == 1)
			{
				$objGn->grabarLogx("se actualizó el estatus del enrolamiento");
				$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
			}
			else
			{
				$objGn->grabarLogx("no se pudo actualizar el estatus del enrolamiento");
				$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
			}
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos["respuesta"] = 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 58] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function  guardarcontrolenviofolio($iFolioSolicitud,$IdentificadorEnvio)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrApi = array('iFolioSolicitud' => $iFolioSolicitud,
						'IdentificadorEnvio' => $IdentificadorEnvio
		);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('guardarcontrolenviofolio',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $reg['respuesta'];

			}
			$objGn->grabarLogx("se guardo el control de envio del folio");
			$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos['respuesta'] = 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 59] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function  fnactualizadatossolicitanterenapo($nombreSolicitante,$apellidoPatSolicitante,$apellidoMatSolicitante,$iFolioCons)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrApi= array('nombreSolicitante' => $nombreSolicitante,
				'apellidoPatSolicitante' => $apellidoPatSolicitante,
				'apellidoMatSolicitante' => $apellidoMatSolicitante,
				'iFolioCons' => $iFolioCons
		);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('fnactualizadatossolicitanterenapo',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $reg['respuesta'];

			}
			$objGn->grabarLogx("se guardo los datos del solicitante");
			$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos['respuesta'] = 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 60] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function  ejecutaServicioFolio($iFolioCons,$iEmpleado,$serviciofolio)
	{
		//se crea arreglo a retornar
		$arrResp = array();
		$idServidorFolio = 7;
		//se crea objeto para invocar los metodos de la clase
		$objGn = new CMetodoGeneral();

		//se declaran varibles en vacio y 0 por si falla la incacion tenga algo por default
		$arrResp->descripcionRespuesta= '';
		$arrResp->folioServicioAfore = 0;
		$arrResp->respondioServicio = 0;
		$arrResp->respondioEI = 0;

		//se obtine la fecha del servidor
		date_default_timezone_set('America/Mazatlan');
		setlocale(LC_TIME, 'spanish');

		//Se crea array a mandar
		$array = array(
			    'folioSolicitud' => $iFolioCons,
			    'numeroEmpleado' => $iEmpleado
			    );

		//Crear XML a enviar de parametros
		$xml = $objGn->obtenerXML($array);

		$mensaje = "XML:".$xml;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		$act = self::ActualizaestatusSolconstancia($iFolioCons);
		//Ejecutar el Servicio
		$arrResp = $objGn->consumirServicioEjecutarAplicacion($serviciofolio, $idServidorFolio, $xml);

		//pasa la respuesta obtenida del metodo anterior
		$mensaje = $arrResp->descripcionRespuesta;
		$folioServicioAfore = $arrResp->folioServicioAfore;
		$respuestaservicio = $arrResp->respondioServicio;
		//$respuestaEI = $arrResp->respondioEI;

		$mensajelog = "Mensaje:".$mensaje.'FolioAfore:'.$folioServicioAfore."RespServicioInv:".$respuestaservicio;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensajelog);

		return $arrResp;
	}


	//funcion que permite actualizar el campo estatusproceso a 3, que es enviado a procesar
	function ActualizaestatusSolconstancia($iFolioCons)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi= array('iFolioCons' => $iFolioCons);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('ActualizaestatusSolconstancia',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $reg['respuesta'];

			}
			if ($arrDatos['respuesta'] == 1)
			{
				$mensaje = "Se Actualizo el estatusproceso a 3 para el folio: ".$iFolioCons;
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos['respuesta'] = 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion ] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function  validarRespuestaFolio($iFolioCons)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi = array('iFolioCons' => $iFolioCons);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('validarRespuestaFolio',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $reg['respuesta'];
				$arrDatos["mensaje"] = trim($reg["mensaje"]);
			}
			$objGn->grabarLogx("obteniendo respuesta del folio");
			$objGn->grabarLogx("[respuesta: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos['respuesta'] = 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 62] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function  generarFolioEnrolamiento($iFolioCons,$iEmpleado,$sCurp,$iTipoConstancia)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi = array('iFolioCons' => $iFolioCons,
				'iEmpleado' => $iEmpleado,
				'sCurp' => $sCurp,
				'iTipoConstancia' => $iTipoConstancia
		);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('generarFolioEnrolamiento',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $reg['ires'];
			}
			$objGn->grabarLogx("se genero registro en la tabla de enrolamiento para el tipo solicitante");
			$objGn->grabarLogx("[folio de enrolamiento: ] -->".$arrDatos["respuesta"]);
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos['respuesta'] = 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 63] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}

	public static function actualizarestatusprocesosolconstancia($iFolioCons)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrApi = array();

		$arrApi = array('iFolioCons' => $iFolioCons);

		$objGn->grabarLogx("Ejecucion de API Rest");

		$resultAPI = $objAPI->consumirApi('actualizarestatusprocesosolconstancia',$arrApi,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $reg['respuesta'];

			}
			if ($arrDatos['respuesta'] == 1)
			{
				$mensaje = "Se Actualizo el estatusproceso a 4 para el folio: ".$iFolioCons." de la tabla solconstancia";
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
		}
		else
		{
			// Si existe un error en la consulta mostrará el siguiente mensaje
			$arrDatos['estatus'] = -1;
			$arrDatos['respuesta'] = 0;

			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 64] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}
		return $arrDatos;
	}

	public static function obtenerMensajeErrorPrevalidador($cCodigo)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("mensaje"=>'');
		$arrCurp = array(
				"cCodigo"=>$cCodigo
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('obtenerMensajeErrorPrevalidador',$arrCurp,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg)
				{
					$arrDatos['mensaje'] = $reg['mensaje'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');

				$arrDatos['estatus'] = ERR__;
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function obtenerBandMensajePromotor()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();

		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('obtenerBandMensajePromotor',$arrDatos,$GLOBALS["controlador"]);
			
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
					
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";
					$arrDatos['valor'] = $resultAPI['valor'];
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 71] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function actualizarBandMensajePromotor($foliosolicitud, $valor)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$arrCurp = array(
			"foliosolicitud"=>$foliosolicitud,
			"valor"=>$valor
		);

		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('actualizarBandMensajePromotor',$arrCurp,$GLOBALS["controlador"]);
			
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $resultAPI['respuesta'];
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 72] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function obtenerLigaIne($iEmpleado,$sCurp,$pasoRenapo,$iAutenticacion)
	{
		$objGn = new CMetodoGeneral();
		$iPmodulo = $objGn->getRealIP();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("mensaje"=>'');
		$arrData = array(
			"empleado" => $iEmpleado,
			"curp" => $sCurp,
			"pasoRenapo" => $pasoRenapo,
			"ipModulo" => $iPmodulo,
			"iAutenticacion" => $iAutenticacion
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('obtenerLigaIne',$arrData,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['liga'] = str_replace("\\", "", trim($resultAPI["registros"]["liga"]));
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 73] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function obtenerEstatusIne($sCurp)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("mensaje"=>'');
		$arrData = array(
			"curp" => $sCurp
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('obtenerEstatusIne', $arrData, $GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["estatusine"]  = (int)$resultAPI["registros"]["status"];
				$arrDatos["telefonoine"] = $resultAPI["registros"]["telefonoine"];
				$arrDatos["companiatel"] = (int)$resultAPI["registros"]["companiatel"];
				$arrDatos["foliosoline"] = (int)$resultAPI["registros"]["folioine"];
				
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 74] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function actualizarFolioIne($iFolioCons, $sCurp, $iEmpleado, $foliosoline)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("mensaje"=>'');
		$arrData = array(
			"folioCons" => $iFolioCons,
			"curp" => $sCurp,
			"empleado" => $iEmpleado,
			"foliosoline" => $foliosoline
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('actualizarFolioIne',$arrData,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['actualizo'] = $resultAPI["registros"]['actualizo'];
				
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 75] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function actualizarSolEstatusExpiden($iFolioCons,$sCurp)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("mensaje"=>'');
		$arrData = array(
			"folioCons" => $iFolioCons,
			"curp" => $sCurp
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest: actualizarSolEstatusExpiden --> folioCons: ".$iFolioCons.", curp: ".$sCurp);

			$resultAPI = $objAPI->consumirApi('actualizarSolEstatusExpiden',$arrData,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['actualizaestatus'] = $resultAPI['registros']['actualizaestatus'];

				$objGn->grabarLogx("Resultado de actualizarSolEstatusExpiden: ".$arrDatos['actualizaestatus']);
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 87] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function fntieneautenticacionine($sCurp,$iEmpleado)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrDatos = array();
		$arrCurp = array(
			"sCurp"=>$sCurp,
			"iEmpleado"=>$iEmpleado
		);

		try
		{
			//Ejecuta la consulta
			$resultAPI = $objAPI->consumirApi('fntieneautenticacionine', $arrCurp, $GLOBALS["controlador"]);
			
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI['registros'] as $reg)
				{
					$arrDatos['respuesta'] = $reg['respuesta'];
				}

				$objGn->grabarLogx("se valido correctamente la curp si ya realizó o no la autenticacion ine ".$sCurp);
				$objGn->grabarLogx("la validacion resulto: ".$arrDatos["respuesta"]);
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');

				$arrDatos["respuesta"] = 0;
				return $arrDatos;
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function validarcodigoautenticacion($iFolioCons, $codigoIne)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array('iFolioCons' => $iFolioCons, 'codigoIne' => $codigoIne);

		try
		{
			$objGn->grabarLogx("Inicio API Rest con validarcodigoautenticacion");
			$resultAPI = $objAPI->consumirApi('validarcodigoautenticacion',$arrData,$GLOBALS["controlador"]);
			
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcio
				$arrDatos['descripcion'] = "EXITO AL EJECUTAR LA CONSULTA";
				$arrDatos["validacion"] = $resultAPI["registros"]["irespuesta"];
				
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 77] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function obtenerRecuperacionFolioConstancia($sCurp)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array('sCurp' => $sCurp);

		try
		{
			$objGn->grabarLogx("Inicio API Rest con obtenerRecuperacionFolioConstancia");
			$resultAPI = $objAPI->consumirApi('obtenerRecuperacionFolioConstancia',$arrData,$GLOBALS["controlador"]);
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcio
				$arrDatos['descripcion'] = "EXITO AL EJECUTAR LA CONSULTA";
				$arrDatos['folio'] =  $resultAPI['registros']['folio'];
				$arrDatos['estatus']= 1;
				
				$objGn->grabarLogx('Respuesta obtenerRecuperacionFolioConstancia: '.$arrDatos['folio']);
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 78] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function llamarapi($consulta,$arrDatosCaptura)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		//declara un arreglo
		$arrDatos = array();
		$iRespuesta = 0;

		$resultAPI = $objAPI->consumirApi($consulta,$arrDatosCaptura,$GLOBALS["controlador"]);
		//Verifica que se haya ejecutado correctamente
		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			//indicador que asigna estatus 1, osea correctamente y su descripcion
			$arrDatos['estatus'] = OK___;
			$arrDatos['descripcion'] = "EXITO";
			
			$arrDatos['registros'] = $resultAPI['registros'];

			//$arrDatos['fecha'] = $resulSet['sfechaalta'];
			foreach($resultAPI['registros'] as $reg)
			{
				$arrDatos['registros'][]=array_map('trim', $reg);
				$iRespuesta = $reg['respuesta'];
			}
		}
		else
		{
		    if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 79] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}
		return $arrDatos;
	}

	public static function verificaExpedientePermanente($iFolioAfore)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array('iFolioSerAfore' => $iFolioAfore);

		try
		{
			$objGn->grabarLogx("Inicio API Rest con verificaExpedientePermanente");
			$resultAPI = $objAPI->consumirApi('verificaExpedientePermanente',$arrData,$GLOBALS["controlador"]);
			
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)			
			{
				$arrDatos['descripcion'] = "EXITO AL EJECUTAR LA CONSULTA";
				$arrDatos['estatusexpiden'] =  $resultAPI['registros']['estatusexpiden'];
				$arrDatos['estatusenrol'] =  $resultAPI['registros']['estatusenrol'];
				$arrDatos['indicadordecimo'] = $resultAPI['registros']['indicadordecimo'];
				$arrDatos['curpduplicado'] =  $resultAPI['registros']['curpduplicado'];
				
				$objGn->grabarLogx('estatusexpiden: '.$arrDatos['estatusexpiden'].', estatusenrol: '.$arrDatos['estatusenrol'].', indicadordecimo: '.$arrDatos['indicadordecimo'].', curpduplicado: '.$arrDatos['curpduplicado']);
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 81] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function guardarHuellasIndices($NombreArchivoizquierdo,$NombreArchivoderecho,$sCurpTrabajador,$iEmpleado,$sIpRemoto)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("mensaje"=>'');
		$arrData = array(
			"sNombreArchivoIzq" => $NombreArchivoizquierdo,
			"sNombreArchivoDer" => $NombreArchivoderecho,
			"sCurpTrabajador" => $sCurpTrabajador,
			"iEmpleado" => $iEmpleado,
			"ipModulo" => $sIpRemoto
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('guardarHuellasIndices',$arrData,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $resultAPI["registros"]['respuesta'];
				
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 82] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function limpiarHuellasServicio($NombreArchivoizquierdo,$NombreArchivoderecho)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array("mensaje"=>'');
		$arrData = array(
			"sNombreArchivoIzq" => $NombreArchivoizquierdo,
			"sNombreArchivoDer" => $NombreArchivoderecho
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('limpiarHuellasServicio',$arrData,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['respuesta'] = $resultAPI["borrado"];
				
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 83] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function verificaEjecucionServicio()
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array('estatus' => 0);

		try
		{
			$objGn->grabarLogx("Inicio API Rest verificaEjecucionServicio");

			$resultAPI = $objAPI->consumirApi('verificaEjecucionServicio',$arrData,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg  => $key)
				{
					$arrDatos["estatus"] = $key['estatus'];
					$arrDatos["enrolamientopermanente"] = $key['enrolamientopermanente'];
				}
				$objGn->grabarLogx('estatus: '.$arrDatos["estatus"].', enrolamientopermanente: '.$arrDatos["enrolamientopermanente"]);
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 84] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function elminarinformacionServicio()
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array('estatus' => 0);

		try
		{
			$objGn->grabarLogx("Inicio API Rest elminarinformacionServicio");
			$resultAPI = $objAPI->consumirApi('elminarinformacionServicio',$arrData,$GLOBALS["controlador"]);
			
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				
				$arrDatos['descripcion'] = "EXITO AL EJECUTAR LA CONSULTA";
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 85] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function obtenerEstatusAfiliacion($sCurp)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array('sCurp' => $sCurp);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerEstatusAfiliacion");
			$resultAPI = $objAPI->consumirApi('obtenerEstatusAfiliacion',$arrData,$GLOBALS["controlador"]);
			
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				
				$arrDatos['descripcion'] = "EXITO AL EJECUTAR LA CONSULTA";
				$arrDatos['iestatusproceso'] = (int)$resultAPI["registros"]['iestatusproceso'];
				
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 86] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//OBTENCION PARA ENVIO DE CORREO O MENSAJE
	//OPCION 303
	public static function verificarTelefono($numero, $numeroLada)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array(
			"sNumero" => $numero,
			"sNumeroLada" => $numeroLada
		);
		try
		{
			$objGn->grabarLogx("Incio API Rest verificarTelefono");

			$resultAPI = $objAPI->consumirApi('verificarTelefono',$arrData,$GLOBALS["controlador"]);
			$objGn->grabarLogx($resultAPI);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['irespuesta'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg)
				{
					$arrDatos['respuesta'] = $reg['respuesta'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 303] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	
	//OPTION 506
	public static function obtenerErrorMensajeFct($cFolioConoTrasp)
	{
		sleep(3);
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array(
			"sFolioConoTrasp" => $cFolioConoTrasp,
		);
		try
		{
			$objGn->grabarLogx("Incio API Rest obtenerErrorMensajeFct");
			$objGn->grabarLogx($arrData['sFolioConoTrasp']);
			$objGn->grabarLogx('entre a l apirrest');
			$resultAPI = $objAPI->consumirApi('obtenerErrorMensajeFct',$arrData,$GLOBALS["controlador"]);
			$objGn->grabarLogx('sali del apirrest');
			$objGn->grabarLogx($resultAPI);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				
				$arrDatos['respuesta'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["irespuesta"] = $resultAPI['irespuesta'];
				$arrDatos["icodigo"] = $resultAPI['icodigo'];
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 506] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//OPTION 507
	public static function insertarTipoEntregaCorreoTelefono($tipoEntrega,$cCurptrabajado,$tipoCorreoTelefono,$ipservidor,$iEmpleado)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array(
			"stipoEntrega" => $tipoEntrega,
			"sCurptrabajado" => $cCurptrabajado,
			"stipoCorreoTelefono" => $tipoCorreoTelefono,
			"ipservidor" => $ipservidor,
			"iEmpleado" => $iEmpleado
		);
		try
		{
			$objGn->grabarLogx($iEmpleado);
			$objGn->grabarLogx("Incio API Rest insertarTipoEntregaCorreoTelefono");
			$objGn->grabarLogx('entre a l apirrest');
			$resultAPI = $objAPI->consumirApi('insertarTipoEntregaCorreoTelefono',$arrData,$GLOBALS["controlador"]);
			$objGn->grabarLogx('sali del apirrest');
			$objGn->grabarLogx($resultAPI);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				
				
				$arrDatos['respuesta'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg)
				{
					$arrDatos['irespuesta'] = $reg['respuesta'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 507 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//OPTION 508
	public static function fnGuardarFolioColsolicitud($cfolioTrasCono,$cCurpBucar)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array(
			"sFolioTrasCono" => $cfolioTrasCono,
			"sCurpBucar" => $cCurpBucar
		);
		try
		{
			$objGn->grabarLogx("Incio API Rest fnGuardarFolioColsolicitud");
			$objGn->grabarLogx($arrData['sFolioTrasCono']);
			$objGn->grabarLogx('entre a l apirrest');
			$resultAPI = $objAPI->consumirApi('fnGuardarFolioColsolicitud',$arrData,$GLOBALS["controlador"]);
			$objGn->grabarLogx('sali del apirrest');
			$objGn->grabarLogx($resultAPI);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				$arrDatos['respuesta'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg)
				{
					$arrDatos['irespuesta'] = $reg['respuesta'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 508] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//OPTION 509
	public static function agregarDatosConsultaFolio($iEmpleado, $sCurp, $iFolioConocimientoTraspaso, $iFoliosol){
					
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrDatos 	= array("result" => array());
		
		$arrApi = array(
			'empleado' 				=> $iEmpleado,
			'curp' 					=> $sCurp,
			'claveaforegenerofolio' => 568,
			'tipofolio' 			=> 27,
			'folioregtrasp' 		=> $iFolioConocimientoTraspaso,
			'foliosol' 				=> $iFoliosol
		);

		try {
			$objGn->grabarLogx("Inicio API Rest agregarDatosConsultaFolio");

			$resultAPI = $objAPI->consumirApi('agregardatosconsultafolioafiliacion',$arrApi,$GLOBALS["controlador"]);
			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);
			$objGn->grabarLogx("Fin API Rest agregardatosconsultafolioafiliacion");

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach ($resultAPI["registros"] as $reg)
				{
					$arrDatos["result"] = $reg;
				}

			} else {

				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 509 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		} 
		catch (Exception $e) 
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//OPTION 510
	public static function obtencionFolioSulicitud($sCurp)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrData = array(
			"sCurpBucar" => $sCurp
		);
		try
		{			
			//indicador que asigna estatus 1, osea correctamente y su descripcion
			$arrResp= Cconstancia::obtenernumerofolio();
			if($arrResp){
				$arrDatos['respuesta'] = 1;
				$arrDatos['descripcion'] = "EXITO";
					$numerofolio= $arrResp;
					$arrDatos['irespuesta'] = $numerofolio;
											
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
				}
			}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
    
    //OPCION 511
	public static function obtenerIPServidor($sIpRemoto)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrDatos 	= array("IP"=>'');

		$arrApi = array(
				'sIpRemoto' => $sIpRemoto
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerIPServidor");
			
			$resultAPI = $objAPI->consumirApi('obtenerIPServidor',$arrApi,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";

				foreach($resultAPI["registros"] as $reg)
				{
					$arrDatos["IP"] = $reg["ipofflinelnx"];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 511] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//Folio 891
	public static function envioctdimg($folioafiliacion,$iFirma)
	{
		$rutapdf = Cconstancia::consumirdocumentoacuse($iFirma,$folioafiliacion);
		$zip = new \ZipArchive();
		//16453768
		if($iFirma == 1)
		{
			$nomenclatura = 'acusecartaderechosfirma_'.$folioafiliacion.'';
		}
		else
		{
			$nomenclatura = 'acusecartaderechos_'.$folioafiliacion.'';
		}
			$im = new Imagick();
			$im->setResolution(300,300);
			$im->readimage("/sysx/progs/web/salida/cartaderechos/".$nomenclatura.".pdf"); 
			$im->setImageFormat("jpeg");
			$extension = pathinfo($nomenclatura, PATHINFO_FILENAME);
			$im->writeImage("/sysx/progs/web/salida/cartaderechos/acusecartaderechos/".$nomenclatura.".jpg"); 
			$cadena = "/sysx/progs/web/salida/cartaderechos/acusecartaderechos/".$extension.".jpg";
			$im->clear(); 
			$im->destroy();
		
		
		$arregloenvioexp["mensaje"] = Cconstancia::envioexp($cadena,$folioafiliacion,$nomenclatura.".jpg");

		return $arregloenvioexp;
	}

	//Folio 891
	public static function envioexp($nombrearchivo,$folioafiliacion,$nomenclatura)
	{
		$objAPI = new Capirestconstanciaafiliacion();

		$im = file_get_contents($nombrearchivo);
		$imdata = base64_encode($im);

		$archivo = $folioafiliacion.' '.$nomenclatura;
		$archivo = strtoupper($archivo);

		
		$datos = array();

		$arrApi = array(
			"folioafiliacion"=>$folioafiliacion,
			"archivo"=>$archivo,
			"imdata"=>$imdata
		);

		try
		{
			$resultAPI = $objAPI->consumirApi('envioexp',$arrApi,$GLOBALS["controlador"]);
			
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$datos = "Se Inserto Correctamente";
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion ] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $datos;
	}

	//Folio 891
	public static function consumirdocumentoacuse($firma,$folioafiliacion){
	
		$ch = curl_init();
		
		$host = $_SERVER["HTTP_HOST"];

		$arreglodatos = array('folio' => $folioafiliacion,'ifuente' => $firma); 

		$url='http://'.$host.'/impacusecartaderechos/impacusecartaderechos.php';

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
		curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($arreglodatos));
		$response = curl_exec($ch);
		curl_close($ch);
		$decode = json_decode($response);
		return $decode;
	}

	//Folio 891
	public static function insertaDatosAcuse($nombre,$curp,$folio, $opcionacuse){
		
		$objAPI = new Capirestconstanciaafiliacion();
		$arrDatos = array();

		$arrApi = array(
			"nombre"=>$nombre,
			"curp"=>$curp,
			"folio"=>$folio,
			"opcionacuse"=>$opcionacuse
		);

		try
		{
			$resultAPI = $objAPI->consumirApi('insertaDatosAcuse',$arrApi,$GLOBALS["controlador"]);
			
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				foreach($resultAPI['registros'] as $reg)
				{			
					$arrDatos['estatus']= $reg["retorno"];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 93] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}
	
	public static function ActualizarExpedientePermante($iFolioCons,$sCurp)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrDatos = array("result"=>'');

		$arrData = array(
			"folioConst" => $iFolioCons,
			"curp" => $sCurp
		);
		$objGn->grabarLogx("foliocons: " . $iFolioCons. " curp: ". $sCurp);

		try
		{
			$objGn->grabarLogx("Inicio API Rest ActualizarExpedientePermante");
			$resultAPI = $objAPI->consumirApi('ActualizarExpedientePermante',$arrData,$GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos['actualizaestatus'] = $resultAPI['actualizaestatus']['actualizaestatus'];

			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 88] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

   //--------Folio 1028.1---------------------

	public static function cambiarNombreImg($oldName, $newName)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
    		'oldName' => $oldName,
			'newName' => $newName
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest renombrarImgFirmas");

			$resultAPI = $objAPI->consumirApi('renombrarImgFirmas', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI;
	
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 601] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}
    
	public static function enrolhuellasservicio($sCURPTrab, $iNumFuncionario, $iFolioSolicitud, $iTipoPerson, $iTipoServ, $sTipoOperac, $iNumDedo, $iNist, $sDispositivo, $iFap, $sImagen, $sTipoServicio, $cTemplate)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
    		'sCURPTrab' => $sCURPTrab,
			'iNumFuncionario' => $iNumFuncionario,
			'iFolioSolicitud' => $iFolioSolicitud,
			'iTipoPerson' => $iTipoPerson,
			'iTipoServ' => $iTipoServ,
			'sTipoOperac' => $sTipoOperac,
			'iNumDedo' => $iNumDedo,
			'iNist' => $iNist,
			'sDispositivo' => $sDispositivo,
			'iFap' => $iFap,
			'sImagen' => $sImagen,
			'sTipoServicio' => $sTipoServicio,
			'cTemplate' => $cTemplate
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest validarHuella");

			$resultAPI = $objAPI->consumirApi('validarHuella', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI;
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 602] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function buscarTextoVideo($iTipodialogo,$iFolio)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
    		'iTipodialogo' => $iTipodialogo,
			'iFolio' => $iFolio
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerdialogograbadormovil");

			$resultAPI = $objAPI->consumirApi('obtenerdialogograbadormovil', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{

				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI;
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 603] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function reubicarVideo($nombrearchivo, $iFolio)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
    		'nombrearchivo' => $nombrearchivo,
			'iFolio' => $iFolio
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest subirvideo");

			$resultAPI = $objAPI->consumirApi('subirvideo', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI;
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 604] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function guardarTarjeta($cNumeroTarjeta, $ipCliente)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
    		'cNumeroTarjeta' => $cNumeroTarjeta,
			'ipCliente' => $ipCliente
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest almacenanumerotarjetamovil");

			$resultAPI = $objAPI->consumirApi('almacenanumerotarjetamovil', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI;
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 605] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function validarTarjetaBcpl($cNumTarjetaBCPL, $cTemplate)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
    		'cNumTarjetaBCPL' => $cNumTarjetaBCPL,
			'cTemplate' => $cTemplate
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest consultarCLienteBCplMovil");

			$resultAPI = $objAPI->consumirApi('consultarCLienteBCplMovil', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI;
	
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 606] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function indexarFormatoEnrolamiento($iFolio, $iEmpleadoEnrol, $documento, $ipOffline)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
    		'iFolio' => $iFolio,
			'iEmpleadoEnrol' => $iEmpleadoEnrol,
			'documento' => $documento,
			'ipOffline' => $ipOffline
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest indexarFormatoEnrolamiento");

			$resultAPI = $objAPI->consumirApi('indexarFormatoEnrolamiento', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI;
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 607] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function moverFormatoEnrol($folio)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
    		'folio' => $folio
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest moverFormatoEnrol");

			$resultAPI = $objAPI->consumirApi('moverFormatoEnrol', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI;
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 608] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function actualizaIDRE($folio)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
    		'folio' => $folio
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest actualizaIDRE");

			$resultAPI = $objAPI->consumirApi('actualizaIDRE', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI;
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 609] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function almacenaDispositivoAfiliacion($folio,$sistemaOperativo,$tipoConexion)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array(
			'folio' => $folio,
			'sistemaOperativo' => $sistemaOperativo,
			'tipoConexion' => $tipoConexion
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest almacenaDispositivoAfiliacion");

			$resultAPI = $objAPI->consumirApi('almacenaDispositivoAfiliacion', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI;
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 610] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function ligaMenuAfore()
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();

    	$arrData = array('metodo' => 'ligaMenuAfore');

		try
		{
			$objGn->grabarLogx("Inicio API Rest ligaMenuAfore");

			$resultAPI = $objAPI->consumirApi('ligaMenuAfore', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				

				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["respuesta"] = $resultAPI['respuesta'];
	
			}
			else
			{
				header('HTTP/1.1 409 Conflict');
				return "[opcion 611] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function cerrarSesion($empleado)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();
		$arrData = array(
    		'codigoEmpleado' => $empleado
		);
		
		$controlador = "CtrlAuth";

		try
		{
			$objGn->grabarLogx("Inicio API Rest eliminarToken");

			$resultAPI = $objAPI->consumirApi('eliminarToken', $arrData, $controlador);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				

				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["result"] = $resultAPI['result'];
				session_destroy();
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "[opcion 612] Error al consultar la API [Datos] " + json_encode($resultAPI);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	public static function generarArchivoHuellas($NombreArchivo,$Template)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

		$arrDatos = array();
		$arrData = array(
			"nombreArchivo" => $NombreArchivo,
			"template" => $Template
		);

		try
		{
			$objGn->grabarLogx("Incio API Rest");

			$resultAPI = $objAPI->consumirApi('generarArchivoHuellas', $arrData, $GLOBALS["controlador"]);

			if($resultAPI)
			{
				

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos['respuesta'] = $reg['respuesta'];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta de la funcion guardarHuellasIndices";
				throw new Exception("constanciaafiliacion.php\guardarHuellasIndices"."\tError al ejecutar la consulta \t"." | " );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//Folio 539 Obtiene la llave de datos para el webservice de bancoppel, obtenida del folio proporcionado por renapo
	public static function obtenerLlaveDatosBancoppel($curp){
		$arrDatos = array();
		$objGn = new CMetodoGeneral();
		//Valida si se abrio a bd
		try{
			$objAPI = new Capirestconstanciaafiliacion();

			$arrData = array(
				'curp' => $curp
			);

			$resultAPI = $objAPI->consumirApi('obtenerLlaveDatosBancoppel', $arrData, $GLOBALS["controlador"]);	
			//$resultAPI = json_decode($resultAPI, true);

			if($resultAPI){
				//Verifica que se haya ejecutado correctamente
				if($resultAPI['estatus'] == 1)
				{	
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI['respuesta'] as $reg)
					{		
						$arrDatos['respuesta'][] = $reg;
					}
				}
				else
				{
					$arrDatos['estatus'] = -1;	
					$arrDatos['Error'] = $resultAPI['descripcion'];	
				}
			}else{
				$arrDatos['estatus'] = -1;	
				$arrDatos['Error'] = "Error en la api";	
			}
		}
		catch(Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);	
		}
		
		return $arrDatos;
	}

	//Folio 539 Guarda en la tabla bitacorarespuestabancoppel cada que se utiliza el WS de Bancoppel
	public static function guardarBitacoraRespuestaBancoppel($fechaconsulta,$horaconsulta,$folioafiliacion, $tienda, $empleado, $idanverso, $idreverso, $curp, $appaterno, $apmaterno, $nombres, $rfc, $fechanac, $entidad, $genero, $nacionalidad, $edoCivil, $numClienteCoppel, $numClienteBancoppel, $nvlEstudio, $profesion, $giroNegocio, $telefono1, $telefono2, $correo, $calle, $numExterior, $numInterior, $codigoPostal, $colonia, $ciudad, $delegacion, $estado, $pais, $calleCobranza, $numExteriorCobranza, $numInteriorCobranza, $codigoPostalCobranza, $coloniaCobranza, $ciudadCobranza, $delegacionCobranza,$estadoCobranza,$paisCobranza,$calleLaboral,$numExteriorLaboral,$numInteriorLaboral,$codigoPostalLaboral,$coloniaLaboral,$ciudadLaboral,$delegacionLaboral,$estadoLaboral,$paisLaboral,$apPaternoRef1, $apMaternoRef1, $nombresRef1, $apPaternoRef2,$apMaternoRef2,$nombresRef2,$ctaTradicionalVigente,$ctaNomina,$opcionbcpl){

		$fechaconsulta=date("Y-m-d");
		$horaconsulta=date("H:i:s"); 
		if($numClienteCoppel == '')
		{
			$numClienteCoppel = 0;
		}

		//funccion que permite obtener la tienda
		$objAPI = new Capirestconstanciaafiliacion();
		$iTienda = Cconstancia::obtenertienda();

		if($iTienda==0){

			$arrTienda = array(
				'iEmpleado' => $empleado
			);

			$iTienda = $objAPI->consumirApi('obtenerTiendaEmpleado', $arrTienda, $GLOBALS["controlador"]);
		
		}

		$arrData = array(
			"folioafiliacion" => $folioafiliacion,
			"fechaconsulta" => $fechaconsulta,
			"horaconsulta" => $horaconsulta,
			"iTienda" => $iTienda,
			"empleado" => $empleado,
			"idanverso" => $idanverso,
			"idreverso" => $idreverso,
			"curp" => $curp,
			"appaterno" => $appaterno,
			"apmaterno" => $apmaterno,
			"nombres" => $nombres,
			"rfc" => $rfc,
			"fechanac" => $fechanac,
			"entidad" => $entidad,
			"genero" => $genero,
			"nacionalidad" => $nacionalidad,
			"edoCivil" => $edoCivil,
			"numClienteCoppel" => $numClienteCoppel,
			"numClienteBancoppel" => $numClienteBancoppel,
			"nvlEstudio" => $nvlEstudio,
			"profesion" => $profesion,
			"giroNegocio" => $giroNegocio,
			"telefono1" => $telefono1,
			"telefono2" => $telefono2,
			"correo" => $correo,
			"calle" => $calle,
			"numExterior" => $numExterior,
			"numInterior" => $numInterior,
			"codigoPostal" => $codigoPostal,
			"colonia" => $colonia,
			"ciudad" => $ciudad,
			"delegacion" => $delegacion,
			"estado" => $estado,
			"pais" => $pais,
			"calleCobranza" => $calleCobranza,
			"numExteriorCobranza" => $numExteriorCobranza,
			"numInteriorCobranza" => $numInteriorCobranza,
			"codigoPostalCobranza" => $codigoPostalCobranza,
			"coloniaCobranza" => $coloniaCobranza,
			"ciudadCobranza" => $ciudadCobranza,
			"delegacionCobranza" => $delegacionCobranza,
			"estadoCobranza" => $estadoCobranza,
			"paisCobranza" => $paisCobranza,
			"calleLaboral" => $calleLaboral,
			"numExteriorLaboral" => $numExteriorLaboral,
			"numInteriorLaboral" => $numInteriorLaboral,
			"codigoPostalLaboral" => $codigoPostalLaboral,
			"coloniaLaboral" => $coloniaLaboral,
			"ciudadLaboral" => $ciudadLaboral,
			"delegacionLaboral" => $delegacionLaboral,
			"estadoLaboral" => $estadoLaboral,
			"paisLaboral" => $paisLaboral,
			"apPaternoRef1" => $apPaternoRef1,
			"apMaternoRef1" => $apMaternoRef1,
			"nombresRef1" => $nombresRef1,
			"apPaternoRef2" => $apPaternoRef2,
			"apMaternoRef2" => $apMaternoRef2,
			"nombresRef2" => $nombresRef2,
			"ctaTradicionalVigente" => $ctaTradicionalVigente,
			"ctaNomina" => $ctaNomina,
			"opcionbcpl" => $opcionbcpl
		);
		
		$objGn = new CMetodoGeneral();

		//Valida si se abrio a bd
		try{

			$resultAPI = $objAPI->consumirApi('guardarBitacoraRespuestaBancoppel', $arrData, $GLOBALS["controlador"]);	
			//$resultAPI = json_decode($resultAPI, true);
			//Verifica que se haya ejecutado correctamente
			if($resultAPI)
			{	
				if($resultAPI['estatus'] == 1){
					
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = 'EXITO';

					foreach($resultAPI['respuesta'] as $reg)
					{	
						$arrDatos['respuesta']=$reg['respuesta'];
					}

				}else{
					$arrDatos['estatus'] = $resultAPI['estatus'];
					$arrDatos['Error'] = $resultAPI['descripcion'];
				}
				
			}
			else
			{
				$arrDatos['estatus'] = -1;	
				$arrDatos['Error'] = "Error al ejecutar la API";	

			}
		}
		catch(Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);	
		}
		
		return $arrDatos;
	}

	

	public static function validaEstatusVideo($folioafiliacion){
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrDatos = array("estatus" => 0, "descripcion" => '', "resultado" => '');

		$arrData = array("folioafiliacion" => $folioafiliacion);

		$objGn->grabarLogx("validaEstatusVideo => folioafiliacion: ".$folioafiliacion);

		try
		{
			$resultAPI = $objAPI->consumirApi('validaEstatusVideo',$arrData,$GLOBALS["controlador"]);
			
			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				//indicador que asigna estatus 1, osea correctamente y su descripcion
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				foreach($resultAPI['registros'] as $reg)
				{
					//var_dump('Resultado: ['.$reg['resultado'].']');
					$arrDatos['resultado'] = $reg['resultado'];
				}
				$objGn->grabarLogx('Respuesta validaEstatusVideo: '.$arrDatos['resultado']);
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');
				$mensaje= "[opcion 3 ] Error al consultar la API [Datos] " + json_encode($resultAPI);
				$arrDatos['estatus'] = -1;
				$arrDatos['descripcion'] = $mensaje;
				$objGn->grabarLogx($mensaje);
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$arrDatos['estatus'] = -1;
			$arrDatos['descripcion'] = $mensaje;
			$objGn->grabarLogx($mensaje);
		}
		return $arrDatos;
	}


	//Folio 539 Obtiene las entidades de nacimiento para el combo de captura de datos bancoppel
	public static function obtenerEntidadNacimiento(){
		$arrDatos = array();
		$objGn = new CMetodoGeneral();
		$controlador = "CtrlconstanciaAfiliacion";
		//Valida si se abrio a bd
		try{
			$objAPI = new Capirestconstanciaafiliacion();

			$arrData = array();

			$objGn->grabarLogx( '[' . __FILE__ . ']' . "------ Antes de ejecutar el api");
			$resultAPI = $objAPI->consumirApi('obtenerEntidadNacimiento', $arrData, $controlador);
			//$resultAPI = json_decode($resultAPI, true);
			$objGn->grabarLogx( '[' . __FILE__ . ']' . $resultAPI);
			//Verifica que se haya ejecutado correctamente
			if($resultAPI)
			{	
				if($resultAPI['estatus'] == 1){
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = 'EXITO';

					foreach($resultAPI['respuesta'] as $reg)
					{	
						$arrDatos['respuesta'][]=$reg;
					}

				}else{
					$arrDatos['estatus'] = $resultAPI['estatus'];
					$arrDatos['Error'] = $resultAPI['descripcion'];
				}
				
			}
			else
			{
				$arrDatos['estatus'] = -1;	
				$arrDatos['Error'] = "Error al ejecutar la API";	

			}
		}
		catch(Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);	
		}
		
		return $arrDatos;
	}

	//Folio 539 Obtiene los parametros, la URL y manda a llamar el WS Bancoppel
	public static function respuestaWSBancoppel($nombres, $appaterno, $apmaterno, $fechanac, $genero, $entidad){
		$respUrl = Cconstancia::obtenerurlwsbancoppel();
		$urlWsBancoppel = $respUrl['url']; 

		$objGn = new CMetodoGeneral();
		$fecha_nacimiento1 = explode('-',$fechanac);
		if(strlen($fecha_nacimiento1[2]) == 4)
		{
			$fechanac = $fecha_nacimiento1[2].'-'.$fecha_nacimiento1[1].'-'.$fecha_nacimiento1[0];
			$objGn->grabarLogx("Nueva fecha de nacimiento ->".$fechaNacimiento);
		}
		

		$data = array("nombres" => $nombres, "apellidop" => $appaterno, "apellidom" => $apmaterno, "nacimiento" => $fechanac, "genero" => $genero, "entnacimiento" => $entidad);

		$resp = Cconstancia::consumirWsBancoppel($urlWsBancoppel, $data);
		
		return $resp;
	}

	//Folio 539 Consume el ws de bancoppel, recibe la URL y los parametros para el WS en un array
	public static function consumirWsBancoppel($urlWs, $data)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objGn->grabarLogx("Entro a consumir WS Bancoppel");
		////////////////////////////SECCION GENERA XML QUE SE USARA PARA EL REQUEST DEL LOGIN////////////////////////
		$xmllogin = Cconstancia::xmllogin();
		
		$Amodificar = fopen("../php/clases/xml/soap.xml", "w");

			fwrite($Amodificar,$xmllogin[0]);
		
		fclose($Amodificar);
		////////////////////////////SECCION GENERA XML QUE SE USARA PARA EL REQUEST DEL LOGIN////////////////////////
		$token = Cconstancia::consultatoken('',$_SERVER['SERVER_ADDR']);
		$tokenconsumo = $token["stoken"];
		if($tokenconsumo == '')
		{
		////////////////////////////SECCION INVOCA EL CONSUMO DEL METODO BCPL_LOGIN Y OBTIENE EL ID_SESION QUE SERA USADO PARA EL CONSUMO POSTERIOR////////////////////////
		$salida = shell_exec('/bin/curl -H "Content-Type: text/xml" -H "SOAPAction:BCPL_LOGIN" --noproxy "*" --insecure -X POST -d @/sysx/progs/web/constanciasafiliacion/php/clases/xml/soap.xml https://10.26.216.21:8443/BancoppelWS/TransactionAccount?wsdl ');
		$objGn->grabarLogx("Respuesta Login -> ".$salida);
		
		$find = strpos($salida,"ID_SESSION");
		$find = $find+11;
		$lastfind = strrpos($salida,"ID_SESSION");
		$posiciones = $lastfind - $find;
		$posiciones = $posiciones-2;
		$identificadorsesion = substr($salida,$find,$posiciones); 
		$token = Cconstancia::insertartoken($identificadorsesion,$_SERVER['SERVER_ADDR']);
		////////////////////////////SECCION INVOCA EL CONSUMO DEL METODO BCPL_LOGIN Y OBTIENE EL ID_SESION QUE SERA USADO PARA EL CONSUMO POSTERIOR////////////////////////
		}
		else
		{
			$identificadorsesion = trim($tokenconsumo);
		}
		
		////////////////////////////SECCION GENERA XML QUE SE USARA PARA EL REQUEST DEL BCPL_CTES_AFORE////////////////////////
		$xmlclientes = Cconstancia::xmlclientes($xmllogin[1],$identificadorsesion,$data);
		$objGn->grabarLogx("XML Clientes -> ".$xmlclientes);
		$Aclientes = fopen("../php/clases/xml/soap2.xml", "w");

			fwrite($Aclientes,$xmlclientes);
		
		fclose($Aclientes);
		////////////////////////////SECCION GENERA XML QUE SE USARA PARA EL REQUEST DEL LOGIN////////////////////////
		
		
		////////////////////////////SECCION INVOCA EL CONSUMO DEL METODO BCPL_CTES_AFORE Y OBTIENE LOS DATOS////////////////////////
		$salidaafore = shell_exec('/bin/curl -H "Content-Type: text/xml" -H "SOAPAction:BCPL_CTES_AFORE" --noproxy "*" --insecure -X POST -d @/sysx/progs/web/constanciasafiliacion/php/clases/xml/soap2.xml https://10.26.216.21:8443/BancoppelWS/TransactionAccount?wsdl');
		$objGn->grabarLogx("Respuesta Clientes -> ".$salidaafore);
		
		$findresolucion = strpos($salidaafore,"</COD_RET>");
		$corteresolucion = substr($salidaafore,$findresolucion-4,4);  
		//este corte resolucion te genera un codigo si es 0000 significa que encontro datos, si regresa otra cosa es por que esta tronando
		$objGn->grabarLogx("Corte resolucion:  ".$corteresolucion." -> Cadena completa (Fin de resolucion): ".$findresolucion);
		if($corteresolucion == "0000")
		{
			$findfirstdata = strpos($salidaafore,"<DATA>");
			$findsecondtdata = strrpos($salidaafore,"</DATA>");
			$cuerpo = $findsecondtdata-$findfirstdata;
			$cortedata = substr($salidaafore,$findfirstdata,$cuerpo+7);
			
			$arrayretorno = explode("</DATA>",$cortedata);
			$iteradortotal = count($arrayretorno)-1;
			
			unset($arrayretorno[$iteradortotal]);
			
			for ($a = 0; $a < $iteradortotal; $a++) 
			{
				$arreglo[] = simplexml_load_string($arrayretorno[0]."</DATA>");
				$objGn->grabarLogx("Arreglo en la posicion ".$a.": ".$arreglo[$a]);
				
			}
			$iteradortotal2 = count($arreglo);
			for ($b = 0; $b < $iteradortotal2; $b++) 
			{
				$arreglo[$b]->ID_ANVERSO = "";
				$arreglo[$b]->ID_REVERSO = "";
			}
			
			return($arreglo);

		}
		else
		{
			
		}
		////////////////////////////SECCION INVOCA EL CONSUMO DEL METODO BCPL_CTES_AFORE Y OBTIENE LOS DATOS////////////////////////
		
	}

	//Folio 539 hace login para consumir el WS de bancoppel
	public static function xmllogin()
	{
			$datos = array();
		
			$objAPI = new Capirestconstanciaafiliacion();

			$arrData = array();

			$resultAPI = $objAPI->consumirApi('obtenerParametrosWSBancoppel', $arrData, $GLOBALS["controlador"]);
			//$resultAPI = json_decode($resultAPI, true);
			$ipCliente = $_SERVER['SERVER_ADDR'];
			if($resultAPI)
			{
				if($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['respuesta'] as $reg)
					{
						$objetogenerico =  new stdClass;
						$objetogenerico->ip_origen = $ipCliente;
						$objetogenerico->usuario = $reg["usuario"];
						$objetogenerico->pass = $reg["pass"];
						$objetogenerico->codigotraspagente = $reg["codigotraspagente"];
						$objetogenerico->agentecd = $reg["agentecd"];
						
						$xml="<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:main='http://main.ws.bancoppel/'>
						   <soapenv:Header>
							  <main:ADDRESSING>
								 <!--Optional:-->
								 <IP_ORIGEN>".$ipCliente."</IP_ORIGEN>
								 <!--Optional:-->
								 <IP_DESTINO>?</IP_DESTINO>
							  </main:ADDRESSING>
							  <main:SECURITY>
								 <!--Optional:-->
								 <USER_NAME>".$reg["usuario"]."</USER_NAME>
								 <!--Optional:-->
								 <USER_PASS>".$reg["pass"]."</USER_PASS>
								 <!--Optional:-->
								 <SESSION_ID>?</SESSION_ID>
							  </main:SECURITY>
						   </soapenv:Header>
						   <soapenv:Body>
							  <main:BCPL_LOGIN>
								 <!--Optional:-->
								 <REQUEST>
									<!--Optional:-->
									<AGENT_TRANS_TYPE_CODE>".$reg["codigotraspagente"]."</AGENT_TRANS_TYPE_CODE>
									<!--Optional:-->
									<AGENT_CD>".$reg["agentecd"]."</AGENT_CD>
									<!--Optional:-->
									<DATA>?</DATA>
								 </REQUEST>
							  </main:BCPL_LOGIN>
						   </soapenv:Body>
						</soapenv:Envelope>";
					}
				}
			}
			return array($xml,$objetogenerico);
	}
	
	public static function xmlclientes($datos,$idsesion,$arrgenerales)
	{ 
		
		if($arrgenerales["genero"] == 1)
		{
			$arrgenerales["genero"] = 'M';
		}
		else
		{
			$arrgenerales["genero"] = 'F';
		}

		$nombres = $arrgenerales["nombres"];
		
		$nombre_separados = explode(" ",$nombres);
		$fecha_nacimiento = explode(" ", $arrgenerales["nacimiento"]);
		if(strlen($arrgenerales["entnacimiento"])==1){
			$entidad_nacimiento = "0".$arrgenerales["entnacimiento"];
		}else{
			$entidad_nacimiento = $arrgenerales["entnacimiento"];
		}
		
			$xml = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:main='http://main.ws.bancoppel/'>
   <soapenv:Header>
      <main:ADDRESSING>
         <!--Optional:-->
         <IP_ORIGEN>".$datos->ip_origen."</IP_ORIGEN>
         <!--Optional:-->
         <IP_DESTINO>?</IP_DESTINO>
      </main:ADDRESSING>
      <main:SECURITY>
         <!--Optional:-->
         <USER_NAME>".$datos->usuario."</USER_NAME>
         <!--Optional:-->
         <USER_PASS>".$datos->pass."</USER_PASS>
         <!--Optional:-->
         <SESSION_ID>".$idsesion."</SESSION_ID>
      </main:SECURITY>
   </soapenv:Header>
   <soapenv:Body>
      <main:BCPL_CTES_AFORE>
         <!--Optional:-->
         <REQUEST>
            <!--Optional:-->
            <AGENT_TRANS_TYPE_CODE>".$datos->codigotraspagente."</AGENT_TRANS_TYPE_CODE>
            <!--Optional:-->
            <AGENT_CD>".$datos->agentecd."</AGENT_CD>
            <!--Optional:-->
            <DATA>
			<APELLIDO_PATERNO>".$arrgenerales["apellidop"]."</APELLIDO_PATERNO>
            
               <APELLIDO_MATERNO>".$arrgenerales["apellidom"]."</APELLIDO_MATERNO>

               <PRIMER_NOMBRE>".$nombre_separados[0]."</PRIMER_NOMBRE>
               
               <SEGUNDO_NOMBRE>".$nombre_separados[1]."</SEGUNDO_NOMBRE>

               <FECHA_NACIMIENTO>".$fecha_nacimiento[0]."</FECHA_NACIMIENTO>
               
               <ENTIDAD_NACIMIENTO>".$entidad_nacimiento."</ENTIDAD_NACIMIENTO>

               <GENERO>".$arrgenerales["genero"]."</GENERO>
            </DATA>
         </REQUEST>
      </main:BCPL_CTES_AFORE>
   </soapenv:Body>
</soapenv:Envelope>";
			
			return $xml;
	}

	//Folio 539 Obtiene la url del ws de bancoppel de base de datos
	public static function obtenerurlwsbancoppel(){
		$arrDatos = array();
		$objGn = new CMetodoGeneral();

		//Valida si se abrio a bd
		try{
			$objAPI = new Capirestconstanciaafiliacion();

			$arrData = array();

			$resultAPI = $objAPI->consumirApi('obtenerurlwsbancoppel', $arrData, $GLOBALS["controlador"]);
			//$resultAPI = json_decode($resultAPI, true);

			//Verifica que se haya ejecutado correctamente
			if($resultAPI)
			{	
				if($resultAPI['estatus'] == 1){

					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = 'EXITO';

					foreach($resultAPI['respuesta'] as $reg)
					{	
						$arrDatos['url']=$reg['url'];
					}

				}else{
					$arrDatos['estatus'] = $resultAPI['estatus'];
					$arrDatos['Error'] = $resultAPI['descripcion'];
				}
				
			}
			else
			{
				$arrDatos['estatus'] = -1;	
				$arrDatos['Error'] = "Error al ejecutar la API";	

			}
		}
		catch(Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);	
		}
		
		return $arrDatos;
	}
	
		public static function consultatoken($token,$ipcliente){
		$arrDatos = array();
		$objGn = new CMetodoGeneral();
		
		//Valida si se abrio a bd
		try{
			$objAPI = new Capirestconstanciaafiliacion();

			$arrData = array(
				'token' => $token,
				'direccionip' => $ipcliente
			);

			$resultAPI = $objAPI->consumirApi('consultartoken', $arrData, $GLOBALS["controlador"]);
			//$resultAPI = json_decode($resultAPI, true);

			//Verifica que se haya ejecutado correctamente
			if($resultAPI)
			{	
				if($resultAPI['estatus'] == 1){

					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = 'EXITO';

					foreach($resultAPI['registros'] as $reg)
					{	
						$arrDatos['stoken']=$reg['stoken'];
					}

				}else{
					$arrDatos['estatus'] = $resultAPI['estatus'];
					$arrDatos['Error'] = $resultAPI['descripcion'];
				}
				
			}
			else
			{
				$arrDatos['estatus'] = -1;	
				$arrDatos['Error'] = "Error al ejecutar la API";	

			}
		}
		catch(Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);	
		}
		
		return $arrDatos;
	}
	
			public static function insertartoken($token,$ipcliente){
		$arrDatos = array();
		$objGn = new CMetodoGeneral();
		
		//Valida si se abrio a bd
		try{
			$objAPI = new Capirestconstanciaafiliacion();

			$arrData = array(
				'token' => $token,
				'direccionip' => $ipcliente
			);

			$resultAPI = $objAPI->consumirApi('insertartoken', $arrData, $GLOBALS["controlador"]);
			//$resultAPI = json_decode($resultAPI, true);

			//Verifica que se haya ejecutado correctamente
			if($resultAPI)
			{	
				if($resultAPI['estatus'] == 1){

					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = 'EXITO';

					foreach($resultAPI['registros'] as $reg)
					{	
						$arrDatos['stoken']=$reg['stoken'];
					}

				}else{
					$arrDatos['estatus'] = $resultAPI['estatus'];
					$arrDatos['Error'] = $resultAPI['descripcion'];
				}
				
			}
			else
			{
				$arrDatos['estatus'] = -1;	
				$arrDatos['Error'] = "Error al ejecutar la API";	

			}
		}
		catch(Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);	
		}
		
		return $arrDatos;
	}
	
				public static function verificajfuse($iFolioAfore)
	{
		$arrDatos = array();
		$objGn = new CMetodoGeneral();
		
		//Valida si se abrio a bd
		try{
			$objAPI = new Capirestconstanciaafiliacion();

			$arrData = array(
				'iFolioAfore' => $iFolioAfore
			);

			$resultAPI = $objAPI->consumirApi('verificajfuse', $arrData, $GLOBALS["controlador"]);
			//$resultAPI = json_decode($resultAPI, true);

			//Verifica que se haya ejecutado correctamente
			if($resultAPI)
			{	
				if($resultAPI['estatus'] == 1){

					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = 'EXITO';

					foreach($resultAPI['registros'] as $reg)
					{	
						$arrDatos['enviada']=$reg['fnobtenerstatusjfuse'];
					}

				}else{
					$arrDatos['estatus'] = $resultAPI['estatus'];
					$arrDatos['Error'] = $resultAPI['descripcion'];
				}
				
			}
			else
			{
				$arrDatos['estatus'] = -1;	
				$arrDatos['Error'] = "Error al ejecutar la API";	

			}
		}
		catch(Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);	
		}
		
		return $arrDatos;
	}
	
	
	// Autenticacion dactilar
	public static function fnGestorBandera($iflag)
	{
		$arrDatos = array();
		$objGn = new CMetodoGeneral();
		
		//Valida si se abrio a bd
		try{
			$objAPI = new Capirestconstanciaafiliacion();

			$arrData = array(
				'iflag' => $iflag
			);

			$resultAPI = $objAPI->consumirApi('fnGestorBandera', $arrData, $GLOBALS["controlador"]);
			//$resultAPI = json_decode($resultAPI, true);

			//Verifica que se haya ejecutado correctamente
			if($resultAPI)
			{	
				if($resultAPI['estatus'] == 1){

					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = 'EXITO';

					foreach($resultAPI['registros'] as $reg)
					{	
						$arrDatos['respuesta']=$reg['respuesta'];
					}
					$objGn->grabarLogx("[ ********** fnGestorBandera **********] ");
					$objGn->grabarLogx('Respuesta fnGestorBandera: '.$arrDatos['respuesta']);

				}else{
					$arrDatos['estatus'] = $resultAPI['estatus'];
					$arrDatos['Error'] = $resultAPI['descripcion'];
				}
				
			}
			else
			{
				$arrDatos['estatus'] = -1;	
				$arrDatos['Error'] = "Error al ejecutar la API";	

			}
		}
		catch(Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);	
		}
		
		return $arrDatos;
	}
	// Autenticacion dactilar
	public static function Autenticadactilar($iFolioGestor,$sCurp,$iopc)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();
		$arrDatos = array();

		$arrDatos['iFolioGestor'] = $iFolioGestor;
		$arrDatos['sCurp'] = $sCurp;
		$arrDatos['iopc'] = $iopc;

		$objGn->grabarLogx("Ejecucion de API");

		$resultAPI = $objAPI->consumirApi('Autenticadactilar',$arrDatos,$GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estatus'] = 1;
				$arrDatos['descripcion'] = "EXITO";
				$arrDatos["selloverificacionbiometricavoluntram"] = $reg["selloverificacionbiometricavoluntram"];
				$arrDatos["mensaje"] 							  = $reg["mensaje"];
				$arrDatos["resultadoverificacionvoluntram"] 	  = $reg["resultadoverificacionvoluntram"];
				$arrDatos["banderadiag"] = $reg["banderadiag"];

			}
			$objGn->grabarLogx("[ ********** Autenticadactilar **********] ");
			$objGn->grabarLogx("[selloverificacionbiometricavoluntram: ] -->".$arrDatos["selloverificacionbiometricavoluntram"]);
			$objGn->grabarLogx("[mensaje: ] -->".$arrDatos["mensaje"]);
			$objGn->grabarLogx("[resultadoverificacionvoluntram: ] -->".$arrDatos["resultadoverificacionvoluntram"]);
			$objGn->grabarLogx("[banderadiag: ] -->".$arrDatos["banderadiag"]);
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "[opcion 54] Error al consultar la API [Datos] " + json_encode($resultAPI);
		}

		return $arrDatos;
	}
	
	
}
?>