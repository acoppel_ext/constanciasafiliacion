-- Function: fncambiarstatusdigitalizacionok(integer, integer)

-- DROP FUNCTION fncambiarstatusdigitalizacionok(integer, integer);

CREATE OR REPLACE FUNCTION fncambiarstatusdigitalizacionok(
    integer,
    integer)
  RETURNS integer AS
$BODY$
/*******************************************
ELABORO :  Sergio Enrique Jimenez Ponce
descripcion: Funcion que se ejecurta al terminar de digitalizar y publicar correctamente para cambiar status
esta funcion revisa en que estatus se encuentra para ver a que estatus cambiar. Solamente queda preparada 
para las solicitudes de traspaso con certificado.

***FOLIO: 775.1
**MODIFICO: CESAR EDUARDO PONCE LOPEZ.
DESCRIPCION: se agrega validación despues de confirmar todos los documentos digitalizados, para que confirme si
el folio fue consultado de manera correcta previamente ante RENAPO, de lo contrario se asigna un nuevo estatus.

*******************************************/
DECLARE
	IFolio          	ALIAS FOR $1;
	iTabla 			ALIAS FOR $2;
	itiposolicitud 		INTEGER;
	itipotraspaso 		INTEGER;
	istatusproceso 		INTEGER;
	icodigodiagnostico 	INTEGER;
	istatusfisico 		INTEGER;
	nResp 			INT4;
	nRegresa 		INT4;
	nExisteRenapo	INTEGER;
	iFlagRenapo		INTEGER;
	cCurp			CHARACTER(18);
	cIp 			CHAR(20);

BEGIN
	cCurp				='';
	istatusproceso 		= 0;
	icodigodiagnostico 	= 0;
	nExisteRenapo		= 0;
	istatusfisico 		= 0;
	iFlagRenapo			= 0;
	nRegresa 			= 0;
	cIp 				= inet_client_addr();

	IF iTabla = 1 THEN --colsolicitudes
		SELECT curp,tiposolicitud,tipotraspaso,statusproceso,codigodiagnostico,statusfisico 
		INTO cCurp,itiposolicitud,itipotraspaso,istatusproceso,icodigodiagnostico,istatusfisico 
		FROM colsolicitudes WHERE folio = IFolio; --Obtiene El Estatus Del Proceso Para Manejar El Camino.
		
		IF EXISTS( SELECT 'OK'
				   FROM mae_solicitudescurp AS a
				   INNER JOIN rensolicitudescurp AS b ON a.num_folioafore = b.folioservicioafore
				   WHERE a.idu_codigoerrorrenapo = 0 AND b.foliosolicitud = iFolio) THEN
				   nExisteRenapo = 1;
				ELSE
				   nExisteRenapo = 0;
				END IF;
		SELECT valor INTO iFlagRenapo FROM tb_pafbanderasrenapo WHERE keyx = 1; -- FLAG DE RENAPO, 0 = "RENAPO INACTIVO NO RESPONDE", 1 = "RENAPO RESPONDIENDO"
		--traspaso con certificado
		IF itiposolicitud in( 27 ) THEN
			IF itipotraspaso IN(0,1,327,427,527) THEN
				IF istatusproceso = 1 AND icodigodiagnostico = 1 AND istatusfisico = 1 THEN
					IF nExisteRenapo = 1 THEN -- SI EXISTE CONSULTA A RENAPO Y ESTE O NO ACTIVO.
						SELECT mdcestatus(IFolio,1,6) INTO nResp;
						nRegresa = 1;
					ELSIF nExisteRenapo = 0 AND iFlagRenapo = 1 THEN --SI NO EXISTE CONSULTA A RENAPO y ESTA ACTIVO.
						SELECT fun_actualizastatusprocesosol(1,IFolio,cCurp) INTO nResp;--Actualiza el statusproceso a 18; validando ante renapo.
						SELECT fun_insertverificarcurprenapo(IFolio,1) INTO nResp;--Se almacena folio para validacion de curp ante renapo por API
						nRegresa = 2;
					ELSIF nExisteRenapo = 0 AND iFlagRenapo = 0 THEN --SI NO EXISTE CONSULTA A RENAPO y ESTA DESACTIVADO RENAPO.
						SELECT mdcestatus(IFolio,1,6) INTO nResp;
						SELECT fun_insertverificarcurprenapo(IFolio,2) INTO nResp;
						nRegresa = 1;
					END IF;
				END IF;
			ELSIF itipotraspaso IN (827, 828, 829, 830, 831, 832, 833) THEN --Se agrega tipo tarspaso 48-Dactilar para que al moemnto de publicar cambie el statusproceso
				IF istatusproceso = 1 AND icodigodiagnostico = 1 AND istatusfisico = 1 THEN
					IF nExisteRenapo = 1 THEN -- SI EXISTE CONSULTA A RENAPO Y ESTE O NO ACTIVO.
						SELECT mdcestatus(IFolio,1,6) INTO nResp;
						nRegresa = 1;
					ELSIF nExisteRenapo = 0 AND iFlagRenapo = 1 THEN --SI NO EXISTE CONSULTA A RENAPO y ESTA ACTIVO.
						SELECT fun_actualizastatusprocesosol(1,IFolio,cCurp) INTO nResp;--Actualiza el statusproceso a 18 validando ante renapo.
						SELECT fun_insertverificarcurprenapo(IFolio,1) INTO nResp;--Se almacena folio para validacion de curp ante renapo por API
						nRegresa = 2;
					ELSIF nExisteRenapo = 0 AND iFlagRenapo = 0 THEN --SI NO EXISTE CONSULTA A RENAPO y ESTA DESACTIVADO RENAPO.
						SELECT mdcestatus(IFolio,1,6) INTO nResp;
						SELECT fun_insertverificarcurprenapo(IFolio,2) INTO nResp;
						nRegresa = 1;
					END IF;
				END IF;
			END IF;
		
		ELSIF itiposolicitud = 227  AND itipotraspaso = 827 THEN
			IF istatusproceso = 1 AND icodigodiagnostico = 1 AND istatusfisico = 1 THEN
				SELECT mdcestatus(IFolio,1,2) INTO nResp;
				nRegresa = 1;
			ELSIF istatusproceso = 2 AND icodigodiagnostico = 1 AND istatusfisico = 1 THEN
				IF nExisteRenapo = 1 THEN -- SI EXISTE CONSULTA A RENAPO Y ESTE O NO ACTIVO.
					SELECT mdcestatus(IFolio,1,6) INTO nResp;
					nRegresa = 1;
				ELSIF nExisteRenapo = 0 AND iFlagRenapo = 1 THEN --SI NO EXISTE CONSULTA A RENAPO y ESTA ACTIVO.
					SELECT fun_actualizastatusprocesosol(1,IFolio,cCurp) INTO nResp;--Actualiza el statusproceso a 18 validando ante renapo.
					SELECT fun_insertverificarcurprenapo(IFolio,1) INTO nResp;--Se almacena folio para validacion de curp ante renapo por API
					nRegresa = 2;
				ELSIF nExisteRenapo = 0 AND iFlagRenapo = 0 THEN --SI NO EXISTE CONSULTA A RENAPO y ESTA DESACTIVADO RENAPO.
					SELECT mdcestatus(IFolio,1,6) INTO nResp;
					SELECT fun_insertverificarcurprenapo(IFolio,2) INTO nResp;
					nRegresa = 1;
				END IF;
			END IF;
		
		ELSIF itiposolicitud in( 26,33 ) THEN
			IF istatusproceso = 1 AND icodigodiagnostico = 1 AND istatusfisico = 1 THEN
				IF nExisteRenapo = 1 THEN -- SI EXISTE CONSULTA A RENAPO Y ESTE O NO ACTIVO.
					SELECT mdcestatus(IFolio,1,6) INTO nResp;
					nRegresa = 1;
				ELSIF nExisteRenapo = 0 AND iFlagRenapo = 1 THEN --SI NO EXISTE CONSULTA A RENAPO y ESTA ACTIVO.
					SELECT fun_actualizastatusprocesosol(1,IFolio,cCurp) INTO nResp;--Actualiza el statusproceso a 18 validando ante renapo.
					SELECT fun_insertverificarcurprenapo(IFolio,1) INTO nResp;--Se almacena folio para validacion de curp ante renapo por API
					nRegresa = 2;
				ELSIF nExisteRenapo = 0 AND iFlagRenapo = 0 THEN --SI NO EXISTE CONSULTA A RENAPO y ESTA DESACTIVADO RENAPO.
					SELECT mdcestatus(IFolio,1,6) INTO nResp;
					SELECT fun_insertverificarcurprenapo(IFolio,2) INTO nResp;
					nRegresa = 1;
				END IF;
			END IF;
		END IF;
		
	END IF;
	IF nRegresa = 1 THEN --VALIDA QUE EL PROCESO SEA NORMAL Si nRegresa = 2 se asignara la creacion de lotes desde la API verificarcurprenapoAPI
		SELECT publicaimgftpcrearlotes( IFolio, cIp) INTO nResp;
	ELSE
		nRegresa = 1;
	END IF;
	RETURN nRegresa;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fncambiarstatusdigitalizacionok(integer, integer)
  OWNER TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fncambiarstatusdigitalizacionok(integer, integer) TO public;
GRANT EXECUTE ON FUNCTION fncambiarstatusdigitalizacionok(integer, integer) TO sysaforeglobal;
