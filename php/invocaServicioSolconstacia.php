<?php
include_once ('clases/global.php');
include_once ('clases/CMetodoGeneral.php');
include_once ('clases/CServicioBusTramites.php');
include_once ('clases/Capirestconstanciaafiliacion.php');
include_once("JSON.php");
$json = new Services_JSON();
$arrResp = array();

$controlador = "CtrlconstanciaAfiliacion";

//Se declaran variables donde se Optiene el valor que les fue mandado del js atraves de ajax
//Primera seccion donde se obtienen los datos del formulario
$iEmpleado =  isset($_POST['empleado']) ? $_POST['empleado']: '0';
$opcion = isset($_POST['opcion']) ? $_POST['opcion']: '';
$iTipoConstancia = isset($_POST['tipoconstancia']) ? $_POST['tipoconstancia']: '0'; 
$sCurp = isset($_POST['curp']) ? $_POST['curp']: ''; 
$sAppaterno = isset($_POST['appaterno']) ? $_POST['appaterno']: '';  
$sApmaterno = isset($_POST['apmaterno']) ? $_POST['apmaterno']: ''; 
$sNombre = isset($_POST['nombre']) ? $_POST['nombre']: '';  
$sNss = isset($_POST['nss']) ? $_POST['nss']: '';
$sNumeroCell = isset($_POST['numerocel']) ? $_POST['numerocel']: ''; 
$iCompaniaCel = isset($_POST['companiacel']) ? $_POST['companiacel']: '0'; 
$sTipoComprobante = isset($_POST['tipocomprobante']) ? $_POST['tipocomprobante']: '';
$sFechaComprobante = isset($_POST['fechacomprobante']) ? $_POST['fechacomprobante']: ''; 
$sNumeroCliente = isset($_POST['numclientecoppel']) ? $_POST['numclientecoppel']: '';

//Segunda seccion donde se obtienen los datos del formulario
$iTipoTelefono = isset($_POST['tipotelefono']) ? $_POST['tipotelefono']: '0'; 
$sTelefonoContacto = isset($_POST['telcontacto']) ? $_POST['telcontacto']: '';
$iCompTel = isset($_POST['comptelefonica']) ? $_POST['comptelefonica']: '0';

//Tercera seccion donde se obtienen los datos del formulario
$sCalle = isset($_POST['calle']) ? $_POST['calle']: '';   
$sNumExt = isset($_POST['numeroexterior']) ? $_POST['numeroexterior']: ''; 
$sNumInt = isset($_POST['numerointerior']) ? $_POST['numerointerior']: ''; 
$sCodPost = isset($_POST['codigopostal']) ? $_POST['codigopostal']: '';
$iColonia = isset($_POST['colonia']) ? $_POST['colonia']: '0';
$iDelegacion = isset($_POST['delegacion']) ? $_POST['delegacion']: '0'; 
$iEstado = isset($_POST['estado']) ? $_POST['estado']: '0';   
$iPais = 'MEX';//isset($_POST['pais']) ? $_POST['pais']: '0'; //342 indica que es mexico
$sLugarSol = isset($_POST['lugarsol']) ? $_POST['lugarsol']: ''; 
$claveconsar =  isset($_POST['claveconsar']) ? $_POST['claveconsar']: ''; 

 //Se obtiene el sello
$iSelloVerificacion = isset($_POST['selloverificacion']) ? $_POST['selloverificacion']: '0';

$iFolioSerAfore =  isset($_POST['folioservicioafore']) ? $_POST['folioservicioafore']: '0'; 
$iFolio =  isset($_POST['folio']) ? $_POST['folio']: '0'; 
//para el consulmo del servicio
$idServicio = 9901;
//$idServicio = 9904;
$idServidor = 5;


switch($opcion) 
{
	case 1:
		$arrResp = ejecutarAplicacionsolconstancia($idServicio,$idServidor, $iEmpleado,$iTipoConstancia,$sNss,$sCurp,$sAppaterno,$sApmaterno,$sNombre,$iCompaniaCel,$sNumeroCell,$sCalle,$sNumExt,$sNumInt,$iColonia,$iDelegacion,$sCodPost,$iEstado,$iPais,$sLugarSol,$claveconsar,$iFolio,$iSelloVerificacion);
	break;
	case 2:
		$arrResp =  obtenerRespuestasolconstancia($idServicio,$iFolioSerAfore,$iFolio);
	break;
	case 3:
		$arrResp = obtenerRespuestaRechazo($iFolioSerAfore);
	break;
}
echo $json->encode($arrResp);


 	function ejecutarAplicacionsolconstancia($idServicio,$idServidor, $iEmpleado,$iTipoConstancia,$sNss,$sCurp,$sAppaterno,$sApmaterno,$sNombre,$iCompaniaCel,$sNumeroCell,$sCalle,$sNumExt,$sNumInt,$iColonia,$iDelegacion,$sCodPost,$iEstado,$iPais,$sLugarSol,$claveconsar,$iFolio,$iSelloVerificacion)
	{
		//$objGn->grabarLogx('[' . __FILE__ . ']'.'Entro a metodo del servicio ejecutarAplicacionsolconstancia');
		//se crea arreglo a retornar
		$arrResp = array();

		$arrCons = array();

		//se crea objeto para invocar los metodos de la clase
		$objGn = new CMetodoGeneral();

		//se declaran varibles en vacio y 0 por si falla la incacion tenga algo por default
		$arrResp->descripcionRespuesta= '';
		$arrResp->folioServicioAfore = 0;
		$arrResp->respondioServicio = 0;

		//se obtine la fecha del servidor
		date_default_timezone_set('America/Mazatlan');
		setlocale(LC_TIME, 'spanish');
		$fechaSolicitud =  strftime("%Y/%m/%d");

		if ($iTipoConstancia == 26 || $iTipoConstancia == 33)
		{
			$iTipoConstancia = 1;
		}
		else
		{
			$iTipoConstancia = 2;
		}

		$iEstado = str_pad($iEstado, 2, "0", STR_PAD_LEFT);

		$sLugarSol = str_pad($sLugarSol, 2, "0", STR_PAD_LEFT);
		
		$claveconsar = str_pad($claveconsar, 10, "0", STR_PAD_LEFT);

		//Se crea array a mandar 
		$array = array(
			    'tipoConstancia' => $iTipoConstancia,
			    'identificadorEnvioContrasena' => 1, //SMS
			    'fechaSolicitud' => $fechaSolicitud,
			    'nss' => $sNss,
			    'curp' => $sCurp,
			    'apellidoPaterno' => $sAppaterno,
			    'apellidoMaterno' => $sApmaterno,
			    'nombreTrabajador' => $sNombre,
			    'companiaTelefonica' => $iCompaniaCel,
			    'telContactoCelular' => $sNumeroCell,
			    'telContactoFijo' => 0,
			    'extension' => '0', //no se ocupa
			    'calle' => $sCalle,
			    'numeroExterior' => $sNumExt,
			    'numeroInterior' => $sNumInt,
			    'colonia' => $iColonia,
			    'delegacion' => $iDelegacion,
			    'codigoPostal' => $sCodPost,
			    'entidadFederativa' => $iEstado,
			    'pais' => $iPais,
			    'numeroEmpleado' => $iEmpleado,
			    'lugarFirmaSolicitud' => $sLugarSol,
			    'claveAgentePromotor' => $claveconsar,
				'selloVerificacionBiometrica' => $iSelloVerificacion
			    );
		
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $array);
		
		//Crear XML a enviar de parametros	
		$xml = $objGn->obtenerXML($array);

		$mensaje = "XML:".$xml;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//actualizara el estatus del folio en la tabla solconstancia
		ActualizaestatusSolconstancia($iFolio);

		//Ejecutar el Servicio 
		$arrResp = $objGn->consumirServicioEjecutarAplicacion($idServicio,$idServidor,$xml);

		//pasa la respuesta obtenida del metodo anterior
		$mensaje = $arrResp->descripcionRespuesta;
		$folioServicioAfore = $arrResp->folioServicioAfore;
		$respuestaservicio = $arrResp->respondioServicio;

		$mensajelog = "Mensaje:".$mensaje.'FolioAfore:'.$folioServicioAfore."RespServicioInv:".$respuestaservicio;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensajelog);
	
		return $arrResp;
	}

	function obtenerRespuestasolconstancia($idServicio,$iFolioSerAfore,$iFolio)
	{
		//se crea objeto para invocar los metodos de la clase
		$objGn = new CMetodoGeneral();

		//se crea arreglo a retornar
		$arrResp = array();
		$mensaje = '';
		$arrResp['descripcion'] = '';
		$arrDatos['estatus'] = '0';

		$mensaje = "Parametro a enviarle a consultar: idservidor:".$idServidor. ' Folio Afore:'.$iFolioSerAfore;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//Ejecutar el Servicio 
		$arrResp = $objGn->consumirServicioObtenerRespuesta($idServicio,$iFolioSerAfore);
		//$mensaje = $arrResp;

		$mensaje = $arrResp->descripcionRespuesta;
		$iRespuesta = $arrResp->respondioServicio;

		$mensaje = "Mensaje: ".$mensaje." RespondioSErvicio:".$iRespuesta;
		$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		//solamente si el metodo consumir servicio responde correctamente, este consultara el estatus del folio
		if ($iRespuesta == 1)
		{
			$arrResp = ConsultaSolconstancia($iFolio);
		}

		return $arrResp;
	}

	//funcion que permite consultar la solicitud de la solconstancia
	function ConsultaSolconstancia($iFolio)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos['descripcion'] = '';
		$arrDatos['estatus'] = '';

    	$arrData = array(
			'iFolio' => $iFolio,
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest");

			$resultAPI = $objAPI->consumirApi('ConsultaSolconstancia', $arrData, $GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$resultAPI = json_decode($resultAPI,true);

				$arrDatos['estado'] = OK___;

				foreach($resultAPI["registros"] as $reg)
				{
					$arrDatos['registros'][]=array_map('trim', $reg);
					$arrDatos['estatus'] = (int)$reg['estatus'];
					$arrDatos['diagnostico'] = trim($reg['diagnostico']);
					$arrDatos['curp'] = trim($reg['curp']);

					$mensaje ='estatus:'.$arrDatos['estatus'].' Diag:'.$arrDatos['diagnostico'].' Curp:'.$arrDatos['curp'];
				}
				if ($arrDatos['estatus'] == 4 && $arrDatos['diagnostico'] !='')
				{
					//atraves de la curp se obtiene la descripcion del rechazo
					$arrDatos['descripcion'] = ConsultaRechazo($arrDatos['curp']);
					$mensaje = 'DES:'.$arrDatos['descripcion'];
				}
				else
				{
					$arrDatos['descripcion'] =trim ($arrDatos['descripcion']);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "Error al consultar la API";
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}

	//funcion que permite consultar el rechazo atraves de la curp
	function ConsultaRechazo($sCurp)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();
		$sDescripcion = '';

    	$arrData = array(
			'sCurp' => $sCurp,
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest");

			$resultAPI = $objAPI->consumirApi('ConsultaRechazo', $arrData, $GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$resultAPI = json_decode($resultAPI,true);

				foreach($resultAPI["registros"] as $reg)
				{
					$arrDatos['registros'][]=array_map('trim', $reg);
					$sDescripcion = utf8_encode($reg['descripcion']);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "Error al consultar la API";
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $sDescripcion;	
	}

	//funcion que permite actualizar el campo estatusproceso a 3, que es enviado a procesar
	function ActualizaestatusSolconstancia($iFolio)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos = array();
		$sRespuesta = '';

    	$arrData = array(
			'iFolio' => $iFolio
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest");

			$resultAPI = $objAPI->consumirApi('ActualizaestatusSolconstanciaSer', $arrData, $GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$resultAPI = json_decode($resultAPI,true);

				foreach($resultAPI["registros"] as $reg)
				{
					$arrDatos['registros'][]=array_map('trim', $reg);
					$sRespuesta = $reg['respuesta'];

					if ($sRespuesta == 1)
					{
						$mensaje = "Se Actualizo el estatuscontingente a 1 para el folio: ".$iFolio;
						$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
					}
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "Error al consultar la API";
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
	}

	function obtenerRespuestaRechazo($iFolioSerAfore)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$datos = array();

    	$arrData = array(
			'iFolioSerAfore' => $iFolioSerAfore,
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest");

			$resultAPI = $objAPI->consumirApi('obtenerRespuestaRechazo', $arrData, $GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$resultAPI = json_decode($resultAPI,true);

				foreach($resultAPI["registros"] as $reg)
				{
					$datos['rechazado']  = trim($reg['rechazo']);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "Error al consultar la API";
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}

		return $datos;
	}
?>