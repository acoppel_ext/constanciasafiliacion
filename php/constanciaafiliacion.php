<?php
include_once ('clases/Cconstaciaafiliacion.php');
include_once ('clases/CMetodoGeneral.php');
include_once("JSON.php");
$json = new Services_JSON();
$arrResp = array();
$objGn = new CMetodoGeneral();

if(isset ($_POST['estatus'])){
	//var_dump($_POST['estatus']);
}

$sIpRemoto 		= '';
//Se declaran variables donde se Optiene el valor que les fue mandado del js atraves de ajax
//Primera seccion donde se obtienen los datos del formulario
$iEmpleado =  isset($_POST['empleado']) ? $_POST['empleado']: '0';
$opcion = isset($_POST['opcion']) ? $_POST['opcion']: '';
$iTipoConstancia = isset($_POST['tipoconstancia']) ? $_POST['tipoconstancia']: '0';
$sCurp = isset($_POST['curp']) ? $_POST['curp']: '';
$sAppaterno = isset($_POST['appaterno']) ? $_POST['appaterno']: '';
$sApmaterno = isset($_POST['apmaterno']) ? $_POST['apmaterno']: '';
$sNombre = isset($_POST['nombre']) ? $_POST['nombre']: '';
$sNss = isset($_POST['nss']) ? $_POST['nss']: '';
$sNumeroCell = isset($_POST['numerocel']) ? $_POST['numerocel']: '';
$iCompaniaCel = isset($_POST['companiacel']) ? $_POST['companiacel']: '0';
$sTipoComprobante = isset($_POST['tipocomprobante']) ? $_POST['tipocomprobante']: '';
$sFechaComprobante = isset($_POST['fechacomprobante']) ? $_POST['fechacomprobante']: '';
$sNumeroCliente = isset($_POST['numclientecoppel']) ? $_POST['numclientecoppel']: '';
//variables a usar para obtener la cadena a ejecutar del morpho.
$iSensor = isset($_POST['sensor']) ? $_POST['sensor']: '0';
$iOpcionmorpho = isset($_POST['opcionmorpho']) ? $_POST['opcionmorpho']: '0';
$iNumeroproceso = isset($_POST['numeroproceso']) ? $_POST['numeroproceso']: '0';
$iTipousuario = isset($_POST['tipousuario']) ? $_POST['tipousuario']: '0';
$iFlagcompara = isset($_POST['flagcompara']) ? $_POST['flagcompara']: '0';

$iFolioBcpl = isset($_POST['foliobcpl']) ? $_POST['foliobcpl']: '0';
$iFolioConstanciaImplicaciones = isset($_POST['iFolioConstanciaImplicaciones']) ? $_POST['iFolioConstanciaImplicaciones']: '';
$aforeCedente = isset($_POST['aforeCedente']) ? $_POST['aforeCedente']: '';
$tipoTraspaso = $_POST['tipoTraspaso'];

$iTipoExcepcion = isset($_POST['tipoexcepcion']) ? $_POST['tipoexcepcion']: '0';

//Segunda seccion donde se obtienen los datos del formulario
$iTipoTelefono = isset($_POST['tipotelefono']) ? $_POST['tipotelefono']: '0';
$sTelefonoContacto = isset($_POST['telcontacto']) ? $_POST['telcontacto']: '';
$iCompTel = isset($_POST['comptelefonica']) ? $_POST['comptelefonica']: '0';

//Tercera seccion donde se obtienen los datos del formulario
$sCalle = isset($_POST['calle']) ? $_POST['calle']: '';
$sNumExt = isset($_POST['numeroexterior']) ? $_POST['numeroexterior']: '';
$sNumInt = isset($_POST['numerointerior']) ? $_POST['numerointerior']: '';
$sCodPost = isset($_POST['codigopostal']) ? $_POST['codigopostal']: '';
$iColonia = isset($_POST['colonia']) ? $_POST['colonia']: '0';
$iDelegacion = isset($_POST['delegacion']) ? $_POST['delegacion']: '0';
$iEstado = isset($_POST['estado']) ? $_POST['estado']: '0';
$iPais = 342;//isset($_POST['pais']) ? $_POST['pais']: '0'; //342 indica que es mexico
$sLugarSol = isset($_POST['lugarsol']) ? $_POST['lugarsol']: '';


$sDesCol = isset($_POST['desCol']) ? $_POST['desCol']: '';
$sDesMun = isset($_POST['desMun']) ? $_POST['desMun']: '';
$sDesEdo = isset($_POST['desEdo']) ? $_POST['desEdo']: '';
$sDesPais = isset($_POST['desPais']) ? $_POST['desPais']: '';
$iFolioSerAfore =  isset($_POST['folioservicioafore']) ? $_POST['folioservicioafore']: '0';
$iEstatusProceso = isset($_POST['estatusproceso']) ? $_POST['estatusproceso']: '0';
$iFirma = isset($_POST['firma']) ? $_POST['firma']: 0;

//Variable para consumir un web service
$sCurpEmpleado = isset($_POST['curpEmpleado']) ? $_POST['curpEmpleado']: '';
$sCurpTrabajador = isset($_POST['curpTrabajador']) ? $_POST['curpTrabajador']: '';
$sCurpSolicitante = isset($_POST['curpsolicitante']) ? $_POST['curpsolicitante']: '';
$iTiposolicitante = isset($_POST['tiposolicitante']) ? $_POST['tiposolicitante']: '';
$tPersona = isset($_POST['tipoPersona']) ? $_POST['tipoPersona']: '';
$idServicio = isset($_POST['tipoServicio']) ? $_POST['tipoServicio']: '';
$idOperacion = isset($_POST['tipoOperacion']) ? $_POST['tipoOperacion']: '';
$Seleccionado = !isset($_POST['checkSeleccionado']) ? 1 : $_POST['checkSeleccionado'];
$Motivo = isset($_POST['motivoSinHuella']) ? $_POST['motivoSinHuella']: '';
$codigo = isset($_POST['codigoHuella']) ? $_POST['codigoHuella']: '';
$idServidor = 5;

//parametros para obterner los parametros de la tabla
$consultafolio = isset($_POST['cfolio']) ? $_POST['cfolio']: '0';
$verificacionFolio = isset($_POST['vfolio']) ? $_POST['vfolio']: '0';

//Parametros que se necesitan para la actualizacion de SolConstancia
$folioactualizar =  isset($_POST['folioaActualizar']) ? $_POST['folioaActualizar']: '0';
$estatusconsulta =  isset($_POST['Cstatus']) ? $_POST['Cstatus']: '';
$estatusexpedienteconsulta =  isset($_POST['Cstatusexpe']) ? $_POST['Cstatusexpe']: '';
$estatusenrolamientoconsulta =  isset($_POST['Cstatusenrol']) ? $_POST['Cstatusenrol']: '';
$diagnosticoconsulta =  isset($_POST['Cdiagnostico']) ? $_POST['Cdiagnostico']: '';
$estatusverificacion =  isset($_POST['Vstatus']) ? $_POST['Vstatus']: '';
$diagnosticoverificacion =  isset($_POST['Vdiagnostico']) ? $_POST['Vdiagnostico']: '';
$selloverificacion =  isset($_POST['Vsello']) ? $_POST['Vsello']: '0';

$diagnostico = isset($_POST['diagnostico']) ? $_POST['diagnostico']: '';

$folioOperacionC = isset($_POST['folioOperacionC']) ? $_POST['folioOperacionC']: '0';
$folioOperacionV = isset($_POST['folioOperacionV']) ? $_POST['folioOperacionV']: '0';


$idServicioC = 9909;
$idServicioV = 9910;
$idOperacionC = '0101';
$idOperacionV = '0401';

//Bloque de variables de Alta Unica
$Keyx = isset($_POST['keyx']) ? $_POST['keyx']: '0';
$numTargeta = isset($_POST['numTargeta']) ? $_POST['numTargeta']: '0';
$folioHuellaAltaUnica = isset($_POST['folioHuellaAltaUnica']) ? $_POST['folioHuellaAltaUnica']: '0';
//$numTrabajador = isset($_POST['numTrabajador']) ? $_POST['numTrabajador']: '0';
$idSesionAltaUnica = isset($_POST['idSesionAltaUnica']) ? $_POST['idSesionAltaUnica']: '0';
$prefijo = isset($_POST['prefijo']) ? $_POST['prefijo']: '0';
$folioretorno = isset($_POST['folioretorno']) ? $_POST['folioretorno']: '0';
$keyxbancoppel = isset($_POST['keyxbancoppel']) ? $_POST['keyxbancoppel']: '0';
$idServicioAltaUnica = 1234;
$idServidorAltaUnica = 4321;

/*consulta respuesta de RENAPO */
$iFolioAfore = isset($_POST['folioservicioafore']) ? $_POST['folioservicioafore']: '';
$iFolioCons = isset($_POST['folioConst']) ? $_POST['folioConst']: '';
$iOpcionRenapo = isset($_POST['opcionRenapo']) ? $_POST['opcionRenapo']: '';
$cDescrip 	= isset($_POST['descripcion']) ? $_POST['descripcion']: '';
$cLiga = isset($_POST['liga']) ? $_POST['liga']: '';

//Estado Civil
$ifolioactualizaestadocivil= isset($_POST['folioactualizaestadocivil']) ? $_POST['folioactualizaestadocivil']: '0';
$iestadocivil 		= isset($_POST['estadocivil']) ? 	  $_POST['estadocivil']: '';

$sIpRemoto = $objGn->getRealIP();

$datosPrevalidador = isset($_POST['data']) ? $_POST['data']: '';
$datosRespuestaPrevalidador = isset($_POST['data']) ? $_POST['data']: '';

$excepcion = isset($_POST['excepcion']) ? $_POST['excepcion']: '0';
$folioS = isset($_POST['folioS']) ? $_POST['folioS']: '0';
$TipoDialogo = isset($_POST['tipodialogo']) ? $_POST['tipodialogo']: '0';
$iFolioEnrolamiento = isset($_POST['folioEnrolamiento']) ? $_POST['folioEnrolamiento']: 0;
$iEstatusEnrolamiento = isset($_POST['estatus']) ? $_POST['estatus']: 0;
$IdentificadorEnvio = isset($_POST['recepcionfolio']) ? $_POST['recepcionfolio']: 0;
$estatusvideo = isset($_POST['estatusvideo']) ? $_POST['estatusvideo']: 0;
$nombreSolicitante = isset($_POST['nombreSolicitante']) ? $_POST['nombreSolicitante']: '';
$apellidoPatSolicitante = isset($_POST['apellidoPatSolicitante']) ? $_POST['apellidoPatSolicitante']: '';
$apellidoMatSolicitante = isset($_POST['apellidoMatSolicitante']) ? $_POST['apellidoMatSolicitante']: '';
$serviciofolio = isset($_POST['serviciofolio']) ? $_POST['serviciofolio']: 0;
$nombreVideo = isset($_POST['nombrevideo']) ? $_POST['nombrevideo']: '';

$cCodigo = isset($_POST['cCodigo']) ? $_POST['cCodigo']: '';

$iFolioSolicitud = isset($_POST['iFolioSolicitud']) ? $_POST['iFolioSolicitud']: '';
$iValor = isset($_POST['iValor']) ? $_POST['iValor']: '';

$folioPrevalidador = isset($_POST['folioPrevalidador']) ? $_POST['folioPrevalidador']: 0;

$pasoRenapo = isset($_POST['pasorenapo']) ? $_POST['pasorenapo']: 0;
$foliosoline = isset($_POST['foliosoline']) ? $_POST['foliosoline']: '';
$foliosolbancoppel = isset($_POST['foliosolbancoppel']) ? $_POST['foliosolbancoppel']: '';
$idCons = isset($_POST['idcons']) ? $_POST['idcons']: '';
$arrDatosCaptura = isset($_POST['datoscaptura']) ? $_POST['datoscaptura']: '';

$NombreArchivoizquierdo = isset($_POST['nombrearchivoizquierdo']) ? $_POST['nombrearchivoizquierdo']: '';
$NombreArchivoderecho = isset($_POST['nombrearchivoderecho']) ? $_POST['nombrearchivoderecho']: '';

$codigoIne = isset($_POST['codigoine']) ? $_POST['codigoine']: '';
$iAutenticacion = isset($_POST['iAutenticacion']) ? $_POST['iAutenticacion']: '';

$iFolioConocimientoTraspaso = isset($_POST['iFolioConocimientoTraspaso']) ? $_POST['iFolioConocimientoTraspaso']: '';
$cel = isset($_POST['cel']) ? $_POST['cel']: '';
$celLada = isset($_POST['celLada']) ? $_POST['celLada']: '';

$cFolioConoTrasp = isset($_POST['cFolioConoTrasp']) ? $_POST['cFolioConoTrasp']: '';

$tipoEntrega = isset($_POST['tipoEntrega']) ? $_POST['tipoEntrega']: '';
$cCurptrabajado = isset($_POST['cCurptrabajado']) ? $_POST['cCurptrabajado']: '';
$tipoCorreoTelefono = isset($_POST['tipoCorreoTelefono']) ? $_POST['tipoCorreoTelefono']: '';

$cfolioTrasCono = isset($_POST['cfolioTrasCono']) ? $_POST['cfolioTrasCono']: '';
$cCurpBucar = isset($_POST['cCurpBucar']) ? $_POST['cCurpBucar']: '';

$ipservidor = isset($_POST['ipservidor']) ? $_POST['ipservidor']: '';

//Variables 891
$folioafiliacion = isset($_POST['folioafiliacion']) ? $_POST['folioafiliacion']: 0;
$NombreCompleto = isset($_POST['NombreCompleto']) ? $_POST['NombreCompleto']: '';
$iFoliosol = isset($_POST['ifoliosol']) ? $_POST['ifoliosol']: 0;
$opcionacuse = isset($_POST['sOpcionAcuse']) ? $_POST['sOpcionAcuse']: '';

//Variabkes 1028.1
$oldName = isset($_POST['oldName']) ? $_POST['oldName']: '';
$newName = isset($_POST['newName']) ? $_POST['newName']: '';
$sCURPTrab = isset($_POST['sCURPTrab']) ? $_POST['sCURPTrab']: '';
$iNumFuncionario = isset($_POST['iNumFuncionario']) ? $_POST['iNumFuncionario']: 0;
$iFolioSolicitud = isset($_POST['iFolioSolicitud']) ? $_POST['iFolioSolicitud']: 0;
$iTipoPerson = isset($_POST['iTipoPerson']) ? $_POST['iTipoPerson']: 0;
$iTipoServ = isset($_POST['iTipoServ']) ? $_POST['iTipoServ']: 0;
$sTipoOperac = isset($_POST['sTipoOperac']) ? $_POST['sTipoOperac']: 0;
$iNumDedo = isset($_POST['iNumDedo']) ? $_POST['iNumDedo']: 0;
$iNist = isset($_POST['iNist']) ? $_POST['iNist']: 0;
$sDispositivo = isset($_POST['sDispositivo']) ? $_POST['sDispositivo']: '';
$iFap = isset($_POST['iFap']) ? $_POST['iFap']: 0;
$sImagen = isset($_POST['sImagen']) ? $_POST['sImagen']: '';
$sTipoServicio = isset($_POST['sTipoServicio']) ? $_POST['sTipoServicio']: '';
$cTemplate = isset($_POST['cTemplate']) ? $_POST['cTemplate']: '';
$iTipodialogo = isset($_POST['iTipodialogo']) ? $_POST['iTipodialogo']: 0;
$iFolio = isset($_POST['iFolio']) ? $_POST['iFolio']: 0;
$nombrearchivo = isset($_POST['nombrearchivo']) ? $_POST['nombrearchivo']: '';
$cNumeroTarjeta = isset($_POST['cNumeroTarjeta']) ? $_POST['cNumeroTarjeta']: '';
$ipCliente = isset($_POST['ipCliente']) ? $_POST['ipCliente']: 0;
$cNumTarjetaBCPL = isset($_POST['cNumTarjetaBCPL']) ? $_POST['cNumTarjetaBCPL']: '';
$iEmpleadoEnrol = isset($_POST['iEmpleadoEnrol']) ? $_POST['iEmpleadoEnrol']: 0;
$documento = isset($_POST['documento']) ? $_POST['documento']: '';
$ipOffline = isset($_POST['ipOffline']) ? $_POST['ipOffline']: '';
$folio = isset($_POST['folio']) ? $_POST['folio']: 0;
$sistemaOperativo = isset($_POST['sistemaOperativo']) ? $_POST['sistemaOperativo']: '';
$tipoConexion = isset($_POST['tipoConexion']) ? $_POST['tipoConexion']: 0;
$authorization = isset($_POST['token']) ? $_POST['token']: '';

//Folio 539 Bitacora Bancoppel
$fechaconsulta = isset($_POST['fechaconsulta']) ? $_POST['fechaconsulta']: '1900-01-01';
$horaconsulta = isset($_POST['horaconsulta']) ? $_POST['horaconsulta']: '';
$folioafiliacion = isset($_POST['folioafiliacion']) ? $_POST['folioafiliacion']: 0;
$tienda= isset($_POST['tienda']) ? $_POST['tienda']: 0;
$empleado= isset($_POST['empleado']) ? $_POST['empleado']: 0;
$idanverso= isset($_POST['idanverso']) ? $_POST['idanverso']: ''; 
$idreverso= isset($_POST['idreverso']) ? $_POST['idreverso']: ''; 
$curp= isset($_POST['sCurp']) ? $_POST['sCurp']: ''; 
$appaterno= isset($_POST['appaterno']) ? $_POST['appaterno']: '';
$apmaterno= isset($_POST['apmaterno']) ? $_POST['apmaterno']: '';
$nombres= isset($_POST['nombres']) ? $_POST['nombres']: '';
$rfc= isset($_POST['rfc']) ? $_POST['rfc']: '';
$fechanac= isset($_POST['fechanac']) ? $_POST['fechanac']: '1900-01-01';
$entidad= isset($_POST['entidad']) ? $_POST['entidad']: 0;
$genero= isset($_POST['genero']) ? $_POST['genero']: 0;
$nacionalidad= isset($_POST['nacionalidad']) ? $_POST['nacionalidad']: 0;
$edoCivil= isset($_POST['edocivil']) ? $_POST['edocivil']: 0;
$numClienteCoppel= isset($_POST['numclientecoppel']) ? $_POST['numclientecoppel']: '';
$numClienteBancoppel= isset($_POST['numclientebancoppel']) ? $_POST['numclientebancoppel']: '';
$nvlEstudio= isset($_POST['nvlestudio']) ? $_POST['nvlestudio']: 0;
$profesion= isset($_POST['profesion']) ? $_POST['profesion']: 0;
$giroNegocio= isset($_POST['gironegocio']) ? $_POST['gironegocio']: 0;
$telefono1= isset($_POST['telefono1']) ? $_POST['telefono1']: '';
$telefono2= isset($_POST['telefono2']) ? $_POST['telefono2']: '';
$correo= isset($_POST['correo']) ? $_POST['correo']: '';
$calle= isset($_POST['calle']) ? $_POST['calle']: '';
$numExterior= isset($_POST['numexterior']) ? $_POST['numexterior']: '';
$numInterior= isset($_POST['numinterior']) ? $_POST['numinterior']: '';
$codigoPostal= isset($_POST['codigopostal']) ? $_POST['codigopostal']: '';
$colonia= isset($_POST['colonia']) ? $_POST['colonia']: 0;
$ciudad= isset($_POST['ciudad']) ? $_POST['ciudad']: 0;
$delegacion= isset($_POST['delegacion']) ? $_POST['delegacion']: 0;
$estado= isset($_POST['estado']) ? $_POST['estado']: 0;
$pais= isset($_POST['pais']) ? $_POST['pais']: '';
$calleCobranza= isset($_POST['callecobranza']) ? $_POST['callecobranza']: '';
$numExteriorCobranza= isset($_POST['numexteriorcobranza']) ? $_POST['numexteriorcobranza']: '';
$numInteriorCobranza= isset($_POST['numinteriorcobranza']) ? $_POST['numinteriorcobranza']: '';
$codigoPostalCobranza= isset($_POST['codigopostalcobranza']) ? $_POST['codigopostalcobranza']: '';
$coloniaCobranza= isset($_POST['coloniacobranza']) ? $_POST['coloniacobranza']: 0;
$ciudadCobranza= isset($_POST['ciudadcobranza']) ? $_POST['ciudadcobranza']: 0;
$delegacionCobranza= isset($_POST['delegacioncobranza']) ? $_POST['delegacioncobranza']: 0;
$estadoCobranza= isset($_POST['estadocobranza']) ? $_POST['estadocobranza']: 0;
$paisCobranza= isset($_POST['paiscobranza']) ? $_POST['paiscobranza']: '';
$calleLaboral= isset($_POST['callelaboral']) ? $_POST['callelaboral']: '';
$numExteriorLaboral= isset($_POST['numexteriorlaboral']) ? $_POST['numexteriorlaboral']: '';
$numInteriorLaboral= isset($_POST['numinteriorlaboral']) ? $_POST['numinteriorlaboral']: '';
$codigoPostalLaboral= isset($_POST['codigopostallaboral']) ? $_POST['codigopostallaboral']: '';
$coloniaLaboral= isset($_POST['colonialaboral']) ? $_POST['colonialaboral']: 0;
$ciudadLaboral= isset($_POST['ciudadlaboral']) ? $_POST['ciudadlaboral']: 0;
$delegacionLaboral= isset($_POST['delegacionlaboral']) ? $_POST['delegacionlaboral']: 0;
$estadoLaboral= isset($_POST['estadolaboral']) ? $_POST['estadolaboral']: 0;
$paisLaboral= isset($_POST['paislaboral']) ? $_POST['paislaboral']: '';
$apPaternoRef1= isset($_POST['appaternoref1']) ? $_POST['appaternoref1']: '';
$apMaternoRef1= isset($_POST['apmaternoref1']) ? $_POST['apmaternoref1']: '';
$nombresRef1= isset($_POST['nombresref1']) ? $_POST['nombresref1']: '';
$apPaternoRef2= isset($_POST['appaternoref2']) ? $_POST['appaternoref2']: '';
$apMaternoRef2= isset($_POST['apmaternoref2']) ? $_POST['apmaternoref2']: '';
$nombresRef2= isset($_POST['nombresref2']) ? $_POST['nombresref2']: '';
$ctatradicionalvigente= isset($_POST['ctatradicionalvigente']) ? $_POST['ctatradicionalvigente']: '';
$ctaNomina= isset($_POST['ctanomina']) ? $_POST['ctanomina']: '';
$opcionbcpl = isset($_POST['opcionbcpl']) ? $_POST['opcionbcpl']: '';

//Autenticacion dactilar
$iflag 			= isset($_POST['iflag']) ? $_POST['iflag']: 0;
$iFolioGestor	= isset($_POST['iFolioGestor']) ? $_POST['iFolioGestor']: 0;
$iopc			= isset($_POST['iopc']) ? $_POST['iopc']: 0;


switch($opcion)
{
	case 1:
	  	//Va al metodo Almacenar datos, donde aplicara validacion previa a ser almacenado el registro
	  	 $arrResp = Cconstancia::AlmacenaDatosConstacia($iFolioSolicitud,$iEmpleado,$iTipoConstancia,$sCurp,$sAppaterno,$sApmaterno,$sNombre,$sNss,$sNumeroCell,$iCompaniaCel,$sTipoComprobante,$sFechaComprobante,$sNumeroCliente,$iTipoTelefono,$sTelefonoContacto,$iCompTel,$sCalle,$sNumExt,$sNumInt,$sCodPost,$iColonia,$iDelegacion,$iEstado,$iPais,$sLugarSol,$sDesCol,$sDesMun,$sDesEdo,$sDesPais,$iFirma,$dExpediente,$dEnrolamiento,$dVerificacion,$sVerificacion,$vVerificacion,$iFolioBcpl,$sCurpSolicitante,$iTiposolicitante, $iFolioConstanciaImplicaciones, $aforeCedente, $tipoTraspaso, $folioPrevalidador,$iTipoExcepcion,$iFolioConocimientoTraspaso);
		break;
	case 2:
		//consulta el catalogo de comprobante de domicilio
		$arrResp = Cconstancia::ConsultacatalogoComprobante();
	    break;
	case 3:
		//consulta el catalogo de compañias celulares
		$arrResp = Cconstancia::ConsultacatalogoCompaCelulares();
		break;
	case 4:
		//consulta la curp en la tabla solconstancia
		$arrResp = Cconstancia::Consultacurp($sCurp,$iTipoConstancia);
		break;
	case 5:
		//consulta la curp en la tabla solconstancia
		$arrResp = Cconstancia::ConsultaRechazoCurp($sCurp);
		break;
	case 6:
		//consulta la curp en la tabla solconstancia
		$arrResp = Cconstancia::ConsultacatalogoLugarSolicitud();
		break;
	case 7:
		$arrResp = Cconstancia::obtenerdatospromotor($iEmpleado);
		break;
	case 8:
		$arrResp = new stdClass();
		date_default_timezone_set('America/Mazatlan');
		setlocale(LC_TIME, 'spanish');
		$arrResp->resp = strftime("%y-%m-%d");
		break;
	case 9:
		//Funcion que regresa la curp en caso de que exista en la tabla colsolicitudes
		//y cumpla con ciertos parametros y no se debe de almacenar.
		$arrResp = Cconstancia::consultacolsolicitudes($sCurp);
		break;
	case 10:
		$arrResp = Cconstancia::consultainformacionsolconstancia($sCurp,$iEstatusProceso);
		break;
	case 11:
		//Funcion que regresa la curp en caso de que exista en la tabla colsolicitudes
		//y cumpla con ciertos parametros y no se debe de almacenar.
		//CAMBIO: TAMBIEN REVISA EL TIPO DE SOLICITUD QUE TIENE GUARDADO EN BD.
		//SI ES INDEPENDIENTE O ISSSTE, QUE PERMITA AFILIAR UN IMSS
		$arrResp = Cconstancia::consultatipoconstancia($sCurp);
		break;
	case 12:
		//Funcion que regresa el estatus de la invocacion del servicio
		$arrResp = Cconstancia::consultaInvocacionServicio($sCurp);
		break;
	case 13:
		//funcion que regresa si el promotor se encuentra enrolado
		$arrResp = Cconstancia::obtenerestatusempleado($iEmpleado);
		break;
	case 14:
		//funcion que regresa si el promotor se encuentra enrolado
		$arrResp = Cconstancia::obtenercurpempleado($iEmpleado);
		break;
	case 16:
		$arrResp = Cconstancia::obtenernumerofolio();
	break;
	case 17:
		$arrResp = Cconstancia::ejecutarAplicacionConstanciaConsulta($sCurpEmpleado, $sCurpTrabajador, $tPersona, $idServicio, $idServidor, $idOperacion);
	break;
	case 18:
		$arrResp = Cconstancia::consultadatosbiometricos($consultafolio, $verificacionFolio);
	break;
	case 19:
		$arrResp = Cconstancia::actualizarSolConstancia($folioactualizar, $estatusconsulta, $estatusexpedienteconsulta, $estatusenrolamientoconsulta, $diagnosticoconsulta, $estatusverificacion, $diagnosticoverificacion, $selloverificacion);
	break;
	case 20:
		$arrResp = Cconstancia::obtenerenlace();
	break;
	case 21:
		$arrResp = Cconstancia::obtenerestatusEnrolamiento($sCurp, $iFolioAfore);
	break;
	case 22:
		$arrResp = Cconstancia::ejecutarAplicacionConstanciaVerificacion($sCurpEmpleado, $sCurpTrabajador, $tPersona, $idServicio, $idServidor, $idOperacion, $Seleccionado, $Motivo, $codigo);
	break;
	case 23:
		$arrResp = Cconstancia::dianosticoErrores($diagnostico);
	break;
	case 24:
		$arrResp = Cconstancia::obtenerRespuestaEnrolHuellaServicio($folioOperacionC, $folioOperacionV, $sCurp, $idServicioC, $idServicioV, $idOperacionC, $idOperacionV);
	break;
	case 25:
		$arrResp = Cconstancia::grabarLogJs($estatusconsulta, $diagnosticoconsulta, $estatusverificacion, $diagnosticoverificacion, $estatusexpedienteconsulta, $estatusenrolamientoconsulta, $selloverificacion, $iTipoConstancia);
	break;
	case 27:
		$arrResp = Cconstancia::obtenermensajerespuestaAU($folioretorno);
	break;
	case 28:
		$arrResp = Cconstancia::obteneriformacionclientebancoppel($keyxbancoppel);
	break;
	case 29:
		//Metodo para revisar si el numero de telefono se encuentra en la lista negra
		$arrResp = Cconstancia::validarListaNegraTelefonoCel($sNumeroCell ,  $iEmpleado , $sCurp);
	break;
	case 30:
		$arrResp = Cconstancia::actualizarflagclientebcpl($keyxbancoppel);
	break;
	case 31:
		$arrResp = Cconstancia::validacion($keyxbancoppel);
	break;

	/******* Consulta la respuesta del servicio de RENAPO ******/
	case 32:
		$arrResp = Cconstancia::consultaRespuestaRenapo($iFolioAfore);
	break;

	case 33:
		$arrResp = Cconstancia::consultarDatosRenapo($iFolioAfore);
		$arrResp=array_map("utf8_encode", $arrResp );
	break;
	case 34:
		$arrResp = Cconstancia::validarCurpenReanpo($sCurp);
	break;
	case 35:
		$arrResp = cConstancia::actualizarFolRenapo($iFolioCons, $iFolioAfore);
	break;
	case 36:
	 	$objGn->grabarLogx( '[' . __FILE__ . ']' .'entro a obtener la liga de renapo con parametros '.$iOpcionRenapo.' '.$iFolioAfore);
		$arrResp = cConstancia::obtenerLigaRenapo($iOpcionRenapo,$iFolioAfore);
	break;
	case 37:
		$objGn->grabarLogx('Se Registra que: '.$cDescrip);
	break;
	case 38:
		$objGn->grabarLogx('bucar liga : '.$cLiga);
		if( file_exists($cLiga) )
		{
			$objGn->grabarLogx('SI'.$cLiga);
			$bandera = 1;
		}
		else
		{
			$objGn->grabarLogx('NO'.$cLiga);
			$bandera = 0;
		}
		$objGn->grabarLogx('vale bandera : '.$bandera);
		return $bandera;
	break;
	case 39:
		//Metodo para revisar si el numero de telefono se encuentra en la lista negra
		$arrResp = Cconstancia::validarListaNegraTelefonofijo( $sTelefonoContacto , $iEmpleado , $sCurp);
	break;
	case 40:
		//consulta el catalogo de Estado Civil
		$arrResp = Cconstancia::Consultacatalogoestadocivil();
	break;
	case 41:
		//consulta el catalogo de Estado Civil
		$arrResp = Cconstancia::ActualizarEstadoCivil($ifolioactualizaestadocivil, $iestadocivil);
	break;
	case 42:
		//Metodo para revisar si el numero de telefono es movil o fijo
		$arrResp = Cconstancia::validaTelefonoCelular($sNumeroCell);
	break;
	case 43:
		//Metodo para retornar los datos del trabajador con afiliacion previa
		$objGn->grabarLogx('NO-->'.$sCurp);
		$arrResp = Cconstancia::validarregistroprevio($sCurp);
	break;
	case 44:
		$arrResp = Cconstancia::validarExpediente($sCurp,$sIpRemoto);
		break;
	case 45:
		$arrResp = Cconstancia::grabarExcepcion($excepcion,$iFolioCons,$iEmpleado);
		break;
	case 46:
		$arrResp = Cconstancia::ConsultacatalogoSolicitante();
	break;
	case 47:
		$arrResp = Cconstancia::ConsultarDialogoVideo($TipoDialogo);
	break;
	case 48:
		$arrResp = Cconstancia::ObtenerUrlsVideo();
	break;
	case 49:
		$arrResp = Cconstancia::fnobtenerDatosCapturaVideo($iFolioCons, $iEmpleado);
	break;
	case 50:
		$arrResp = Cconstancia::obtenerEstatusVideo($iFolioCons);
	break;
	case 51:
		$arrResp = Cconstancia::validarenrolamientoConstancia($sCurp);
	break;
	case 52:
		$arrResp = Cconstancia::guardarVideoAfiliacion($iFolioCons,$nombreVideo);
	break;
	case 53:
		$arrResp = Cconstancia::obtenerClaveOperacion();
	break;
	case 54:
		$arrResp = Cconstancia::obtenerestatusfinado($iFolioCons);
		break;
	case 55:
		$arrResp = Cconstancia::actualizarestatusvideo($iFolioCons, $estatusvideo);
	break;
	case 56:
		$arrResp = Cconstancia::obtenerIpCluster();
	break;
	case 57:
		$arrResp = Cconstancia::verificarEnrolamiento($iFolioCons);
	break;
	case 58:
		$arrResp = Cconstancia::actualizarEstatusEnrolamiento($iFolioEnrolamiento,$iEstatusEnrolamiento,$sCurp);
	break;
	case 59:
		$arrResp = Cconstancia::guardarcontrolenviofolio($iFolioCons,$IdentificadorEnvio);
	break;
	case 60:
		$arrResp = Cconstancia::fnactualizadatossolicitanterenapo($nombreSolicitante,$apellidoPatSolicitante,$apellidoMatSolicitante,$iFolioCons);
	break;
	case 61:
		$arrResp = Cconstancia::ejecutaServicioFolio($iFolioCons,$iEmpleado,$serviciofolio);
	break;
	case 62:
		$arrResp = Cconstancia::validarRespuestaFolio($iFolioCons);
	break;
	case 63:
		$arrResp = Cconstancia::generarFolioEnrolamiento($iFolioCons,$iEmpleado,$sCurp,$iTipoConstancia);
	break;
	case 64:
		$arrResp = Cconstancia::actualizarestatusprocesosolconstancia($iFolioCons);
	break;
	case 65:
	$arrResp = Cconstancia::registrarDatosPrevalidador($datosPrevalidador);
	break;
	case 66:
		$arrResp = Cconstancia::respuestaPrevalidador($idServidor, $datosRespuestaPrevalidador);
	break;
	case 67:
		$arrResp = Cconstancia::obtenerSelloPrevalidador($sCurpEmpleado, $sCurpTrabajador, $tPersona, $idServicio, $idServidor, $idOperacion);
	break;
	case 68:
		$arrResp = Cconstancia::ejecutaPrevalidacion();
		break;
	case 69:
		$arrResp = Cconstancia::obtenerRespuestaPrevalidador($datosPrevalidador);
		break;
	case 70:
		$arrResp = Cconstancia::obtenerMensajeErrorPrevalidador($cCodigo);
		break;
	case 71:
		$arrResp = Cconstancia::obtenerBandMensajePromotor();
		break;
	case 72:
		$arrResp = Cconstancia::actualizarBandMensajePromotor($iFolioSolicitud, $iValor);
		break;
	case 73:
	$arrResp = Cconstancia::obtenerLigaIne($iEmpleado,$sCurp,$pasoRenapo,$iAutenticacion);
		break;
	case 74:
		$arrResp = Cconstancia::obtenerEstatusIne($sCurp);
		break;
	case 75:
		$arrResp = Cconstancia::actualizarFolioIne($iFolioCons, $sCurp, $iEmpleado, $foliosoline);
		break;
	case 76:
		$arrResp = Cconstancia::fntieneautenticacionine($sCurp,$iEmpleado);
		break;
	case 77:
		$arrResp = Cconstancia::validarcodigoautenticacion($iFolioCons, $codigoIne);
		break;
	case 78:
		$arrResp = Cconstancia::obtenerRecuperacionFolioConstancia($sCurp);
		break;
	case 79: $arrResp = Cconstancia::llamarapi($idCons, $arrDatosCaptura);
		break;
	case 80: $arrResp = $sIpRemoto;
		break;
	case 81:
		$arrResp = Cconstancia::verificaExpedientePermanente($iFolioAfore);
		break;
	case 82:
		$arrResp = Cconstancia::guardarHuellasIndices($NombreArchivoizquierdo,$NombreArchivoderecho,$sCurpTrabajador,$iEmpleado,$sIpRemoto);
		break;
	case 83:
		$arrResp = Cconstancia::limpiarHuellasServicio($NombreArchivoizquierdo,$NombreArchivoderecho);
		break;
	case 84:
		$arrResp = Cconstancia::verificaEjecucionServicio();
		break;
	case 85:
		$arrResp = Cconstancia::elminarinformacionServicio();
		break;
	case 86:
		$arrResp = Cconstancia::obtenerEstatusAfiliacion($sCurp);
		break;
	case 87:
		$arrResp = Cconstancia::actualizarSolEstatusExpiden($iFolioCons,$sCurp);
		break;
	case 88:
		$arrResp = Cconstancia::ActualizarExpedientePermante($iFolioCons,$sCurp);
		break;
		//obtencion para mandar correo o mensaje
	//Folio 539
	case 89:
		$arrResp = Cconstancia::obtenerLlaveDatosBancoppel($sCurp);
		break;
	//Folio 539
	case 90:
		$arrResp = Cconstancia::obtenerEntidadNacimiento();
		break;
	//Folio 539
	case 91:
		$arrResp = Cconstancia::respuestaWSBancoppel($nombres, $appaterno, $apmaterno, $fechanac, $genero, $entidad);
	break;

	//-----------------------------------------------------------------------------------------------------------------------
	//Folio 891
	case 92:
		$arrResp = Cconstancia::envioctdimg($folioafiliacion,$iFirma);
		break;
	//Folio 891 Guarda datos acuse carta derechos
	case 93:
		$arrResp = Cconstancia::insertaDatosAcuse($NombreCompleto, $sCurp, $iFoliosol, $opcionacuse);
		break;
	//Folio 539
	case 94:
		$arrResp = Cconstancia::guardarBitacoraRespuestaBancoppel($fechaconsulta,$horaconsulta,$folioafiliacion, $tienda, $empleado, $idanverso, $idreverso, $curp, $appaterno, $apmaterno, $nombres, $rfc, $fechanac, $entidad, $genero, $nacionalidad, $edoCivil, $numClienteCoppel, $numClienteBancoppel, $nvlEstudio, $profesion, $giroNegocio, $telefono1, $telefono2, $correo, $calle, $numExterior, $numInterior, $codigoPostal, $colonia, $ciudad, $delegacion, $estado, $pais, $calleCobranza, $numExteriorCobranza, $numInteriorCobranza, $codigoPostalCobranza, $coloniaCobranza, $ciudadCobranza, $delegacionCobranza,$estadoCobranza,$paisCobranza,$calleLaboral,$numExteriorLaboral,$numInteriorLaboral,$codigoPostalLaboral,$coloniaLaboral,$ciudadLaboral,$delegacionLaboral,$estadoLaboral,$paisLaboral,$apPaternoRef1, $apMaternoRef1, $nombresRef1, $apPaternoRef2,$apMaternoRef2,$nombresRef2,$ctatradicionalvigente,$ctaNomina,$opcionbcpl);
	break;
	//-----------------------------------------------------------------------------------------------------------------------
	
	case 95:
		$arrResp = Cconstancia::validaEstatusVideo($folioafiliacion);
		break;
	case 96:
		$arrResp = Cconstancia::verificajfuse($iFolioAfore);
		break;

		
	// Autenticacion dactilar
	case 97:
		$arrResp = Cconstancia::Autenticadactilar($iFolioGestor,$sCurp,$iopc);
	break;	
	case 98:
		$arrResp = Cconstancia::fnGestorBandera($iflag);
	break;
	

	// Folio 1028.1
	case 201:
		$arrResp = Cconstancia::generarArchivoHuellas($NombreArchivo, $Template);
		break;
	//-----------------------------------------------------------------------------------------------------------------------
	case 303:
		$arrResp = Cconstancia::verificarTelefono($cel, $celLada);
		break;
	case 506:
		$arrResp = Cconstancia::obtenerErrorMensajeFct($cFolioConoTrasp);
		break;
	case 507:
		  $arrResp = Cconstancia::insertarTipoEntregaCorreoTelefono($tipoEntrega,$cCurptrabajado,$tipoCorreoTelefono,$sIpRemoto,$iEmpleado);
		break;
	case 508:
		$arrResp = Cconstancia::fnGuardarFolioColsolicitud($cfolioTrasCono,$cCurpBucar);
		  break;
	case 509:
		$arrResp = Cconstancia::agregarDatosConsultaFolio($iEmpleado, $sCurp, $iFolioConocimientoTraspaso, $iFolioSolicitud);
		break;
	case 510:
		$arrResp = Cconstancia::obtencionFolioSulicitud($sCurp);
		break;
	case 511:
		$arrResp = Cconstancia::obtenerIPServidor($sIpRemoto);
		break;
	/*---------Folio 1028.1-----------------------
    ----------------------------------------------*/
	case 601:
		$arrResp = Cconstancia::cambiarNombreImg($oldName, $newName);
		break;
	case 602:
		$arrResp = Cconstancia::enrolhuellasservicio($sCURPTrab, $iNumFuncionario, $iFolioSolicitud, $iTipoPerson, $iTipoServ, $sTipoOperac, $iNumDedo, $iNist, $sDispositivo, $iFap, $sImagen, $sTipoServicio, $cTemplate);
		break;
	case 603:
		$arrResp = Cconstancia::buscarTextoVideo($iTipodialogo,$iFolio);
		break;
	case 604:
		$arrResp = Cconstancia::reubicarVideo($nombrearchivo, $iFolio);
		break;
	case 605:
		$arrResp = Cconstancia::guardarTarjeta($cNumeroTarjeta, $ipCliente);
		break;
	case 606:
		$arrResp = Cconstancia::validarTarjetaBcpl($cNumTarjetaBCPL, $cTemplate);
		break;
	case 607:
		$arrResp = Cconstancia::indexarFormatoEnrolamiento($iFolio, $iEmpleadoEnrol, $documento, $ipOffline);
		break;
	case 608:
		$arrResp = Cconstancia::moverFormatoEnrol($folio);
		break;
    case 609:
		$arrResp = Cconstancia::actualizaIDRE($folio);
		break;
	case 610:
		$arrResp = Cconstancia::almacenaDispositivoAfiliacion($folio,$sistemaOperativo,$tipoConexion);
		break;
	case 611:
		$arrResp = Cconstancia::ligaMenuAfore();
		break;
	case 612:
		$arrResp = Cconstancia::cerrarSesion($iEmpleado);
		break;
	case 613:
		$_SESSION["authorization"] = $authorization;
		break;	
}
echo $json->encode($arrResp);

?>