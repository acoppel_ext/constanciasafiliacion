<?php
error_reporting(1);
include_once ('global2.php');
//include("fpdf.php");
include_once('../../../fpdf/fpdf.php');
include_once('../../../fpdi/fpdi.php');
include_once ("CMetodoGeneral.php");
include_once ('CServicioBusTramites.php');
include_once("JSON.php");
include_once ("CLog.php");
include_once("ObtencionRespuesta.php");
include_once('Capirestconstanciaafiliacion.php');
define("RUTA_SALIDA","/sysx/progs/web/salida/constanciasafiliacion/");

date_default_timezone_set('America/Mazatlan');

$controlador = "CtrlconstanciaAfiliacion";

//$json = new Services_JSON();

$sCurp = isset($_GET["sCurp"]) ? $_GET["sCurp"] : 0;
$iMarca = 0;
$iFirma = 0;
$iOpcion = 1;

switch($iOpcion) 
{
	case 1:
		$arrResp = generarConstanciaPDF($sCurp, $iMarca, $iFirma);
	break;
}


function consultaCatalogoAfore()
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$arrDatos = array();
	$arrData = array();

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('consultaCatalogoAfore', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$arrDatos['estatus'] = 1;
			$arrDatos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{			
				$arrDatos['registros'][]=array_map('trim', $reg);
				$clave = $arrDatos['registros']['claveafore'];
				$afore = $arrDatos['registros']['nombreafore'];
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $arrDatos;
}

function validaSolicitud($sCurp, $iNumeroEmpleado, $sContrasenia)
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$arrResp['respuesta'] = 0;
	$arrResp['descripcion'] = "";
	$arrResp['folio'] = 0;
	$arrResp['tipoconstancia'] = 0;

	try
	{
		$arrResp = CMetodoGeneral::obtenerCurpPromotor($iNumeroEmpleado); 
		$sCurpPromotor = $arrResp['curpPromotor'];
		$arrResp['descripcion'] = '';
		$estado = $arrResp['respuesta'];

		switch ($estado) {
			case -1:
				$arrResp['respuesta'] = -1;
				$arrResp['descripcion'] = "Error, Promotor  favor de reportarlo a Mesa de Ayuda";
					CLog::escribirLog( '[' . __FILE__ . ']' . "No se obtuvo la curp de promotor");			
				break;
			case 0:
				$arrResp['respuesta'] = 0;
				$arrResp['descripcion'] = "Promotor no valido para realizar esta operación";
				break;
			case 1:
				$arrData = array(
					'sCurp' => $sCurp,
					'sContrasenia' => $sContrasenia
				);

				$objGn->grabarLogx("Inicio API Rest");

				$resultAPI = $objAPI->consumirApi('validaSolicitud', $arrData, $GLOBALS["controlador"]);
				
				if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
				{

					foreach($resultAPI["registros"]  as $reg)
					{						
						$arrResp['respuesta'] = $reg['respuesta'];
						$arrResp['descripcion'] =  $reg['descripcion'];
						$arrResp['folio'] = $reg['folio'];
						$arrResp['tipoconstancia'] = $reg['tipoconstancia'];
						$arrResp['publicoimpconstancia'] = $reg['publicoimpconstancia'];
						$arrResp['confirma'] = $reg['confirma'];
					}
						$respuesta =$arrResp['respuesta'];
						CLog::escribirLog("Descripción de Respuesta: ".$arrResp['descripcion']);
						CLog::escribirLog("Tipo Constancia = ".$arrResp['tipoconstancia']);
				}
				else
				{
					if(isset($resultAPI["tipo"]))
						header('HTTP/1.1 401 Unauthorized');
					else
						header('HTTP/1.1 409 Conflict');

					return "Error al consultar la API";
				}
				break;
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $arrResp;
}

function ejecutarAplicacion($idServicio,$idServidor,$sCurp, $sContrasenia, $iNumeroEmpleado)
{
	$arrCurp = array();
	$arrResp = array();

	$arrResp->descripcionRespuesta= '';
	$arrResp->folioServicioAfore = 0;

	$arrResp['curpPromotor'] = '';
	$xml = '';

	$arrCurp = CMetodoGeneral::obtenerCurpPromotor($iNumeroEmpleado); 
	$sCurpPromotor = $arrCurp['curpPromotor'];

	$array = array(
			'identificadorObtencionConstancia' => '1',
			'curp' => $sCurp,
			'curpPromotor' => $sCurpPromotor,
			'contrasena' => $sContrasenia,
			'claveAfore' => '568',
			'numeroEmpleado' => $iNumeroEmpleado
			);
	//Crear XML a enviar de parametros	
	$xml = CMetodoGeneral::obtenerXML($array);

	//Ejecutar el Servicio 
	$arrResp = CMetodoGeneral::consumirServicioEjecutarAplicacion($idServicio,$idServidor,$xml);

	$mensaje = $arrResp->descripcionRespuesta;
	$folioServicioAfore = $arrResp->folioServicioAfore;
	$iRespuesta = $arrResp->respondioServicio;

	CLog::escribirLog("mensaje: ".$mensaje. "Valor de Respuesta: ". $iRespuesta) ;
	
	return $arrResp;
}

function obtenerRespuesta($idServicio, $folioServicioAfore)
{
	$arrResp = array();

	$arrResp->descripcionRespuesta= '';
	$arrResp->respondioServicio = 0;

	//Ejecutar el Servicio 
	$arrResp = CMetodoGeneral::consumirServicioObtenerRespuesta($idServicio,$folioServicioAfore);
	$mensaje = $arrResp->descripcionRespuesta;
	$iRespuesta = $arrResp->respondioServicio;

	CLog::escribirLog("mensaje: ".$mensaje. "Valor de Respuesta: ". $iRespuesta) ;

	return $arrResp;

}

function obtenerDatosConstancia($sCurp)
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$arrResp['respuesta'] = 0;	
	$arrResp['descripcion'] = '';

	$arrData = array(
		"sCurp" => $sCurp
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('obtenerDatosConstancia', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$arrResp['estatus'] = 1;
			$arrResp['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{			
				$arrResp['fechavigenciacontrasenia'] = $reg['fechavigenciacontrasenia'];
				$arrResp['fechageneraconstancia'] = $reg['fechageneraconstancia'];
				$arrResp['fechavigenciaconstancia'] = $reg['fechavigenciaconstancia'];
				$arrResp['folioconstancia'] = $reg['folioconstancia'];
				$arrResp['nombreaforeactual'] = $reg['nombreaforeactual'];
				$arrResp['claveafore'] = $reg['claveafore'];
				$arrResp['nss'] = $reg['nss'];
				$arrResp['curp'] = $reg['curp'];
				$arrResp['nombres'] = $reg['nombres'];
				$arrResp['appaterno'] = $reg['appaterno'];
				$arrResp['apmaterno'] = $reg['apmaterno'];
				$arrResp['tipoconstancia'] = $reg['tipoconstancia'];
				$arrResp['lugarsolicitud'] = $reg['lugarsolicitud'];
				$arrResp['fechaemision'] = $reg['fechaemision'];
				$arrResp['rendimiento'] = $reg['rendimiento'];
				$arrResp['folio'] = $reg['folio'];	
			}
			$arrResp['respuesta'] =1;
			$arrResp['descripcion'] = "Se obtuvo información de trabajador";
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	
	$mensaje = $arrResp['descripcion'];
	CLog::escribirLog($mensaje);

	return $arrResp;
}

function generarConstanciaPDF($sCurp,$iMarca,$iFirma)
{		
	$arrValida =array();
	$arrResp =array();
	$arrRespuestaPublica =array();
	$respuesta = new stdClass();
	$respuesta->descestatus = '';
	$respuesta->estado = 0;
	$arrValida = validarDiagnosticoConstancia($sCurp);
	$iRespuesta = $arrValida['respuesta'];
	$cCadenaImagenJpg = '';
	$cFolioCliente = 0;
	$sNombreImagenConstancia = '';
	$iCveOperacion = 0;
	$pdf->align = "center";
	$sRutaImagenNombreTrabajador = '';
	$sRutaImagenFirmaTrabajador = '';
	$sRutaImagen = '';
	$cCadenaImagen = '';
	
	$sizeFontTitulo = 16;
				
	$iAltoRen = 5.5;
	$iBorder = 0;
				
	$iPidActual = getmypid();
	$objMetodoGeneral = new CMetodoGeneral();
	CLog::escribirLog("ejecuta: ".$iRespuesta);
	
	if ($iRespuesta == 1)
	{
		$iRespuestaEjecucion = $arrValida['respuestaEjecucion'];
		$respuesta->estadoEjecucion = $iRespuestaEjecucion;	
		CLog::escribirLog("diagnostico: ".$iRespuestaEjecucion);
		if($iRespuestaEjecucion == 1)
		{				
			$arrResp['respuesta'] = 0;
			$arrResp['estado'] = '';
			
			
			$pdf = new FPDF();
			$pdf->AliasNbPages();
			$pdf->AddPage('L','Letter');

			$arrResp = obtenerDatosConstancia($sCurp);
			
			if($arrResp['respuesta'] == 1)
			{
				//FORMATO CONTANCIA
				//$pdf->SetAutoPageBreak(false, 2);
				$pdf->SetAutoPageBreak(true, 0);
				$iAnchoHojaMM = 215;					
				$iAltoHojaMM = 279;
				
				$cFolioCliente = $arrResp['folio'];
				$tipoConstancia = $arrResp['tipoconstancia'];
				
				
				if ($tipoConstancia == 27)//Plantilla para Traspaso
				{
					$pdf->Image('/sysx/progs/web/plantillas/plconstanciasafiliacion/ConstanciaTraspaso_50.jpg',7,5,$iAltoHojaMM-10, $iAnchoHojaMM-15); 
				}
				else if ($tipoConstancia == 26 || $tipoConstancia == 33)//Plantilla para Registro
				{
					$pdf->Image('/sysx/progs/web/plantillas/plconstanciasafiliacion/Constancia_50.jpg',7,5,$iAltoHojaMM-10, $iAnchoHojaMM-15);
				}
				
				//$pdf->Image('/sysx/progs/web/plantillas/plconstanciasafiliacion/publicidad_constancia.JPG', 10, 163, 134, 30);
				
				$fechaEmision = $arrResp['fechaemision'];
				$diaEmision = substr ( $fechaEmision ,8, 2);
				$mesEmision = substr ( $fechaEmision ,5, 2);
				$anioEmision = substr ( $fechaEmision ,0, 4);
				
				$pdf->SetFont('Arial','',11);
				$pdf->Text(223,56 , utf8_decode($diaEmision));
				$pdf->Text(234,56 , utf8_decode($mesEmision));
				$pdf->Text(244,56 , utf8_decode($anioEmision));
				
				$pdf->SetFont('Arial','',11);
				$pdf->Text(160,56 , 'SUC COMERCIAL');
				
				if (strlen($arrResp['nombres']) <= 23) //26 es el largo maximo para poder utilizar font a 12
				{
					$pdf->SetFont('Arial','',11);
					$pdf->Text(90,74 , $arrResp['nombres']);
					$pdf->Text(150,74 , $arrResp['appaterno']);
					$pdf->Text(213,74 , $arrResp['apmaterno']);
					//$pdf->Text(70,95 , utf8_decode($arrResp['curp']));
				}
				else if(strlen($arrResp['nombres']) > 23 && strlen($arrResp['nombres']) <= 34) //Si sobrepasa los 26 bajar el font a 10 para que no se encime
				{
					$pdf->SetFont('Arial','',11);
					$pdf->Text(60,73 , $arrResp['nombres']);
					$pdf->Text(150,73 , $arrResp['appaterno']);
					$pdf->Text(213,73 , $arrResp['apmaterno']);
				}
				else if(strlen($arrResp['nombres']) >= 35) //Si sobrepasa los 26 bajar el font a 10 para que no se encime
				{
					$pdf->SetFont('Arial','',10);
					$pdf->Text(60,73 , $arrResp['nombres']);
					$pdf->Text(150,73 , $arrResp['appaterno']);
					$pdf->Text(213,73 , $arrResp['apmaterno']);
				}
				//CURP
				$pdf->SetFont('Arial','',11);
				$pdf->Text(81,94 , utf8_decode($arrResp['curp']));
				
				//FOLIO CONSTANCIA
				$pdf->SetFont('Arial','',11.5);
				$pdf->Text(130,113.5 , utf8_decode($arrResp['folioconstancia']));
				
				//FECHA VIGENCIA
				$fechaVigenciaConstancia = $arrResp['fechavigenciaconstancia'];
				$diaVigenciaConstancia = substr ( $fechaVigenciaConstancia ,8, 2);
				$mesVigenciaConstancia = substr ( $fechaVigenciaConstancia ,5, 2);
				$anioVigenciaConstancia = substr( $fechaVigenciaConstancia ,0, 4);
				
				$pdf->SetFont('Arial','',11);
				$pdf->Text(234,189 , utf8_decode($mesVigenciaConstancia));
				$pdf->Text(222,189 , utf8_decode($diaVigenciaConstancia));
				$pdf->Text(244,189 , utf8_decode($anioVigenciaConstancia));
				
			
				if ($tipoConstancia == 27)//Plantilla para Traspaso
				{
					$rutaPdf = "/tmp/imgcontancias/pdf/".$iPidActual."ConstanciaTraspaso".$sCurp.".pdf";
					$rutaPdfTemp = "/tmp/imgcontancias/pdf/".$cFolioCliente."_".$iPidActual."ConstanciaTraspaso".$sCurp."_TEMP.pdf";
					$respuesta->rutaPdf = "tmp/imgcontancias/pdf/".$iPidActual."ConstanciaTraspaso".$sCurp.".pdf";
					$respuesta->rutaPdfTemp = "/tmp/imgcontancias/pdf/".$cFolioCliente."_".$iPidActual."ConstanciaTraspaso".$sCurp."_TEMP.pdf";
					$sNombreImagenConstancia = $iPidActual."ConstanciaTraspaso".$sCurp.".pdf";
				}
				else if ($tipoConstancia == 26 || $tipoConstancia == 33)//Plantilla para Registro
				{
					$rutaPdf = "/tmp/imgcontancias/pdf/".$iPidActual."ConstanciaRegistro".$sCurp.".pdf";
					$rutaPdfTemp = "/tmp/imgcontancias/pdf/".$cFolioCliente."_".$iPidActual."ConstanciaRegistro".$sCurp."_TEMP.pdf";
					$respuesta->rutaPdf = "tmp/imgcontancias/pdf/".$iPidActual."ConstanciaRegistro".$sCurp.".pdf";
					$respuesta->rutaPdfTemp = "/tmp/imgcontancias/pdf/".$cFolioCliente."_".$iPidActual."ConstanciaRegistro".$sCurp."_TEMP.pdf";
					$sNombreImagenConstancia = $iPidActual."ConstanciaRegistro".$sCurp.".pdf";
				}
			
				
				if ($tipoConstancia == 27)//Plantilla para Traspaso
				{
					$pdf->Text( 205,93.5 , utf8_decode($arrResp['nombreaforeactual']));
					
					$rendimiento = $arrResp['rendimiento'];
					switch ($rendimiento) 
					{
						case '0':
								$descRendimiento = "IGUAL";
							break;
						case '1':
								$descRendimiento = "MAYOR";
							break;
						case '2':
								$descRendimiento = "MENOR";
							break;
						default:
								$descRendimiento = "";
							break;
					}
					$pdf->Text(38.5,151.5 ,utf8_decode($descRendimiento));
				}
				
				//MARCAR CASILLAS---------------------------------------------------------------------------------------------------
				if ($iFirma == 1)
				{
					//ASESORIA DEL TRAMITE
					if($iMarca == 1)//SI
					{
						$pdf->SetXY(203,126);
						$pdf->SetFont('Arial','B',$sizeFontTitulo+5);
						$pdf->Cell( 9, $iAltoRen ,'X' ,$iBorder  , 0, 'L');	
					}
				}
				try
				{
					CLog::escribirLog("VALIDA CAMPO FIRMA ".$iFirma);
					switch ($iFirma)
					{
						case 0: //NO HAY SIGNPAD
							$pdf->SetXY(203,126);
							$pdf->SetFont('Arial','B',$sizeFontTitulo+5);
							$pdf->Cell( 9, $iAltoRen ,'X' ,$iBorder  , 0, 'L');	
							$pdf->Output($rutaPdfTemp);
							chmod($rutaPdfTemp, 0777);
							CLog::escribirLog("Se guarda y muestra PDF");
							//echo (rutaPdfTemp);
							$respuesta->descestatus = 'EXITO';
							$respuesta->estado = 1;
							break;
						case 2: //CON HUELLA
							$pdf->Output($rutaPdf);
							CLog::escribirLog("Se guarda y muestra PDF");
							$respuesta->descestatus = 'EXITO';
							$respuesta->estado = 1;
							break;
						case 1: //CON FIRMA DIGITAL
							
							$pdf->Output($rutaPdf);
							copy($rutaPdf, $rutaPdfTemp);
							
							$pdf = new FPDI();
													
							//FIRMA CLIENTE--------------------------------------------------------------------------------------------------------------
							$rutaFirma ='/sysx/progs/web/entrada/firmas/CTR0_'.$cFolioCliente.'_FTRAB.JPG';  
							$rutaNombre = "/sysx/progs/web/entrada/firmas/CTR0_".$cFolioCliente."_NTRAB.JPG";
							
							//$rutaFirma ='/sysx/progs/web/entrada/firmas/CTR0_6706135_FTRAB.JPG';  
							//$rutaNombre = "/sysx/progs/web/entrada/firmas/CTR0_6706135_NTRAB.JPG";
							
							
							//self::unirImagenesPdf($rutaPdf,$rutaNombre,$rutaFirma);

							chmod($rutaPdf, 0777);
							$pageCount = $pdf->setSourceFile($rutaPdf);
							$tplIdx = $pdf->importPage(1);
							$pdf->addPage('L','Letter');
							$pdf->useTemplate($tplIdx, null, null, 0, 0, true);		
							
							CLog::escribirLog("Se agregan las firmas al PDF");
							
							//SE GUARDA PDF Y SE MUESTRA EN EL NAVEGADOR
							CLog::escribirLog("Folio: ". intval($cFolioCliente));
							$arrRespuestaPublica = self::actualizarInfoPublicacion(intval($cFolioCliente), $sCurp, $iFirma, $rutaPdfTemp);

							if($arrRespuestaPublica["estado"] == 1)
							{
								$pdf->Output($rutaPdf, 'F');
								CLog::escribirLog("Se guarda y muestra PDF");
								$respuesta->descestatus = 'EXITO';
								$respuesta->estado = 1;	
							}
							else
							{
								CLog::escribirLog("No se pudo actualizar la firma en base de datos");
								$respuesta->descestatus = 'PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.';
								$respuesta->estado = -1;	
							}								

							break;
						default:
							break;
					}
				
				}
				
				catch (Exception $Ex) 
				{
					$respuesta->descestatus = $Ex->getMessage();
					$respuesta->estado = -9;
					CLog::escribirLog($respuesta->descestatus);
				}

				CLog::escribirLog("Se guarda y muestra PDF");
				
				/*$respuesta->descestatus = 'EXITO';
				$respuesta->rutaPdf =  "../salida/constanciasafiliacion/".$iPidActual."ConstanciaTraspaso".$sCurp.".pdf";
				$respuesta->rutaPdfTemp = $rutaPdfTemp;
				$respuesta->estado = 1;	*/
			}
		}
	}
	return($respuesta);		
}

function unirImagenesPdf($rutaPdf,$rutaNombre,$rutaFirma)
{
	$imagenDocumento = new Imagick();
	$imagenNombre = new Imagick();
	$imagenFirma = new Imagick();
	
	$imagenDocumento->setResolution(300,300);
		
	$imagenDocumento->readImage($rutaPdf);
	$imagenNombre->readImage($rutaNombre);
	$imagenFirma->readImage($rutaFirma);
	
	$arrResp = array();
	$arrResp["estado"] = -1;
		
	$imagenDocumento->setImageFormat('jpeg');
	$imagenDocumento->setCompression(Imagick::COMPRESSION_JPEG);
	$imagenDocumento->setCompressionQuality(100);
	
	$imagenNombre->resizeImage(850,200,  Imagick::FILTER_BOX, 1);
	$imagenFirma ->resizeImage(850,240,  Imagick::FILTER_BOX, 1);
	
	
	//	$imagenDocumento->compositeImage($imagenNombre, Imagick::COMPOSITE_BUMPMAP, 300,1700);
	//	$imagenDocumento->compositeImage($imagenFirma, Imagick::COMPOSITE_BUMPMAP, 2300,2000);

	$imagenDocumento->compositeImage($imagenNombre, Imagick::COMPOSITE_BUMPMAP, 400,1600);
	$imagenDocumento->compositeImage($imagenFirma, Imagick::COMPOSITE_BUMPMAP, 2100,1850);		
	
	if($imagenDocumento->writeImage($rutaPdf))
	{
		$arrResp["estado"] = 1;
	}

	return $arrResp;
}

function actualizarInfoPublicacion($folioSolicitud, $sCurp, $iFirma)
{
	CLog::escribirLog("INICIA ACTUALIZACION DE LA PUBLICACIÓN DE IMAGEN");
	$arrRespuestaPublica = array();
	$iCveOperacion = -1;
	$objMetodoGeneral = new CMetodoGeneral();

	try
	{
		CLog::escribirLog("VALIDA CAMPO FIRMA: ".$iFirma);
		if ($iFirma == 1)
		{
			$arrRespuestaPublica = self::ejecutaSentencia(1, 1, $folioSolicitud, $iFirma);
			if ($arrRespuestaPublica['respuesta'] > -2)	
			{
				$sImagenImpConstancia = '';
				unset($arrRespuestaPublica);
				$arrRespuestaPublica = self::ejecutaSentencia(2, $folioSolicitud, 4, 0);	
				if ($arrRespuestaPublica['respuesta'] > -2)	
				{
					$iCveOperacion = $arrRespuestaPublica['respuesta'];	
					CLog::escribirLog('iCveOperacion = '.$iCveOperacion);
					unset($arrRespuestaPublica);
					$arrRespuestaPublica = $objMetodoGeneral->actualizarfirmapublicacion($folioSolicitud, 2);
					if($arrRespuestaPublica["estado"] == OK___)
					{
						CLog::escribirLog("Se actualizo campo firmaimpconstancia");
						unset($arrRespuestaPublica);

						$sFechaActual = date("Ymd");
						$sNomeclatura = "CTR0";
						$sImagenImpConstancia = sprintf("%d_%018s%06d%s%s.tif",$folioSolicitud, $sCurp, $iCveOperacion, $sFechaActual, $sNomeclatura );
						CLog::escribirLog('sImagenImpConstancia = '.$sImagenImpConstancia);
						$arrRespuestaPublica = self::ejecutaSentencia(3, $folioSolicitud, $sImagenImpConstancia, IPSERVIDOR);
						
						if ($arrRespuestaPublica['respuesta'] > -2)	
						{								
							CLog::escribirLog("Se actualizo imagen en colimagenesindexadas. [revisrt fuente]");											
							$arrRespuestaPublica['respuesta'] = OK___;
							$arrRespuestaPublica["estado"] = 1;	
						}
						else
						{
							CLog::escribirLog($arrRespuestaPublica['descripcion']);
							$arrRespuestaPublica['respuesta'] = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
							$arrRespuestaPublica["estado"] = -1;
						}
					}
					else
					{
						CLog::escribirLog("No se pudo actualizar la firma");
						$arrRespuestaPublica['respuesta'] = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
						$arrRespuestaPublica["estado"] = -2;	
					}						
				}
				else
				{
					CLog::escribirLog($arrRespuestaPublica['descripcion']);
					$arrRespuestaPublica['respuesta'] = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
					$arrRespuestaPublica["estado"] = -7;
				}
			}
			else
			{
				CLog::escribirLog($arrRespuestaPublica['descripcion']);
				$arrRespuestaPublica['respuesta'] = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
				$arrRespuestaPublica["estado"] = -8;
			}
		}
		else
		{
			CLog::escribirLog("PDF SIN FIRMAS DIGITALES");
			$arrRespuestaPublica['respuesta'] = 'EXITO';
			$arrRespuestaPublica["estado"]= 1;
		}
		
	}
	
	catch (Exception $Ex) 
	{			
		$arrRespuestaPublica['respuesta'] = $Ex->getMessage();
		$arrRespuestaPublica["estado"]= ERR__;
	}
	
	return $arrRespuestaPublica;
}

function publicarImpresionConstancia($folioSolicitud, $sCurp, $iFirma, $sRutaPdfTemp)
{
	CLog::escribirLog("INICIA PUBLICACIÓN DE IMAGEN DE IMPRESION DE CONSTANCIA");
	$arrRespuestaPublica = array();
	$respuesta = new stdClass();
	$respuesta->descestatus = '';
	$respuesta->estado = 0;
	$iCveOperacion = -1;
	$sImagenSolConstancia = "";
	$sRutaAbsoluta = "";
	
	$sRutaImagenNombreTrabajador = '/sysx/progs/web/entrada/firmas/CTR0_'.$folioSolicitud.'_NTRAB.JPG';
	$sRutaImagenFirmaTrabajador = '/sysx/progs/web/entrada/firmas/CTR0_'.$folioSolicitud.'_FTRAB.JPG';
	$sRutaAbsoluta = basename($sRutaPdfTemp);
	
	try
	{
		CLog::escribirLog("VALIDA CAMPO FIRMA: ".$iFirma);
		if ($iFirma == 1)
		{
			$arrRespuestaPublica = self::ejecutaSentencia(1, 1, $folioSolicitud, $iFirma);
			if ($arrRespuestaPublica['respuesta'] > -2)	
			{
				$sImagenImpConstancia = '';
				unset($arrRespuestaPublica);
				$arrRespuestaPublica = self::ejecutaSentencia(2, $folioSolicitud, 4, 0);	
				if ($arrRespuestaPublica['respuesta'] > -2)	
				{
					$iCveOperacion = $arrRespuestaPublica['respuesta'];	
					if(file_exists($sRutaPdfTemp))
					{
						CLog::escribirLog("El PDF existe");
						$objMetodoGeneral = new CMetodoGeneral();
						unset($arrRespuestaPublica);
						$arrRespuestaPublica = $objMetodoGeneral->convertirPDFaImagen($folioSolicitud, $sCurp, $iCveOperacion, $sRutaAbsoluta, 2);
						if($arrRespuestaPublica["estado"] == OK___)
						{
							CLog::escribirLog("Se convirtio PDF a imagen");
							$sImagenImpConstancia = $arrRespuestaPublica["archivo"]; 
							unset($arrRespuestaPublica);
							$sRutaImagen = RUTA_SALIDA.$sImagenImpConstancia;
							chmod($sRutaImagen, 0777);
							$arrRespuestaPublica = $objMetodoGeneral->unirImagenes(2, $sImagenImpConstancia, $sRutaImagenNombreTrabajador, $sRutaImagenFirmaTrabajador, 0);	
							if($arrRespuestaPublica["estado"] == OK___)
							{						
								CLog::escribirLog("Se unieron imagenes correctamente");
								unset($arrRespuestaPublica);
								$arrRespuestaPublica = $objMetodoGeneral->publicarImagenConstancia($sImagenImpConstancia);
								if($arrRespuestaPublica["estado"] == OK___)
								{
									CLog::escribirLog("Se publico imagen correctamente");
									unset($arrRespuestaPublica);
									$arrRespuestaPublica = $objMetodoGeneral->actualizarfirmapublicacion($folioSolicitud, 2);
									if($arrRespuestaPublica["estado"] == OK___)
									{
										CLog::escribirLog("Se actualizo campo firmaimpconstancia");
										unset($arrRespuestaPublica);
										$arrRespuestaPublica = self::ejecutaSentencia(3, $folioSolicitud, $sImagenImpConstancia, IPSERVIDOR);
										if ($arrRespuestaPublica['respuesta'] > -2)	
										{								
											CLog::escribirLog("Se actualizo imagen en colimagenesindexadas. [revisrt fuente]");
											//BORRAR NOMBRE Y FIRMA DEL CLIENTE DEL SERVIDOR
											/*
												20151025: Euduardo Osuna
												se comenta el borrado hasta definir en sistemas que se hace con los archivos
											*/
											/*
											array_map('unlink', glob("/sysx/progs/web/entrada/firmas/CTR0_".$folioSolicitud."_NTRAB.JPG"));
											array_map('unlink', glob("/sysx/progs/web/entrada/firmas/CTR0_".$folioSolicitud."_FTRAB.JPG"));*/
											
											$respuesta->descestatus = 'EXITO';
											$respuesta->estado = 1;	
										}
										else
										{
											CLog::escribirLog($arrRespuestaPublica['descripcion']);
											$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
											$respuesta->estado = -1;
										}
									}
									else
									{
										CLog::escribirLog("No se pudo actualizar la firma");
										$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
										$respuesta->estado = -2;	
									}
								}
								else
								{
									CLog::escribirLog("No se pudo publicar la imagen de la Impresion de la Constancia en el Servidor Intermedio");
									$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
									$respuesta->estado = -3;		
								}
							}
							else
							{
								CLog::escribirLog("Ocurrio un error al unir las firmas al pdf.".$sRutaPdfTemp);
								$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
								$respuesta->estado = -4;
							}
							
						}
						else
						{
							CLog::escribirLog("Ocurrio un error al generar convertir el archivo PDF a Imagen ".$sRutaPdfTemp);
							$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
							$respuesta->estado = -5;
						}
					}
					else
					{
						CLog::escribirLog("No se encontro el archivo PDF -> ".$sRutaPdfTemp);
						$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
						$respuesta->estado = -6;
					}
				}
				else
				{
					CLog::escribirLog($arrRespuestaPublica['descripcion']);
					$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
					$respuesta->estado = -7;
				}
			}
			else
			{
				CLog::escribirLog($arrRespuestaPublica['descripcion']);
				$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
				$respuesta->estado = -8;
			}
		}
		else
		{
			CLog::escribirLog("PDF SIN FIRMAS DIGITALES");
			$respuesta->descestatus = 'EXITO';
			$respuesta->estado = 1;
		}
		
	}
	
	catch (Exception $Ex) 
	{
		//SE BORRAN FIRMAS DEL SERVIDOR
		/*
			20151025: Euduardo Osuna
			se comenta el borrado hasta definir en sistemas que se hace con los archivos
		*/
		/*array_map('unlink', glob("/sysx/progs/web/entrada/firmas/CTR0_".$folioSolicitud."_NTRAB.JPG"));
		array_map('unlink', glob("/sysx/progs/web/entrada/firmas/CTR0_".$folioSolicitud."_FTRAB.JPG"));*/
		
		CLog::escribirLog("Se borran las firmas del servidor");
		$respuesta->descestatus = $Ex->getMessage();
		$respuesta->estado = ERR__;
	}
	
	return $respuesta;
}

function validarDiagnosticoConstancia($sCurp)
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$arrResp['respuesta'] = 0;	
	$arrResp['descripcion'] = '';

	$arrData = array(
		"sCurp" => $sCurp
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('validarDiagnosticoConstancia', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$arrResp['estatus'] = 1;
			$arrResp['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{			
				$arrResp['respuestaEjecucion']= (int)$reg['respuesta'];
				$arrResp['diagnostico']= $reg['diagnostico'];
				$arrResp['descripciondiagnostico']= utf8_encode($reg['descripciondiagnostico']);
			}

			$arrResp['respuesta'] = 1;
			$arrResp['descripcion'] = "Se obtuvo diagnóstico";
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	
	$mensaje = $arrResp['descripcion'];
	CLog::escribirLog($mensaje);

	return $arrResp;
}

function grabarInformacionConstanciaImpresaESar($sCurp,$sContrasenia,$fechaEmision,$aforeActual,$fechaVigenciaConstancia,$folioConstancia)
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$arrDatos = array();
	$arrDatos['estatus'] = 0;
	$arrDatos['descripcion'] = "";

	$fechaEmision = str_replace("/", "-", $fechaEmision );
	$fechaVigenciaConstancia = str_replace("/", "-", $fechaVigenciaConstancia);

	$arrData = array(
		"sCurp" => $sCurp,
		"sContrasenia" => $sContrasenia,
		"fechaEmision" => $fechaEmision,
		"aforeActual" => $aforeActual,
		"fechaVigenciaConstancia" => $fechaVigenciaConstancia,
		"folioConstancia" => $folioConstancia
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('grabarInformacionConstanciaImpresaESar', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$arrDatos['estatus'] = 1;
			$arrDatos['descripcion'] = "EXITO";
			foreach($resultAPI["registros"] as $reg)
			{
				$arrDatos['folioSolicitud'] = (int)$resultAPI["fnactualizardatosconstanciaesar"];
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}

	return $arrDatos;
}

function validarDigitalizacion($folioSolicitud)
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$arrDatos = array();

	$arrData = array(
		"folioSolicitud" => $folioSolicitud
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('validarDigitalizacion', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$arrDatos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{
				$arrDatos['estatus'] = (int)$reg['fnexistedocumentopublicado'];
				$iRespuesta = $arrDatos['estatus'];		
				CLog::escribirLog("Respuesta: ".$iRespuesta );
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $arrDatos;	
}

function obternerDiasHbiles($fecha)
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$datos = array();

	$fechaEmision=$fecha;

	$arrData = array(
		"fechaEmision" => $fechaEmision
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('obternerDiasHbiles', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$datos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{
				$datos->fechaMaxima = $reg['fechamaxima'];
			}

			$datos->codigoRespuesta = OK___;
			$datos->descripcion = "";				
			CLog::escribirLog( '[' . __FILE__ . '] Se obtubo la fecha habil a 10 días correctamente.');
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $datos;
}

function obternerFechaActual()
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$datos = array();
	$arrData = array();

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('obternerFechaActual', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$datos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{
				$datos->fechaMaxima = $reg['fechaactual'];
			}

			$datos->codigoRespuesta = OK___;
			$datos->descripcion = "";				
			CLog::escribirLog( '[' . __FILE__ . '] Se obtubo la fecha habil a 10 días correctamente.');
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $datos;
}

function revisarAfore($aforeRevisa)
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$datos = array();
	$arrData = array(
		'aforeRevisa' => $aforeRevisa
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('revisarAfore', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$datos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{
				$datos->afore = $reg['afore'];
			}

			if( $datos->afore != "" )
			{
				$datos->codigoRespuesta = OK___;
				$datos->descripcion = "";			
				CLog::escribirLog( '[' . __FILE__ . '] Se obtubo la afore actual correctamente.');
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $datos;
}	

function ejecutaSentencia($iOpcionFuncion,$iParametro1,$iParametro2,$iParametro3)
{
	/*
	Opcion Funcion [1]:
	-->iParametro1:Opcion de la funcion de la Base de Datos.
	-->iParametro2:Campo <folio> de la tabla solconstancia, folio de la solicitud.
	-->iParametro3:Estatus de la signpad[0->no Conectada,1->Firma Digital,2->Cliente Sin Firma]
	Opcion Funcion [2]:
	-->iParametro1:Campo <folio> de la tabla solconstancia, folio de la solicitud.
	-->iParametro2:Identificador del proceso, dependiendo de el, es en la tabla donde buscara la informacion, 4 para REGISTRO y TRASPASO.
	-->iParametro3:Vacio
	Opcion Funcion [3]:
	-->iParametro1:Campo <folio> de la tabla solconstancia, folio de la solicitud.
	-->iParametro2:Nombre de la imagen indexada.
	-->iParametro3:IP sel servidor.
	Opcion Funcion [4]:
	-->iParametro1:Opcion de la funcion de la Base de Datos.
	-->iParametro2:Campo <folio> de la tabla solconstancia, folio de la solicitud.
	-->iParametro3:Nada 0.
	*/
	
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$arrRespuesta = array();
	$arrData = array(
		'iOpcionFuncion' => $iOpcionFuncion,
		'iParametro1' => $iParametro1,
		'iParametro2' => $iParametro2,
		'iParametro3' => $iParametro3
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('ejecutaSentencia', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$arrRespuesta['respuesta'] = (int)$resulSet->fetchColumn();
			$arrRespuesta['descripcion'] = "Se ejecuto correctamente la consulta."
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}

	return $arrRespuesta;
}

/*funcion que se utiliza para verificar el enrolamiento del trabajador, en caso de que el trabajador si cuente con un enrolamiento
la función de postgresql regresara el folio con el cual este se enrolo.*/
function verificarEnrolamiento($iFolioSolicitud)
{

	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$datos = array();
	$arrData = array(
		'iFolioSolicitud' => $iFolioSolicitud
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('verificarEnrolamiento', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$datos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{
				$datos['respuesta'] = $reg['respuesta'];
				$datos['estatus'] = OK___;
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $datos;
}

/*funcion que obtiene el flag para verificar si se ocupara lanzar la aplicacion de audio
	Recibe
		folio
		numero de control con el cual se ejecutara la funcion:
			1-flag.
			2-obtener Nombre que se formara en la cadena del nombre del audio.
	Retorna
			El flag en caso de mandarle el numero un 1 en el segundo parametro
			el nombre en caso de mandarle un 2 como segundo parametro
*/
function obtenerFlagAudio($iFolioSolicitud, $iOpcion)
{

	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$datos = array();
	$datos['estatus'] = _DEFAULT_;

	$arrData = array(
		'iFolioSolicitud' => $iFolioSolicitud,
		'iOpcion' => $iOpcion
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('obtenerFlagAudio', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$datos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{
				$datos['respuesta'] = $reg['respuesta'];
				$datos['estatus'] = OK___;
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $datos;
}

//funcion que permite almacenar en la tabla enrolexcepcionestrabajador la excepcion del audio
function guardarExcepcionAudio($iFolioEnrolamiento, $sCodigo)
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$datos = array();
	$datos['estatus'] = _DEFAULT_;

	$iNumDedo = 0;

	$arrData = array(
		'iFolioEnrolamiento' => $iFolioEnrolamiento,
		'sCodigo' => $sCodigo,
		'iNumDedo' => $iNumDedo
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('guardarExcepcionAudio', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$datos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{
				$datos['respuesta'] = (int)$reg['respuesta'];
				$datos['estatus'] = OK___;
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $datos;
}

//funcion para grabar en log el estatus que regresa ala pagina el soundviewer
function grabarLogJsAudio($iRespuesta, $iFolioEnrolamiento)
{
	$cSql = '';
	$objGn = new CMetodoGeneral();
	$arrDatos = array();
	$arrDatos['estatus'] = _DEFAULT_;
	
	$cSql = "grabarLogJs-> Estatus que devolvio soundViewer: $iRespuesta , Folioenrolamiento: $iFolioEnrolamiento  ";
				
	$objGn->grabarLogx($cSql);

	if($cSql != '')
	{
		$arrDatos['estatus'] = OK___;
		$arrDatos['descripcion'] = "EXITO";
	}
	else
	{
		$arrDatos['estatus'] = ERR__;
	}
	
	return $arrDatos;	
}

//funcion que actualiza el estatus del Enrolamiento del Trabajador
function actualizarEstatusEnrolamiento($iFolioEnrolamiento, $iEstatusEnrolamiento)
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$arrDatos['estatus'] = _DEFAULT_;

	$arrData = array(
		'iFolioEnrolamiento' => $iFolioEnrolamiento,
		'iEstatusEnrolamiento' => $iEstatusEnrolamiento
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('actualizarEstatusEnrolamiento', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$arrDatos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"]  as $reg)
			{
				$arrDatos['respuesta'] = $reg['respuesta'];
				$arrDatos['estatus'] = OK___;
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $arrDatos;
}	
?>