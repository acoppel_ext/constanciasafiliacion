-- Table: identificartraspasodactilar

-- DROP TABLE identificartraspasodactilar;

CREATE TABLE identificartraspasodactilar
(
  keyx serial NOT NULL,
  fechaalta timestamp with time zone NOT NULL DEFAULT now(),
  foliosolicitud bigint NOT NULL DEFAULT 0,
  curptrabajador character(18) NOT NULL DEFAULT ''::bpchar,
  curpempleado character(18) NOT NULL DEFAULT ''::bpchar,
  tipooperacion character(4) NOT NULL DEFAULT ''::bpchar,
  identificador character(50) NOT NULL DEFAULT ''::bpchar,
  resultadooperacion character(2) NOT NULL DEFAULT ''::bpchar,
  diagnosticos character(30) NOT NULL DEFAULT ''::bpchar,
  selloverificaciondactilar bigint NOT NULL DEFAULT 0
)
WITH (
  OIDS=FALSE
);
ALTER TABLE identificartraspasodactilar
  OWNER TO sysaforeglobal;

-- Index: indidentificartraspasodactilar

-- DROP INDEX indidentificartraspasodactilar;

CREATE INDEX indidentificartraspasodactilar
  ON identificartraspasodactilar
  USING btree
  (foliosolicitud);
