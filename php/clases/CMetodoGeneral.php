<?php
include_once ('global.php');
include_once ('Capirestconstanciaafiliacion.php');
define('RUTA_LOGX',					'/sysx/progs/afore/log/constanciaafiliacion');
define("RUTA_SALIDA","/sysx/progs/web/salida/constanciasafiliacion/");

$controlador = "CtrlconstanciaAfiliacion";

class CMetodoGeneral
{
	
	function __construct()
	{
		date_default_timezone_set('America/Mazatlan');
	}
	
	var $cnxDb;
	var $arrError;
	
	public function grabarLogx($cadLogx)
	{
		$cIpCliente = $this->getRealIP();
		$rutaLog =  RUTA_LOGX .  '-' . date("Y-m-d") . ".log"; 
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}

    public function getRealIP() 
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
           
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
               
		return $_SERVER['REMOTE_ADDR'];
    }  

	public function leerXmlServ($archivo, $idElem)
	{
		$res = "";
		if( file_exists($archivo) )
		{
			$datosXml = simplexml_load_file($archivo, "SimpleXMLElement",LIBXML_NOCDATA);
			if($datosXml)
			{
				echo "leyo xml 0000 <br>";
				print_r($datosXml);

				foreach($datosXml->Servidor as $elem)
				{
					echo "<br> " . $idElem .  ' Vs ' .  $elem->key . "<br>";
					print_r($elem);

					if( $elem->id == $idElem )
						$res = $elem;
				}
			}
			else
			{
				$this->grabarLogx("formato XML invalido");
				$res = false; 
			}
		} 
		else
		{
			$res = "El archivo " . $archivo . " no existe";
			$this->grabarLogx($res);
			$res = false; 
		}
		return $res;
	}

	public function obtenerConexionBdPostgres($DireccionIp, $BaseDato, $UsrBaseDato, $PasswdBaseDato)
	{
		global $cnxDb;
		if( pg_connection_status($cnxDb) != PGSQL_CONNECTION_OK )
		{
			$cadConexion = "host=" . $DireccionIp . " dbname=" . $BaseDato . " user=" . $UsrBaseDato . " password=" . $PasswdBaseDato;
			$cnxDb = pg_pconnect($cadConexion);
			if( $cnxDb )
				$this->grabarLogx('[obtenerConexionBdPostgres] Conexion establecida');
		}
		
		return $cnxDb;
	}

	public function obtenerXML($array)
	{
		$xml = "";
		$xml = "<map>";
		foreach($array as $key => $value)
		{
			$mykey = $key;
			$myvalue= $value;
			$xml .= "<entry><key>".trim($mykey)."</key>"."<value>".trim($myvalue)."</value></entry>";  	
		}
		$xml .= "</map>";
		return $xml;
	}

	public function consumirServicioEjecutarAplicacion($idServicio,$idServidor,$parametros)
	{

		$servicioBusTramite = new CServicioBusTramites();	
		$datos= array();	
		$datos = $servicioBusTramite->servicioEjecutarAplicacion($idServicio, $idServidor, $parametros);

		return ($datos);			
	}
	
	public function consumirServicioObtenerRespuesta($idServicio,$folioServicioAfore)	
	{
		$servicioBusTramite = new CServicioBusTramites();	
		$datos= array();	
		$datos = $servicioBusTramite->servicioObtenerRespuesta( $idServicio,$folioServicioAfore);
		return ($datos);	

	}

	public function obtenerCurpPromotor($iNumeroEmpleado)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrDatos['respuesta'] = 0;
		$arrDatos['curpPromotor'] = "";
		$arrDatos['descripcion'] = "";

    	$arrData = array(
			'iNumeroEmpleado' => $iNumeroEmpleado
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest");

			$resultAPI = $objAPI->consumirApi('obtenerCurpPromotor', $arrData, $GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{

				$arrDatos['estatus'] = 1;

				foreach($resultAPI['registros'] as $reg)
				{						
					$arrDatos['curpPromotor'] = $reg['curp'];
				}
					$sCurpPromotor =$arrDatos['curpPromotor'];
				
				if($sCurpPromotor != "" )
				{
					$arrDatos['respuesta'] = 1;
					$arrDatos['descripcion'] = "Curp Valido";
				}
			}
			else
			{
				if(isset($resultAPI["tipo"])){
					header('HTTP/1.1 401 Unauthorized');
					return "Error al consultar la API";
				}		
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrDatos;
	}
	
	//METODOS PARA LA EL PROCESO PREVIA PUBLICACION
	function convertirPDFaImagen($iFolio, $sCurp, $iClaveOperacion, $sArchivoPFD, $iDentificadorProceso)
	{
		self::grabarLogx("0");
		$sNombreImagenOut = "";
		$sNombreImagenAuxOut = "";
		$sFechaActual = date("Ymd");
		$sNomeclatura = "";
		
		self::grabarLogx($sArchivoPFD);		
		$im = new Imagick();
		
		$arrResp = array();
		$arrResp["estado"] = -1;
		$arrResp["archivo"] = "";
		
		//Se asigna la resolución de la imagen
		$im->setResolution(300,300);
		
		//Se lee la ruta donde se encuentra el PDF
		$im->readImage(RUTA_SALIDA.$sArchivoPFD);
		self::grabarLogx(RUTA_SALIDA.$sArchivoPFD);
		
		//Se asigna el formato de la imagen final
		$im->setImageFormat('tif');
		$im->setImageBackgroundColor("white");
		$im->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
		$im->transformImageColorspace(Imagick::COLORSPACE_GRAY);
		
		if($iDentificadorProceso == 1){//SOLICITUD DE CONSTANCIA
			$im->resizeImage(2550,3300,Imagick::FILTER_BOX,1);
			$sNomeclatura = "SCTR";
		}
		else{ //CONSTANCIA
			$im->resizeImage(3300,2550,Imagick::FILTER_BOX,1);	
			$sNomeclatura = "CTR0";
		}
		
		$im->setImageType(Imagick::IMGTYPE_BILEVEL);
		$im->setImageDepth(1);
		$im->setImageChannelDepth(Imagick::CHANNEL_ALL, 1);
		$im->setImageCompression(Imagick::COMPRESSION_GROUP4);
		
		//Se aplica la calidad de 100% a la imagen
		$im->setCompressionQuality(100);

		//save image file
		$sNombreImgenOut = sprintf("%d_%018s%06d%s%s.tif",$iFolio, $sCurp, $iClaveOperacion, $sFechaActual, $sNomeclatura );
		$sNombreImagenAuxOut = $sNombreImgenOut;
		self::grabarLogx($sNombreImgenOut);
		if($im->writeImage(RUTA_SALIDA.$sNombreImgenOut))
		{
			self::grabarLogx("SE GENERO IMAGEN CON EXITO");
			$arrResp["estado"] = OK___;
			$arrResp["archivo"] = $sNombreImagenAuxOut;
		}
		self::grabarLogx("sNombreImagenOut: ".$sNombreImagenAuxOut);

		return $arrResp;
	}

	function publicarImagenConstancia($sArchivo)
	{
		$nombreImagen = RUTA_SALIDA.$sArchivo;
		$cadena = "";
		$arrREsp = array();
		$arrREsp["estado"] = -1;
		$iRet = -1;

		$cadena = '/usr/local/bin/wput -B -t 10 -u '.$nombreImagen.' ftp://'.USUARIO_PUBLICA_FTP.':'.PASS_PUBLICA_FTP.'@'.IP_PUBLICACION_IMAGENES.'/sysx/smbxtempol/'.$sArchivo;

		$iRet = self::my_shell_exec($cadena);
		self::grabarLogx(" iRet: ".$iRet);

		if($iRet == 0)
		{
			$arrREsp["estado"] = 1;
		}

		return $arrREsp;
	}

	function my_shell_exec($cmd, &$stdout=null, &$stderr=null) {

		$proc = proc_open($cmd,[
			1 => ['pipe','w'],
			2 => ['pipe','w'],
		],$pipes);
		$stdout = stream_get_contents($pipes[1]);
		fclose($pipes[1]);
		$stderr = stream_get_contents($pipes[2]);
		fclose($pipes[2]);
		return proc_close($proc);
	}

	function actualizarfirmapublicacion($iFolio,$iOpcion)
	{
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconstanciaafiliacion();

    	$arrREsp = array();
		$arrREsp["estado"] = -1;

    	$arrData = array(
			'iFolio' => $iFolio,
			'iOpcion' => $iOpcion
    	);

		try
		{
			$objGn->grabarLogx("Inicio API Rest");

			$resultAPI = $objAPI->consumirApi('actualizarfirmapublicacion', $arrData, $GLOBALS["controlador"]);

			if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
			{
				$arrREsp["estado"] = 1;
				self::grabarLogx('[CMetodoGeneral::actualizarfirmapublicacion]Estado:'.$arrREsp["estado"]);
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
				else
					header('HTTP/1.1 409 Conflict');

				return "Error al consultar la API";
			}
		}
		catch (Exception $e)
		{
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		return $arrREsp;
	}
	
	function unirImagenes($iOpcion, $sRutaImagenDocumento, $sRutaImagenNombreTrabajador, $sRutaImagenFirmaTrabajador, $sRutaImagenFirmaPromotor)
	{		
		if ($iOpcion == 1) //IMPRESION DE LA SOLICITUD
		{
			//SE CREAN TRES OBJETOS

			$imagenDocumento = new Imagick(RUTA_SALIDA.$sRutaImagenDocumento);
			$imagenNombreTrabajador = new Imagick($sRutaImagenNombreTrabajador);
			$imagenFirmaTrabajador = new Imagick($sRutaImagenFirmaTrabajador);
			$imagenFirmaPromotor = new Imagick($sRutaImagenFirmaPromotor);

			$arrResp = array();
			$arrResp["estado"] = -1;
			$arrResp["archivo"] = "";		

			//SE MODIFICA EL TAMAÃ‘O DE LA FIRMA
			$imagenNombreTrabajador->resizeImage(680, 100, Imagick::FILTER_BOX, 1);
			$imagenFirmaTrabajador->resizeImage(800, 250, Imagick::FILTER_BOX, 1);
			$imagenFirmaPromotor->resizeImage(800, 250, Imagick::FILTER_BOX, 1);		

			/*//SE ASIGNA EL COLORSPACE DEL MISMO COLOR 
			$imagenDocumento->setImageColorspace($imagenNombreTrabajador->getImageColorspace());
			$imagenDocumento->setImageColorspace($imagenFirmaTrabajador->getImageColorspace());
			$imagenDocumento->setImageColorspace($imagenFirmaPromotor->getImageColorspace());
			*/
			
			//LA Primera IMAGEN SE SOBREPONE EN LA PRIMERA IMAGEN
			$imagenDocumento->compositeImage($imagenNombreTrabajador, Imagick::COMPOSITE_BUMPMAP, 790, 1100);

			//LA SEGUNDA IMAGEN SE SOBREPONE EN LA PRIMERA IMAGEN
			$imagenDocumento->compositeImage($imagenFirmaTrabajador, Imagick::COMPOSITE_BUMPMAP, 1380, 2535);		

			//LA TERCERA IMAGEN SE SOBREPONE EN LA PRIMERA IMAGEN
			$imagenDocumento->compositeImage($imagenFirmaPromotor, Imagick::COMPOSITE_BUMPMAP,380, 2535);

			$imagenDocumento->writeImage(RUTA_SALIDA.$sRutaImagenDocumento);
			$arrResp["estado"] = OK___;
			$arrResp["archivo"] = $imagenDocumento;
			self::grabarLogx('Se unieron las imagenes correctamente');
		}
		else if ($iOpcion == 2)//IMPRESION DE LA CONSTANCIA
		{
			//SE CREAN TRES OBJETOS
			$imagenDocumento = new Imagick(RUTA_SALIDA.$sRutaImagenDocumento);
			$imagenNombreTrabajador = new Imagick($sRutaImagenNombreTrabajador);
			$imagenFirmaTrabajador = new Imagick($sRutaImagenFirmaTrabajador);
		
			$arrResp = array();
			$arrResp["estado"] = -1;
			$arrResp["archivo"] = "";
		
			//SE MODIFICA EL TAMANO DE LA FIRMA
			$imagenNombreTrabajador->resizeImage(920, 378, Imagick::FILTER_BOX, 1);
			$imagenFirmaTrabajador->resizeImage(920, 378, Imagick::FILTER_BOX, 1);
			
			//SE ASIGNA EL COLORSPACE DEL MISMO COLOR 
			$imagenDocumento->setImageColorspace($imagenNombreTrabajador->getImageColorspace());
			$imagenDocumento->setImageColorspace($imagenFirmaTrabajador->getImageColorspace());
			
			//LA SEGUNDA IMAGEN SE SOBREPONE EN LA PRIMERA IMAGEN
			$imagenDocumento->compositeImage($imagenNombreTrabajador, Imagick::COMPOSITE_BUMPMAP, 355, 1405);
			
			//LA TERCERA IMAGEN SE SOBREPONE EN LA PRIMERA IMAGEN
			$imagenDocumento->compositeImage($imagenFirmaTrabajador, Imagick::COMPOSITE_BUMPMAP, 1220, 1890);
			
			$imagenDocumento->writeImage(RUTA_SALIDA.$sRutaImagenDocumento);
			$arrResp["estado"] = OK___;
			$arrResp["archivo"] = $imagenDocumento;

		}

		return $arrResp;
	}
}
?>