// MOVIL
/* Funcion que recibe la respuesta del componente EnrolMovil */
function getComponenteHuella(estatusProceso, jsonFingers, opcionHuella, jsonTemplate) {

    marcarLog('getComponenteHuella estatusProceso ' + estatusProceso);

    if (estatusProceso == 0 || estatusProceso == "0") {
        respuestaWebService(103, iOpcion);
    } else {
        if (opcionHuella == 0) {

            // HUELLA PROMOTOR
            document.getElementById("fingerString0").value = jsonFingers;
            var template = JSON.parse(jsonTemplate);
            document.getElementById("fingerTemplate0").value = template["FingerTemplate"][0]["Template"]; 
            var statusFinger0 = enrolhuellasservicio(opcionHuella, 2, 9909, "0101");

            if (statusFinger0 == 1 || statusFinger0 == 2) {
                statusFinger0 = 1;
            }
            setTimeout(function () {
                respuestaWebService(statusFinger0, iOpcion);
            }, 4000);

        }

        if (iTipoAfiliacion == 27 || iTipoAfiliacion == 1 || iTipoAfiliacion == 527) {
            if (opcionHuella == 1) {
                // HUELLA TRABAJADOR
                document.getElementById("fingerString1").value = jsonFingers;
                var template = JSON.parse(jsonTemplate);
                document.getElementById("fingerTemplate1").value = template["FingerTemplate"][0]["Template"];

                var statusFinger1 = enrolhuellasservicio(opcionHuella, 3, 9910, "0401");

                if (statusFinger1 == 1 || statusFinger1 == 2) {
                    statusFinger1 = 1;
                }

                setTimeout(function () {
                    respuestaWebService(statusFinger1, iOpcion);
                }, 4000);

            }
            if (opcionHuella == 2) {
                // HUELLA PROMOTOR
                document.getElementById("fingerString2").value = jsonFingers;
                var template = JSON.parse(jsonTemplate);
                document.getElementById("fingerTemplate2").value = template["FingerTemplate"][0]["Template"];
                if (bPrevalidacionEnCurso) {
                    var statusFinger2 = enrolhuellasservicio(opcionHuella, 2, 9911, "0501");
                } else {
                    var statusFinger2 = enrolhuellasservicio(opcionHuella, 2, 9910, "0401");
                }

                if (statusFinger2 == 1 || statusFinger2 == 2) {
                    statusFinger2 = 1;
                }

                setTimeout(function () {
                    respuestaWebService(statusFinger2, iOpcion);
                }, 4000);
            }
        }
        if (opcionHuella == 3) {
            // HUELLA CLIENTE BCPL
            document.getElementById("fingerBCPL").value = jsonFingers;
            var template = JSON.parse(jsonTemplate);
            document.getElementById("fingerBCPLTemplate").value = template["FingerTemplate"][0]["Template"];

            var statusValidacion = validarTarjetaBcpl(cTarjetaBCPL, iKeyxtargeta);

            setTimeout(function () {
                respuestaWebService(parseInt(statusValidacion), iOpcion);
            }, 1000);
        }
    }
}

// MOVIL
/* Funcion que recibe la respuesta del componente EnrolMovil */
function getComponenteHuellaTerc(estatusProceso, opcionHuella, TemplateDer, TemplateIzq) {
    marcarLog('getComponenteHuellaTerc opcionHuella ' +opcionHuella);

    if (estatusProceso == 0 || estatusProceso == "0") {

        respuestaWebServiceHuella(0);

    } else {

        TemplateDerTF = TemplateDer;
        TemplateIzqTF = TemplateIzq;
        bBanderaHuellaMovil = true;
        subirHuellas();
        respuestaWebServiceHuella(1);

    }
}

// MOVIL
/* Funcion que recibe la respuesta del componente FirmaDigital */
function getComponenteFirma() {

    cambiarNombreImg("FARE_" + folio + "_FTRAB.JPG", "FAHD_" + folio + "_FTRAB.JPG");
}

// MOVIL
/* Funcion que recibe la respuesta del componente FirmaDigital */
function getComponenteDigitalizar(estatusProceso) {
    marcarLog('getComponenteDigitalizar estatusProceso ' + estatusProceso);
    setTimeout(function () {
        respuestaWebService(parseInt(estatusProceso), iOpcion);
    }, 2000);
}

function getComponenteEnrolamiento(estatusProceso) {
    marcarLog('getComponenteDigitalizar estatusProceso ' + estatusProceso);
    setTimeout(function () {
        respuestaWebService(parseInt(estatusProceso), iOpcion);
    }, 2000);
}

// MOVIL
/* Funcion para renombrar imagenes cargadas desde al servidor desde la app FirmaDigital */
function cambiarNombreImg(oldName, newName) {
    marcarLog('cambiarNombreImg oldName ' + oldName);
    $.ajax({
        data: {
            opcion: 601,
            oldName: oldName,
            newName: newName
        },
        success: function (data) {
            bRetorna = data.respuesta;
        }
    });

    return bRetorna;
}

function enrolhuellasservicio(oHuella, iTipoP, iTipoS, iTipoO) {

    marcarLog('enrolhuellasservicio oHuella ' + oHuella);
    
    var bRetorna = "";
    var sCURPTrab = sCurpTrabajador;
    var iNumFuncionario = iEmpleado;
    var iFolioSolicitud = folio;
    var iTipoPerson = iTipoP;
    var iTipoServ = iTipoS;
    var sTipoOperac = iTipoO;
    var iNumDedo = 2;

    if (oHuella == 0) {
        sTipoOperac = "0101";
        var base64Imagen = $("#fingerString0").val();
        var cTemplate = $("#fingerTemplate0").val();
    }
    if (oHuella == 1) {
        sTipoOperac = "0401";
        var base64Imagen = $("#fingerString1").val();
        var cTemplate = $("#fingerTemplate1").val();
    }
    if (oHuella == 2) {
        if (bPrevalidacionEnCurso) {
            sTipoOperac = "0501";
        } else {
            sTipoOperac = "0401";
        }
        var base64Imagen = $("#fingerString2").val();
        var cTemplate = $("#fingerTemplate2").val();
    }

    var base64Imagen = JSON.parse(base64Imagen);
    var iNist = base64Imagen["Fingerprints"][0]["Nfiq"];
    var sDispositivo = base64Imagen["Serial"];
    var iFap = 50;
    var sImagen = base64Imagen["Fingerprints"][0]["B64FingerImage"];
    var sTipoServicio = "";

    $.ajax({
        data: {
            opcion: 602,
            sCURPTrab: sCURPTrab,
            iNumFuncionario: iNumFuncionario,
            iFolioSolicitud: iFolioSolicitud,
            iTipoPerson: iTipoPerson,
            iTipoServ: iTipoServ,
            sTipoOperac: sTipoOperac,
            iNumDedo: iNumDedo,
            iNist: iNist,
            sDispositivo: sDispositivo,
            iFap: iFap,
            sImagen: sImagen,
            sTipoServicio: sTipoServicio,
            cTemplate: cTemplate
        },
        success: function (data) {
            bRetorna = data.respuesta;
        }
    });

    return bRetorna;
}

function buscarTextoVideo(textoExcepcion) {
    marcarLog('buscarTextoVideo textoExcepcion ' + textoExcepcion);
    var iTipodialogo = textoExcepcion;
    var iFolio = folio;
    var bRetorna = "";
    $.ajax(
        {
            data: { opcion: 603, iTipodialogo: iTipodialogo, iFolio: iFolio },
            success: function (data) {
                bRetorna = data.respuesta;
            }
        });

    return bRetorna;
}

function getComponenteVideo(estatusOpcion) {
    marcarLog('getComponenteVideo estatusOpcion ' + estatusOpcion);
    opcionVideoP = 2;

    $('.content-loader').show();

    if (parseInt(estatusOpcion) == 1) {

        setTimeout(function () {
            reubicarVideo();
        }, 5000);

    } else {
        $(".content-loader").hide();
        MensajeGrabacion('Promotor, se detectó un error, favor de realizar nuevamente la grabación de video.');
    }
}

function reubicarVideo() {
    var nombrearchivo = nombreVideoPublica;
    marcarLog('reubicarVideo nombrearchivo ' + nombrearchivo);
    $('.content-loader').show();
    $.ajax(
        {
            data: { opcion: 604, nombrearchivo: nombrearchivo, iFolio: folio },
            success: function (data) {
                $('.content-loader').hide();

                if (parseInt(data) == 0 && iContadorVideo < 3) {
                    setTimeout(function () {
                        reubicarVideo();
                    }, 3000);

                    iContadorVideo++;
                } else {
                    guardarVideoAfiliacion(nombrearchivo);
                    respuestaWebServicePublicaVideo('1');
                }
            }
        });

    return bRetorna;
}

function guardarVideoAfiliacion(nombrearchivo) {
    marcarLog('guardarVideoAfiliacion nombrearchivo ' + nombrearchivo);
    $.ajax(
        {
            data: { opcion: 52, folioConst: folio, nombrevideo: nombrearchivo },
            success: function (data) {
                //pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
                datos = data.respuesta;
            }
        }
    );
}

// MOVIL
/* Funcion que recibe la respuesta del componente EnrolMovil */
function getComponenteEnlace(estatusProceso) {
    marcarLog('getComponenteEnlace estatusProceso ' + estatusProceso);
    respuestaWebService(parseInt(estatusProceso), iOpcion);

}

function capturarTarjeta() {

    var sHtml = '<div class=\"form-group\">' +
        '<table>' +
        '<tr>' +
        '<td colspan=\"2\" align="center">' +
        '<img src="imagen/tarjeta.png" width="200"><br><br>' +
        '</td>' +
        '</tr>' +
        '<tr id="divTarjeta">' +
        '<td>' +
        '<label for="numTarjet">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;N&uacute;mero Tarjeta:</label>' +
        '</td>' +
        '<td>' +
        '<input type="tel" class="form-control" id="numTarjet">' +
        '</td>' +
        '</tr>' +
        '<tr id="divTarjetaC">' +
        '<td>' +
        '<label for="numTarjetC">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Confirmar N&uacute;m.:</label>' +
        '</td>' +
        '<td>' +
        '<input type="tel" class="form-control" id="numTarjetC">' +
        '</td>' +
        '</tr>' +
        '<tr id="divCvc" style="display:none">' +
        '<td>' +
        '<label for="CVC">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Código de verificación:</label>' +
        '</td>' +
        '<td>' +
        '<input type="tel" class="form-control" id="numCvc" size=3 maxlength=3>' +
        '</td>' +
        '</tr>' +
        '</table>' +
        '<center><br><label style="font-size:9px; color:#FF0000;" id="mensajeError"></label></center>' +
        '</div>';

    var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
    myDlg.capturartarjeta(475, 440, 'Capturar Tarjeta');
    myDlg.mostrar(sHtml);
}

function validarTarjeta() {
    sNumTarj = $("#numTarjet").val();
    sNumTarjC = $("#numTarjetC").val();
    if (sNumTarj.length < 16 || sNumTarj == "4169123456789010") {
        $('#mensajeError').html("N&uacute;mero de tarjeta incorrecto.");
    } else {
        $('#mensajeError').html("");
        if (sNumTarj == sNumTarjC) {
            document.getElementById("divTarjeta").style.display = "none";
            document.getElementById("divTarjetaC").style.display = "none";
            document.getElementById("divCvc").style.display = "block";
            document.getElementById("botones1").style.display = "none";
            document.getElementById("botones2").style.display = "block";
            $('#mensajeError').html("");
        } else {

            $('#mensajeError').html("Los N&uacute;meros no coinciden.");

        }

    }
}

function validarCVC() {

    sNumCvc = $("#numCvc").val();
    if (sNumCvc.length < 3) {
        $('#mensajeError').html("Ingrese los tres dígitos del CVC.");
    } else {
        $('#mensajeError').html("");
        guardarTarjeta(sNumTarj);
    }
}

function guardarTarjeta(sNumTarj) {

    var bRetorna = 0;
    $.ajax(
        {
            data: { opcion: 605, cNumeroTarjeta: sNumTarj, ipCliente: folio },
            success: function (data) {
                bRetorna = data;
                cerrar();
                respuestaWebService(sNumTarj);
            }
        });

    return bRetorna;
}

function validarTarjetaBcpl(cNumTarjetaBCPL, cTemplate) {
    var bRetorna = 0;
    $.ajax(
        {
            data: { opcion: 606, cNumTarjetaBCPL: cNumTarjetaBCPL, cTemplate: cTemplateBCPL },
            success: function (data) {
                bRetorna = data;
            }
        });

    return bRetorna;
}

//MOVIL
function generarArchivoHuellas(nombreArchivo, template) {
    marcarLog('generarArchivoHuellas nombreArchivo ' + nombreArchivo);
    var bRetorna = 0;
    $.ajax(
        {
            async: false,
            cache: false,
            data: { opcion: 201, nombreArchivo: nombreArchivo, template: template },
            url: 'php/constanciaafiliacion.php',
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.respuesta == 1) {
                    marcarLog('se grabaron las huellas exitosamente');
                    bRetorna = 1;
                }
                else {
                    marcarLog('error al ejecutar el metodo -> guardarHuellasIndices');
                    if (intentosGenerarArchivo < 4) {
                        intentosGenerarArchivo++;
                        generarArchivoHuellas(nombreArchivo, template);
                    }
                }
            },
            error: function (a, b, c) { },
            beforeSend: function () { }
        });
    return bRetorna;

}