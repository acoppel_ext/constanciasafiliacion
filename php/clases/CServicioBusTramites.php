<?php
error_reporting(1);
include_once ('global.php');
include_once ('CLogImpresion.php');
include_once('librerias/nusoap.php');
include_once('InvocaServicioBusTramite.php');
include_once("ObtencionRespuesta.php");
include_once("CMetodoGeneral.php");
include_once ('Capirestconstanciaafiliacion.php');

date_default_timezone_set('America/Mexico_City');

$controlador = "CtrlconstanciaAfiliacion";

class CServicioBusTramites
{

	public function servicioEjecutarAplicacion($idServicio, $idservidor, $parametros)
	{
		$objAPI = new Capirestconstanciaafiliacion();
    	$arrDatos = array();

    	$arrData = array(
			'idServicio' => $idServicio
		);

		$reg=array();
		$datos = array("respondioServicio"=>0,"folioServicioAfore"=>0,"descripcionRespuesta"=>"");
		$response = array();
		$invocaServicioBusTramite = new InvocaServicioBusTramite();
		$objGeneral= new CMetodoGeneral();

		$invocaServicioBusTramite->idServicio    = $idServicio;
		$invocaServicioBusTramite->parametros = $parametros;

		$resultAPI = $objAPI->consumirApi('servicioEjecutarAplicacion', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			
			$datos['estatus'] = 1;
			$datos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"] as $reg)
			{
				$ipServidor  = $reg['ipservidor'];
				$puerto      = $reg['puerto'];
				$url         = $reg['urlservicio'];
				$protocolo   =  $reg['protocolo'];
			}

			$mensaje =  "Se ejecuto la consulta Correctamente";

			$urlServicio = $protocolo . $ipServidor . ":" . $puerto . $url ;

			$client = new SoapClient($urlServicio,
										array('trace' => true, 'exceptions' => true));		
			try 
			{
				$response = $client->ejecutarAplicacion($invocaServicioBusTramite);
				$datos = $response->respuesta;
			} 
			catch (SoapFault $fault) 
			{ 
				//echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
				$datos['descripcionRespuesta']='Se presento problemas al consultar servicio';
				$objGeneral->grabarLogx( '[' . __FILE__ . '] [ servicioEjecutarAplicacion ] Error al ejecutar el servicio: SOAPFault: '.$fault->faultcode."-".$fault->faultstring);
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
		}
		
		return($datos);
	}

	public function servicioObtenerRespuesta($idServicio, $folioServicioAfore)
	{
		$objAPI = new Capirestconstanciaafiliacion();
		$arrDatos = array();
		$objGeneral= new CMetodoGeneral();

    	$arrData = array(
			'idServicio' => $idServicio,
			'folioServicioAfore' => $folioServicioAfore
		);

		$reg=array();
		$datos =array("respondioServicio"=>0,"descripcionRespuesta"=>"");
		$response = array();
		$obtencionRespuesta = new ObtencionRespuesta();
		$obtencionRespuesta->folioServicioAfore = $folioServicioAfore;

		$resultAPI = $objAPI->consumirApi('servicioEjecutarAplicacion', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			

			$datos['estatus'] = 1;
			$datos['descripcion'] = "EXITO";

			foreach($resultAPI["registros"] as $reg)
			{
				$ipServidor  = $reg['ipservidor'];
				$puerto      = $reg['puerto'];
				$url         = $reg['urlservicio'];
				$protocolo   =  $reg['protocolo'];
			}

			$mensaje =  "Se ejecuto la consulta Correctamente";

			$urlServicio = $protocolo . $ipServidor . ":" . $puerto . $url ;

			$client = new SoapClient($urlServicio,
										array('trace' => true, 'exceptions' => true));		
			try 
			{
				$response = $client->obtenerRespuesta($obtencionRespuesta);
		        $datos = $response->respuesta;
			} 
			catch (SoapFault $fault) 
			{ 
				//echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
				$datos['descripcionRespuesta']='Se presento problemas al consultar servicio';
				$objGeneral->grabarLogx( '[' . __FILE__ . '] [ servicioObtenerRespuesta ] Error al ejecutar el servicio: SOAPFault: '.$fault->faultcode."-".$fault->faultstring);
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}

		return($datos);
	}
}
?>