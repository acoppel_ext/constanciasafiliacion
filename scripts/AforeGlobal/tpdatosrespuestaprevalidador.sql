-- Type: tpdatosrespuestaprevalidador

DROP TYPE tpdatosrespuestaprevalidador CASCADE;

CREATE TYPE tpdatosrespuestaprevalidador AS
   (valmarcas character(1),
    valfoliosconstanciasimplicaciones character(1),
    valregimenpensionario character(1),
    valnumtrasptreintaseismeses character(1),
    valcitactiva character(1),
    valrendimmenor character(1),
    traspasopreviorendimientos character(1),
    valcurpfuncionarioadministradora character(1),
    foliosolicitud bigint,
    valcurptrabajador character(1),
    valnsstrabajador character(1),
    valapellidopaterno character(1),
    valapellidomaterno character(1),
    valnombretrab character(1),
    cveenttransbd character(3),
    motrec character(3),
    folioservicioafore bigint,
    clavepeticionservicio bigint,
    codigo_error integer,
    curp_promotor character(18),
    fecha1traspasoprevio date,
    fecha2traspasoprevio date,
    fecha3traspasoprevio date,
    fecha4traspasoprevio date,
    fecha5traspasoprevio date);
    
ALTER TYPE tpdatosrespuestaprevalidador
  OWNER TO postgres;