<?php
	//define('ERR__',-99);
	define('OK__', 1);
	define("path_wsdl", "ccuentas/ccuenta.wsdl");
	date_default_timezone_set('America/Mazatlan');
	ini_set('default_socket_timeout', 600);
	ini_set("soap.wsdl_cache_enabled" , FALSE);
	include_once("JSON.php");
	include_once ('clases/CMetodoGeneral.php');

	$iTienda = 0;
	$iCliente = 0;
	$json = new Services_JSON();
	$opcionWs = array( 'compression'=>true, 'exceptions'=>true, 'trace'=>true, 'use' => SOAP_ENCODED, 'style'=> SOAP_RPC, 'location'=> "", 'ipcartera'=>"");
	$arrResp = array();
	$arrBdAforeGlobal=array("servidor"=> "", "BaseDeDato"=> "" , 
							"Usuario"=> "" , "contrasenia"=> "");

	$ctrlServicios = array("direccionip"=>"", "puerto"=>0, "puertocartera"=>0);
	$S00011Recibe = array("iTipoMensaje"=>11, "lValorSeguridad"=>0, "iTipoOpcion"=>1, "iTienda"=>0, "cIp"=>"\0");
	/*//TODO: estructura C++ recibida en el inet. Solo es informativo
	struct S00011Recibe
	{
		short int       iTipoMensaje;		// Se envia 11, porque es el tipo de mensaje que espera el servicio
		int             lValorSeguridad;	// Se debe mandar 0, ya que no se utiliza
		int             iTipoOpcion;		// Se envia 1 para indicar que se va a consultar una tienda, 2 para buscar por ciudad
		short int       iDato;				// se envia el numero de la tienda
		char            cIp[15];			// Solo se debe incializar en 0
	};*/
	$S00011Respuesta = array("iRespuesta"=>0, "sIpServRedPorCiudad"=>"", "sIpCarteraHlla"=>"");
	/*//TODO: estructura C++ de respuesta en el inet. Solo es informativo
	struct S00011Respuesta
	{
		int  iRespuesta;				// estado del proceso
		char sIpServRedPorCiudad[15],	// ipServidorRedXCd
			 sIpCarteraHlla[15]; 		// ip de la cartera
	};
		//iRespuesta:
		//	1: Exito. Tomar la Ip del campo sIpRespuesta.
		//	-1: No se establecio conexion al servidor central [En el servicio]
		//	-2: Error al recibir respuesta [En el servicio]
		//	-3: No existe ciudad
		//	-4: No existe tienda
	*/
/*//TODO: esto se ocuparia si se invoca la pagina desde el navegador de internet
	if( isset($_GET['clientecoppel']) )
	{
		$iCliente = $_GET['clientecoppel'];
		$arrResp = obtenerIpDeControlServicios();
		if($arrResp['estatus'] == OK__)
		{
			$arrResp = obtenerIpCarteraEnLinea($iTienda);
			if($arrResp['estatus'] == OK__)
			{
				$arrResp = validarClienteEnCarteraLinea($iCliente);
			}
		}
	}
	else
	{
		$arrResp['estatus'] = ERR__;
		$arrResp['error'] = 'Parametros incorrectos';
		$arrResp['descripcion'] = "PARAMETROS INCORRECTOS";
	}
	echo $json->encode($arrResp);
*/

	function validarCliente($iClienteCoppel)
	{
		$arrResp = obtenerIpDeControlServicios();
		if($arrResp['estatus'] == OK__)
		{
			$arrResp = obtenerIpCarteraEnLinea(1);
			if($arrResp['estatus'] == OK__)
			{
				$objGn = new CMetodoGeneral();
				$objGn->grabarLogx("REINTENTO 1");
				$arrResp = validarClienteEnCarteraLinea($iClienteCoppel,0);
			}
		}
		return $arrResp;
	}

	function obtenerIpDeControlServicios()
	{//TODO: obtener la ip de la cartera en linea en base al numero de la tienda
		global $arrBdAforeGlobal;
		global $ctrlServicios;
		global $S00011Recibe;
		$cnxBd = null;
		$arrResp = array();
		$arrErr = array();
		$sSql = "";
		$ipTienda = "";
		try
		{
			//TODO: leer los parametros de conexion a la base de datos del archivo webconfig
			$configaraciones = simplexml_load_file("../../conf/webconfig.xml");
			$ipServidor = $configaraciones->Spa;
			$sUsuario = $configaraciones->Usuario;
			$sBasedeDatos = $configaraciones->Basededatos;
			$sPassword = $configaraciones->Password;

			$arrBdAforeGlobal['servidor'] = $ipServidor;
			$arrBdAforeGlobal['BaseDeDato']= $sBasedeDatos; 
			$arrBdAforeGlobal['Usuario']= $sUsuario;
			$arrBdAforeGlobal['contrasenia']= $sPassword;
			//TODO:-------------------------------------------------

			$cnxBd =  new PDO( "pgsql:host=".$arrBdAforeGlobal['servidor'].";port=5432;dbname=".$arrBdAforeGlobal['BaseDeDato'], $arrBdAforeGlobal['Usuario'], $arrBdAforeGlobal['contrasenia']);
			if($cnxBd)
			{
				$sSql = "SELECT Concepto, Tipo4, Tipo5 FROM pafhardcode WHERE tipo1 = 1 AND tipo2 = 1 AND tipo3 = 40";
				$resulSet = $cnxBd->query($sSql);
				if($resulSet)
				{
					foreach($resulSet as $reg)
					{
						$ctrlServicios['direccionip'] = TRIM($reg['concepto']);
						$ctrlServicios['puerto'] = $reg['tipo4'];
						$ctrlServicios['puertocartera'] = $reg['tipo5'];
						break;
					}
					if($ctrlServicios['direccionip'] == '' || $ctrlServicios['puerto'] <= 0)
					{
						$arrResp['estatus'] = ERR__;
						$arrResp['error'] = '[hardcode] no se encontro informacion de control servicios';
						$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
					}
					else
					{
						$arrResp['estatus'] = OK__;
						$arrResp['error'] = '';
						$arrResp['descripcion'] = "";
					}
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$arrResp['estatus'] = ERR__;
					$arrResp['error'] = '[hardcode] Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
					$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
				}

				$ipTienda = obtenerIpTienda();
				$sSql = "SELECT tipo3 FROM pafhardcode WHERE tipo1 = 12 AND concepto2 = '" . $ipTienda . "'";
				$resulSet = $cnxBd->query($sSql);
				if($resulSet)
				{
					foreach($resulSet as $reg)
					{
						$S00011Recibe['iTienda'] = $reg['tipo3'];
					}
					if($S00011Recibe['iTienda']>0)
					{
						$arrResp['estatus'] = OK__;
						$arrResp['error'] = '';
						$arrResp['descripcion'] = "";
					}
					else
					{
						$arrResp['estatus'] = ERR__;
						$arrResp['error'] = '[hardcode] no se encontro tienda asignada en tabla de control Afore.' . $sSql;
						$arrResp['descripcion'] = "TIENDA NO ESTA REGISTRADA EN CONTROL AFORE";
					}
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$arrResp['estatus'] = ERR__;
					$arrResp['error'] = '[hardcode] Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2];
					$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
				}
			}
			else
			{
				$arrResp['estatus'] = ERR__;
				$arrResp['error'] = '[cnxBd] ' . $cnxBd->errorInfo();
				$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
			}
			$cnxBd = null;
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
			$arrResp['estatus'] = ERR__;
			$arrResp['error'] = '[PDO::ex] ' . $ex->getMessage();
			$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
			die();
		}
		unset($arrErr);
		return $arrResp;
	}
	function obtenerIpCarteraEnLinea($iOpcion)
	{//TODO: obtener la ip de la cartera en linea, para ello se consume el servicio control_servicios de tiendas
		$objGn = new CMetodoGeneral();
		$objGn->grabarLogx("ENTRO A LA FUNCION obtenerIpCarteraEnLinea");
		global $arrBdAforeGlobal;
		global $ctrlServicios;
		global $opcionWs;
		global $S00011Recibe;
		global $S00011Respuesta;
		$sockErrNum = 0;
		$sockErrDesc = '';
		$sockTimeOut = 30;
		$sockByteIO = 0;
		$buffBinario='';
		$arrResp = array();
		$arrResSock = array();
		//$S00011Recibe['iTienda'] = $iTienda;
		try
		{
			$sock = fsockopen($ctrlServicios['direccionip'], $ctrlServicios['puerto'], $sockErrNum, $sockErrDesc, $sockTimeOut);
			if($sock)
			{
				$buffBinario = pack('siisc', $S00011Recibe['iTipoMensaje'], $S00011Recibe['lValorSeguridad'], 
									$S00011Recibe['iTipoOpcion'], $S00011Recibe['iTienda'], $S00011Recibe['cIp']);
				$sockByteIO = fwrite($sock, $buffBinario);
				$buffBinario = '';
				$buffBinario = fgets($sock);
				$arrResSock = unpack('iiRespuesta/cdireccionip', $buffBinario);
				$S00011Respuesta['iRespuesta'] = $arrResSock['iRespuesta'];
				if($S00011Respuesta['iRespuesta'] == 1)
				{
					$S00011Respuesta['sIpServRedPorCiudad'] = substr($buffBinario, 4,14);
					$S00011Respuesta['sIpCarteraHlla'] = substr($buffBinario, 19,15);;
					$opcionWs['ipcartera']=$S00011Respuesta['sIpServRedPorCiudad'];
					$objGn->grabarLogx("opcionWs['ipcartera']: ".$opcionWs['ipcartera']);
					$arrResp['estatus'] = OK__;
					$arrResp['error'] = '';
					$arrResp['descripcion'] = "";
				}
				else
				{
					switch ($S00011Respuesta['iRespuesta'])
					 {
						case -1:
							$arrResp['estatus'] = ERR__;
							$arrResp['error'] = '[-1] No se establecio conexion al servidor central [En el servicio]';
							$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
							$objGn->grabarLogx("No se establecio conexion al servidor central");
							break;
						case -2:
							$arrResp['estatus'] = ERR__;
							$arrResp['error'] = '[-2] Error al recibir respuesta [En el servicio]';
							$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
							$objGn->grabarLogx("Error al recibir respuesta");
							break;
						case -3:
							$arrResp['estatus'] = ERR__;
							$arrResp['error'] = '[-2] No existe ciudad';
							$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
							$objGn->grabarLogx("No existe ciudad");
							break;
						case -4:
							$arrResp['estatus'] = ERR__;
							$arrResp['error'] = '[-2] No existe ciudad';
							$arrResp['descripcion'] = "NO EXISTE TIENDA";
							$objGn->grabarLogx("No existe tienda");
							break;
						default:
							$arrResp['estatus'] = ERR__;
							$arrResp['error'] = '[' . $S00011Respuesta['iRespuesta'] . '] Excepcion no controlada';
							$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
							$objGn->grabarLogx("Excepcion no controlada");
							break;
					}
				}
			}
			else
			{
				$arrResp['estatus'] = ERR__;
				$arrResp['error'] = '[sock] Error en cnx: '.$sockErrNum. '-'.$sockErrDesc;
				$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
				$objGn->grabarLogx("bandera 4");
			}
		}
		catch (Exception $Ex)
		{
			$arrResp['estatus'] = ERR__;
			$arrResp['error'] = '[Ex] Error: '.$Ex->getMessage();
			$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
			$objGn->grabarLogx("bandera 5");
		}
		unset($arrResSock);
		
		//folio 997
		if($iOpcion==1)
		{
			$cnxBd =  new PDO( "pgsql:host=".$arrBdAforeGlobal['servidor'].";port=5432;dbname=".$arrBdAforeGlobal['BaseDeDato'], $arrBdAforeGlobal['Usuario'], $arrBdAforeGlobal['contrasenia']);
			if($cnxBd)
			{
				$sSql = "SELECT Concepto FROM pafhardcode WHERE tipo1 = 1 AND tipo2 = 1 AND tipo3 = 134";
				$objGn->grabarLogx($sSql);
				$resulSet = $cnxBd->query($sSql);
				if($resulSet)
				{
						$objGn->grabarLogx("SE CONSULTO CORRECTAMENTE pafhardcode 1");
						
						foreach($resulSet as $reg)
						{	
							$opcionWs['location']="http://". TRIM($reg['concepto']) .":".$ctrlServicios['puertocartera'];	
							$objGn->grabarLogx("opcionWs :".$opcionWs['location']);							
						}
				}
				else
				{
					$objGn->grabarLogx("ERROR AL EJECUTAR LA CONSULTAR IP CARTERA 1");
				}
			}
			else
			{
				$objGn->grabarLogx("NO PUDO ABRIR CONEXION PARA CONSULTAR IP CARTERA 1");
			}
		}
		else if($iOpcion==2)
		{
			$cnxBd =  new PDO( "pgsql:host=".$arrBdAforeGlobal['servidor'].";port=5432;dbname=".$arrBdAforeGlobal['BaseDeDato'], $arrBdAforeGlobal['Usuario'], $arrBdAforeGlobal['contrasenia']);
			if($cnxBd)
			{
				$objGn->grabarLogx("SE CONSULTO CORRECTAMENTE pafhardcode 2");
				
				$sSql = "SELECT Concepto FROM pafhardcode WHERE tipo1 = 1 AND tipo2 = 1 AND tipo3 = 135";
				$resulSet = $cnxBd->query($sSql);
				if($resulSet)
				{
						foreach($resulSet as $reg)
						{						
							$opcionWs['location']="http://". TRIM($reg['concepto']) .":".$ctrlServicios['puertocartera'];					
							$objGn->grabarLogx("opcionWs :".$opcionWs['location']);										
						}
				}
				else
				{
					$objGn->grabarLogx("ERROR AL EJECUTAR LA CONSULTAR IP CARTERA 2");
				}
			}
			else
			{
				$objGn->grabarLogx("NO PUDO ABRIR CONEXION PARA CONSULTAR IP CARTERA 2");
			}
		}
		
		return $arrResp;
	}
	function validarClienteEnCarteraLinea($iCliente,$iBandera)
	{//TODO: consultar al cliente en la cartera
	
		$objGn = new CMetodoGeneral();
		$objGn->grabarLogx("NUMERO CLIENTE: ".$iCliente);
		
		global $opcionWs;
		$arrResp = array();
		$objCompleto = array();
		$arrX = array();
		$resWs = null;
		$datosXml = null;
		try
		{
		
			global $opcionWs;
			$clienteWs = new SoapClient(path_wsdl, $opcionWs);
			$resWs = $clienteWs->__call("leerClienteCoppel",array( "numerocliente"=>$iCliente ,"cIpCartera"=>$opcionWs['ipcartera']));
			$objCompleto = get_object_vars($resWs);
			$datosXml = simplexml_load_string($objCompleto['Respuesta']);
			if($datosXml)
			{
				foreach($datosXml as $elem)
				{
					$arrX = get_object_vars($elem);
					if( $arrX['nombre'] == -1 )
					{
						$arrResp['estatus'] = $arrX['nombre'];
						$arrResp['error'] = $arrX['apellidopaterno'];
						$arrResp['descripcion'] = $arrX['apellidopaterno'];
						$objGn->grabarLogx("CLIENTE NO ENCONTRADO EN CARTERA");
					}
					else
					{
						$arrResp['estatus'] = OK__;
						$arrResp['error'] = '';
						$arrResp['descripcion'] = "CLIENTE ENCONTRADO EN CARTERA";
						$objGn->grabarLogx("CLIENTE ENCONTRADO EN CARTERA");
					}
				}
			}
			else
			{
				$arrResp['estatus'] = ERR__;
				$arrResp['error'] = '[xml] formato XML invalido';
				$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
				$objGn->grabarLogx("FORMATO XML INVALIDO");
				
			}

		}
		catch(SoapFault $soapEx)
		{
			$arrResp['estatus'] = ERR__;
			$arrResp['error'] = '[soapEx] ' . $soapEx->getMessage();
			$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";			
			if($iBandera != 1)
			{
				$objGn->grabarLogx("ERROR EN EL CONSUMO Soap");
				$objGn->grabarLogx("REINTENTO 2");
				obtenerIpCarteraEnLinea(2);
				validarClienteEnCarteraLinea($iCliente,1);
			}
		}
		catch (Exception $Ex)
		{
			$arrResp['estatus'] = ERR__;
			$arrResp['error'] = '[Ex] ' . $Ex->getMessage();
			$arrResp['descripcion'] = "ERROR AL VALIDAR CLIENTE EN CARTERA";
			$objGn->grabarLogx("CLIENTE ENCONTRADO EN CARTERA");
		}
		unset($arrX);
		unset($objCompleto);
		unset($resWs);
		unset($datosXml);
		return $arrResp;
	}
	function obtenerIpTienda()
	{
		$ipCliente = "";
		
		if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) )
			$ipCliente = $_SERVER['HTTP_X_FORWARDED_FOR']; 
		else
			$ipCliente = $_SERVER['REMOTE_ADDR'];

		return $ipCliente;
	}
?> 
