-- Function: fnobtenerdoctosdigitalizacion(integer, integer)

-- DROP FUNCTION fnobtenerdoctosdigitalizacion(integer, integer);

CREATE OR REPLACE FUNCTION fnobtenerdoctosdigitalizacion(
    integer,
    integer)
  RETURNS integer AS
$BODY$

/*
Funcion que retorna el identificador de la tabla pafindeximg que se debera de consultar con la funcion fnpafIndexGetDoctos()
Desarrollo: Sergio Jimenez
Aplicacion: PAFDIGIDOCTOSAFILIACION.EXE
fecha   : 20130816
*/

/*
Se agregaron funcionalidad para retornar el identificador de los documentos para registro/registro independiente, solicitud
de constancia para registro o traspaso, constancia para registro o traspaso, constancia de implicaciones.
Desarrollo: Carlos Ibarra
Aplicacion: DIGIDOCTOSCLIENTE.EXE
fecha   : 20150228
*/

/*
Se quitaron funcionalidades para evitar la consulta en colcertificadotraspaso de esta manera no se tomara en cuenta el tipo
de autenticacion (Bancoppel y/o PROSA) ni con su tipo de claveoperacion)
Programador: Ignacio Rodelo
Aplicacion: DIGIDOCTOSCLIENTE.EXE
Fecha     :20160420
*/

/*
Se modifico la funcionalidad para quitar la Constancia de Solicitud y que siga autoindexando el Formato de Enrolamiento
Programador: Adan Gonzalez
Aplicacion: DIGIDOCTOSCLIENTE.EXE
Fecha     :20190214
*/

/*
Se agrego una nueva clave de OperaciÃ³n para las Solicitudes de AfiliaciÃ³n por Traspaso con AutenticaciÃ³n INE
Desarrollo: Adan Gonzalez
Aplicacion: DIGIDOCTOSCLIENTE.EXE
fecha   : 20190517
*/

/*
Se agrego una nueva clave de Operacion para las Solicitudes de Afiliacion por Traspaso con Autenticacion 48 Folio Dactilar
Desarrollo: David Beltran
Aplicacion: DIGIDOCTOSCLIENTE.EXE
fecha   : 20200727
*/

DECLARE
	lFolio ALIAS FOR $1;
	lTabla ALIAS FOR $2;
	itiposolicitud integer;
	itipotraspaso integer;
	itieneedocta integer;
	iestatusproceso integer;
	iestatusfisico integer;
	icodigodiagnostico integer;
	cidtipoautenticacion char(3);
	lClaveOperacion int4;--identificador de la pafindeximg
	iTipoConstancia integer;
	cDiagnosticoProcesar char(4);
BEGIN
	lClaveOperacion = 0; --Indica el tipo de solicitud
	itiposolicitud = 0;
	itipotraspaso  = 0;
	itieneedocta  = 0;
	iestatusproceso  = 0;
	iestatusfisico = 0;
	icodigodiagnostico = 0;
	iTipoConstancia = 0;
	cDiagnosticoProcesar = '-1';

	--lTabla = 1 -->Significa que consultara el folio en la colsolicitudes
	IF lTabla = 1 THEN
		select tiposolicitud,tipotraspaso,tieneedocta,statusproceso,codigodiagnostico,statusfisico
		into itiposolicitud,itipotraspaso,itieneedocta,iestatusproceso,icodigodiagnostico,iestatusfisico
		from colsolicitudes as a
		where a.folio = lFolio
		--se quitara el 7 para evitar agrupar
		GROUP BY 1,2,3,4,5,6;

		--RAISE NOTICE '%',itiposolicitud;
		IF itiposolicitud = 27 THEN
			IF iestatusproceso = 1 AND icodigodiagnostico = 1 AND iestatusfisico = 1 THEN
				IF itipotraspaso IN (0,1,327,427,527) THEN
					IF itieneedocta = 1 THEN
					      /*
						 Muestro todos los documentos
						 DO00|CA00|OTRO|P000|CURP|ID00|ST00|RN0#|EDOC|FTO0
					      */
						lClaveOperacion =  278;
					END IF;
				ELSIF itipotraspaso = 827 THEN
					IF itieneedocta = 4 THEN
						 /*
						Muestro todos los documentos
						DO00|CA00|OTRO|P000|CURP|ID00|ST00|RN0#|FTO0|CFEC
						 */
						lClaveOperacion = 276;
					END IF;
				ELSIF itipotraspaso IN (828, 829) THEN --Traspasos IMSS e ISSSTE con AutenticaciÃ³n INE
					lClaveOperacion = 500; --Identificador en la tabla pafindeximg
				ELSIF itipotraspaso IN (830, 831) THEN --Traspasos IMSS e ISSSTE con AutenticaciÃ³n BCPL
					lClaveOperacion = 509; --Identificador en la tabla pafindeximg
				ELSIF itipotraspaso IN (832,833) THEN  
					lClaveOperacion = 524;  --Traspasos IMSS e ISSSTE con Autenticacion 48 Folio Dactialr
				END IF;
			END IF;
		ELSIF itiposolicitud = 227  THEN
			IF itipotraspaso = 827 THEN
				IF itieneedocta = 4 THEN
					IF iestatusproceso = 1 AND icodigodiagnostico = 1 AND iestatusfisico = 1 THEN
					      /*
						 Muestro todos los documentos
						 DO00|CA00|OTRO|P000|CURP|ID00|ST00|RN0#|FTO0|CFEC
					      */
						lClaveOperacion = 275;
					ELSIF iestatusproceso = 2 AND icodigodiagnostico = 1 AND iestatusfisico = 1 THEN
					      /*
						 Muestro todos los documentos
						 ST00|CERT
					      */
						lClaveOperacion = 277;
					END IF;
				END IF;
			END IF;
		ELSIF itiposolicitud in (26,33) THEN --REGISTRO /REGISTRO INDEPENDIENTE
			lClaveOperacion =  01;
		END IF;

	--lTabla = 2 -->Significa que consultara el folio en la celsolicitudes
	ELSIF lTabla = 2 THEN
		select tiposolicitud into itiposolicitud from celsolicitudes where folio = lFolio;
		IF itiposolicitud = 626  THEN
			/*
				muestro todos los documentos
				DO00|CA00|OTRO|P000|CURP|ID00|SR00|RN0#|FTO0
			 */
			lClaveOperacion = 273;
		END IF;
	ELSIF lTabla = 3 THEN -- SOLICITUD DE CONSTANCIA  TRASP/REG.
		SELECT tipoconstancia into iTipoConstancia FROM solconstancia where folio = lFolio;
		IF FOUND THEN
			IF iTipoConstancia = 26 OR iTipoConstancia = 33 THEN
					lClaveOperacion = 303;
			ELSIF iTipoConstancia = 27 THEN
					lClaveOperacion = 302;
			END IF;
			UPDATE firmadigitalafiliacion SET firmasolconstancia = 1, publicosolconstancia = 1 WHERE folio = lFolio;
		END IF;
	ELSIF lTabla = 4 THEN -- CONSTANCIA  TRASP/REG.
		SELECT tipoconstancia into iTipoConstancia FROM solconstancia where folio = lFolio;
		IF FOUND THEN
			IF iTipoConstancia = 26 OR iTipoConstancia = 33 THEN
				lClaveOperacion = 307;
			ELSIF iTipoConstancia = 27 THEN
				lClaveOperacion = 306;
			END IF;
		END IF;
	ELSIF lTabla = 7 THEN -- CONSTANCIA DE IMPLICACIONES
		SELECT TRIM(diagnosticoprocesar) INTO cDiagnosticoProcesar FROM solconstanciaimplicaciones WHERE folioservicio = lFolio::char(21);
		IF FOUND THEN
			IF cDiagnosticoProcesar = '' THEN
				lClaveOperacion = 331;
			ELSE
					lClaveOperacion = 330;
			END IF;
		END IF;
	END IF;
	RETURN lClaveOperacion;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fnobtenerdoctosdigitalizacion(integer, integer)
  OWNER TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fnobtenerdoctosdigitalizacion(integer, integer) TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fnobtenerdoctosdigitalizacion(integer, integer) TO public;
