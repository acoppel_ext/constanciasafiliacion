<?php
include("clases/fpdf.php");
include("../../fpdi/fpdi.php");
include_once("clases/CMetodoGeneral.php");
include_once("clases/global.php");
include_once("clases/Capirestconstanciaafiliacion.php");
date_default_timezone_set('America/Mazatlan');

$controlador = "CtrlconstanciaAfiliacion";

$objGn = new CMetodoGeneral();
$arrErr = array();
$arrResp = array();

$arrDatosPublicacion = array('folio' => $_POST["folio"],
							'curp' => $_POST["curp"],
							'ifirma' => $_POST["ifirma"],
							'rutaPdfTemp' => $_POST["rutaPdfTemp"]
							);

publicarSolicitudConstancia($arrDatosPublicacion);

function publicarSolicitudConstancia($arrDatosPublicacion)
{
		global $objGn;
		$objGn->grabarLogx("INICIA PUBLICACIÓN DE IMAGEN DE SOLICITUD DE CONSTANCIA");
		$arrResp = array();
		$respuesta = new stdClass();
		$pdf = new FPDF();
		$iCveOperacion = -1;
		$sImagenSolConstancia = "";
		$sRutaAbsoluta = "";
		
		$sRutaImagenNombreTrabajador = '/sysx/progs/web/entrada/firmas/SCTR_'.$arrDatosPublicacion["folio"].'_NTRAB.JPG'; 
		$sRutaImagenFirmaTrabajador = '/sysx/progs/web/entrada/firmas/SCTR_'.$arrDatosPublicacion["folio"].'_FTRAB.JPG'; 
		$sRutaImagenFirmaPromotor = '/sysx/progs/web/entrada/firmas/SCTR_'.$arrDatosPublicacion["folio"].'_FPROM.JPG';
		$sRutaAbsoluta = basename($arrDatosPublicacion["rutaPdfTemp"]);
		try
		{
			$objGn->grabarLogx("VALIDA CAMPO FIRMA: ".$arrDatosPublicacion["ifirma"]);
			if ($arrDatosPublicacion["ifirma"] == 1)
			{
				$arrResp = obtenerClaveOperacion($arrDatosPublicacion["folio"]);
				if($arrResp["estado"] == OK___)
				{
					$objGn->grabarLogx("Ruta del PDF: ".$sRutaAbsoluta);
					if(file_exists($arrDatosPublicacion["rutaPdfTemp"]))
					{
						$objGn->grabarLogx("El PDF existe");
						$iCveOperacion = $arrResp["clave"];
						unset($arrResp);
						$arrResp = $objGn->convertirPDFaImagen($arrDatosPublicacion["folio"], $arrDatosPublicacion["curp"], $iCveOperacion, $sRutaAbsoluta, 1 );
						if($arrResp["estado"] == OK___)
						{
							$objGn->grabarLogx("Se convirtio PDF a imagen");
							$sImagenSolConstancia = $arrResp["archivo"]; unset($arrResp);
							$arrResp = $objGn->unirImagenes(1,$sImagenSolConstancia, $sRutaImagenNombreTrabajador, $sRutaImagenFirmaTrabajador, $sRutaImagenFirmaPromotor);										
							if($arrResp["estado"] == OK___)
							{				
								$objGn->grabarLogx("Se unieron imagenes correctamente");
								unset($arrResp);
								$arrResp = $objGn->publicarImagenConstancia($sImagenSolConstancia);
								if($arrResp["estado"] == OK___)
								{
									$objGn->grabarLogx("Se publico imagen correctamente");
									unset($arrResp);
									$arrResp = $objGn->actualizarfirmapublicacion($arrDatosPublicacion["folio"], 1);
									if($arrResp["estado"] == OK___)
									{
										$objGn->grabarLogx("Se actualizo campo firmasolconstancia [revisar fuente]");
										//SE BORRAN FIRMAS DEL SERVIDOR
										/*
											20151025: Euduardo Osuna
											se comenta el borrado hasta definir en sistemas que se hace con los archivos
										*/
										/*array_map('unlink', glob("/sysx/progs/web/entrada/firmas/SCTR_".$arrDatosPublicacion["folio"]."_NTRAB.JPG"));
										array_map('unlink', glob("/sysx/progs/web/entrada/firmas/SCTR_".$arrDatosPublicacion["folio"]."_FTRAB.JPG"));
										array_map('unlink', glob("/sysx/progs/web/entrada/firmas/SCTR_".$arrDatosPublicacion["folio"]."_FPROM.JPG"));*/
										
										CLogImpresion::escribirLog("Se borran las firmas del servidor");
										$respuesta->descestatus = 'EXITO';
										$respuesta->estado = 1;	
									}
									else
									{
										$respuesta->descestatus = "Ocurrio un error al actualizar la firma";
										$respuesta->estado = -1;	
									}
								}
								else
								{
									$objGn->grabarLogx("No se pudo publicar la imagen de la solicitud de constancia en el servidor intermedio");
									$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
									$respuesta->estado = -1;		
								}
							}
							else
							{
								$objGn->grabarLogx("No se pudo unir las imagenes de las firmaas en la constancia");
								$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
								$respuesta->estado = -1;		
							}	
						}
						else
						{
							$objGn->grabarLogx("Ocurrio un error al generar convertir el archivo PDF a Imagen ".$arrDatosPublicacion["rutaPdfTemp"]);
							$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
							$respuesta->estado = -1;
						}
					}
					else
					{
						$objGn->grabarLogx("No se encontro el archivo PDF -> ".$arrDatosPublicacion["rutaPdfTemp"]);
						$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
						$respuesta->estado = -2;
					}
				}
				else
				{
					$respuesta->descestatus = "PROMOTOR: El documento no pudo ser indexado automáticamente. Favor de realizar la Impresión de Constancia Nuevamente.";
					$respuesta->estado = -3;
				}
			}
			else
			{
				$respuesta->descestatus = 'EXITO';
				$respuesta->estado = 1;
			}
			
		}
		
		catch (Exception $Ex) 
		{
			//SE BORRAN FIRMAS DEL SERVIDOR
			/*
				20151025: Euduardo Osuna
				se comenta el borrado hasta definir en sistemas que se hace con los archivos
			*/
			/*array_map('unlink', glob("/sysx/progs/web/entrada/firmas/SCTR_".$arrDatosPublicacion["folio"]."_NTRAB.JPG"));
			array_map('unlink', glob("/sysx/progs/web/entrada/firmas/SCTR_".$arrDatosPublicacion["folio"]."_FTRAB.JPG"));
			array_map('unlink', glob("/sysx/progs/web/entrada/firmas/SCTR_".$arrDatosPublicacion["folio"]."_FPROM.JPG"));*/
			
			CLogImpresion::escribirLog("Se borran las firmas del servidor");
			$respuesta->descestatus = $Ex->getMessage();
			$respuesta->estado = ERR__;
		}

	echo json_encode($respuesta);
}

function obtenerClaveOperacion($iFolio)
{
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestconstanciaafiliacion();

	$arrDatos = array();
	$arrDatos['clave'] = 0;
	$arrDatos['respuesta'] = 0;
	$arrDatos['estado'] = -1;

	$arrData = array(
		'iPmodulo' => $iFolio,
	);

	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('obtenerClaveOperacion', $arrData, $GLOBALS["controlador"]);

		if(isset($resultAPI['estatus']) && $resultAPI['estatus'] == 1)
		{
			$resultAPI = json_decode($resultAPI,true);

			foreach($resultAPI['registros'] AS $reg)
			{
				$arrDatos['estado'] = 1;
				$arrDatos["clave"] = trim($reg["claveop"]);
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
			else
				header('HTTP/1.1 409 Conflict');

			return "Error al consultar la API";
		}
	}
	catch (Exception $e)
	{
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	return $arrDatos;
}
?>
