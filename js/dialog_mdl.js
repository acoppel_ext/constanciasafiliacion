function dialog_ac(divDialogo)
{
	var divDlgMain=divDialogo;
	var iAltoDef=200;
	var iAnchoDef=250;
	var iAltoDlg=0;
	var iAnchoDlg=0;
	
	this.espera=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
				'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
	};
	
	this.crear=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { cerrar(); };
	};
	
	this.mdlcrear=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'<input type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { HeHuellagerente(); };
		document.getElementById('btnModalAcep').onclick=function() { HeHuellagerente(); };
	};
	
	this.modalGerente=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'&nbsp;&nbsp;&nbsp;<input  type=\"button\" id=\"btnCancelar\" value=\"Cancelar\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { closeNavegador(); };
		document.getElementById('btnCancelar').onclick=function() { closeNavegador(); };
		document.getElementById('btnModalAcep').onclick=function() { levantarAppplet(); };
	};
	
	this.modalHuella=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'&nbsp;&nbsp;&nbsp;<input  type=\"button\" id=\"btnCancelar\" value=\"Cancelar\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { closeNavegador(); };
		document.getElementById('btnCancelar').onclick=function() { closeNavegador(); };
		document.getElementById('btnModalAcep').onclick=function() { levantarAppplet(); };
	};

	this.modalErrorgerente=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'<input type=\"button\" id=\"btnReintentar\" value=\"Reintentar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'&nbsp;&nbsp;&nbsp;<input type=\"button\" id=\"btnCancelar\" value=\"Cancelar\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { closeNavegador(); };
		document.getElementById('btnCancelar').onclick=function() { closeNavegador(); };
		document.getElementById('btnReintentar').onclick=function() { HeHuellagerente(); };
	};

	this.modalPublicaVideo=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+				
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
				'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'<input type=\"button\" id=\"btnAceptar\" value=\"Aceptar\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnAceptar').onclick=function() { opcionejecutaPublicaVideo(); };
	};

	this.modalcierre=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'<input type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { closeNavegador(); };
		document.getElementById('btnModalAcep').onclick=function() { closeNavegador(); };
	};

	this.modalcierremensaje=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'<input type=\"button\" id=\"btnModalAcep\" value=\"Si\" class=\"BtnModal\" />'+
				'&nbsp;&nbsp;&nbsp;<input type=\"button\" id=\"btnCancelar\" value=\"No\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnCancelar').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { closeNavegador(); };
	};
	
	this.modalObtenerRespuestaVerificacionEmpleado=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'<input type=\"button\" id=\"btnModalAcep\" value=\"Si\" class=\"BtnModal\" />'+
				'&nbsp;&nbsp;&nbsp;<input type=\"button\" id=\"btnCancelar\" value=\"No\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { closeNavegador(); };
		document.getElementById('btnCancelar').onclick=function() { closeNavegador(); };
		document.getElementById('btnModalAcep').onclick=function() { ObtenerRespuesta(); };
	};
	
	this.capturartarjeta=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" style="width: 100%;">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" >'+
					'</div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr id="botones1"><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnCancelar\" value=\"Cancelar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
				'&nbsp;&nbsp;&nbsp;<input  type=\"button\" id=\"btnModalAcep\" value=\"Confirmar\" class=\"BtnModal\" />'+
				'</td></tr>'+
				'<tr id="botones2" style= "display:none"><td align=\"center\" colSpan=\"4\">'+
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type=\"button\" id=\"btnCancelar\" value=\"Cancelar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
				'&nbsp;&nbsp;&nbsp;<input type=\"button\" id=\"btnModalAcepCVC\" value=\"Aceptar\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnCancelar').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { validarTarjeta(); };
		document.getElementById('btnModalAcepCVC').onclick=function() { validarCVC(); };
	};
	
	this.modalpopup=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
	};

	//-----------------------------------------------------------------------------------
	this.crearFirmatrabajador=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Continuar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { levantarApppletTrabajadorFirma(); };
	};
	
	function levantarApppletTrabajadorFirma()
	{
		sParametros = "1 1 " + "STI0_" + iFolioServicio + "-S_FTRAB";
		ejecutarApplet(sParametros);
		$("#divDlgMain").hide();
	}
	
	this.MensajesExepciones=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Continuar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { cerrar(); };
	};
	
	this.MensajesAlertas=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Continuar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { cerrar(); };
	};

	this.MensajeFinalGuardar=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { habilitarBotones(); };
	};

	function habilitarBotones()
	{
		
		if (iGuardarMsjError == 1)
		{
			$('#btnguardar').attr('disabled', false);
			iGuardarMsjError = 0;
		}
		else
		{
			$('#btnguardar').attr('disabled', true);
			$('#btnimprimirsolicitud').attr('disabled', false);
			$('#cboTipoPago').attr('disabled', true);
			$('#cboBanco').attr('disabled', true);
			$('#txtNumeroCuenta').attr('disabled', true);	
		}
		$("#divDlgMain").hide();
	}

	this.MensajeFinalGuardarFallo=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { habilitarBotones();  };
	};

	this.MensajeFirmaConectada=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { MensajeTrabajadorAlerta();};
	};

	this.MensajeURL=function(iAncho, iAlto, sTitulo, url)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { levantarPagina(url);};
	};

	this.MensajeURLSolVigente=function(iAncho, iAlto, sTitulo, url)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'<input type=\"button\" id=\"btnModalAcep\" value=\"Si\" class=\"BtnModal\" />'+
				'&nbsp;&nbsp;&nbsp;<input type=\"button\" id=\"btnCancelar\" value=\"No\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnCancelar').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { levantarPagina(url); };
	};

	this.MensajeAudio=function(iAncho, iAlto, sTitulo, parametrosAudio)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { levantarAudio(parametrosAudio);};
	};
	
	this.MensajeVideo=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { grabarVideo();};
	};
	this.MensajeHuellas=function(iAncho, iAlto, sTitulo, parametrosHuellas)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"10%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { validarHuellaCliente(parametrosHuellas);};
	};

	//-----------------------------------------------------------------------------------
	function compruebaTecla(evt){
		var tecla = evt.which || evt.keyCode;
		if(tecla == 27)
		{
			if (evt != null)
			{
				evt.which = 0;
	            evt.preventDefault();
	            evt.stopPropagation();
	            evt.keyCode = 0;
	            evt.returnValue = false;
	            evt.cancelBubble = true;
	        }
	        else
	        {
	        	event.keyCode = 0;
	            event.returnValue = false;
	            event.cancelBubble = true;
	        }
		}
	}

	this.mostrar=function(sContenido)
	{
		document.getElementById('divDialog').style.width=iAnchoDlg+'px';
		document.getElementById('divDialog').style.height=iAltoDlg+'px';
		divContentDlg = document.getElementById('divContentDlg');
		divContentDlg.innerHTML=sContenido;
		//document.getElementById('divDialog').style.marginTop = ((document.body.clientHeight/2) - (iAltoDlg/2)) +  'px';
		//document.getElementById('divDialog').style.marginTop = ((500) - (400)) +  'px';
    	divDlgMain.style.display = 'block';
	};
	
	function cerrar(){
		divDlgMain.innerHTML="";
		divDlgMain.style.display = 'none';
	};
	
	this.agregarMensaje=function(sContenido)
	{
		divContentDlg = document.getElementById('divContentDlg');
		divContentDlg.innerHTML=divContentDlg.innerHTML+sContenido;
	};

	this.mostrarDialogo=function()
	{
		document.getElementById('divDialog').style.width=iAnchoDlg+'px';
		document.getElementById('divDialog').style.height=iAltoDlg+'px';
		document.getElementById('divDialog').style.marginTop = ((document.body.clientHeight/2) - (iAltoDlg/2)) +  'px';
    	divDlgMain.style.display = 'block';
	};
}

/*******************************<991.1>*******************************/
function dialog_frame(divDialogo)
{
	var divDlgMain 	= divDialogo;
	var iAltoDlg 	= 40;
	var iAnchoDlg 	= 40;

	this.FramePagina=function(iAncho, iAlto, sUrl)
	{
		divDlgMain.innerHTML = "";
		if(iAlto == 'undefined' || iAlto == 0) {iAltoDlg=iAltoDlg;}
		else {iAltoDlg = iAlto;}

		if(iAncho == 'undefined' || iAncho == 0) {iAnchoDlg = iAnchoDlg;}
		else {iAnchoDlg = iAncho;}

		var sHtml=
			'<div id=\"divDialog\">'+
			'	<iframe src="' + sUrl + '" class="iframe-mdl" allowfullscreen></iframe>' +
			'</div>';
		divDlgMain.innerHTML=sHtml;

		document.getElementById('divDialog').style.width 		= iAnchoDlg+'%';
		document.getElementById('divDialog').style.height 		= iAltoDlg+'%';
		document.getElementById('divDialog').style.marginTop 	= '5%';
    	divDlgMain.style.display = 'block';
	};
}
/*******************************</991.1>*******************************/

function levantarAppplet()
{
	opcionEjecuta(sParametros);
	$("#divDlgMain").hide();
}

function HeHuellagerente()
{	
	sParametro = '';
	iOpcion = VALIDACION_GERENTE;
	opcionEjecuta(sParametros);
	$("#divDlgMain").hide();
};

function closeNavegador()
{
	//firefox
	if(navigator.appName.indexOf('Netscape') >= 0 )
	{
		//NAVEGADOR FIREFOX
		javascript:window.close();
	}
	else 
	{
		if(navigator.appName.indexOf('Microsoft') >= 0)
		{//internet explorer
			var ventana = window.self;
			ventana.opener = window.self;
			ventana.close();
		}
	}
}

//Busca respuesta que regreso el consumo del webservices de verificacion de empleado(promotor)
function ObtenerRespuesta()
{
	$("#divDlgMain").hide();
	ObtenerRespuestaVerficicacionempleado();
}