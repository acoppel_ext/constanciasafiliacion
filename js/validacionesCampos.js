$(document).ready(function () {
    if (OSName == "Android") {
        $("#txtApellidoMatero, #txtApellidoPaterno, #txtNombre").on(
            "textInput",
            e => {
                var keyCode = e.originalEvent.data.charCodeAt(0);

                if (
                    !(
                        (keyCode >= 65 && keyCode <= 90) ||
                        (keyCode >= 97 && keyCode <= 122) ||
                        keyCode == 32 ||
                        keyCode == 39 ||
                        (keyCode >= 45 && keyCode <= 47) ||
                        keyCode == 95 ||
                        (keyCode >= 152 && keyCode <= 154) ||
                        keyCode == 129 ||
                        keyCode == 132 ||
                        keyCode == 137 ||
                        keyCode == 139 ||
                        keyCode == 142 ||
                        keyCode == 148
                    )
                ) {
                    e.preventDefault();
                }
            }
        );

        $("#txTelCelular").on("textInput", e => {
            var keyCode = e.originalEvent.data.charCodeAt(0);

            if ($("#txTelCelular").val().length == 10) {
                if (keyCode != 8 && keyCode != 0) {
                    e.preventDefault();
                }
            } else {
                if ((keyCode < 48 || keyCode > 57) && keyCode != 0 && keyCode != 8) {
                    e.preventDefault();
                }
            }
        });

        $("#txtTelContacto").on("textInput", e => {
            var keyCode = e.originalEvent.data.charCodeAt(0);

            if ($("#txtTelContacto").val().length == 10) {
                if (keyCode != 8 && keyCode != 0) {
                    e.preventDefault();
                }
            } else {
                if ((keyCode < 48 || keyCode > 57) && keyCode != 0 && keyCode != 8) {
                    e.preventDefault();
                }
            }
        });

        $("#numTarjet").on("textInput", e => {
            var keyCode = e.originalEvent.data.charCodeAt(0);

            if ($("#numTarjet").val().length == 16) {
                if (keyCode != 8 && keyCode != 0) {
                    e.preventDefault();
                }
            } else {
                if ((keyCode < 48 || keyCode > 57) && keyCode != 0 && keyCode != 8) {
                    e.preventDefault();
                }
            }
        });

        $("#numTarjetC").on("textInput", e => {
            var keyCode = e.originalEvent.data.charCodeAt(0);

            if ($("#numTarjetC").val().length == 16) {
                if (keyCode != 8 && keyCode != 0) {
                    e.preventDefault();
                }
            } else {
                if ((keyCode < 48 || keyCode > 57) && keyCode != 0 && keyCode != 8) {
                    e.preventDefault();
                }
            }
        });

        $("#txtClienteCoppel").on("textInput", e => {
            var keyCode = e.originalEvent.data.charCodeAt(0);

            if ($("#txtClienteCoppel").val().length == 11) {
                if (keyCode != 8 && keyCode != 0) {
                    e.preventDefault();
                }
            } else {
                if ((keyCode < 48 || keyCode > 57) && keyCode != 0 && keyCode != 8) {
                    e.preventDefault();
                }
            }
        });

        $("#txtNss").on("textInput", e => {
            var keyCode = e.originalEvent.data.charCodeAt(0);

            if ($("#txtNss").val().length == 11) {
                if (keyCode != 8 && keyCode != 0) {
                    e.preventDefault();
                }
            } else {
                if ((keyCode < 48 || keyCode > 57) && keyCode != 0 && keyCode != 8) {
                    e.preventDefault();
                }
            }
        });

        $("body").on("keyup", "#txtFolio", function (event) {
            var texto = document.getElementById("txtFolio").value;
            texto = texto.toString();

            var expReg = /^[a-zA-Z0-9]*$/;

            if (expReg.test(texto)) {
                if ($(this).val().length >= 6) {
                    $(this).val(
                        $(this)
                            .val()
                            .substr(0, 6)
                    );

                    if (event.which != 8 && event.which != 0) {
                        event.preventDefault();
                    } else {
                        return validarKeypressCurp(e);
                    }
                } else {
                    return validarKeypressCurp(e);
                }
            } else {
                var texto = $("#txtFolio")
                    .val()
                    .replace(/[\W_]/gi, "");
                $("#txtFolio").val(texto);
            }
        });

        $("body").on("keyup", "#txtCurp", function (e) {
            var texto = $("#txtCurp").val();
            texto = texto.toString();

            var expReg = /^[a-zA-Z0-9]*$/;

            if (expReg.test(texto)) {
                if ($(this).val().length >= 18) {
                    $(this).val(
                        $(this)
                            .val()
                            .substr(0, 18)
                    );

                    if (e.which != 8 && e.which != 0) {
                        e.preventDefault();
                    } else {
                        return validarKeypressCurp(e);
                    }
                } else {
                    return validarKeypressCurp(e);
                }
            } else {
                var texto = $("#txtCurp")
                    .val()
                    .replace(/[\W_]/gi, "");
                $("#txtCurp").val(texto);
            }
        });
        $("#cboExcepciones").attr("disabled", true);
        $("#cboExcepciones").val(26);
        $("#cboTipoRecepcion").attr("disabled", false);
    } else {
        // MODULO
        $("#cboTipoRecepcion").attr("disabled", true);
        $("body").on("keydown", "#numTarjet", function (event) {
            if ($(this).val().length == 16) {
                if (event.which != 8 && event.which != 0) {
                    event.preventDefault();
                }
            } else {
                if (event.which < 48 || event.which > 58) {
                    if (
                        event.which != 8 &&
                        event.which != 0 &&
                        (event.which < 96 || event.which > 105)
                    ) {
                        event.preventDefault();
                    }
                }
            }
        });
        $("body").on("keydown", "#numTarjetC", function (event) {
            if ($(this).val().length == 16) {
                if (event.which != 8 && event.which != 0) {
                    event.preventDefault();
                }
            } else {
                if (event.which < 48 || event.which > 58) {
                    if (
                        event.which != 8 &&
                        event.which != 0 &&
                        (event.which < 96 || event.which > 105)
                    ) {
                        event.preventDefault();
                    }
                }
            }
        });

        $("#txtNombre, #txtApellidoPaterno,#txtApellidoMatero").keypress(function (
            event
        ) {
            return validarKeypressNombresApellidos(event);
        });

        $("body").on("keydown", "#txTelCelular,#txtTelContacto", function (event) {
            if ($(this).val().length == 10) {
                if (event.which != 8 && event.which != 0) {
                    event.preventDefault();
                }
            } else {
                if (event.which < 48 || event.which > 58) {
                    if (
                        event.which != 8 &&
                        event.which != 0 &&
                        (event.which < 96 || event.which > 105)
                    ) {
                        event.preventDefault();
                    }
                }
            }
        });

        $("body").on("keydown", "#txtClienteCoppel", function (event) {
            if ($(this).val().length == 11) {
                if (event.which != 8 && event.which != 0) {
                    event.preventDefault();
                }
            } else {
                if (event.which < 48 || event.which > 58) {
                    if (
                        event.which != 8 &&
                        event.which != 0 &&
                        (event.which < 96 || event.which > 105)
                    ) {
                        event.preventDefault();
                    }
                }
            }
        });

        $("body").on("keydown", "#txtNss", function (event) {
            if ($(this).val().length == 11) {
                if (event.which != 8 && event.which != 0) {
                    event.preventDefault();
                }
            } else {
                if (event.which < 48 || event.which > 58) {
                    if (event.which != 8 && (event.which < 96 || event.which > 105)) {
                        event.preventDefault();
                    }
                }
            }
        });
    }

    $('#txtCurp').focusout(function () {
        var iCurpLongitudTitular = $("#txtCurp").val().length
        if (iCurpLongitudTitular == 18) {

            iRespuestaRenapoTitular = 0;
            var iCurpTitular = $("#txtCurp").val();
            if (validarCurp(iCurpTitular) == 1) {
                if (!bTerceraFigura) {
                    $("#btnConsultaCurp").attr("disabled", false);
                    if (iTipoAfiliacion == 26 || iTipoAfiliacion == 27 || iTipoAfiliacion == 527) //Si es un idependiente
                        $("#txtNss").attr("disabled", false);
                }
                else {
                    $('#bRenapoSol').attr('disabled', false);
                    document.getElementById("bRenapoSol").classList.add("habilitarImg");
                    var iCurpLongitudTercera = $("#txtCurp2").val().length;
                    if (iCurpLongitudTercera == 18)
                        $("#btnConsultaCurp").attr("disabled", false);
                    else
                        $("#btnConsultaCurp").attr("disabled", true);
                }
            }
            else {
                sTitle = "";
                sMensaje = "Favor de verificar la CURP que capturaste y vuelve a intentar";
                mostrarMensaje(sMensaje, sTitle);
                $("#btnConsultaCurp").attr("disabled", true);
                $("#bRenapoSol").attr("disabled", true);
                $("#txtNss").attr("disabled", true);
            }

        }
        else {
            $("#btnConsultaCurp").attr("disabled", true);
            $("#bRenapoSol").attr("disabled", true);
            $("#txtNss").attr("disabled", true);
        }
    });

    $('#txtCurp').on('keyup', function () {

        var iCurpLongitudTitular = $("#txtCurp").val().length
        if (iCurpLongitudTitular == 18) {

            iRespuestaRenapoTitular = 0;
            var iCurpTitular = $("#txtCurp").val();
            if (validarCurp(iCurpTitular) == 1) {
                if (!bTerceraFigura) {
                    $("#btnConsultaCurp").attr("disabled", false);
                    if (iTipoAfiliacion == 26 || iTipoAfiliacion == 27 || iTipoAfiliacion == 527) //Si es un idependiente
                        $("#txtNss").attr("disabled", false);
                }
                else {
                    $('#bRenapoSol').attr('disabled', false);
                    document.getElementById("bRenapoSol").classList.add("habilitarImg");
                    var iCurpLongitudTercera = $("#txtCurp2").val().length;
                    if (iCurpLongitudTercera == 18)
                        $("#btnConsultaCurp").attr("disabled", false);
                    else
                        $("#btnConsultaCurp").attr("disabled", true);
                }
            }
            else {
                sTitle = "";
                sMensaje = "Favor de verificar la CURP que capturaste y vuelve a intentar";
                mostrarMensaje(sMensaje, sTitle);
                $("#btnConsultaCurp").attr("disabled", true);
                $("#bRenapoSol").attr("disabled", true);
                $("#txtNss").attr("disabled", true);
            }

        }
        else {
            $("#btnConsultaCurp").attr("disabled", true);
            $("#bRenapoSol").attr("disabled", true);
            $("#txtNss").attr("disabled", true);
        }
    });


    $('#txtCurp2').focusout(function () {
        var iCurpLongitudSolicitante = $("#txtCurp2").val().length
        if (iCurpLongitudSolicitante == 18) {
            var CurpSolicitante = $("#txtCurp2").val();
            if (validarCurp(CurpSolicitante) == 1) {
                $("#btnConsultaCurp").attr("disabled", false);
                if (iTipoAfiliacion == 26 || iTipoAfiliacion == 27 || iTipoAfiliacion == 527) //Si es un idependiente
                    $("#txtNss").attr("disabled", false);
            }
            else {
                sTitle = "";
                sMensaje = "Favor de verificar la CURP que capturaste y vuelve a intentar";
                mostrarMensaje(sMensaje, sTitle);
                $("#btnConsultaCurp").attr("disabled", true);
                $("#txtNss").attr("disabled", true);
            }
        }
        else {
            $("#btnConsultaCurp").attr("disabled", true);
            $("#txtNss").attr("disabled", true);
        }
    });

    $('#txtCurp2').on('keyup', function () {
        var iCurpLongitudSolicitante = $("#txtCurp2").val().length
        if (iCurpLongitudSolicitante == 18) {
            var CurpSolicitante = $("#txtCurp2").val();
            if (validarCurp(CurpSolicitante) == 1) {
                $("#btnConsultaCurp").attr("disabled", false);
                if (iTipoAfiliacion == 26 || iTipoAfiliacion == 27 || iTipoAfiliacion == 527) //Si es un idependiente
                    $("#txtNss").attr("disabled", false);
            }
            else {
                sTitle = "";
                sMensaje = "Favor de verificar la CURP que capturaste y vuelve a intentar";
                mostrarMensaje(sMensaje, sTitle);
                $("#btnConsultaCurp").attr("disabled", true);
                $("#txtNss").attr("disabled", true);
            }
        }
        else {
            $("#btnConsultaCurp").attr("disabled", true);
            $("#txtNss").attr("disabled", true);
        }
    });
});

function validarCurp(curp) {//valida que una curp tenga una estructura correcta
    var esValida = 0;
    //AAAA######AAAAAA##
    if (curp.match(/^([A-Z])([A,E,I,O,U,X])([A-Z]{2})([0-9]{2})([0-1])([0-9])([0-3])([0-9])([M,H])([A-Z]{2})([B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3})([0-9,A-Z])([0-9])$/i)) {
        esValida = 1;
    }
    return esValida;
}

function NumText(string) {//solo letras y numeros
    var out = '';
    //Se añaden las letras validas
    var filtro = 'abcdefghjklmnñpqrtuvwxyzABCDEFGHJKLMNÑPQRTUVWXYZ2346789';//Caracteres validos

    for (var i = 0; i < string.length; i++)
        if (filtro.indexOf(string.charAt(i)) != -1)
            out += string.charAt(i);
    out = out.toUpperCase();
    return out;
}

function validar_letras(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8) return true;
    patron = /[A-Za-zñÑáéíóúÝÉÝÓÚüÜ\s]/; // Acepta números y letras
    te = String.fromCharCode(tecla);
    return patron.test(te);
}